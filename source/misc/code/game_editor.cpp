//#include "imgui_demo.cpp"

void TestMouseRaycasting(editor_state* EditorState, render_list* RenderList)
{
    raycast_info RaycastInfo;
    if (RendererRaycast(EditorState->EntityMaster, &RaycastInfo,
                        EditorState->MouseWorldPosition,
                        EditorState->MouseWorldDirection,
                        1000))
    {
        DrawCube(RenderList, RaycastInfo.Position, v3_ONE * 0.1f, v4_BLUE);
    }
}

void DrawEntity(render_list* RenderList, entity* Entity, real32 ElapsedTime)
{
    if (Entity->Enabled)
    {
        if (Entity->RenderMesh != NULL)
        {
                DrawMesh(RenderList, Entity, Entity->RenderMesh, Entity->Material, RenderMode_Fill);
        }
    }
}

void ImGuiMatrix4(matrix4 Mat)
{
    ImGui::InputFloat4(" ", &Mat.Row0.x);
    ImGui::InputFloat4(" ", &Mat.Row1.x);
    ImGui::InputFloat4(" ", &Mat.Row2.x);
    ImGui::InputFloat4(" ", &Mat.Row3.x);
}

void PlayerFPSRotation(entity* Player, game_controller* Controller, 
                       float DeltaTime, real32 MouseSensitivity)
{
    //YamProf(FpsRotation);
    ResolveLocalToWorld(Player);
    
    float PlayerRotationSpeed = MouseSensitivity; //Tweak
    
    float KeyboardCameraRotationX = 0;
    float KeyboardCameraRotationY = 0;
    if (Controller->ArrowKeyUp.IsDown)
    {
        KeyboardCameraRotationX = 1;
    }
    else if (Controller->ArrowKeyDown.IsDown)
    {
        KeyboardCameraRotationX = -1;
    }
    if (Controller->ArrowKeyLeft.IsDown)
    
    {
        KeyboardCameraRotationY = -1;
    }
    else if (Controller->ArrowKeyRight.IsDown)
    {
        KeyboardCameraRotationY = 1;
    }
    
#if 1
    //rotation calc
    real32 DeltaX = ((float)Controller->MouseDeltaY + KeyboardCameraRotationX) * DeltaTime * PlayerRotationSpeed;
    // legal range of 0 - 90, 270 - 360
    real32 PotentialX = FixUpNegativeEulerAngles(Player->PlayerRotationX + DeltaX);
    PotentialX = fmod(PotentialX, PI * 2);
    
    real32 MaxTop = PI + (PI / 2);
    real32 MaxBottom = PI / 2;
    
    if (Player->PlayerRotationX > MaxBottom && Player->PlayerRotationX < MaxTop)
    {
        // out of bounds rotation fell through
        Assert(0);
    }
    
    if (Player->PlayerRotationX >= MaxTop - FLT_EPSILON && PotentialX > MaxBottom && PotentialX < MaxTop)
    {
        Player->PlayerRotationX = MaxTop;
    }
    else if (Player->PlayerRotationX <= MaxBottom + FLT_EPSILON && PotentialX > MaxBottom && PotentialX < MaxTop)
    {
        Player->PlayerRotationX = MaxBottom;
    }
    else
    {
        if (PotentialX > MaxBottom && PotentialX < MaxTop)
        {
            // out of bounds rotation fell through
            Assert(0);
        }
        Player->PlayerRotationX = PotentialX;
    }
    
    Player->PlayerRotationY += ((float)Controller->MouseDeltaX + KeyboardCameraRotationY) * DeltaTime * PlayerRotationSpeed;
    Player->PlayerRotationY = fmod(Player->PlayerRotationY, PI * 2);
    
    //http://www.3dgep.com/understanding-the-view-matrix/
    float cosPitch = cos(-Player->PlayerRotationX);
    float sinPitch = sin(-Player->PlayerRotationX);
    float cosYaw = cos(-Player->PlayerRotationY);
    float sinYaw = sin(-Player->PlayerRotationY);
    
    v3 xaxis = { cosYaw, 0, -sinYaw };
    v3 yaxis = { sinYaw * sinPitch, cosPitch, cosYaw * sinPitch };
    v3 zaxis = { sinYaw * cosPitch, -sinPitch, cosPitch * cosYaw };
    v3 eye = Player->Position;
    
    // Create a 4x4 view matrix from the right, up, forward and eye position vectors
    matrix4 viewMatrix = {
        V4(xaxis.x, yaxis.x, zaxis.x, 0),
        V4(xaxis.y, yaxis.y, zaxis.y, 0),
        V4(xaxis.z, yaxis.z, zaxis.z, 0),
        V4(-Inner(xaxis, eye), -Inner(yaxis, eye), -Inner(zaxis, eye), 1)
    };
    viewMatrix = Transpose(viewMatrix);
    Player->LocalToWorld = (viewMatrix);
    Player->Forward = zaxis;
    Player->Right = xaxis;
    Player->Up = yaxis;
    
    Player->LocalRotation = MatrixToQuaternion(Player->LocalToWorld);
    
    /*
       TODO(james): this should just always be called, this is for when we have objects that are
       children of the camera. Unforunately this is doing LocalToWorld -> LocalRotation, 
       then in ResolveLocalToWorld we convert LocalRotation to a matrix again... something
       goes bad in there somewhere. Maybe a view matrix cannot be a TRS matrix?
    */
#if 0    
    SetDirty(Player);
    
    ResolveLocalToWorld(Player);
#endif
#endif
}

void PlayerFPSMovement(transform* PlayerTransform, matrix4* Camera, game_controller* Controller, float DeltaTime)
{
    float PlayerMoveSpeed = 7; //Tweak
    
    v3 InputDirection = v3_ZERO;
    InputDirection.x = Controller->MoveHorizontalAxis;
    InputDirection.y = Controller->MoveVerticalAxis;
    InputDirection.z = Controller->MoveForwardAxis;
    
    PlayerTransform->LocalPosition += ((InputDirection.x * PlayerTransform->Right) + (InputDirection.y * PlayerTransform->Up) + (InputDirection.z * PlayerTransform->Forward)) * PlayerMoveSpeed * DeltaTime;
    
    PlayerTransform->Position = PlayerTransform->LocalPosition;
    
    //SetTranslate(Camera, PlayerTransform->Position);
}

bool32 SaveEntities(scene* CurrentScene, linked_list* Master, platform_api* PlatformAPI)
{
    // serialization/scenes/scene_name
    STRING(SceneLocation, 255);
    AppendStringEx(&SceneLocation, "serialization/scenes/%s", &CurrentScene->Name[0]);
    
    // serialization/scenes/scene_name/entities
    STRING(SceneEntitiesLocation, 255);
    AppendStringEx(&SceneEntitiesLocation, "%s/entities", SceneLocation.Text);
    
    bool32 IsNewScene = false;
    bool32 MoveResult = true;
    if (PlatformAPI->FileExists(SceneLocation.Text))
    {
        MoveResult = PlatformAPI->MoveFile(SceneLocation.Text, 
                                                  "serialization/scenes/backup_scene");
    }
    else
    {
        IsNewScene = true;
        PlatformAPI->CreateDirectory(SceneLocation.Text);
        PlatformAPI->CreateDirectory(SceneEntitiesLocation.Text);
    }
    
    if (MoveResult)
    {
        linked_list_member* Member = Master->FirstMember;
        while (Member != NULL)
        {
            entity* Entity = (entity*)Member->Payload;
            if (Entity->SerializeToFile)
            {
                SerializeEntity(SceneEntitiesLocation.Text, Entity, PlatformAPI);
            }
            Member = Member->NextNode;
        } 
        
        if (!IsNewScene)
        {
            PlatformAPI->DeleteDirectory("serialization/scenes/backup_scene");
        }
        
        MyConsole.AddLog("Entity save complete.");
    }
    else
    {
        MyConsole.AddLog("Couldn't save entities!");
        
        //TODO(james): delete botched "entities" folder, restore "old_entities"
    }
    
    return MoveResult;
}

real32 Snapping(real32 OldValue, real32 NewValue, real32 Interval)
{
    real32 Result = 0;
    if (OldValue == NewValue)
    {
        return Result;
    }
    
    if (NewValue < OldValue)
    {
        Interval = -Interval;
    }
    
    real32 Overflow = fmod(AbsoluteValue(OldValue) + AbsoluteValue(Interval), Interval);
    
    int32 SnapCountOld = FloorReal32ToInt32(OldValue / Interval);
    int32 SnapCountNew = FloorReal32ToInt32(NewValue / Interval);
    uint32 SnapCount = SnapCountNew - SnapCountOld;
    
    if (Overflow != 0) //snap to grid nearest grid line
    {
        if (Interval > 0)
        {
            if (OldValue >= 0)
            {
                Overflow = Interval - Overflow;
            }
            
            if (NewValue >= OldValue + (Overflow))
            {
                Result = Overflow;
            }
        }
        else
        {
            Overflow = -Overflow;
            if (OldValue <= 0)
            {
                Overflow = Interval - Overflow;
            }
            
            if (NewValue <= OldValue + (Overflow))
            {
                Result = Overflow;
            }
        }
    }
    else // snap to next grid line
    {
        if (Interval > 0)
        {
            if (NewValue >= OldValue + Interval)
            {
                Result = Interval;
            }
        }
        else
        {
            if (NewValue <= OldValue + Interval)
            {
                Result = Interval;
            }
        }
    }
    
    if (Result == Interval && SnapCount > 0)
    {
        SnapCount--;
    }
    
    Result += AbsoluteValue(Interval) * SnapCount;
    return Result;
}

void DrawEntityHighlight(render_list* RenderList, entity* Entity,
                         v4 HighlightColor, v4 ColliderColor)
{
    real32 ExtraScale = 0.1f; //extra scale is so we can be sure that outline isnt overlapped by faces
    transform ExtraScaled = (transform)*Entity;
    ExtraScaled.LocalScale += V3(ExtraScale, ExtraScale, ExtraScale);
    material Material = {};
    if (Entity->RenderMesh != NULL)
    {
    
        Material.Color = HighlightColor;
        DrawMesh(RenderList, &ExtraScaled, Entity->RenderMesh, Material,
                 RenderMode_Wireframe);
    }
    
    if (Entity->ColliderType == ColliderType_Mesh && Entity->ColliderMesh != NULL)
    {
        Material.Color = ColliderColor;
        DrawMesh(RenderList, &ExtraScaled, Entity->ColliderMesh, Material,
                 RenderMode_Wireframe);
    }
    else if (Entity->ColliderType == ColliderType_Sphere)
    {
        DrawSphereSkeleton(RenderList, Entity->LocalPosition,
                           Entity->Forward, Entity->Right, Entity->Up,
                           Entity->SphereColliderRadius,
                           ColliderColor);
    }
    else if (Entity->ColliderType == ColliderType_Capsule)
    {
        DrawCapsuleFrame(RenderList, Entity,
                         Entity->CapsuleColliderRadius, Entity->CapsuleColliderHeight);
    }
    else if (Entity->ColliderType == ColliderType_Cylinder)
    {
        DrawCylinderFrame(RenderList, Entity,
                          Entity->CylinderColliderRadius,
                          Entity->CylinderColliderHeight);
    }
}

void PushUndo(editor_state* EditorState, game_state* GameState)
{
    Assert(EditorState->UndoStackPosition < 128);
    //editor_state* EditorState = &Memory->EditorState;
    EditorState->UndoStackPosition++;
    undo_stack_entry* Entry = &EditorState->UndoStackEntries[EditorState->UndoStackPosition];
    
    Entry->GameStateChange = PushStruct(&EditorState->UndoStackMemory, game_state);
    *Entry->GameStateChange = *GameState;
    
    Entry->GameStateArenaSize = (uint32)GameState->GameArena.Used;
    Entry->GameStateArenaChange = (uint8*)PushSize_(&EditorState->UndoStackMemory, GameState->GameArena.Used);
    Copy(Entry->GameStateArenaSize, GameState->GameArena.Base, Entry->GameStateArenaChange);
    
}

void PopUndo(editor_state* EditorState, game_state* GameState)
{
    if (EditorState->UndoStackPosition >= 0)
    {
            undo_stack_entry Entry = EditorState->UndoStackEntries[EditorState->UndoStackPosition];
            
        EditorState->UndoStackEntries[EditorState->UndoStackPosition] = {};//take this to 0
        EditorState->UndoStackPosition--;
        
        *GameState = *Entry.GameStateChange;
        Copy(Entry.GameStateArenaSize, Entry.GameStateArenaChange, GameState->GameArena.Base);
        
        EditorState->UndoStackMemory.Used -= Entry.GameStateArenaSize;
        EditorState->UndoStackMemory.Used -= sizeof(game_state);
        if (EditorState->UndoStackPosition >= 0)
        {
            // push arena's base back
            EditorState->UndoStackMemory.Base = EditorState->UndoStackEntries[EditorState->UndoStackPosition].GameStateArenaChange;
        }
        else
        {
            Assert(EditorState->UndoStackMemory.Used == 0);
        }
        
        
        //TODO(james): preserve editor camera
    }
}

void TranslationTool(editor_state* EditorState, game_state* GameState, render_list* RenderList,
                     game_input* Input, bool32 IsWindowFocused)
{
    //mouse up, deselect handle
    if (!Input->Controllers[0].LeftMouseClick.IsDown)
    {
        EditorState->SelectedHandle = NULL;
    }
    
    bool32 CanMoveEntity = false;
    
    if (EditorState->SelectedEntity != NULL)
    {
        if (EditorState->GamePlaybackState != GamePlayback_Stopped)
        {
            if (EditorState->SelectedEntity->IsStatic)
            {
                // TODO(james): we have selected a static obj at runtime, we should indicate
                // to the user that the handles will not work. Grey them out or something
                CanMoveEntity = false;
            }
            else
            {
                CanMoveEntity = true;
            }
        }
        else
        {
            CanMoveEntity = true;
        }
    }
    
    
    if (CanMoveEntity)
    {
        if (!EditorState->Handle->Enabled)
            Enable(EditorState->Handle);
            
        // Dragging a handle
        if (Input->Controllers[0].LeftMouseClick.IsDown && EditorState->SelectedHandle != NULL)
        {
            collision_ray Ray;
            Ray.Position = EditorState->MouseWorldPosition;
            Ray.Direction = EditorState->MouseWorldDirection;
            Ray.MaxDistance = 10000;
            
            //Find cooresponding axis that is most perpendicular to camera
            v3 HackyFindAnotherAxis;
            v3 OtherAxisA;
            v3 OtherAxisB;
            v3 HandleDirection;
            if (EditorState->SelectedHandle == EditorState->XAxisHandle)
            {
                OtherAxisA = EditorState->SelectedHandle->Up;
                OtherAxisB = EditorState->SelectedHandle->Forward;
                HandleDirection = EditorState->SelectedHandle->Right;
            }
            else if (EditorState->SelectedHandle == EditorState->YAxisHandle)
            {
                OtherAxisA = EditorState->SelectedHandle->Right;
                OtherAxisB = EditorState->SelectedHandle->Forward;
                HandleDirection = EditorState->SelectedHandle->Up;
            }
            else //z axis
            {
                OtherAxisA = EditorState->SelectedHandle->Up;
                OtherAxisB = EditorState->SelectedHandle->Right;
                HandleDirection = EditorState->SelectedHandle->Forward;
            }
            //find which axis is most perpendicular to camera
            v3 PlayerToEntityDir = Normalize(EditorState->SelectedEntity->Position - EditorState->EditorCamera->Position);
            real32 InnerA = Inner(PlayerToEntityDir, OtherAxisA);
            real32 InnerB = Inner(PlayerToEntityDir, OtherAxisB);
            if (AbsoluteValue(InnerA) < AbsoluteValue(InnerB))
            {
                HackyFindAnotherAxis = OtherAxisA;
            }
            else
            {
                HackyFindAnotherAxis = OtherAxisB;
            }
            
            //make plane normal aligned as possible with the entity/camera direction
            v3 PlaneNormal = Cross(HackyFindAnotherAxis, HandleDirection);
            //DrawQuad(RenderList,EditorState->SelectedEntity->Position, V3(10,10,10), PlaneNormal, v4_WHITE);
            
            if (EditorState->StartingPosTranslation == V3(1000, 1000, 1000))
            {
                EditorState->StartingPosTranslation = EditorState->SelectedEntity->Position;
            }
            
            v3 IntersectionPoint = {};
            if (ProjectRayOnPlane(Ray, EditorState->StartingPosTranslation,
                                  PlaneNormal,
                                  &IntersectionPoint, false))
            {
                if (!EditorState->TranslationHandleDragged)
                {
                    EditorState->TranslationHandleDragged = true;
                    PushUndo(EditorState, GameState);
                }
                
                v3 IntersectionDirection = IntersectionPoint - EditorState->StartingPosTranslation;
                v3 MovementDelta = ProjectDirectionOnDirection(IntersectionDirection, HandleDirection);
                if (EditorState->InitMovementDelta == V3(1000, 1000, 1000))
                {
                    EditorState->InitMovementDelta = MovementDelta;
                    MovementDelta = v3_ZERO;
                }
                else
                {
                    MovementDelta = MovementDelta - EditorState->InitMovementDelta;
                }
                
                DrawCube(RenderList,
                         IntersectionPoint, V3(0.1f, 0.1f, 0.1f), v4_BLUE);
                //helpful debug rendering
                /*
                DrawLine(RenderList, EditorState->SelectedEntity->Position, EditorState->SelectedEntity->Position + (PlaneNormal * 10), v4_WHITE);
                DrawCube(RenderList, EditorState->SelectedEntity->Position + MovementDelta, V3(0.5f, 0.5f, 0.5f), v4_ORANGE, RenderMode_fill);
*/
                
                //NOTE(james): The snapping is all in global space,
                //we might want local snapping option sometime?
                if (Input->Controllers[0].CtrlKey.IsDown) // snapping!
                {
                    v3 NewPos = EditorState->StartingPosTranslation + MovementDelta;
                    v3 OldPos = EditorState->StartingPosTranslation;
                    
                    v3 Flipper;
                    Flipper.x = NewPos.x < OldPos.x ? -1 : 1;
                    Flipper.y = NewPos.y < OldPos.y ? -1 : 1;
                    Flipper.z = NewPos.z < OldPos.z ? -1 : 1;
                    
                    v3 SnappedMovementDelta = {};
                    SnappedMovementDelta.x = Snapping(OldPos.x, NewPos.x, EditorState->TranslationSnappingInterval);
                    SnappedMovementDelta.y = Snapping(OldPos.y, NewPos.y, EditorState->TranslationSnappingInterval);
                    SnappedMovementDelta.z = Snapping(OldPos.z, NewPos.z, EditorState->TranslationSnappingInterval);
                    SnappedMovementDelta = Hadamard(SnappedMovementDelta, Flipper);
                    //Assert(SnappedMovementDelta.x < 5);
                    SetPosition(EditorState->SelectedEntity, EditorState->StartingPosTranslation + SnappedMovementDelta);
                }
                else //no snapping
                {
                    SetPosition(EditorState->SelectedEntity, EditorState->StartingPosTranslation + MovementDelta);
                }
            }
        }
        else
        {
            EditorState->InitMovementDelta = V3(1000, 1000, 1000);
            EditorState->StartingPosTranslation = V3(1000, 1000, 1000);
        }
        
        //
        // Place Handle
        //
        if (EditorState->SelectedEntity != EditorState->EditorCamera)
        {
            SetPosition(EditorState->Handle, EditorState->SelectedEntity->Position);
            if (EditorState->RotateDragger)
            {
                SetWorldRotation(EditorState->Handle, EditorState->SelectedEntity->Rotation);
            }
            else if (!EditorState->WorldSpaceDragging && !EditorState->RotateDragger && EditorState->SelectedEntity->Parent != NULL)
            {
                SetWorldRotation(EditorState->Handle, EditorState->SelectedEntity->Parent->Rotation);
            }
            else
            {
                SetWorldRotation(EditorState->Handle, V4(0, 0, 0, 1));
            }
            
            //NOTE(james): fake screen space thing
            
            real32 DistanceFromCam = Distance(EditorState->Handle->Position,
                                              EditorState->EditorCamera->Position);
            SetLocalScale(EditorState->Handle, V3(0.25f, 0.25f, 0.25f) * DistanceFromCam);
        }
    }
    else // nothing selected, so disable
    {
        if (EditorState->Handle->Enabled && EditorState->SelectedEntity == NULL)
            Disable(EditorState->Handle);
    }
    
    
    if (Input->Controllers[0].LeftMouseClick.UpThisFrame && EditorState->TranslationHandleDragged)
    {
        EditorState->TranslationHandleDragged = false;
    }
}

#define ARCBALL_CIRCLE_RADIUS 250
// NOTE(james): Part of the Arcball solution, Shoemake 92
v3 PointOnSphere(rectangle2 WindowDim, v2 MousePosition)
{
    real32 Radius = ARCBALL_CIRCLE_RADIUS;
    v2 EditorWindowCenter = GetCenter(RectMinMax(V2(0, 0), GetDim(WindowDim)));
    v2 MousePosViewport = ScreenCoordToViewportCoord(WindowDim, MousePosition);
    v2 MouseRadialDir = (MousePosViewport - EditorWindowCenter) * (1 / Radius);
    real32 R = LengthSq(MouseRadialDir);
    v3 PositionOnSphere;
    if (R > 1)
    {
        real32 S = 1 / SquareRoot(R);
        PositionOnSphere.x = S * MouseRadialDir.x;
        PositionOnSphere.y = S * MouseRadialDir.y;
        PositionOnSphere.z = 0.0f;
    }
    else
    {
        PositionOnSphere.x = MouseRadialDir.x;
        PositionOnSphere.y = MouseRadialDir.y;
        PositionOnSphere.z = SquareRoot(1 - R);
    }
    
    return PositionOnSphere;
}

//point on sphere = sphereIntersectionPoint - SphereCenter
v3 ConstrainedPointOnSphere(v3 PointOnSphere, v3 ConstraintAxis)
{
    real32 Dot = Inner(PointOnSphere, ConstraintAxis);
    v3 Proj = PointOnSphere - (ConstraintAxis * Dot);
    real32 Norm = LengthSq(Proj);
    
    v3 Result = {};
    if (Norm > 0)
    {
        real32 S = 1 / Norm;
        if (Proj.z < 0)
        {
            //S= -S;
        }
        Result = Proj * S;
    }
    else
    {
        Assert(0);
    }
    return Result;
}

v3 SelectAxis(v3 SpherePos, v3 Right, v3 Up, v3 Forward)
{
    Assert(ApproximatelyEqual(Length(SpherePos), 1));
    v3 ConstrainedX = Normalize(ConstrainedPointOnSphere(SpherePos, Right));
    v3 ConstrainedY = Normalize(ConstrainedPointOnSphere(SpherePos, Up));
    v3 ConstrainedZ = Normalize(ConstrainedPointOnSphere(SpherePos, Forward));
    real32 XAxisCloseness = Inner(SpherePos, ConstrainedX);
    real32 YAxisCloseness = Inner(SpherePos, ConstrainedY);
    real32 ZAxisCloseness = Inner(SpherePos, ConstrainedZ);
    
    v3 Result = v3_ZERO;
    if (XAxisCloseness > YAxisCloseness && XAxisCloseness > ZAxisCloseness && XAxisCloseness > 0.95f)
    {
        Result = Right;
    }
    else if (YAxisCloseness > XAxisCloseness && YAxisCloseness > ZAxisCloseness && YAxisCloseness > 0.95f)
    {
        Result = Up;
    }
    else if (ZAxisCloseness > YAxisCloseness && ZAxisCloseness > XAxisCloseness && ZAxisCloseness > 0.95f)
    {
        Result = Forward;
    }
    return Result;
}

void DrawRotationAxis(render_list* RenderList, v3 Position, v3 Axis, v3 Perp, v3 ConstraintAxis, v4 DefaultColor)
{
    if (ApproximatelyEqualV3(Axis, ConstraintAxis))
    {
        DrawCircle(RenderList, Position, Axis, Perp, 1, v4_YELLOW);
    }
    else
    {
        DrawCircle(RenderList, Position, Axis, Perp, 1, DefaultColor);
    }
}

// TODO(james): toggle snapping on and off with left-ctrl key holding
void RotationTool(editor_state* EditorState, render_list* RenderList,
                  game_input* Input, bool32 IsWindowFocused, raycast_info OnClickHandleCollisionInfo)
{
    
    if (EditorState->SelectedEntity != NULL)
    {
        //
        // Handle Housekeeping
        //
        if (!EditorState->RotationHandleSphereCollider->Enabled)
        {
            Enable(EditorState->RotationHandleSphereCollider);
        }
        SetPosition(EditorState->RotationHandleSphereCollider, EditorState->SelectedEntity->Position);
        
        v3 SphereCenterPos = EditorState->RotationHandleSphereCollider->Position;
        bool32 IsSnapping = Input->Controllers[0].CtrlKey.IsDown;
        real32 Step = EditorState->RotationSnappingInterval;
        
        //
        // Pick Axis
        //
        v3 RightConstraintAxis = v3_RIGHT;
        v3 UpConstraintAxis = v3_UP;
        v3 ForwardConstraintAxis = v3_FORWARD;
        if (!EditorState->WorldSpaceRotating)
        {
            RightConstraintAxis = EditorState->SelectedEntity->Right;
            UpConstraintAxis = EditorState->SelectedEntity->Up;
            ForwardConstraintAxis = EditorState->SelectedEntity->Forward;
        }
        
        //
        // Drawing
        //
        
        DrawRotationAxis(RenderList, SphereCenterPos, RightConstraintAxis, ForwardConstraintAxis, EditorState->ConstraintAxis, v4_RED);
        DrawRotationAxis(RenderList, SphereCenterPos, UpConstraintAxis, RightConstraintAxis, EditorState->ConstraintAxis, v4_GREEN);
        DrawRotationAxis(RenderList, SphereCenterPos, ForwardConstraintAxis, RightConstraintAxis,  EditorState->ConstraintAxis, v4_BLUE);
        
        #if 0
        //draw snapping arms
        if (IsSnapping)
        {
            if (EditorState->ConstraintAxis != v3_ZERO)
            {
                v3 PerpAxis = Normalize(RandomPerpendicular(EditorState->ConstraintAxis));
                
                for (int32 Index = 0;
                     Index < ((PI * 2) / Step);
                     ++Index)
                {
                    v4 Quat = AngleAxisToQuaternion(Step * Index, EditorState->ConstraintAxis);
                    v3 Offset = RotateVectorByQuaternion(PerpAxis, Quat) * 0.9f;
                    DrawRay(RenderList, SphereCenterPos, Offset, v4_GREY);
                }
            }
        }
        
        
        if (EditorState->SelectedEntity)DrawCoordinateSystemLines(RenderList, EditorState->SelectedEntity);
        #endif
        
        //
        // Update
        //
        if (Input->Controllers[0].LeftMouseClick.DownThisFrame && OnClickHandleCollisionInfo.Entity != NULL)
        {
            v3 PositionOnSphereUnconstrained = Normalize(OnClickHandleCollisionInfo.Position - SphereCenterPos);
            EditorState->ConstraintAxis = SelectAxis(PositionOnSphereUnconstrained,
                                                     RightConstraintAxis, UpConstraintAxis, ForwardConstraintAxis);
            
            if (EditorState->ConstraintAxis != v3_ZERO)
            {
                v3 SpherePos = Normalize(ConstrainedPointOnSphere(PositionOnSphereUnconstrained, EditorState->ConstraintAxis));
                
                if (IsSnapping)
                {
                    v3 PerpAxis = Normalize(RandomPerpendicular(EditorState->ConstraintAxis));
                    real32 AngleFinal = Angle(PerpAxis, SpherePos);
                    real32 SnappedAngle = Snapping(0, AngleFinal, Step);
                    real32 SnappingDiff = InverseLerp(0, AngleFinal, SnappedAngle);
                    EditorState->SpherePosInitial = Slerp(PerpAxis, SpherePos, SnappingDiff);
                }
                else
                {
                    EditorState->SpherePosInitial = SpherePos;
                }
            }
            else
            {
                EditorState->SpherePosInitial = PositionOnSphereUnconstrained;
            }
            
            EditorState->InitialRotation = EditorState->SelectedEntity->LocalRotation;
        }
        
        //TODO(james): just raycast against the sphere, not the whole scene
        raycast_info OnHoldCollisionInfo;
        bool32 Collision = CollisionRaycast(EditorState->CollisionGroup,
                                            &OnHoldCollisionInfo,
                                            EditorState->MouseWorldPosition, EditorState->MouseWorldDirection,
                                            1000, CollisionLayer_Editor, CollisionLayer_Editor);
        //IsHeld
        if (EditorState->SpherePosInitial != v3_ZERO && Input->Controllers[0].LeftMouseClick.IsDown && Collision)
        {
            v3 SpherePosCurrent;
            v3 SpherePosUnconstrained = Normalize(OnHoldCollisionInfo.Position - SphereCenterPos);
            //DrawRay(RenderList, SphereCenterPos, SpherePosUnconstrained, v4_WHITE);
            
            if (EditorState->ConstraintAxis != v3_ZERO)
            {
                v3 SpherePosConstrained = Normalize(ConstrainedPointOnSphere(SpherePosUnconstrained, EditorState->ConstraintAxis));
                if (IsSnapping)
                {
                    v3 PerpAxis = Normalize(RandomPerpendicular(EditorState->ConstraintAxis));
                    real32 AngleStart = Angle(PerpAxis, EditorState->SpherePosInitial);
                    
                    real32 AngleFinal = AngleStart + Angle(EditorState->SpherePosInitial, SpherePosConstrained);
                    real32 SnappedAngle = Snapping(0, AngleFinal, Step);
                    real32 SnappingDiff = InverseLerp(AngleStart, AngleFinal, SnappedAngle);
                    v3 SnappedPos = Slerp(EditorState->SpherePosInitial, SpherePosConstrained, SnappingDiff);
                    SpherePosCurrent = SnappedPos;
                    
                    //DrawRay(RenderList, SphereCenterPos, SpherePosCurrent, v4_YELLOW);
                }
                else
                {
                    SpherePosCurrent = SpherePosConstrained;
                }
            }
            else
            {
                SpherePosCurrent = SpherePosUnconstrained;
            }
            
            //
            // Calc Rotation
            //
            v4 RotationDelta = {}; //this is the difference between when the drag began and now
            RotationDelta.xyz = Cross(SpherePosCurrent, EditorState->SpherePosInitial);
            RotationDelta.w = Inner(SpherePosCurrent, EditorState->SpherePosInitial);
            RotationDelta = Slerp(v4_ZERO_ROTATION, Normalize(RotationDelta), 0.5f);
            
            SetLocalRotation(EditorState->SelectedEntity,
                             CombineQuaternions(EditorState->InitialRotation, RotationDelta));
        }
        
        if (EditorState->SpherePosInitial != v3_ZERO && !Input->Controllers[0].LeftMouseClick.IsDown) //IsUp
        {
            EditorState->SpherePosInitial = v3_ZERO;
            EditorState->ConstraintAxis = v3_ZERO;
        }
    }
    else
    {
        if (EditorState->RotationHandleSphereCollider->Enabled)
            Disable(EditorState->RotationHandleSphereCollider);
    }
    
}

void Selection(editor_state* EditorState, game_state* GameState, render_list* RenderList,
               game_input* Input, bool32 IsWindowFocused)
{
    YamProf(Selection);
    
    bool IsMouseInViewport = MouseInViewport(Input->ApplicationDim, RenderList->Viewport, Input->Controllers[0].MousePosition);
    
    //if (IsWindowFocused)DebugBreak();
    
    raycast_info RaycastHandlesInfo = {};
    if (Input->Controllers[0].LeftMouseClick.DownThisFrame && IsMouseInViewport && IsWindowFocused)
    {
        raycast_info ScreenRaycastInfo = {};
        
        if (RendererRaycast(EditorState->EntityMaster, &ScreenRaycastInfo, EditorState->MouseWorldPosition, EditorState->MouseWorldDirection, 1000))
        {
            //DrawEntityHighlight(RenderList, ScreenRaycastInfo.Entity, v4_BLUE, v4_GREEN);
        }
        
        if (CollisionRaycast(EditorState->CollisionGroup,
                             &RaycastHandlesInfo,
                             EditorState->MouseWorldPosition, EditorState->MouseWorldDirection,
                             1000, CollisionLayer_Editor, CollisionLayer_Editor))
        {
            if (RaycastHandlesInfo.Entity == EditorState->XAxisHandle || RaycastHandlesInfo.Entity == EditorState->YAxisHandle || RaycastHandlesInfo.Entity == EditorState->ZAxisHandle || RaycastHandlesInfo.Entity == EditorState->RotationHandleSphereCollider)
            {
                EditorState->SelectedHandle = RaycastHandlesInfo.Entity;
            }
        }
        else if (ScreenRaycastInfo.Entity != NULL)
        {
            if (ScreenRaycastInfo.Entity != EditorState->XAxisHandle && ScreenRaycastInfo.Entity != EditorState->YAxisHandle && ScreenRaycastInfo.Entity != EditorState->ZAxisHandle && ScreenRaycastInfo.Entity != EditorState->RotationHandleSphereCollider)
            {
                EditorState->SelectedEntity = ScreenRaycastInfo.Entity;
            }
        }
        else if (EditorState->SelectedEntity == ScreenRaycastInfo.Entity)
        {
            //do nothing, clicked on same object twice
        }
        else
        {
            //clear selection
            EditorState->SelectedEntity = NULL;
            EditorState->SelectedHandle = NULL;
        }
    }
    
    //
    //
    //
    
    if (EditorState->UseRotationTool)
    {
        if (EditorState->Handle->Enabled)
            Disable(EditorState->Handle);
        RotationTool(EditorState, RenderList, Input, IsWindowFocused, RaycastHandlesInfo);
    }
    else
    {
        if (EditorState->RotationHandleSphereCollider->Enabled)
            Disable(EditorState->RotationHandleSphereCollider);
        TranslationTool(EditorState, GameState, RenderList, Input, IsWindowFocused);
    }
}

// could do a recursive descent of transform tree instead
bool32 IsTransformOrParentOfTransform(transform* Entity, transform* PotentialMatch)
{
    bool32 Result = false;
    
    while (PotentialMatch != NULL)
    {
        if (StringsAreEqualAbsolute(GetName(Entity), GetName(PotentialMatch)))
        {
            Result = true;
            break;
        }
        if (PotentialMatch->Child != NULL)
        {
            Result = IsTransformOrParentOfTransform(Entity, PotentialMatch->Child);
        }
        
        PotentialMatch = (entity*)PotentialMatch->NextSibling;
    }
    return Result;
}

// could do a recursive descent of transform tree instead
bool32 IsParentOfTransform(transform* Entity, transform* PotentialMatch)
{
    bool32 Result = false;
    
    if (Entity->Parent == PotentialMatch)
        Result = true;
    
    return Result;
}

void ColliderTypeToString(string* ColliderTypeName, collider_type ColliderType)
{
    SetString(ColliderTypeName, collider_type_strings[ColliderType]);
}

void EntityTypeToString(string* EntityTypeName, entity_type EntityType)
{
    SetString(EntityTypeName, entity_type_strings[EntityType]);
}

void ClearCollisionProperties(entity* Entity)
{
    Entity->ColliderType = ColliderType_None;
    
    Entity->IsCharacterController = false;
    
    Entity->CollisionLayer = CollisionLayer_Default;
    Entity->ColliderAABB = {};
    
    ClearString(&Entity->ColliderMeshPath);
    Entity->ColliderMesh = NULL;
    Entity->ColliderMeshInstanceSpace = IdentityMatrix();
    Entity->SphereColliderRadius = 0;
    
    Entity->CapsuleColliderRadius = 0;
    Entity->CylinderColliderRadius = 0;
    
    Entity->CapsuleColliderHeight = 0;
    Entity->CylinderColliderHeight = 0;
    
    Entity->Configs[0] = {};
    Entity->Configs[1] = {};
    
    Entity->Mass = 1;
    Entity->IsTrigger = false;
    Entity->UseGravity = false;
    Entity->IsStatic = false;
    
    Entity->CollidedThisFrame = false;
    
    //Entity->InverseInertiaTensor = IdentityMatrix();
}

scene* CreateScene(editor_state* EditorState, char* SceneName)
{
    Assert(EditorState->SceneCount < MAX_SCENES - 1);
    scene* Scene = &EditorState->Scenes[EditorState->SceneCount++];
    
    CopyString(SceneName, &Scene->Name[0], 128);
     return Scene;
}

void FindAllScenes(editor_state* EditorState, platform_api* PlatformAPI)
{
    directory Directory = {};
    find_handle FindHandle = {};
    bool32 Success = PlatformAPI->GetFirstSubdirectory("serialization/scenes", &Directory, &FindHandle);
    while(Success)
    {
        CreateScene(EditorState, Directory.Name.Text);
        
        Success = PlatformAPI->GetNextSubdirectory(&Directory, &FindHandle);
    }
    
    Assert(EditorState->SceneCount > 0);
    //TODO(james): keep track of last active scene, reload that if we find it
    //EditorState->CurrentScene = &EditorState->Scenes[0];
}

void LoadEditorSettings(editor_settings* Settings, temporary_memory* Temp, platform_api* PlatformAPI)
{
    char* EditorSettingsFile = LoadTextFile("serialization/editor_settings.txt", Temp, PlatformAPI);
    
    tokenizer Tokenizer = {};
    Tokenizer.At = EditorSettingsFile;
    
    GetToken(&Tokenizer); // struct type
    GetToken(&Tokenizer); // struct identifier
    
    GetToken(&Tokenizer); // equals sign
    DeserializeStruct(&Tokenizer, Settings, 
                      MembersOf_editor_settings, ArrayCount(MembersOf_editor_settings));
}

void SaveEditorSettings(editor_state* EditorState, platform_api* PlatformAPI)
{
    for (int i = 0; i< 128; i++)
    {
        EditorState->EditorSettings.LastOpenScene[i] = '\0';
    }
    CopyString(EditorState->CurrentScene->Name, EditorState->EditorSettings.LastOpenScene, 128);
    
    STRING(File, 1000);
    SerializeStruct(ArrayCount(MembersOf_editor_settings), MembersOf_editor_settings, (void*)&EditorState->EditorSettings, 0, "editor_settings", "EditorSettings", &File);
    
    PlatformAPI->WriteBinaryFile("serialization/editor_settings.txt", (uint8*)File.Text, StringLength(File.Text));
}


void EntityPropertiesGui(editor_state* State, render_assets* RenderAssets, collision_group* Group)
{
    entity* Entity = State->SelectedEntity;
    
    //
    // Transform
    //
    //NOTE(james): detecting a selection change so we can reset the edit string
    if (State->LastSelectedEntity != Entity)
    {
        SetString(&State->EditName, GetName(Entity));
        State->LastSelectedEntity = Entity;
    }
    
    if (ImGui::InputText("Name", State->EditName.Text, State->EditName.Size, ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_CharsNoBlank)) //TODO(james): alphanumeric filter, particularly, no slashes!
    {
        //ChangeName(Entity, State->EditName.Text);//TODO(james): size safety?
        ResolveEntityPath(State->EntityMaster, Entity, State->EditName.Text);
    }
    
    bool32 PlaceholderEnabled = Entity->Enabled;
    if (ImGui::Checkbox("Enabled", &PlaceholderEnabled))
    {
        if (PlaceholderEnabled)
        {
            Enable(Entity);
        }
        else
        {
            Disable(Entity);
        }
    }
    
    if (ImGui::DragFloat3("Local Position", Entity->LocalPosition.E, 0.01f, -10, 10))
    {
        SetDirty(Entity);
        ResolveLocalToWorld(Entity);
    }
    
    if (ImGui::DragFloat4("Local Rotation Quaternion", Entity->LocalRotation.E, 0.01f, -1, 1))
    {
        SetDirty(Entity);
        ResolveLocalToWorld(Entity);
    }
    
    v3 SaveScale = Entity->LocalScale;
    if (ImGui::DragFloat3("Local Scale", Entity->LocalScale.E, 0.01f, MIN_TRANSFORM_SCALE, MAX_TRANSFORM_SCALE))
    {
        if (Entity->LocalScale.x > 0 && Entity->LocalScale.y > 0 && Entity->LocalScale.z > 0)
        {
            SetDirty(Entity);
            ResolveLocalToWorld(Entity);
        }
        else
        {
            Entity->LocalScale = SaveScale;
        }
    }
    
    //
    // World space coordinates
    //
    ImGui::Separator();
    
    if (ImGui::DragFloat3("World Position", Entity->Position.E, -10, 10))
    {
        SetDirty(Entity);
        ResolveLocalToWorld(Entity);
    }
    
    if (ImGui::DragFloat4("Rotation Quaternion", Entity->Rotation.E, 0.01f, -1, 1))
    {
        SetDirty(Entity);
        ResolveLocalToWorld(Entity);
    }
    
    if (ImGui::Button("Reset Local Rotation"))
    {
        SetLocalRotation(Entity, v4_ZERO_ROTATION);
    }
    
    //
    // Collision
    //
    ImGui::Separator();
    
    //ImGui::SliderFloat3("Linear Velocity", Entity->LinearVelocity.E, -5, 5);
    
    if (ImGui::Button("Collider Type"))
        ImGui::OpenPopup("Select Collider Type");
    ImGui::SameLine();
    STRING(ColliderTypeName, 64);
    ColliderTypeToString(&ColliderTypeName, Entity->ColliderType);
    ImGui::Text(Entity->ColliderType == ColliderType_None ? "<None>" : ColliderTypeName.Text);
    if (ImGui::BeginPopup("Select Collider Type"))
    {
        ImGui::Text("Collider Types");
        ImGui::Separator();
        
        if (ImGui::Selectable("None"))
        {
            ClearCollisionProperties(Entity);
            RemoveEntityFromCollisionList(Group, Entity->CollisionGroupMember);
        }
        else if (ImGui::Selectable("Mesh"))
        {
            ClearCollisionProperties(Entity);
            Entity->ColliderType = ColliderType_Mesh;
        }
        else if (ImGui::Selectable("Sphere"))
        {
            ClearCollisionProperties(Entity);
            Entity->SphereColliderRadius = 0.5f;
            AddColliderSphere(Group, Entity, Entity->SphereColliderRadius, Entity->UseGravity,
                              Entity->IsTrigger);
        }
        else if (ImGui::Selectable("Capsule"))
        {
            ClearCollisionProperties(Entity);
            Entity->CapsuleColliderRadius = 0.25f;
            Entity->CapsuleColliderHeight = 1;
            AddColliderCapsule(Group, Entity,
                               Entity->CapsuleColliderRadius, Entity->CapsuleColliderHeight,
                               Entity->UseGravity, Entity->IsTrigger);
        }
        else if (ImGui::Selectable("Cylinder"))
        {
            ClearCollisionProperties(Entity);
            Entity->CylinderColliderRadius = 0.25f;
            Entity->CylinderColliderHeight = 1;
            AddColliderCylinder(Group, Entity,
                                Entity->CylinderColliderRadius, Entity->CylinderColliderHeight,
                                Entity->UseGravity, Entity->IsTrigger);
        }
        
        ImGui::EndPopup();
    }
    
    switch (Entity->ColliderType)
    {
        case ColliderType_Mesh:
        {
            if (ImGui::Button("Collider Mesh"))
                ImGui::OpenPopup("Select Collider Mesh");
            ImGui::SameLine();
            ImGui::Text(StringLength(Entity->ColliderMeshPath) == 0 ? "<None>" : Entity->ColliderMeshPath.Text);
            if (ImGui::BeginPopup("Select Collider Mesh"))
            {
                ImGui::Text("Collider Meshes");
                ImGui::Separator();
                if (ImGui::Selectable("None"))
                {
                    if (Entity->ColliderMesh != NULL)
                    {
                        Entity->ColliderMesh = NULL;
                        ClearString(&Entity->ColliderMeshPath);
                        RemoveEntityFromCollisionList(Group, Entity->CollisionGroupMember);
                        Entity->ColliderType = ColliderType_None;
                        Entity->CollisionGroupMember = NULL;
                    }
                }
                
                linked_list_member* Asset = RenderAssets->Meshes.FirstMember;
                do
                {
                    transform* Transform = (transform*)Asset->Payload;
                    if (ImGui::Selectable(Transform->Path.Text))
                    {
                        SetString(&Entity->ColliderMeshPath, Transform->Path);
                        if (Entity->ColliderMesh == NULL)
                        {
                            AddColliderMesh(Group, Entity, (mesh*)Transform,
                                            Entity->UseGravity, Entity->IsTrigger);
                        }
                    }
                    Asset = Asset->NextNode;
                } while (Asset != NULL);
                
                ImGui::EndPopup();
            }
            break;
        }
        
        case ColliderType_Sphere:
        {
            ImGui::DragFloat("Sphere Radius", &Entity->SphereColliderRadius);
            break;
        }
        case ColliderType_Capsule:
        {
            ImGui::DragFloat("Capsule Radius", &Entity->CapsuleColliderRadius);
            ImGui::DragFloat("Capsule Height", &Entity->CapsuleColliderHeight);
            break;
        }
        case ColliderType_Cylinder:
        {
            ImGui::DragFloat("Cylinder Radius", &Entity->CylinderColliderRadius);
            ImGui::DragFloat("Cylinder Height", &Entity->CylinderColliderHeight);
            break;
        }
    }
    
    if (ImGui::Button("Collision Layer"))
        ImGui::OpenPopup("Select Collision Layer");
    ImGui::SameLine();
    STRING(ColliderLayerName, 64);
    SetString(&ColliderLayerName, collider_type_strings[Entity->CollisionLayer]);
    ImGui::Text(ColliderLayerName.Text);
    if (ImGui::BeginPopup("Select Collision Layer"))
    {
        ImGui::Text("Collision Layers");
        ImGui::Separator();
        
        if (ImGui::Selectable("Default"))
        {
            Entity->CollisionLayer = CollisionLayer_Default;
        }
        else if (ImGui::Selectable("Editor"))
        {
            Entity->CollisionLayer = CollisionLayer_Editor;
        }
        else if (ImGui::Selectable("Player Bullet"))
        {
            Entity->CollisionLayer = CollisionLayer_PlayerBullet;
        }
        else if (ImGui::Selectable("Enemy Bullet"))
        {
            Entity->CollisionLayer = CollisionLayer_EnemyBullet;
        }
        else if (ImGui::Selectable("BulletKillZone"))
        {
            Entity->CollisionLayer = CollisionLayer_BulletKillZone;
        }
        else if (ImGui::Selectable("Player"))
        {
            Entity->CollisionLayer = CollisionLayer_Player;
        }
        else if (ImGui::Selectable("Enemy"))
        {
            Entity->CollisionLayer = CollisionLayer_Enemy;
        }
        ImGui::EndPopup();
    }
    
    bool CollisionPropertyChanged = false;
    
    bool IsTrigger = (bool)Entity->IsTrigger;
    
    ImGui::Checkbox("Is Trigger", &IsTrigger);
    Entity->IsTrigger = IsTrigger;
    
    CollisionPropertyChanged |= ImGui::Checkbox("Use Gravity", &Entity->UseGravity);
    
    CollisionPropertyChanged |= ImGui::Checkbox("Is Static", &Entity->IsStatic);
    
    ImGui::Checkbox("Lock Rotation", &Entity->LockRotation);
    
    ImGui::DragFloat("Coefficient of Restitution", &Entity->CoefficientOfRestitution,
                     0.01f, 0.0f, 1.0f, "%.6f");
    ImGui::DragFloat("XZ Friction", &Entity->XZFriction,
                     0.01f, 0.0f, 5.0f, "%.2f");
    
    //ImGui::DragFloat("pen", &Entity->Contact.PenetrationDepth);
    /*
    ImGui::DragFloat("source pen", &Entity->Configs[Entity->SourceConfig].Contact.PenetrationDepth,
                     0,0,0,"%.6f");
    ImGui::DragFloat("target pen", &Entity->Configs[Entity->TargetConfig].Contact.PenetrationDepth,
                     0,0,0,"%.6f");
    ImGui::Checkbox("Is Resting Source", &Entity->Configs[Entity->SourceConfig].IsResting);
    ImGui::Checkbox("Is Resting Target", &Entity->Configs[Entity->TargetConfig].IsResting);
    */
    
    rb_config* Config = &Entity->Configs[Entity->SourceConfig];
    ImGui::DragFloat3("Force", &Config->Force.x,
                      0.01f, -1.0f, 1.0f);
    
    // NOTE(james): have to re-do init code when we change some of these properties
    if (CollisionPropertyChanged && Entity->ColliderType == ColliderType_Mesh)
    {
        RemoveEntityFromCollisionList(Group, Entity->CollisionGroupMember);
        AddColliderMesh(Group, Entity, (mesh*)Entity->ColliderMesh,
                        Entity->UseGravity, Entity->IsTrigger);
    }
    
    //
    // Rendering
    //
    ImGui::Separator();
    
    if (Entity->RenderMeshPath.Text != NULL && Entity->RenderMesh == NULL)
    {
        ImGui::TextColored(ImVec4(1, 0, 0, 1), "Mesh asset not found");
    }
    
    if (ImGui::Button("Render Mesh"))
        ImGui::OpenPopup("Select Render Mesh");
    ImGui::SameLine();
    ImGui::Text(StringLength(Entity->RenderMeshPath) == 0 ? "<None>" : Entity->RenderMeshPath.Text);
    if (ImGui::BeginPopup("Select Render Mesh"))
    {
        ImGui::Text("Render Meshes");
        ImGui::Separator();
        if (ImGui::Selectable("None"))
        {
            if (Entity->RenderMesh != NULL)
            {
                Entity->RenderMesh = NULL;
                ClearString(&Entity->RenderMeshPath);
            }
        }
        linked_list_member* Asset = RenderAssets->Meshes.FirstMember;
        do
        {
            transform* Transform = (transform*)Asset->Payload;
            if (ImGui::Selectable(Transform->Path.Text))
            {
                SetString(&Entity->RenderMeshPath, Transform->Path);
                Entity->RenderMesh = (mesh*)Transform;
            }
            Asset = Asset->NextNode;
        } while (Asset != NULL);
        
        ImGui::EndPopup();
    }
    ImGui::ColorEdit4("Render Color", &Entity->Material.Color.E[0]);
    
    ImGui::Checkbox("Is Transparent", &Entity->Material.IsTransparent);
    
    ImGui::Checkbox("Cast Shadows", &Entity->Material.CastShadows);
    ImGui::Checkbox("Recieve Shadows", &Entity->Material.RecieveShadows);
    
    ImGui::Checkbox("Is Directional Light", &Entity->IsDirectionalLight);
    
    if (ImGui::Button("Entity Type"))
        ImGui::OpenPopup("Select Entity Type");
    ImGui::SameLine();
    STRING(EntityTypeName, 64);
    EntityTypeToString(&EntityTypeName, Entity->EntityType);
    ImGui::Text(EntityTypeName.Text);
    if (ImGui::BeginPopup("Select Entity Type"))
    {
        ImGui::Text("Entity Types");
        ImGui::Separator();
        
        if (ImGui::Selectable("Default"))
        {
            Entity->EntityType = EntityType_Default;
        }
        else if (ImGui::Selectable("Enemy Shooter"))
        {
            Entity->EntityType = EntityType_EnemyShooter;
        }
        else if (ImGui::Selectable("Enemy Chaser"))
        {
            Entity->EntityType = EntityType_EnemyChaser;
        }
        else if (ImGui::Selectable("Game Win Zone"))
        {
            Entity->EntityType = EntityType_GameWinZone;
        }
        else if (ImGui::Selectable("Player Kill Zone"))
        {
            Entity->EntityType = EntityType_PlayerKillZone;
        }
        ImGui::EndPopup();
    }
    
    ImGui::Checkbox("Is Gun Mesh", &Entity->IsPlayerGun);
}

void EntityGui(entity* Entity, entity** SelectedEntity, entity** DroppedOnEntity,
               bool* IsDragging, bool* IsDropping)
{
    if (*SelectedEntity != NULL)
    {
        entity* SelectedEntityPtr = *SelectedEntity;
        if (IsParentOfTransform(SelectedEntityPtr, Entity))
        {
            ImGui::SetNextTreeNodeOpen(true);
        }
    }
    
    if (Entity->Enabled)
    {
        ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1, 1, 1, 1));
    }
    else
    {
        ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.3f, 0.3f, 0.3f, 1));
    }
    
    ImGuiTreeNodeFlags NodeFlags = ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_OpenOnArrow;
    
    NodeFlags |= Entity == *SelectedEntity ? ImGuiTreeNodeFlags_Selected : 0; //highlight selected entity
    NodeFlags |= (!Entity->Child) ? ImGuiTreeNodeFlags_Leaf : 0;
    
    bool32 NodeOpen = ImGui::TreeNodeEx(GetName(Entity), NodeFlags);
    ImGui::PopStyleColor();
    
    // entity or one of its children is selected
    if ((Entity->Child && NodeOpen) || ImGui::IsItemClicked())
    {
        // Item was picked this frame, so select it
        if (ImGui::IsItemClicked())
        {
            *SelectedEntity = Entity;
        }
        
        // draw entity's children
        if (Entity->Child && NodeOpen)
        {
            EntityGui((entity*)Entity->Child, SelectedEntity, DroppedOnEntity, IsDragging, IsDropping);
            if (Entity->Child->NextSibling != NULL)
            {
                transform* ChildSibling = Entity->Child->NextSibling;
                while (ChildSibling != NULL)
                {
                    EntityGui((entity*)ChildSibling, SelectedEntity, DroppedOnEntity, IsDragging, IsDropping);
                    ChildSibling = ChildSibling->NextSibling;
                }
            }
        }
    }
    
    if (*SelectedEntity == Entity)
    {
        if (*IsDropping) //only active for one full pass through the entity list, which happens over two frames
        {
            *IsDropping = false;
            *DroppedOnEntity = *SelectedEntity;
        }
        
        if (ImGui::IsItemActive())
        {
            if (ImGui::IsMouseDragging(0) && !(*IsDropping))
            {
                *IsDragging = true;
            }
        }
        else if (*IsDragging)
        {
            *IsDropping = true;
            *IsDragging = false;
        }
        else
        {
            *IsDragging = false;
            *IsDropping = false;
        }
    }
    else
    {
        if (*IsDropping && ImGui::IsItemHovered())
        {
            *IsDropping = false;
            *DroppedOnEntity = Entity;
            //SetParent(*SelectedEntity, Entity);
        }
    }
    
    if (NodeOpen) //Nodes call TreePush() when they are opened
    {
        ImGui::TreePop();
    }
}

void SceneGraphGui(linked_list* EntityMaster, entity** SelectedEntity)
{
    entity* DroppedOnEntity = NULL;
    static bool IsDragging = false;
    static bool IsDropping = false;
    
    linked_list_member* Member = EntityMaster->FirstMember;
    while (Member != NULL)
    {
        entity* Entity = (entity*)Member->Payload;
        if (Entity->ShowInSceneGraph && Entity->Parent == NULL)
        {
            EntityGui(Entity, SelectedEntity, &DroppedOnEntity, &IsDragging, &IsDropping);
        }
        
        Member = Member->NextNode;
    } 
    
    //ImGui::Checkbox("Dragging", &IsDragging);
    //ImGui::Checkbox("Dropping", &IsDropping);
    
    if (DroppedOnEntity != NULL)
    {
        if (*SelectedEntity == DroppedOnEntity)
        {
            if ((*SelectedEntity)->Parent != NULL)
            {
                v3 CurrentWorldPosition = (*SelectedEntity)->Position;
                RemoveParent(EntityMaster, *SelectedEntity);
                SetPosition(*SelectedEntity, CurrentWorldPosition);
            }
        }
        else
        {
            v3 CurrentWorldPosition = (*SelectedEntity)->Position;
            //TODO(james):scale isn't accounting for higher than 1 depth scenegraph hierarchy
            v3 CurrentScale = (*SelectedEntity)->LocalScale;
            v4 CurrentRotation = (*SelectedEntity)->LocalRotation;
            
            SetParent(EntityMaster, *SelectedEntity, DroppedOnEntity);
            
            SetPosition(*SelectedEntity, CurrentWorldPosition, false);
            SetScale(*SelectedEntity, CurrentScale, false);
            SetWorldRotation(*SelectedEntity, CurrentRotation);
        }
    }
}

global_variable bool32 Clicked;
void ModelAssetGui(mesh* Mesh, mesh** SelectedMesh, mesh** DroppedOnMesh,
                   bool* IsDragging, bool* IsDropping, bool32 IsRoot)
{
    if (*SelectedMesh != NULL)
    {
        mesh* SelectedMeshPtr = *SelectedMesh;
        if (IsParentOfTransform(SelectedMeshPtr, Mesh))
        {
            ImGui::SetNextTreeNodeOpen(true);
        }
    }
    
    ImGuiTreeNodeFlags NodeFlags = ImGuiTreeNodeFlags_OpenOnDoubleClick | ImGuiTreeNodeFlags_OpenOnArrow;
    
    NodeFlags |= Mesh == *SelectedMesh ? ImGuiTreeNodeFlags_Selected : 0; //highlight selected entity
    NodeFlags |= (!Mesh->Child) ? ImGuiTreeNodeFlags_Leaf : 0;
    
    bool32 NodeOpen = ImGui::TreeNodeEx(GetName(Mesh), NodeFlags);
    
    // mesh or one of its children is selected
    if ((Mesh->Child && NodeOpen) || ImGui::IsItemClicked())
    {
        // Item was picked this frame, so select it
        if (ImGui::IsItemClicked())
        {
            *SelectedMesh = Mesh;
        }
        
        // draw mesh's children
        if (Mesh->Child && NodeOpen)
        {
            //ImGui::SetItemAllowOverlap();
            ModelAssetGui((mesh*)Mesh->Child, SelectedMesh, DroppedOnMesh, IsDragging, IsDropping, false);
            if (Mesh->Child->NextSibling != NULL)
            {
                transform* ChildSibling = Mesh->Child->NextSibling;
                while (ChildSibling != NULL)
                {
                    ModelAssetGui((mesh*)ChildSibling, SelectedMesh, DroppedOnMesh, IsDragging, IsDropping, false);
                    ChildSibling = ChildSibling->NextSibling;
                }
            }
        }
    }
    
    if (*SelectedMesh == Mesh)
    {
        if (*IsDropping) //only active for one full pass through the mesh list, which happens over two frames
        {
            *IsDropping = false;
            *DroppedOnMesh = *SelectedMesh;
        }
        
        if (ImGui::IsItemActive())
        {
            if (ImGui::IsMouseDragging(0) && !(*IsDropping) && IsRoot)
            {
                *IsDragging = true;
            }
        }
        else if (*IsDragging)
        {
            *IsDropping = true;
            *IsDragging = false;
        }
        else
        {
            *IsDragging = false;
            *IsDropping = false;
        }
    }
    else
    {
        if (*IsDropping && ImGui::IsItemHovered())
        {
            *IsDropping = false;
            *DroppedOnMesh = Mesh;
            //SetParent(*SelectedMesh, Mesh);
        }
    }
    
    if (NodeOpen)
        ImGui::TreePop();
}

void AssetBrowser(game_and_editor_memory* Memory,
                  memory_arena* TransientArena,
                  platform_api* PlatformAPI,
                  render_list* RenderList,
                  game_input* Input)
{
    YamProf(AssetBrowser);
    game_state* GameState = &Memory->GameState;
    editor_state* EditorState = &Memory->EditorState;
    
    mesh* DroppedOnMesh = NULL; //TODO(james): get rid of this
    static bool IsDragging = false;
    
    ImGui::Checkbox("Dragging", &IsDragging);
    ImGui::Checkbox("Dropping", &EditorState->IsDroppingAsset);
    
    //SetNextTreeNodeOpen(true);
    if (ImGui::CollapsingHeader("Models"))
    {
        linked_list_member* Member = GameState->RenderAssets.Models.FirstMember;
        while (Member != NULL)
        {
            mesh* Mesh = (mesh*)Member->Payload;
            ModelAssetGui(Mesh, &EditorState->SelectedMesh, &DroppedOnMesh,
                          &IsDragging, &EditorState->IsDroppingAsset, true);
            
            Member = Member->NextNode;
        } 
    }
    
    if (EditorState->IsDroppingAsset)
        MyConsole.AddLog("Dropping!");
    
    if (ImGui::CollapsingHeader("Textures"))
    {
        for (uint32 TextureIndex = 0;
             TextureIndex < GameState->RenderAssets.TextureCount;
             ++TextureIndex)
        {
             if (ImGui::Button(GameState->RenderAssets.Textures[TextureIndex]->DebugName))
            {
                EditorState->SelectedTexture = GameState->RenderAssets.Textures[TextureIndex];
                EditorState->TextureZoom = 0.5f;
            }
        }
        
    }
    
    //
    // Selected texture viewer
    //
    if (EditorState->SelectedTexture != NULL)
    {
        bool IsOpen = true;
        if (ImGui::Begin("Texture Viewer", &IsOpen, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::SliderFloat("Zoom", &EditorState->TextureZoom, 0.01f, 5);
            ImGui::Image((void*)(intptr_t)EditorState->SelectedTexture->Handle,
                         ImVec2(EditorState->SelectedTexture->Width*EditorState->TextureZoom, EditorState->SelectedTexture->Height*EditorState->TextureZoom),
                         ImVec2(0, 0), ImVec2(1, 1),
                         ImColor(255, 255, 255, 255), ImColor(255, 255, 255, 128));
            if (!IsOpen)
            {
                EditorState->SelectedTexture = NULL;
            }
        }
        
        ImGui::End();
    }
}

entity* MeshAssetToEntityInstance(linked_list* EntityMaster, mesh* Mesh, entity* Parent = NULL)
{
    entity* Entity = CreateEntity(EntityMaster, GetName(Mesh), Parent);
    
    Entity->LocalPosition = Mesh->LocalPosition;
    Entity->LocalRotation = Mesh->LocalRotation;
    Entity->LocalScale = Mesh->LocalScale;
    SetDirty(Entity);
    ResolveLocalToWorld(Entity);
    
    //NOTE(james): mesh might have been an empty node in the model
    if (Mesh->Vertices.Count > 0)
    {
        Entity->RenderMesh = Mesh;
        if (Mesh->SerializedPath.Text[0] != '\0')
        {
            SetString(&Entity->RenderMeshPath, Mesh->SerializedPath);
        }
        else if (Mesh->Path.Text[0] != '\0')
        {
            SetString(&Entity->RenderMeshPath, Mesh->Path);
        }
        else
        {
            Assert(0);
        }
    }
    
    if (Mesh->ImportedMaterial)
    {
        Entity->Material = *Mesh->ImportedMaterial;
    }
    
    Entity->SerializeToFile = true; //TODO(james): might want this to be optional
    
    return Entity;
}

entity* InstantiateMeshAssetInstance(linked_list* EntityMaster, mesh* Mesh, entity* Parent = NULL)
{
    entity* Entity = MeshAssetToEntityInstance(EntityMaster, Mesh, Parent);
    
    mesh* Child = (mesh*)Mesh->Child;
    while (Child != NULL)
    {
        InstantiateMeshAssetInstance(EntityMaster, Child, Entity);
        Child = (mesh*)Child->NextSibling;
    }
    return Entity;
}

void LoadScene(scene* Scene, game_state* GameState, editor_state* EditorState, 
          temporary_memory* TempMem, platform_api* PlatformAPI)
{
    linked_list_member* Member = GameState->EntityMaster.FirstMember;
    
    while (Member != NULL)
    {
        entity* Entity = (entity*)Member->Payload;
        Member = Member->NextNode;
        if (Entity->SerializeToFile)
        {
            DeleteEntity(Entity, &GameState->EntityMaster, &GameState->CollisionGroup);
            //TODO(james): this is dumb. After the entity is deleted, its linked list members
            // refer to the "free list" so we have to start back from the beginning. Maybe have
            // DeleteEntity return the next valid entity?
            Member = GameState->EntityMaster.FirstMember;
        }
    }
    
    STRING(EntitiesPath, 255);
    AppendStringEx(&EntitiesPath, "serialization/scenes/%s/entities", &Scene->Name[0]);
    DeserializeEntities(&GameState->EntityMaster, &GameState->CollisionGroup,
                        &GameState->RenderAssets,
                        EntitiesPath.Text, TempMem, PlatformAPI);
    
    //TODO(james): this kinda sucks. We have to re-establish entity pointers after deserializing
    GameState->Player = FindEntity(&GameState->EntityMaster, "/Player");
    GameState->PlayerCamera = FindEntity(&GameState->EntityMaster, "/Player/PlayerCamera");
    EditorState->EditorCamera = FindEntity(&GameState->EntityMaster, "/EditorCamera");
    
    EditorState->CurrentScene = Scene;
    
    MyConsole.AddLog("Entity load complete.");
}

// NOTE(james): bar at bottom of screen
bool BeginStatusBar()
{
    ImGuiContext& g = *GImGui;
    real32 height = g.FontBaseSize + g.Style.FramePadding.y * 2.0f;
    ImGui::SetNextWindowPos(ImVec2(0.0f, g.IO.DisplaySize.y-height));
    ImGui::SetNextWindowSize(ImVec2(g.IO.DisplaySize.x, height));
    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
    ImGui::PushStyleVar(ImGuiStyleVar_WindowMinSize, ImVec2(0,0));
    if (!ImGui::Begin("##StatusBar", NULL, ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoSavedSettings|ImGuiWindowFlags_MenuBar)
        || !ImGui::BeginMenuBar())
    {
        ImGui::End();
        ImGui::PopStyleVar(2);
        return false;
    }
    g.CurrentWindow->DC.MenuBarOffsetX += g.Style.DisplaySafeAreaPadding.x;
    return true;
}

void EndStatusBar()
{
    ImGui::EndMenuBar();
    ImGui::End();
    ImGui::PopStyleVar(2);
}

void FillEditorPostProcessOverlays(game_and_editor_memory* Memory, render_list* RenderList)
{
    editor_state* EditorState = &Memory->EditorState;
    game_state* GameState = &Memory->GameState;
    
    //
    //
    //
    FboBegin(RenderList, &EditorState->SelectionFrame);
    ClearColor(RenderList, v4_ZERO);
    ClearDepth(RenderList);
    if (EditorState->SelectedEntity)
    {
        if (EditorState->SelectedEntity->RenderMesh != NULL)
        {
            //NOTE(james): this is wireframe selection 
            //DrawEntityHighlight(RenderList, EditorState->SelectedEntity, v4_GREEN, v4_GREEN);
            
            DisableDepth(RenderList);
            
            SetLineWidth(RenderList, 5);
            transform ScaledTransform = *((transform*)EditorState->SelectedEntity);
            //SetLocalScale(&ScaledTransform,ScaledTransform.LocalScale + V3(0.01f,0.01f,0.01f));
            material Material = {};
            Material.Color = V4(1, 0.10f, 0, 1);
            DrawSolidColorMesh(RenderList, &ScaledTransform, EditorState->SelectedEntity->RenderMesh, Material, RenderMode_Wireframe);
            SetLineWidth(RenderList, 1);
            
            Material.Color = v4_ZERO;
            DrawSolidColorMesh(RenderList, EditorState->SelectedEntity, EditorState->SelectedEntity->RenderMesh, Material);
            
            EnableDepth(RenderList);
        }
        
    }
    FboEnd(RenderList);    
}

void DrawEditorPostProcessOverlays(game_and_editor_memory* Memory, render_list* RenderList)
{
    editor_state* EditorState = &Memory->EditorState;
    DrawRect(RenderList, &EditorState->SelectionFrame.Texture, 
             TextureRect(&EditorState->SelectionFrame.Texture));
}


void DrawEditor3dOverlays(game_and_editor_memory* Memory, render_list* RenderList)
{
    editor_state* EditorState = &Memory->EditorState;
    game_state* GameState = &Memory->GameState;
    
    DrawEntity(RenderList, EditorState->XAxisHandle, GameState->ElapsedTime);
    DrawEntity(RenderList, EditorState->YAxisHandle, GameState->ElapsedTime);
    DrawEntity(RenderList, EditorState->ZAxisHandle, GameState->ElapsedTime);
    
    if (GameState->PlayerViewProjection.E[0] != 0)
    {
        DrawCameraFrustrum(RenderList, &GameState->PlayerViewProjection, v4_WHITE);
    }
}

void EditorCam(game_and_editor_memory* Memory, memory_arena* TransientArena,
               platform_api* PlatformAPI,
               RENDERDOC_API_1_1_1* RenderDocAPI,
               render_list* RenderList, game_input* Input)
{
    editor_state* EditorState = &Memory->EditorState;
    game_state* GameState = &Memory->GameState;
    
    if (Memory->EditorMode && Input->Controllers[0].RightMouseClick.IsDown)
    {
        YamProf(EditorCamera);
        PlayerFPSMovement(EditorState->EditorCamera, &RenderList->CurrentView,
                          &Input->Controllers[0], Input->DeltaTime);
        PlayerFPSRotation(EditorState->EditorCamera,
                          &Input->Controllers[0], Input->DeltaTime, 
                          GameState->PlayerSettings.MouseSensitivity);
    }
    RenderList->CurrentView = (EditorState->EditorCamera->LocalToWorld);
}

void EditorMode(game_and_editor_memory* Memory, memory_arena* TransientArena,
                platform_api* PlatformAPI,
                RENDERDOC_API_1_1_1* RenderDocAPI,
                render_list* RenderList, game_input* Input)
{
    YamProf(EditorMode);
    game_state* GameState = &Memory->GameState;
    editor_state* EditorState = &Memory->EditorState;
    
    temporary_memory TempMem = BeginTemporaryMemory(TransientArena);
    
    
    // Main Menu Bar
    {
        if (ImGui::BeginMainMenuBar())
        {
            YamProf(MainMenuBar);
            
            if (EditorState->GamePlaybackState == GamePlayback_Stopped)
            {
                if (ImGui::BeginMenu("Scene"))
                {
                    if (ImGui::MenuItem("Open Scene"))
                    {
                        EditorState->OpenSceneDialogIsOpen = true;
                    }
                    
                    if (ImGui::MenuItem("Save As New Scene"))
                    {
                        EditorState->SaveAsNewSceneDialogIsOpen = true;
                    }
                    ImGui::EndMenu();
                }
            }
            
            if (ImGui::BeginMenu("Entities"))
            {
                if (ImGui::MenuItem("Create Entity"))
                {
                    entity* Entity = CreateEntity(&GameState->EntityMaster, "Entity");
                    Entity->SerializeToFile = true;
                }
                if (ImGui::MenuItem("Save Entities"))
                {
                    SaveEntities(EditorState->CurrentScene, &GameState->EntityMaster, PlatformAPI);
                }
                if (ImGui::MenuItem("Load Entities"))
                {
                    LoadScene(EditorState->CurrentScene, GameState, EditorState, &TempMem, PlatformAPI);
                }
                ImGui::EndMenu();
            }
            
            if (RenderDocAPI != NULL)
            {
                if (ImGui::BeginMenu("Renderdoc"))
                {
                    bool HitCaptureFrame = false;
                    bool HitLaunchReplayUI = false;
                    
                    ImGui::MenuItem("Trigger Frame Capture", NULL, &HitCaptureFrame);
                    ImGui::MenuItem("Launch Replay UI", NULL, &HitLaunchReplayUI);
                    
                    if (HitCaptureFrame)
                        RenderDocAPI->TriggerCapture();
                    if (HitLaunchReplayUI)
                        RenderDocAPI->LaunchReplayUI(1, NULL);
                    ImGui::EndMenu();
                }
                
            }
            
            if (ImGui::BeginMenu("Tools Options"))
            {
                ImGui::MenuItem("Use Rotation Tool", NULL, (bool*)&EditorState->UseRotationTool);
                if (ImGui::MenuItem("Snapping Options", NULL, false))
                {
                    EditorState->SnappingWindowOpen = true;
                }
                ImGui::Separator();
                ImGui::MenuItem("World Space Dragger", NULL, (bool*)&EditorState->WorldSpaceDragging);
                ImGui::MenuItem("Rotate Dragger", NULL, (bool*)&EditorState->RotateDragger);
                ImGui::Separator();
                ImGui::MenuItem("World Space Rotating", NULL, (bool*)&EditorState->WorldSpaceRotating);
                
                ImGui::EndMenu();
            }
            
            
            STRING(SceneTitle, 128);
            AppendStringEx(&SceneTitle, "Scene - %s", EditorState->CurrentScene);
            
            ImGui::TextColored(ImVec4(0,0,0,1), SceneTitle.Text);
            
            ImGui::EndMainMenuBar();
        }
    }
    
    //
    // Snapping Window
    //
    if (EditorState->SnappingWindowOpen)
    {
        if (ImGui::Begin("Snapping Options", (bool*)&EditorState->SnappingWindowOpen,
                         ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::PushItemWidth(100);
            //
            // Translation
            //
            //NOTE(james): we can't enforce typed in values, so this ensures we always have a positive scale
            real32 SaveTranslation = EditorState->TranslationSnappingInterval;
            ImGui::DragFloat("Translation Snap Interval", &SaveTranslation, 0.0001f, 0, 100);
            if (SaveTranslation > 0)
            {
                EditorState->TranslationSnappingInterval = SaveTranslation;
            }
            
            //
            // Rotation
            //
            real32 DegreeRotationInterval = EditorState->RotationSnappingInterval * RADIANS_TO_DEGREES;
            ImGui::DragFloat("Rotation Snap Interval", &DegreeRotationInterval, 0.5f, 0, 360);
            if (DegreeRotationInterval >= 0.5f && fmod(360.0f, DegreeRotationInterval) == 0)
            {
                EditorState->RotationSnappingInterval = DegreeRotationInterval * DEGREES_TO_RADIANS;
            }
            
            ImGui::PopItemWidth();
        }
        ImGui::End();
    }
    
    //
    // Open Scene Dialog
    //
    if (EditorState->OpenSceneDialogIsOpen)
    {
        if (ImGui::Begin("Open Scene Dialog", (bool*)&EditorState->OpenSceneDialogIsOpen,
                         ImGuiWindowFlags_AlwaysAutoResize))
        {
            for (uint32 SceneIndex = 0;
                 SceneIndex < EditorState->SceneCount;
                 ++SceneIndex)
            {
                scene* Scene = &EditorState->Scenes[SceneIndex];
                if (ImGui::Button(Scene->Name))
                {
                    LoadScene(Scene, GameState, EditorState, &TempMem, PlatformAPI);
                    EditorState->OpenSceneDialogIsOpen = false;
                }
            }
        }
        ImGui::End();
    }
    
    //
    // New Scene Dialog
    //
    if (EditorState->SaveAsNewSceneDialogIsOpen)
    {
        if (ImGui::Begin("New Scene Dialog", (bool*)&EditorState->SaveAsNewSceneDialogIsOpen,
                         ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::InputText("Scene Name", &EditorState->NewSceneName[0], 128, ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_CharsNoBlank);
            //TODO(james): Error checking!
            if (StringLength(&EditorState->NewSceneName[0]) > 0)
            {
                if (ImGui::Button("Save"))
                {
                    scene* NewScene = CreateScene(EditorState, &EditorState->NewSceneName[0]);
                    EditorState->CurrentScene = NewScene;
                    SaveEntities(EditorState->CurrentScene, &GameState->EntityMaster, PlatformAPI);
                    EditorState->SaveAsNewSceneDialogIsOpen = false;
                }
            }
        }
        ImGui::End();
    }
    
    //ImGui::ShowStyleEditor();   
    //ImGui::ShowMetricsWindow();
    //ImGui::ShowTestWindow();

    {
        YamProf(AABBs);
        if (EditorState->GamePlaybackState == GamePlayback_Playing)
        {
            //TODO(james): if we have a game not using collision, the AABBs of the handles won't be updated
            // unless we update them here... if we do have collision then this is redundant...
            //
            // change this API!
            UpdateCollisionAABBs(EditorState->CollisionGroup, true);
            UpdateRendererAABBs(EditorState->EntityMaster, false);
        }
        else
        {
            UpdateCollisionAABBs(EditorState->CollisionGroup, true);
            UpdateRendererAABBs(EditorState->EntityMaster, true);
        }
    }
    
    //FboBegin(RenderList, &EditorState->PostProcessA);
    
    //FboEnd(RenderList);
    
        //
        //
        //
    // draw an xz snapping grid at the position of the handle
    {
        v3 NewPos = EditorState->Handle->Position + V3(EditorState->TranslationSnappingInterval, 0, EditorState->TranslationSnappingInterval);
        v3 OldPos = EditorState->Handle->Position;
        
        v3 SnapToGridDelta = {};
        SnapToGridDelta.x = Snapping(OldPos.x, NewPos.x, EditorState->TranslationSnappingInterval);
        SnapToGridDelta.y = Snapping(OldPos.y, NewPos.y, EditorState->TranslationSnappingInterval);
        SnapToGridDelta.z = Snapping(OldPos.z, NewPos.z, EditorState->TranslationSnappingInterval);
        
        transform T;
        T = TransformTRSLocal(OldPos + SnapToGridDelta, v4_ZERO_ROTATION, v3_ONE * EditorState->TranslationSnappingInterval);
        
        #if 0
        DrawLinesMesh(RenderList, &T, RenderList->Assets->StandardAssets.Grid);
        #endif
    }

    if (Input->Controllers[0].ToggleGUI.DownThisFrame)
    {
        EditorState->IsGuiOpen = !EditorState->IsGuiOpen;
    }

    if (Input->Controllers[0].DeleteKey.DownThisFrame)
    {
        if (EditorState->SelectedEntity)
        {
            DeleteEntity(EditorState->SelectedEntity, EditorState->EntityMaster, EditorState->CollisionGroup);
            EditorState->SelectedEntity = NULL;
        }
    }

    v3 ClickWorldPosARay;
    v3 ClickWorldPosBRay;
    ClickWorldPosARay = CameraToWorldPosition(RenderList->CurrentViewProjection, Input->ApplicationDim, RenderList->Viewport,
                                              Input->Controllers[0].MousePosition, -1);
    ClickWorldPosBRay = CameraToWorldPosition(RenderList->CurrentViewProjection, Input->ApplicationDim, RenderList->Viewport,
                                              Input->Controllers[0].MousePosition, 0.5f);

    EditorState->MouseWorldPosition = ClickWorldPosARay;
    EditorState->MouseWorldDirection = Normalize(ClickWorldPosBRay - ClickWorldPosARay);

    if (Input->Controllers[0].Action1.DownThisFrame && 
        Memory->EditorMode)
    {
        EditorState->UseRotationTool = !EditorState->UseRotationTool;
    }

    Selection(EditorState, GameState, RenderList, Input, Memory->EditorMode);
    //DrawRect(RenderList, RectMinMax(V2(0,0), GetDim(RenderList->ViewportRect)- V2(10,10)), v4_WHITE,GameState->TestTga);

    // Copy
    if (Input->Controllers[0].CtrlKey.IsDown && Input->Controllers[0].CopyKey.DownThisFrame)
    {

        if (EditorState->SelectedEntity)
        {
            EditorState->EntityToCopy = EditorState->SelectedEntity;
        }
    }

    // Paste
    if (Input->Controllers[0].CtrlKey.IsDown && Input->Controllers[0].PasteKey.DownThisFrame)
    {
        if (EditorState->EntityToCopy)
        {
            DuplicateEntity(EditorState->EntityMaster, EditorState->EntityToCopy, true);
        }
    }

    // undo
    if (Input->Controllers[0].CtrlKey.IsDown && Input->Controllers[0].UndoKey.DownThisFrame)
    {
        PopUndo(EditorState, GameState);
    }
    
    // Save Entities
    if (Input->Controllers[0].CtrlKey.IsDown && Input->Controllers[0].MoveBack.DownThisFrame && 
        EditorState->GamePlaybackState == GamePlayback_Stopped) //s key
    {
        SaveEntities(EditorState->CurrentScene, &GameState->EntityMaster, PlatformAPI);
    }

    // Reload entities
    if (Input->Controllers[0].CtrlKey.IsDown && Input->Controllers[0].Action2.DownThisFrame && 
        EditorState->GamePlaybackState == GamePlayback_Stopped) //r key
    {
        LoadScene(EditorState->CurrentScene, GameState, EditorState, &TempMem, PlatformAPI);
    }

//
//
//

    if (EditorState->IsDroppingAsset && MouseInViewport(Input->ApplicationDim,
                                                            RenderList->Viewport,
                                                            Input->Controllers[0].MousePosition))
    {
        InstantiateMeshAssetInstance(EditorState->EntityMaster, EditorState->SelectedMesh);
    }

    //DrawQuad(RenderGroup,V3(0,1,0), V3(1,1,1), Normalize(V3(0,1.0f,0) - GameState->Player->T.LocalPosition), v4_WHITE);
    //DrawQuad(RenderGroup,V3(0,2,0), V3(1,1,1), V3(0,-1,0), v4_WHITE);

    //DrawAllRenderAABBs(&GameState->CollisionGroup, RenderList, v4_RED);

    DrawLine(RenderList, v3_ZERO, v3_RIGHT * 10, v4_RED, 0.02f);
    DrawLine(RenderList, v3_ZERO, v3_UP * 10, v4_GREEN, 0.02f);
    DrawLine(RenderList, v3_ZERO, v3_FORWARD * 10, v4_BLUE, 0.02f);

    //TestMouseRaycasting(EditorState, RenderList);
    //DrawCapsuleFrame(RenderList, GameState->Player,
    //GameState->Player->CapsuleColliderRadius, GameState->Player->CapsuleColliderHeight);
    
    if (EditorState->GamePlaybackState == GamePlayback_Playing)
    {
        //DrawLineCylinderArt(RenderList, GameState, v3_RIGHT);
    }
    
    EndTemporaryMemory(TempMem);
}

inline void
CreateCoordinateSystemHandle(editor_state* EditorState)
{
    EditorState->Handle = CreateEntity(EditorState->EntityMaster, "Handle");
    EditorState->Handle->ShowInSceneGraph = false;
    
    EditorState->XAxisHandle = CreateEntity(EditorState->EntityMaster, "XAxisHandle", EditorState->Handle);
    EditorState->YAxisHandle = CreateEntity(EditorState->EntityMaster, "YAxisHandle", EditorState->Handle);
    EditorState->ZAxisHandle = CreateEntity(EditorState->EntityMaster, "ZAxisHandle", EditorState->Handle);
    
    SetLocalPosition(EditorState->XAxisHandle, V3(0.5f, 0, 0));
    SetLocalScale(EditorState->XAxisHandle, V3(1, 0.1f, 0.1f));
    
    SetLocalPosition(EditorState->YAxisHandle, V3(0, 0.5f, 0));
    SetLocalScale(EditorState->YAxisHandle, V3(0.1f, 1, 0.1f));
    
    SetLocalPosition(EditorState->ZAxisHandle, V3(0, 0, 0.5f));
    SetLocalScale(EditorState->ZAxisHandle, V3(0.1f, 0.1f, 1));
    
    EditorState->XAxisHandle->Material.Color = v4_RED;
    EditorState->YAxisHandle->Material.Color = v4_GREEN;
    EditorState->ZAxisHandle->Material.Color = v4_BLUE;
    
    EditorState->XAxisHandle->Material.RecieveShadows = false;
    EditorState->YAxisHandle->Material.RecieveShadows = false;
    EditorState->ZAxisHandle->Material.RecieveShadows = false;
    
    EditorState->XAxisHandle->EditorOnly = true;
    EditorState->YAxisHandle->EditorOnly = true;
    EditorState->ZAxisHandle->EditorOnly = true;
    
    EditorState->XAxisHandle->RenderMesh = EditorState->RenderAssets->StandardAssets.Cube;
    EditorState->YAxisHandle->RenderMesh = EditorState->RenderAssets->StandardAssets.Cube;
    EditorState->ZAxisHandle->RenderMesh = EditorState->RenderAssets->StandardAssets.Cube;
    
    EditorState->XAxisHandle->IsStatic = false;
    EditorState->YAxisHandle->IsStatic = false;
    EditorState->ZAxisHandle->IsStatic = false;
    
    EditorState->XAxisHandle->SerializeToFile = false;
    EditorState->YAxisHandle->SerializeToFile = false;
    EditorState->ZAxisHandle->SerializeToFile = false;
    
    EditorState->Handle->SerializeToFile = false;
    
    AddColliderMesh(EditorState->CollisionGroup, EditorState->XAxisHandle, EditorState->RenderAssets->StandardAssets.Cube,
                    false, true, CollisionLayer_Editor);
    AddColliderMesh(EditorState->CollisionGroup, EditorState->YAxisHandle, EditorState->RenderAssets->StandardAssets.Cube,
                    false, true, CollisionLayer_Editor);
    AddColliderMesh(EditorState->CollisionGroup, EditorState->ZAxisHandle, EditorState->RenderAssets->StandardAssets.Cube,
                    false, true, CollisionLayer_Editor);
                    
    EditorState->RotationHandleSphereCollider = CreateEntity(EditorState->EntityMaster, "RotationHandle");
    EditorState->RotationHandleSphereCollider->ShowInSceneGraph = false;
    EditorState->RotationHandleSphereCollider->EditorOnly = true;
    EditorState->RotationHandleSphereCollider->RenderMesh = EditorState->RenderAssets->StandardAssets.Sphere;
    EditorState->RotationHandleSphereCollider->Material.Color = V4(1, 1, 1, 0.5f);
    EditorState->RotationHandleSphereCollider->Material.IsTransparent = true;
    EditorState->RotationHandleSphereCollider->IsStatic = true;
    EditorState->RotationHandleSphereCollider->SerializeToFile = false;
    AddColliderMesh(EditorState->CollisionGroup, EditorState->RotationHandleSphereCollider, EditorState->RenderAssets->StandardAssets.Sphere,
                    false, true, CollisionLayer_Editor);
    Disable(EditorState->RotationHandleSphereCollider);
}