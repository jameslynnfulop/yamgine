#if !defined(YAMGINE_COLLISION_H)

#include "yamgine_entity.h"
#include "yamgine_render_list.h"

#define FIXED_TIME_STEP_DELTA 0.016666666666666666666666

#define RESTING_VELOCITY 0.1f

#define OUTER_DEPTH_EPSILON 0.005f
#define INNER_DEPTH_EPSILON 0

enum collision_layer;

struct simplex_point
{
    v3 PointA;
    v3 PointB;
    v3 Dilation; //minkowski point
};

struct edge
{
    simplex_point A;
    simplex_point B;
};

// NOTE(james): A -> B -> C is CCW
struct triangle
{
    v3 A;
    v3 B;
    v3 C;
};

struct simplex_triangle
{
    simplex_point A;
    simplex_point B;
    simplex_point C;
};

struct polytope
{
    simplex_triangle* Triangles; // NOTE(james): stb array
    edge* Edges; // NOTE(james): stb array
};

struct raycast_info
{
    //entity id
    v3 Position;
    v3 Normal;
    entity* Entity;
};

struct collision_ray
{
    v3 Position;
    v3 Direction;
    real32 MaxDistance;
};
// this would take every collision object in the game
// we then create another list of collision pairs after broadphase
struct collision_group
{
    //just intitializing this to something that will certainly fit everything we need,
    //do something smarter if we have a ton of layers?
    bool32 CollisionRules[CollisionLayer_Count * CollisionLayer_Count];
    linked_list Entries;

    real32 Gravity;

    real64 FrameDeltaTime;
    real64 TimeAccumulator;
    real64 CurrentTime;

    memory_arena ContactsThisFrame;

    memory_arena PermanentColliderVertexArena; //push worldspace vertices in here, fill on game start, clear on game end
    memory_arena TransientColliderVertexArena; //clear every frame, for things like bullets and enemies

    render_list* RenderList;
    bool32 NarrowphaseViz;
    bool32 BroadphaseViz;

    game_state* GameState;

    mesh* ColliderCube;
};

struct simplex
{
    union {
        struct
        {
            simplex_point A;
            simplex_point B;
            simplex_point C;
            simplex_point D;
        };
        simplex_point Array[4];
    };

    uint32 Count;
};

struct collision_pair
{
    entity* A;
    entity* B;
    //Simplex assumes latest point is at the end of the array, and will be ordered
    //in a counter clockwise manner
    simplex Simplex;
    bool32 CollidedThisFrame;
};

struct derivative
{
    v3 DerVelocity;
    v3 DerForce;

    v4 Spin;
    v3 Torque;
};

#define YAMGINE_COLLISION_H
#endif