//system
                                                typedef FMOD_RESULT TYPE_Studio_System_GetEvent(FMOD_STUDIO_SYSTEM *system,const char *path,FMOD_STUDIO_EVENTDESCRIPTION **event
                                                     );
typedef FMOD_RESULT TYPE_Studio_System_Update(FMOD_STUDIO_SYSTEM *system);
typedef FMOD_RESULT TYPE_Studio_System_SetListenerAttributes(
FMOD_STUDIO_SYSTEM *system,
int listener,
FMOD_3D_ATTRIBUTES *attributes
);

//event description
typedef FMOD_RESULT TYPE_Studio_EventDescription_CreateInstance(
FMOD_STUDIO_EVENTDESCRIPTION *eventdescription,
FMOD_STUDIO_EVENTINSTANCE **instance
);

//event instance
 typedef FMOD_RESULT TYPE_Studio_EventInstance_Set3DAttributes(
FMOD_STUDIO_EVENTINSTANCE *eventinstance,
FMOD_3D_ATTRIBUTES *attributes
);
typedef FMOD_RESULT TYPE_Studio_EventInstance_Start(FMOD_STUDIO_EVENTINSTANCE *eventinstance);
typedef FMOD_RESULT TYPE_Studio_EventInstance_Stop(
FMOD_STUDIO_EVENTINSTANCE *eventinstance,
FMOD_STUDIO_STOP_MODE mode
);
typedef FMOD_RESULT TYPE_Studio_EventInstance_SetParameterValue(
FMOD_STUDIO_EVENTINSTANCE *eventinstance,
const char *name,
float value
);

// functions to be called from game layer
struct fmod_api
{
    //system
    TYPE_Studio_System_GetEvent* Studio_System_GetEvent;
    TYPE_Studio_System_Update* Studio_System_Update;
    TYPE_Studio_System_SetListenerAttributes* Studio_System_SetListenerAttributes;
    
    //event description
    TYPE_Studio_EventDescription_CreateInstance* Studio_EventDescription_CreateInstance;
    
    //event instance
    TYPE_Studio_EventInstance_Set3DAttributes* Studio_EventInstance_Set3DAttributes;
    TYPE_Studio_EventInstance_Start* Studio_EventInstance_Start;
    TYPE_Studio_EventInstance_Stop* Studio_EventInstance_Stop;
    TYPE_Studio_EventInstance_SetParameterValue* Studio_EventInstance_SetParameterValue;
};