//
//Features
//
~ OnCollisionExit/Stay, TriggerExit/Stay game code events. Need concept of resting. Potential collision optimization?
   - is resting if broadphase detects both objects still collide and neither object has moved?
   - stay as long as object is within skin?
- multiple levels
- redo
- easier way to debug, visualize collision systems
- scaling tool
- occlusion culling?
- multi-select
- annotate renderer passes with https://www.khronos.org/registry/OpenGL/extensions/KHR/KHR_debug.txt
- redo strings again
- particles?


Update
    
    GameLogic
    OR
    EditorLogic

    DrawScene?


// Workflow
- write visual studio extension - no file editing

//
//Fixes
//
- doesn't add new dynamic entities created in editor during runtime, have to restart app
- figure out memory ownership on transform_lists. Hard to make restorable copies
- "Load entities" bugs out camera
- "Generic" way of descending parent/child hierarchies?
- "Generic" way of looping through linked lists?
- More separation of colliders vs rigidbodies
- Handle reloading scene while game is running
- Try to get rid of EulerToQuaternion in fbx loading
- Work on View matrix math
- Can get a cycle in root entity list, duplicate by reloading scene while entity is selected?
- Hot reload crashes sometimes? only with collision on. Check out replacing polytope stb_arrs with linked lists
- condense various game substates to an enum Pause, Playing, Title, etc
- can we stop requesting a multisampled backbuffer now that we have multisampled FBOs?
- perpsective ignore line rendering
- gizmos being shaded
- implement shader variants, shadowed/not shadowed, textured/not textured
- give imgui their own VAO
- bring back line drawing
- wide lines considered deprecated on laptop? anything > 1.0 
- unify model, prefab, and scene concepts
- auto generate a toolbox scene showing all available assets
- save editor settings when hitting 'X', but not from an Alt+F4
- strings?!
- collision not working?
- loading materials?

- forcing game run while in editor mode
- collision not working?
- camera flipping?
- rotation tool
- game vs editor shadow maps?
- drawing editor highlight over game view?



// GL Modernization Migration Notes
- great migration guide https://www.slideshare.net/tlorach/opengl-nvidia-commandlistapproaching-zerodriveroverhead
   - slides 40 - 43
- fast features for dummies - https://www.khronos.org/assets/uploads/developers/library/2014-gdc/Khronos-OpenGL-Efficiency-GDC-Mar14.pdf
   - slide 5
- http://on-demand.gputechconf.com/siggraph/2014/presentation/SG4117-OpenGL-Scene-Rendering-Techniques.pdf

// outline plan
- draw selected objects to a special fbo as thick wireframe
- blur
- use stencil to clear obj space on fbo
- draw selection fbo over final fbo

//
// Long term TODO/Ideas/Features
//

Rendering
- Cull back-faces optional
- split screen?
- Do box wireframe as just the edges to get rid of current diagonal lines. probably just do a series of draw line commands
- Rim highlight for selections, maybe just render it first as a solid color at a higher scale, then draw it normally on top? Sobel?
  - http://blogs.love2d.org/content/let-it-glow-dynamically-adding-outlines-characters
  - https://stackoverflow.com/questions/9612153/sobel-filter-in-c-c-using-opengl-es
- Trilighting?
- proper sorting for transparent meshes?
- batching of static meshes with the same data layouts, just do glDrawArrays(offset, count)? Might need interleaved data for this to be effective


FMOD
- look into Instant replay system, might want to use in conjunction with existing game replay system?

Assets
- Deallocate mesh path
- Take into account Geometry Transform?
- Figure out whats wrong with sword.fbx import, whats different from the blender export

Transforms
- Try getting rid of LocalPosition,LocalRotation,Position,Rotation... derive them from matrices?

Shipping Eror Handling
- Create debug files on crashes, stack dump, error, file name, line number, etc.
- Variadic debug console output
- Error window popup message box, on crash, maybe Breakpad integration sometime...

General
- Do all scene graphy stuff before going to render. just pass it model matrices

String
- Compress down different versions of StringsAreEqual
- Remove size param when we can
- current version sucks because we can't do copies easily, and the constructor business doesn't really work. I think we need to track the size of the buffer and the current string length, we do a lot of StringLength lookups


Platform
- full redraw when window is dragged off side and brought back
- pause game when window is being dragged or is not focused
- Maybe have RNG be a platform thing? Seems dangerous. Diff behaviour on diff architecture?
    - on Windows you want something like CryptGenRandom, check out the wikipedia page https://en.wikipedia.org/wiki/CryptGenRandom
- Record/replay. Can we exclude player entity from it, or a separate camera? Record/replay just game_state?

Input
- Controller

Collision
- great collision docs here https://code.google.com/archive/p/box2d/downloads?page=1
- Capsule
- Ray GJK?
- "is in frustrum" check
- If a capsule completely encompasses another object, they are not considered collided

Integration "To Research" Notes
- Taylor Series
- Implicit Euler
- Semi-Explicit Euler
- Explicit Euler
- RK4

4coder
- Launch executable from hotkey so I don't have to always run from debugger
- Build options for only doing changed DLL's
- Sometimes compliation feed just says 'exited with code 1' 'exited with code 0' or just freezes?
- 

Shader Gen
- Dont write pre-processor'd out code into generated shaders

Editor GUI
- Fix spacing between topmost tab and main menu bar
- try to get rid of ImVectors, malloc'd memory
- different colored logs, for red errors

Profiling
- hierarchical view times vs total time spent in function across program

Vulkan
- https://vulkan-tutorial.com/Overview

Networking
- debug setup 
eisbehr - Handmade Network Discord - ": for quick turnaround I run my editor and debugger (visual studio) on windows, the project folder is mounted on the server (a virtualbox share in my case, but could be any network drive/share). My build script connects to the server over ssh and builds the project there, the files on both sides are the same since it's using a shared folder. Visual Studio can do remote debugging for linux, for which it also uses ssh."
- Bash on Ubuntu on Windows
- eisbehr's build script https://gist.github.com/eisbehr/715e70a0668a25a0e69951ba09c164ad