member_definition MembersOf_rectangle2[] = 
{
   {0, MetaType_v2, "v2", MetaType_no_base, "no_base", "Min", U32FromPointer(&((rectangle2 *)0)->Min)},
   {0, MetaType_v2, "v2", MetaType_no_base, "no_base", "Max", U32FromPointer(&((rectangle2 *)0)->Max)},
};
member_definition MembersOf_string[] = 
{
   {MetaMemberFlag_IsPointer, MetaType_char, "char", MetaType_no_base, "no_base", "Text", U32FromPointer(&((string *)0)->Text)},
   {0, MetaType_uint32, "uint32", MetaType_no_base, "no_base", "Size", U32FromPointer(&((string *)0)->Size)},
};
member_definition MembersOf_transform[] = 
{
   {0, MetaType_char, "char", MetaType_no_base, "no_base", "__SerializedPathMemory", U32FromPointer(&((transform *)0)->__SerializedPathMemory)},
   {0, MetaType_v3, "v3", MetaType_no_base, "no_base", "LocalPosition", U32FromPointer(&((transform *)0)->LocalPosition)},
   {0, MetaType_v3, "v3", MetaType_no_base, "no_base", "LocalScale", U32FromPointer(&((transform *)0)->LocalScale)},
   {0, MetaType_v4, "v4", MetaType_no_base, "no_base", "LocalRotation", U32FromPointer(&((transform *)0)->LocalRotation)},
};
member_definition MembersOf_material[] = 
{
   {0, MetaType_v4, "v4", MetaType_no_base, "no_base", "Color", U32FromPointer(&((material *)0)->Color)},
   {0, MetaType_bool32, "bool32", MetaType_no_base, "no_base", "IsTransparent", U32FromPointer(&((material *)0)->IsTransparent)},
   {0, MetaType_bool32, "bool32", MetaType_no_base, "no_base", "CastShadows", U32FromPointer(&((material *)0)->CastShadows)},
   {0, MetaType_bool32, "bool32", MetaType_no_base, "no_base", "RecieveShadows", U32FromPointer(&((material *)0)->RecieveShadows)},
};
member_definition MembersOf_aabb[] = 
{
   {0, MetaType_v3, "v3", MetaType_no_base, "no_base", "Position", U32FromPointer(&((aabb *)0)->Position)},
   {0, MetaType_v3, "v3", MetaType_no_base, "no_base", "HalfWidths", U32FromPointer(&((aabb *)0)->HalfWidths)},
};
static char* collision_layer_strings[20] = {
"CollisionLayer_Default",
"CollisionLayer_Editor",
"CollisionLayer_PlayerBullet",
"CollisionLayer_Player",
"CollisionLayer_Enemy",
"CollisionLayer_EnemyBullet",
"CollisionLayer_BulletKillZone",
"CollisionLayer_Count"
};
static char* collider_type_strings[20] = {
"ColliderType_None",
"ColliderType_Mesh",
"ColliderType_Ray",
"ColliderType_Sphere",
"ColliderType_Capsule",
"ColliderType_Cylinder",
"ColliderType_Quad",
"ColliderType_Frustrum",
"ColliderType_count"
};
static char* entity_type_strings[20] = {
"EntityType_Default",
"EntityType_Bullet",
"EntityType_BulletKillZone",
"EntityType_EnemyShooter",
"EntityType_EnemyBullet",
"EntityType_EnemyChaser",
"EntityType_GameWinZone",
"EntityType_PlayerKillZone",
"EntityType_count"
};
member_definition MembersOf_entity[] = 
{
   {0, MetaType_bool32, "bool32", MetaType_transform, "transform", "SerializeToFile", U32FromPointer(&((entity *)0)->SerializeToFile)},
   {0, MetaType_uint32, "uint32", MetaType_transform, "transform", "EntityType", U32FromPointer(&((entity *)0)->EntityType)},
   {0, MetaType_uint32, "uint32", MetaType_transform, "transform", "Enabled", U32FromPointer(&((entity *)0)->Enabled)},
   {0, MetaType_uint32, "uint32", MetaType_transform, "transform", "DeletionPending", U32FromPointer(&((entity *)0)->DeletionPending)},
   {0, MetaType_uint32, "uint32", MetaType_transform, "transform", "ColliderType", U32FromPointer(&((entity *)0)->ColliderType)},
   {0, MetaType_bool32, "bool32", MetaType_transform, "transform", "IsCharacterController", U32FromPointer(&((entity *)0)->IsCharacterController)},
   {0, MetaType_uint32, "uint32", MetaType_transform, "transform", "CollisionLayer", U32FromPointer(&((entity *)0)->CollisionLayer)},
   {0, MetaType_char, "char", MetaType_transform, "transform", "__ColliderMeshPathMemory", U32FromPointer(&((entity *)0)->__ColliderMeshPathMemory)},
   {0, MetaType_real32, "real32", MetaType_transform, "transform", "SphereColliderRadius", U32FromPointer(&((entity *)0)->SphereColliderRadius)},
   {0, MetaType_real32, "real32", MetaType_transform, "transform", "CapsuleColliderRadius", U32FromPointer(&((entity *)0)->CapsuleColliderRadius)},
   {0, MetaType_real32, "real32", MetaType_transform, "transform", "CylinderColliderRadius", U32FromPointer(&((entity *)0)->CylinderColliderRadius)},
   {0, MetaType_real32, "real32", MetaType_transform, "transform", "CapsuleColliderHeight", U32FromPointer(&((entity *)0)->CapsuleColliderHeight)},
   {0, MetaType_real32, "real32", MetaType_transform, "transform", "CylinderColliderHeight", U32FromPointer(&((entity *)0)->CylinderColliderHeight)},
   {0, MetaType_real32, "real32", MetaType_transform, "transform", "Mass", U32FromPointer(&((entity *)0)->Mass)},
   {0, MetaType_bool32, "bool32", MetaType_transform, "transform", "IsTrigger", U32FromPointer(&((entity *)0)->IsTrigger)},
   {0, MetaType_bool32, "bool32", MetaType_transform, "transform", "UseGravity", U32FromPointer(&((entity *)0)->UseGravity)},
   {0, MetaType_bool32, "bool32", MetaType_transform, "transform", "IsStatic", U32FromPointer(&((entity *)0)->IsStatic)},
   {0, MetaType_bool32, "bool32", MetaType_transform, "transform", "LockRotation", U32FromPointer(&((entity *)0)->LockRotation)},
   {0, MetaType_real32, "real32", MetaType_transform, "transform", "CoefficientOfRestitution", U32FromPointer(&((entity *)0)->CoefficientOfRestitution)},
   {0, MetaType_real32, "real32", MetaType_transform, "transform", "XZFriction", U32FromPointer(&((entity *)0)->XZFriction)},
   {0, MetaType_char, "char", MetaType_transform, "transform", "__RenderMeshPathMemory", U32FromPointer(&((entity *)0)->__RenderMeshPathMemory)},
   {0, MetaType_material, "material", MetaType_transform, "transform", "Material", U32FromPointer(&((entity *)0)->Material)},
   {0, MetaType_v2, "v2", MetaType_transform, "transform", "Size", U32FromPointer(&((entity *)0)->Size)},
   {0, MetaType_v2, "v2", MetaType_transform, "transform", "Pivot", U32FromPointer(&((entity *)0)->Pivot)},
   {0, MetaType_bool32, "bool32", MetaType_transform, "transform", "IsDirectionalLight", U32FromPointer(&((entity *)0)->IsDirectionalLight)},
   {0, MetaType_real32, "real32", MetaType_transform, "transform", "PlayerRotationX", U32FromPointer(&((entity *)0)->PlayerRotationX)},
   {0, MetaType_real32, "real32", MetaType_transform, "transform", "PlayerRotationY", U32FromPointer(&((entity *)0)->PlayerRotationY)},
   {0, MetaType_bool32, "bool32", MetaType_transform, "transform", "IsPlayerGun", U32FromPointer(&((entity *)0)->IsPlayerGun)},
};
member_definition MembersOf_player_settings[] = 
{
   {0, MetaType_real32, "real32", MetaType_no_base, "no_base", "MouseSensitivity", U32FromPointer(&((player_settings *)0)->MouseSensitivity)},
   {0, MetaType_bool32, "bool32", MetaType_no_base, "no_base", "Vsync", U32FromPointer(&((player_settings *)0)->Vsync)},
   {0, MetaType_bool32, "bool32", MetaType_no_base, "no_base", "Fullscreen", U32FromPointer(&((player_settings *)0)->Fullscreen)},
   {0, MetaType_v2, "v2", MetaType_no_base, "no_base", "WindowPosition", U32FromPointer(&((player_settings *)0)->WindowPosition)},
};
member_definition MembersOf_glyph_table_entry[] = 
{
   {0, MetaType_char, "char", MetaType_no_base, "no_base", "Glyph", U32FromPointer(&((glyph_table_entry *)0)->Glyph)},
   {0, MetaType_rectangle2, "rectangle2", MetaType_no_base, "no_base", "BoundingBox", U32FromPointer(&((glyph_table_entry *)0)->BoundingBox)},
   {0, MetaType_real32, "real32", MetaType_no_base, "no_base", "Advance", U32FromPointer(&((glyph_table_entry *)0)->Advance)},
   {0, MetaType_rectangle2, "rectangle2", MetaType_no_base, "no_base", "AtlasBox", U32FromPointer(&((glyph_table_entry *)0)->AtlasBox)},
};
member_definition MembersOf_mesh[] = 
{
   {0, MetaType_bool32, "bool32", MetaType_transform, "transform", "IsIndexedMesh", U32FromPointer(&((mesh *)0)->IsIndexedMesh)},
};
member_definition MembersOf_editor_settings[] = 
{
   {0, MetaType_char, "char", MetaType_no_base, "no_base", "LastOpenScene", U32FromPointer(&((editor_settings *)0)->LastOpenScene)},
};
#define META_SERIALIZE_STRUCT(MemberPtr, NextIndentLevel, TypeName, StructName, File) \
    case MetaType_editor_settings: {SerializeStruct(ArrayCount(MembersOf_editor_settings), MembersOf_editor_settings, MemberPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_mesh: {SerializeStruct(ArrayCount(MembersOf_mesh), MembersOf_mesh, MemberPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_glyph_table_entry: {SerializeStruct(ArrayCount(MembersOf_glyph_table_entry), MembersOf_glyph_table_entry, MemberPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_player_settings: {SerializeStruct(ArrayCount(MembersOf_player_settings), MembersOf_player_settings, MemberPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_entity: {SerializeStruct(ArrayCount(MembersOf_entity), MembersOf_entity, MemberPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_aabb: {SerializeStruct(ArrayCount(MembersOf_aabb), MembersOf_aabb, MemberPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_material: {SerializeStruct(ArrayCount(MembersOf_material), MembersOf_material, MemberPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_transform: {SerializeStruct(ArrayCount(MembersOf_transform), MembersOf_transform, MemberPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_string: {SerializeStruct(ArrayCount(MembersOf_string), MembersOf_string, MemberPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_rectangle2: {SerializeStruct(ArrayCount(MembersOf_rectangle2), MembersOf_rectangle2, MemberPtr, (NextIndentLevel), TypeName, StructName, File);} break; 
#define META_DELTA_SERIALIZE_STRUCT(MemberPtr, DefaultStructPtr, NextIndentLevel, TypeName, StructName, File) \
    case MetaType_editor_settings: {DeltaSerializeStruct(ArrayCount(MembersOf_editor_settings), MembersOf_editor_settings, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_mesh: {DeltaSerializeStruct(ArrayCount(MembersOf_mesh), MembersOf_mesh, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_glyph_table_entry: {DeltaSerializeStruct(ArrayCount(MembersOf_glyph_table_entry), MembersOf_glyph_table_entry, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_player_settings: {DeltaSerializeStruct(ArrayCount(MembersOf_player_settings), MembersOf_player_settings, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_entity: {DeltaSerializeStruct(ArrayCount(MembersOf_entity), MembersOf_entity, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_aabb: {DeltaSerializeStruct(ArrayCount(MembersOf_aabb), MembersOf_aabb, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_material: {DeltaSerializeStruct(ArrayCount(MembersOf_material), MembersOf_material, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_transform: {DeltaSerializeStruct(ArrayCount(MembersOf_transform), MembersOf_transform, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_string: {DeltaSerializeStruct(ArrayCount(MembersOf_string), MembersOf_string, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, File);} break; \
    case MetaType_rectangle2: {DeltaSerializeStruct(ArrayCount(MembersOf_rectangle2), MembersOf_rectangle2, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, File);} break; 
