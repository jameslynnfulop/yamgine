#include "game.h"

#define SERVER_IP "192.168.1.156"
#define SERVER_RECIEVE_PORT 27015

#define SERVER_SEND_PORT 27014

#define HEARTBEAT_INTERVAL 0.1

#define CONNECTION_TIMEOUT 5

#if YAMGINE_CLIENT
void InitializeClientNetworking(network_state* State, platform_api* Platform)
{
    YamProf(InitializeClientNetworking);
    State->ClientSendSocket = Platform->CreateSocket();
    Platform->BindSocket(&State->ClientSendSocket, "", SERVER_RECIEVE_PORT);

    State->ClientRecieveSocket = Platform->CreateSocket();
    Platform->BindSocket(&State->ClientRecieveSocket, "", SERVER_SEND_PORT);
}

void ClientHeartbeat(game_state* GameState, platform_api* Platform)
{
    YamProf(Net_ClientHeartbeat);
    network_state* NetworkState = &GameState->NetworkState;

    //
    // Send Heartbeat
    //
    if (GameState->ElapsedTime > NetworkState->LastHeartbeatSendTime + HEARTBEAT_INTERVAL)
    {
        Platform->SendTo(NetworkState->ClientSendSocket, SERVER_IP, SERVER_RECIEVE_PORT, "Yam Heartbeat");
        NetworkState->LastHeartbeatSendTime = GameState->ElapsedTime;
    }

    //
    // Recieve Heartbeat
    //
    char IP[16] = {};
    uint16 Port = 0;
    char RecieveBuffer[1024] = {};
    uint32 BytesRecieved = 1;
    while (BytesRecieved > 0)
    {
        BytesRecieved = Platform->RecvFrom(NetworkState->ClientRecieveSocket,
                                           &RecieveBuffer[0], 1024,
                                           &IP[0], &Port);

        if (StringsAreEqual(RecieveBuffer, "Yam Heartbeat"))
        {
            NetworkState->LastHeartbeatRecieveTime = GameState->ElapsedTime;
            if (!NetworkState->HasConnection)
            {
                NetworkState->HasConnection = true;
                MyConsole.AddLog("Connection to server established.");
            }
        }

        if (NetworkState->HasConnection && GameState->ElapsedTime - NetworkState->LastHeartbeatRecieveTime > CONNECTION_TIMEOUT)
        {
            MyConsole.AddLog("Connection to server lost.");
            NetworkState->HasConnection = false;
        }
    }
}
#endif

#if YAMGINE_SERVER
void InitializeServerNetworking(network_state* State, platform_api* Platform)
{
    State->ServerRecieveSocket = Platform->CreateSocket();
    Platform->BindSocket(&State->ServerRecieveSocket, "", SERVER_RECIEVE_PORT);

    State->ServerSendSocket = Platform->CreateSocket();
    Platform->BindSocket(&State->ServerSendSocket, "", SERVER_SEND_PORT);
}

void ServerHeartbeat(game_state* GameState, platform_api* Platform)
{
    YamProf(Server_Heartbeat);
    network_state* NetworkState = &GameState->NetworkState;

    //
    // Recieve Heartbeat
    //
    char IP[16] = {};
    uint16 Port = 0;
    char RecieveBuffer[1024] = {};
    uint32 BytesRecieved = 1;
    while (BytesRecieved > 0)
    {
        BytesRecieved = Platform->RecvFrom(NetworkState->ServerRecieveSocket,
                                           &RecieveBuffer[0], 1024,
                                           &IP[0], &Port);

        if (StringsAreEqual(RecieveBuffer, "Yam Heartbeat"))
        {
            if (!NetworkState->HasConnection)
            {
                CopyString(IP, NetworkState->ClientIP, 16);
                NetworkState->ClientPort = Port;
                NetworkState->HasConnection = true;
                MyConsole.AddLog("Connection to client.");
                MyConsole.AddLog(NetworkState->ClientIP);
            }
            NetworkState->LastHeartbeatRecieveTime = GameState->ElapsedTime;
        }
    }

    if (NetworkState->HasConnection)
    {
        if (GameState->ElapsedTime - NetworkState->LastHeartbeatRecieveTime > CONNECTION_TIMEOUT)
        {
            MyConsole.AddLog("Connection to client lost.");
            NetworkState->HasConnection = false;
        }

        //
        // Send Heartbeat
        //
        if (GameState->ElapsedTime > NetworkState->LastHeartbeatSendTime + HEARTBEAT_INTERVAL)
        {
            Platform->SendTo(NetworkState->ServerSendSocket, NetworkState->ClientIP, SERVER_SEND_PORT, "Yam Heartbeat");
            NetworkState->LastHeartbeatSendTime = GameState->ElapsedTime;
        }
    }
}
#endif
