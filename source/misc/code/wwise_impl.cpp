#include <stdio.h>
#include <stdlib.h>

// Custom alloc/free functions. These are declared as "extern" in AkMemoryMgr.h
// and MUST be defined by the game developer.

namespace AK
{
#ifdef WIN32
    void * AllocHook( size_t in_size )
    {
        return malloc( in_size );
    }
    void FreeHook( void * in_ptr )
    {
        free( in_ptr );
    }
    // Note: VirtualAllocHook() may be used by I/O pools of the default implementation
    // of the Stream Manager, to allow "true" unbuffered I/O (using FILE_FLAG_NO_BUFFERING
    // - refer to the Windows SDK documentation for more details). This is NOT mandatory;
    // you may implement it with a simple malloc().
    void * VirtualAllocHook(
        void * in_pMemAddress,
        size_t in_size,
        DWORD in_dwAllocationType,
        DWORD in_dwProtect
        )
    {
        return VirtualAlloc( in_pMemAddress, in_size, in_dwAllocationType, in_dwProtect );
    }
    void VirtualFreeHook( 
        void * in_pMemAddress,
        size_t in_size,
        DWORD in_dwFreeType
        )
    {
        VirtualFree( in_pMemAddress, in_size, in_dwFreeType );
    }
#endif
    
}

#include <AK/SoundEngine/Common/AkMemoryMgr.h>                  // Memory Manager
#include <AK/SoundEngine/Common/AkModule.h>                     // Default memory and stream managers
#include <AK/SoundEngine/Common/IAkStreamMgr.h>                 // Streaming Manager
#include <AK/Tools/Common/AkPlatformFuncs.h>                    // Thread defines

// Include for communication between Wwise and the game -- Not needed in the release version
#ifndef AK_OPTIMIZED
#include <AK/Comm/AkCommunication.h>
#endif // AK_OPTIMIZED

#include <AkFileLocationBase.cpp>
#include <AkFilePackage.cpp>
#include <AkFilePackageLUT.cpp>
#include <AkDefaultIOHookBlocking.cpp>
#include <AkFilePackageLowLevelIOBlocking.h>                    // Sample low-level I/O implementation

#include <AK/Plugin/AkRoomVerbFXFactory.h> 
// Factory headers forces linking of the plugin code through AK_STATIC_LINK_PLUGIN.
#include <AK/Plugin/AkVorbisDecoderFactory.h>

// We're using the default Low-Level I/O implementation that's part
// of the SDK's sample code, with the file package extension
global_variable CAkFilePackageLowLevelIOBlocking g_lowLevelIO;

bool WwiseInit(wwise* Wwise, char* ExecutableFolder)
{
    //
    // Create and initialize an instance of the default memory manager. Note
    // that you can override the default memory manager with your own. Refer
    // to the SDK documentation for more information.
    //
    
    AkMemSettings memSettings;
    memSettings.uMaxNumPools = 20;
    
    if ( AK::MemoryMgr::Init( &memSettings ) != AK_Success )
    {
        assert( ! "Could not create the memory manager." );
        return false;
    }
    
    
    //
    // Create and initialize an instance of the default streaming manager. Note
    // that you can override the default streaming manager with your own. Refer
    // to the SDK documentation for more information.
    //
    
    AkStreamMgrSettings stmSettings;
    AK::StreamMgr::GetDefaultSettings( stmSettings );
    
    // Customize the Stream Manager settings here.
    
    if ( !AK::StreamMgr::Create( stmSettings ) )
    {
        assert( ! "Could not create the Streaming Manager" );
        return false;
    }
    
    //
    // Create a streaming device with blocking low-level I/O handshaking.
    // Note that you can override the default low-level I/O module with your own. Refer
    // to the SDK documentation for more information.        
    //
    AkDeviceSettings deviceSettings;
    AK::StreamMgr::GetDefaultDeviceSettings( deviceSettings );
    
    // Customize the streaming device settings here.
    
    // CAkFilePackageLowLevelIOBlocking::Init() creates a streaming device
    // in the Stream Manager, and registers itself as the File Location Resolver.
    if ( g_lowLevelIO.Init( deviceSettings ) != AK_Success )
    {
        assert( ! "Could not create the streaming device and Low-Level I/O system" );
        return false;
    }
    
    //
    // Create the Sound Engine
    // Using default initialization parameters
    //
    
    AkInitSettings initSettings;
    AkPlatformInitSettings platformInitSettings;
    AK::SoundEngine::GetDefaultInitSettings( initSettings );
    AK::SoundEngine::GetDefaultPlatformInitSettings( platformInitSettings );
    
    if ( AK::SoundEngine::Init( &initSettings, &platformInitSettings ) != AK_Success )
    {
        assert( ! "Could not initialize the Sound Engine." );
        return false;
    }
    
    //
    // Initialize the music engine
    // Using default initialization parameters
    //
    
    AkMusicSettings musicInit;
    AK::MusicEngine::GetDefaultInitSettings( musicInit );
    
    if ( AK::MusicEngine::Init( &musicInit ) != AK_Success )
    {
        assert( ! "Could not initialize the Music Engine." );
        return false;
    }
    
#ifndef AK_OPTIMIZED
    //
    // Initialize communications (not in release build!)
    //
    AkCommSettings commSettings;
    AK::Comm::GetDefaultInitSettings( commSettings );
    if ( AK::Comm::Init( commSettings ) != AK_Success )
    {
        assert( ! "Could not initialize communication." );
        return false;
    }
#endif // AK_OPTIMIZED
    
    Wwise->API.SetPosition = &AK::SoundEngine::SetPosition;
    Wwise->API.PostEvent = &AK::SoundEngine::PostEvent;
    Wwise->API.RegisterGameObj = &AK::SoundEngine::RegisterGameObj;
    Wwise->API.UnregisterGameObj = &AK::SoundEngine::UnregisterGameObj;
    Wwise->API.SetState = &AK::SoundEngine::SetState;
    
    return true;
}

void WwiseLoadBanks()
{
    g_lowLevelIO.SetBasePath( AKTEXT("media/audio/GeneratedSoundBanks/Windows") );
    AK::StreamMgr::SetCurrentLanguage( AKTEXT("English(US)") );
    
    //
    // Load banks synchronously (from file name).
    //
    
    AkBankID bankID; // Not used. These banks can be unloaded with their file name.
    AKRESULT eResult = AK::SoundEngine::LoadBank( "Init.bnk", AK_DEFAULT_POOL_ID, bankID );
    assert( eResult == AK_Success );
    eResult = AK::SoundEngine::LoadBank( "InteractiveMusic.bnk", AK_DEFAULT_POOL_ID, bankID );
    assert( eResult == AK_Success );
}

void WwiseTerminate()
{
#ifndef AK_OPTIMIZED
    AK::Comm::Term();
#endif // AK_OPTIMIZED
    AK::MusicEngine::Term();
    AK::SoundEngine::Term();
    g_lowLevelIO.Term();
    if ( AK::IAkStreamMgr::Get() )
        AK::IAkStreamMgr::Get()->Destroy();
    AK::MemoryMgr::Term();
}