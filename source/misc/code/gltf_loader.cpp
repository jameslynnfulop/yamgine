
struct gltf_buffer
{
    uint32 ByteLength;
    void* Bin;
};

struct gltf_buffer_view
{
    gltf_buffer* Buffer;
    uint32 ByteOffset;
    uint32 ByteStride;// only for vertex attributes
    uint32 ByteLength;
    uint32 Target;
};

enum accessor_type
{
    AccessorType_scalar,
    AccessorType_v3
};

struct gltf_accessor
{
    gltf_buffer_view* BufferView;
    uint32 ByteOffset;
    uint32 ComponentType;
    uint32 Count;
    accessor_type Type;
    // max
    // min
};

struct pbr_metallic_roughness
{
    v4 Color;
    real32 MetallicFactor;
};

struct gltf_material
{
    pbr_metallic_roughness PBR;
    char* Name;
};

struct gltf_primitive
{
    gltf_accessor* Positions;
    gltf_accessor* Normals;
    gltf_accessor* Indices;
    
    gltf_material*  Material;
    
    uint32 MeshMode;//triangles, lines, points?
};

struct gltf_mesh
{
    dumb_list Primitives;
    char* Name;
};

struct gltf_node
{
    uint32 ChildCount;
    matrix4 Matrix;
    dumb_list Children;
    //TODO(james): following pretty much same logic as dumb list...
    uint32 ChildrenIndices[8];
    uint32 ChildrenIndicesCount;
    gltf_mesh* Mesh;
    
    //NOTE(james): the nodes are listed flat, but our GltfToYamgine function
    //converts through the tree, so this makes sure we don't add it twice
    bool32 Added;
};

struct gltf_scene
{
    gltf_node* Node;
};

//
//
//

mesh* GltfToYamgine(gltf_node* Node, memory_arena* AssetArena, 
                         platform_api* PlatformAPI, transform* Parent=NULL)
{
    Assert(!Node->Added);
    
    Node->Added = true;
    
    mesh* Mesh = PushStruct(AssetArena, mesh);
    InitializeTransform(Mesh);

    SetRootName(Mesh, "GLTF Node");
    
    Mesh->LocalToWorld = Node->Matrix;
    
    if (Parent != NULL)
    {
        SetTransformParent(Mesh, Parent);
    }
    
    if (Node->Mesh != NULL)
    {
        if (Node->Mesh->Name)
        {
            SetRootName(Mesh, Node->Mesh->Name);
        }
        else
        {
            SetRootName(Mesh, "GLTF Mesh");
        }
        
        //TODO(james): set transform name from mesh name
        for (uint32 PrimitiveIndex = 0; 
             PrimitiveIndex < Node->Mesh->Primitives.Count;
             PrimitiveIndex++)
        {
            gltf_primitive* GltfPrimitive = (gltf_primitive*)GetFromList(&Node->Mesh->Primitives, PrimitiveIndex);
            mesh* Primitive = PushStruct(AssetArena, mesh);
            InitializeTransform(Primitive);
            
            if (Node->Mesh->Name)
            {
                SetRootName(Primitive, Node->Mesh->Name);    
            }
            else
            {
                SetRootName(Primitive, "GLTF Primitive");    
            }
            
            
            {
                PlatformAPI->CreateBuffer(&Primitive->Vertices, CHANNEL_POSITION, CHANNEL_POSITION_WIDTH, MeshResidency_Static);
                
                Primitive->Vertices.Count = GltfPrimitive->Positions->Count;
                Primitive->Vertices.Data = PushArray(AssetArena, Primitive->Vertices.Count, v3);
                
                void* BufferPtr = ((uint8*)GltfPrimitive->Positions->BufferView->Buffer->Bin) + 
                    GltfPrimitive->Positions->BufferView->ByteOffset + 
                    GltfPrimitive->Positions->ByteOffset;
                
                Copy(GltfPrimitive->Positions->BufferView->ByteLength, BufferPtr, Primitive->Vertices.Data);
                
                PlatformAPI->FillBuffer(&Primitive->Vertices);    
            }
            
            if (GltfPrimitive->Normals)
            {
                PlatformAPI->CreateBuffer(&Primitive->Normals, CHANNEL_NORMAL, CHANNEL_NORMAL_WIDTH, MeshResidency_Static);
                
                Primitive->Normals.Count = GltfPrimitive->Normals->Count;
                Primitive->Normals.Data = PushArray(AssetArena, Primitive->Normals.Count, v3);
                
                void* BufferPtr = ((uint8*)GltfPrimitive->Normals->BufferView->Buffer->Bin) + 
                    GltfPrimitive->Normals->BufferView->ByteOffset + 
                    GltfPrimitive->Normals->ByteOffset;
                
                Copy(GltfPrimitive->Normals->BufferView->ByteLength, BufferPtr, Primitive->Normals.Data);
                
                PlatformAPI->FillBuffer(&Primitive->Normals);    
            }
            
            {
                PlatformAPI->CreateBuffer(&Primitive->Elements, CHANNEL_ELEMENT, CHANNEL_ELEMENT_WIDTH, MeshResidency_Static);
                
                Primitive->Elements.Count = GltfPrimitive->Indices->Count;
                Primitive->Elements.Data = PushArray(AssetArena, Primitive->Elements.Count, uint32);
                
                void* BufferPtr = ((uint8*)GltfPrimitive->Indices->BufferView->Buffer->Bin) + 
                    GltfPrimitive->Indices->BufferView->ByteOffset + 
                    GltfPrimitive->Indices->ByteOffset;
                //  NOTE(james): 5126 == Unsigned short, but we only support unsigned int right now, 
                // so we need to up convert
                if (GltfPrimitive->Indices->ComponentType == 5123)
                {
                    for (uint32 Index = 0; Index < Primitive->Elements.Count; Index++)
                    {
                        ((uint32*)Primitive->Elements.Data)[Index] = ((uint16*)BufferPtr)[Index];
                    }
                }
                else if (GltfPrimitive->Indices->ComponentType == 5125)// unsigned int
                {
                    Copy(GltfPrimitive->Indices->BufferView->ByteLength, BufferPtr, Primitive->Elements.Data);    
                }
                else
                {
                    Assert(0);
                }
                
                PlatformAPI->FillBuffer(&Primitive->Elements);    
            }
            
            SetTransformParent(Primitive, Mesh);
        }
    }
    
    // recurse
    if (Node->ChildCount != 0)
    {
        for (uint32 ChildIndex = 0; ChildIndex < Node->ChildCount; ChildIndex++)
        {
            gltf_node* Child = (gltf_node*)GetFromList(&Node->Children, ChildIndex);
            GltfToYamgine(Child, AssetArena, PlatformAPI, Mesh);
        }
    }
    
    return Mesh;
}

//
//
//

matrix4 ParseMatrix(tokenizer* Tokenizer)
{
    matrix4 Result = {};
    
    AssertToken(Tokenizer, Token_OpenBracket);
    for (uint32 Y = 0; Y < 4; Y++)
    {
        for (uint32 X = 0; X < 4; X++)
        {
            token Token = GetToken(Tokenizer);
            float Val = atof(Token.Text);
            Result.E[Y*4+X] = Val;
            if (!(Y == 3 && X == 3))
            {
                AssertToken(Tokenizer, Token_Comma);
            }
        }
    }
    AssertToken(Tokenizer, Token_CloseBracket);
    
    return Result;
}


void ParseAsset(tokenizer* Tokenizer)
{
    token Token = GetToken(Tokenizer);
    while (Token.Type != Token_CloseBrace)
	{
        if (Token.Type == Token_String)
        {
            //NOTE(james): make sure we are version 2.0
            if (TokenEquals(Token, "version"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                Assert(TokenEquals(Token, "2.0"));
            }
        }
        Token = GetToken(Tokenizer);
	}
}

gltf_scene ParseScene(tokenizer* Tokenizer, dumb_list* NodeList)
{
    gltf_scene Scene = {};
    token Token = GetToken(Tokenizer);
    while (Token.Type != Token_CloseBrace)
    {
        if (Token.Type == Token_String)
        {
            if (TokenEquals(Token, "nodes"))
            {
                while (Token.Type != Token_Number)
                {
                    Token = GetToken(Tokenizer);
                }
                //TODO(james): handle having multiple root nodes?
                Scene.Node = (gltf_node*)GetFromList(NodeList, LoadUint32(Token));
            }
        }
        Token = GetToken(Tokenizer);
    }
    return Scene;
}

gltf_node ParseNode(tokenizer* Tokenizer, dumb_list* MeshList)
{
    gltf_node Node = {};
    token Token = GetToken(Tokenizer);
    
	while (Token.Type != Token_CloseBrace)
	{
		if (Token.Type == Token_String)
		{
			if (TokenEquals(Token, "name"))
			{
				//Assert(0);
			}
			else if (TokenEquals(Token, "children"))//this is a list if indices
			{
				while (Token.Type != Token_Number)
				{
					Token = GetToken(Tokenizer);
				}
				
                Node.ChildrenIndices[Node.ChildrenIndicesCount++] = LoadUint32(Token);
			}
			else if (TokenEquals(Token, "matrix"))
			{
                AssertToken(Tokenizer, Token_Colon);
                ParseMatrix(Tokenizer);
                Node.Matrix = IdentityMatrix();
			}
			else if (TokenEquals(Token, "mesh"))
			{
				AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                Node.Mesh = (gltf_mesh*)GetFromList(MeshList, LoadUint32(Token));
			}
			else if (TokenEquals(Token, "translation"))
			{
				Assert(0);
			}
			else if (TokenEquals(Token, "scale"))
			{
				Assert(0);
			}
			else if (TokenEquals(Token, "rotation"))
			{
				Assert(0);
			}
			else if (TokenEquals(Token, "camera"))
			{
				Assert(0);
			}
		}
        
        Token = GetToken(Tokenizer);
	}
    return Node;
}

gltf_primitive ParsePrimitive(tokenizer* Tokenizer, 
                              dumb_list* AccessorList, 
                              dumb_list* MaterialList)
{
    gltf_primitive Primitive = {};
    token Token = GetToken(Tokenizer);
    while (Token.Type != Token_CloseBrace)
    {
        if (Token.Type == Token_String)
        {
            if (TokenEquals(Token, "attributes"))
            {
                AssertToken(Tokenizer, Token_Colon);
                AssertToken(Tokenizer, Token_OpenBrace);
                
                Token = GetToken(Tokenizer);
                while (Token.Type != Token_CloseBrace)
                {
                    if (Token.Type == Token_String)
                    {
                        if (TokenEquals(Token, "POSITION"))
                        {
                            AssertToken(Tokenizer, Token_Colon);
                            Token = GetToken(Tokenizer);
                            uint32 Index = LoadUint32(Token);
                            Primitive.Positions = (gltf_accessor*)GetFromList(AccessorList, Index);
                        }
                        else if (TokenEquals(Token, "NORMAL"))
                        {
                            AssertToken(Tokenizer, Token_Colon);
                            Token = GetToken(Tokenizer);
                            uint32 Index = LoadUint32(Token);
                            Primitive.Normals = (gltf_accessor*)GetFromList(AccessorList, Index);
                        }
                        else if (TokenEquals(Token, "TEXCOORD_0"))
                        {
                            AssertToken(Tokenizer, Token_Colon);
                            Token = GetToken(Tokenizer);
                            uint32 Index = LoadUint32(Token);
                        }
                        else if (TokenEquals(Token, "TANGENT"))
                        {
                            AssertToken(Tokenizer, Token_Colon);
                            Token = GetToken(Tokenizer);
                            uint32 Index = LoadUint32(Token);
                        }
                        else
                        {
                            Assert(0);
                        }
                    }
                    else if (Token.Type == Token_Number)
                    {
                        
                    }
                    Token = GetToken(Tokenizer);
                }
            }
            else if (TokenEquals(Token, "indices"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                uint32 Index = LoadUint32(Token);
                Primitive.Indices = (gltf_accessor*)GetFromList(AccessorList, Index);
            }
            else if (TokenEquals(Token, "mode"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                Primitive.MeshMode = LoadUint32(Token);
            }
            else if (TokenEquals(Token, "material"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                uint32 Index = LoadUint32(Token);
                Primitive.Material = (gltf_material*)GetFromList(MaterialList, Index);
            }
            else
            {
                //Assert(0);
            }
        }
        Token = GetToken(Tokenizer);
    }
    return Primitive;
}

gltf_mesh ParseMesh(tokenizer* Tokenizer, 
                    dumb_list* AccessorList, dumb_list* MaterialList, 
                    temporary_memory* Temp)
{
    gltf_mesh Mesh = {};
    Mesh.Primitives = CreateList(8, Temp->Arena);;
    token Token = GetToken(Tokenizer);
    while (Token.Type != Token_CloseBrace)
    {
        if (Token.Type == Token_String)
        {
            if (TokenEquals(Token, "primitives"))
            {
                AssertToken(Tokenizer, Token_Colon);
                AssertToken(Tokenizer, Token_OpenBracket);
                
                while (Token.Type != Token_CloseBracket)
                {
                    Token = GetToken(Tokenizer);    
                    if (Token.Type == Token_OpenBrace)
                    {
                        gltf_primitive* Primitive = PushStruct(Temp->Arena, gltf_primitive);
                        *Primitive = ParsePrimitive(Tokenizer, AccessorList, MaterialList);
                        AddToList(&Mesh.Primitives, Primitive);
                    }
                }
            }
            else if (TokenEquals(Token, "name"))
            {
                //Assert(0);
            } 
        }
        Token = GetToken(Tokenizer);
    }
    return Mesh;
}

gltf_accessor ParseAccessor(tokenizer* Tokenizer, dumb_list* BufferViews)
{
    gltf_accessor Accessor = {};
    token Token = GetToken(Tokenizer);
    while (Token.Type != Token_CloseBrace)
    {
        if (Token.Type == Token_String)
        {
            if (TokenEquals(Token, "bufferView"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                uint32 BufferIndex = LoadUint32(Token);
                Accessor.BufferView = (gltf_buffer_view*)GetFromList(BufferViews, BufferIndex);
            }
            else if (TokenEquals(Token, "byteOffset"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                Accessor.ByteOffset = LoadUint32(Token);
            }
            else if (TokenEquals(Token, "componentType"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                Accessor.ComponentType = LoadUint32(Token);
            }
            else if (TokenEquals(Token, "count"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                Accessor.Count = LoadUint32(Token);
            }
            // defines the min and max of the thing being accessed
            else if (TokenEquals(Token, "max"))
            {
                AssertToken(Tokenizer, Token_Colon);
                AssertToken(Tokenizer, Token_OpenBracket);
                while (Token.Type != Token_CloseBracket)
                {
                    Token = GetToken(Tokenizer);
                }
            }
            else if (TokenEquals(Token, "min"))
            {
                AssertToken(Tokenizer, Token_Colon);
                AssertToken(Tokenizer, Token_OpenBracket);
                while (Token.Type != Token_CloseBracket)
                {
                    Token = GetToken(Tokenizer);
                }
            }
            else if (TokenEquals(Token, "type"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                if (TokenEquals(Token, "SCALAR"))
                {
                    Accessor.Type = AccessorType_scalar;
                } 
                else if (TokenEquals(Token, "VEC3"))
                {
                    Accessor.Type = AccessorType_v3;
                }
            }
            else
            {
                //Assert(0);
            }
        }
        
        Token = GetToken(Tokenizer);
    }
    return Accessor;
}

gltf_material ParseMaterial(tokenizer* Tokenizer)
{
    gltf_material Material = {};
    token Token = GetToken(Tokenizer);
    while (Token.Type != Token_CloseBrace)
    {
        if (Token.Type == Token_String)
        {
            if (TokenEquals(Token, "pbrMetallicRoughness"))
            {
                while (Token.Type != Token_CloseBrace)
                {
                    if (Token.Type == Token_String)
                    {
                        if (TokenEquals(Token, "baseColorFactor"))
                        {
                            AssertToken(Tokenizer, Token_Colon);
                            AssertToken(Tokenizer, Token_OpenBracket);
                            
                            v4 Color = {};
                            
                            Color.x = atof(GetToken(Tokenizer).Text);
                            AssertToken(Tokenizer, Token_Comma);
                            
                            Color.y = atof(GetToken(Tokenizer).Text);
                            AssertToken(Tokenizer, Token_Comma);
                            
                            Color.z = atof(GetToken(Tokenizer).Text);
                            AssertToken(Tokenizer, Token_Comma);
                            
                            Color.w = atof(GetToken(Tokenizer).Text);
                            
                            AssertToken(Tokenizer, Token_CloseBracket);
                            Material.PBR.Color = Color;
                        }
                        else if (TokenEquals(Token, "metallicFactor"))
                        {
                            
                        }
                        else
                        {
                            //Assert(0);
                        }
                    }
                        
                    Token = GetToken(Tokenizer);
                }
            }
            else if (TokenEquals(Token, "name"))
            {
                //Assert(0);
            }
            else
            {
                //Assert(0);
            }
        }
        
        Token = GetToken(Tokenizer);
    }
    
    return Material;
}

gltf_buffer_view ParseBufferView(tokenizer* Tokenizer, dumb_list* Buffers)
{
    gltf_buffer_view View = {};
    token Token = GetToken(Tokenizer);
    while (Token.Type != Token_CloseBrace)
    {
        if (Token.Type == Token_String)
        {
            if (TokenEquals(Token, "buffer"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                uint32 BufferIndex = LoadUint32(Token);
                View.Buffer = (gltf_buffer*)GetFromList(Buffers, BufferIndex);
            }
            else if (TokenEquals(Token, "byteOffset"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                View.ByteOffset = LoadUint32(Token);
            }
            else if (TokenEquals(Token, "byteLength"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                View.ByteLength = LoadUint32(Token);
            }
            else if (TokenEquals(Token, "byteStride"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                View.ByteStride = LoadUint32(Token);
            }
            else if (TokenEquals(Token, "target"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                View.Target = LoadUint32(Token);
            }
        }
        
        Token = GetToken(Tokenizer);
    }
    
    return View;
}

gltf_buffer ParseBuffer(tokenizer* Tokenizer, void* Bin)
{
    gltf_buffer Buffer = {};
    token Token = GetToken(Tokenizer);
    
    while (Token.Type != Token_CloseBrace)
    {
        if (Token.Type == Token_String)
        {
            if (TokenEquals(Token, "byteLength"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                Buffer.ByteLength = LoadUint32(Token);
            }
            else if (TokenEquals(Token, "uri"))
            {
                AssertToken(Tokenizer, Token_Colon);
                Token = GetToken(Tokenizer);
                Assert(Token.Type == Token_String);
                //TODO(james): some kinda map for bin files
                //char* UriString = (char*)malloc(Token.TextLength);
                //CopyString(UriString, Token.Text, Token.TextLength);
                //uint32 ID = UriToID(UriString);
                Buffer.Bin = Bin;
            }
        }
        
        Token = GetToken(Tokenizer);
    }
    return Buffer;
}

void SkipToEndOfBracketScope(tokenizer* Tokenizer)
{
    uint32 Depth = 0;
    
    bool Parsing = true;
    while (Parsing)
    {
        token Token = GetToken(Tokenizer);
        switch (Token.Type)
        {
            case Token_OpenBracket:
            {
                Depth++;
            }break;
            case Token_CloseBracket:
            {
                Depth--;
                if (Depth == 0)
                {
                    Parsing = false;
                }
            }
        }
    }
}

void LoadGltf(char* File, char* BinFile, char* AssetName, mesh* MeshAsset, memory_arena* AssetArena, 
              temporary_memory* Temp, platform_api* PlatformAPI)
{
    char* GltfFile = LoadTextFile(File, Temp, PlatformAPI);
    
    uint64 FileSize = PlatformAPI->GetFileSize(BinFile);
    void* Bin = PushSize(AssetArena, FileSize);
    PlatformAPI->ReadBinaryFile(BinFile, Bin);
    
    tokenizer _Tokenizer = {};
    tokenizer* Tokenizer = &_Tokenizer;
    Tokenizer->At = GltfFile;
    Tokenizer->EnableMinusToken = false;
    
    // NOTE(james): gltf is defined from most general to most specific, so we need to actually
    // parse the data in reverse order if we don't want to have to do some kinda pointer clean
    // up stage post parse
    token SceneStart = {};
    dumb_list SceneList = CreateList(8, Temp->Arena);
    
    token NodeStart = {};
    dumb_list NodeList = CreateList(8, Temp->Arena);
    
    token MeshStart = {};
    dumb_list MeshList = CreateList(8, Temp->Arena);
    
    token AccessorStart = {};
    dumb_list AccessorList = CreateList(8, Temp->Arena);
    
    token BufferViewStart = {};
    dumb_list BufferViewList = CreateList(8, Temp->Arena);
    
    token BufferStart = {};
    dumb_list BufferList = CreateList(8, Temp->Arena);
    
    dumb_list MaterialList = CreateList(8, Temp->Arena);
    
    bool32 Parsing = true;
    while (Parsing)
    {
        token Token = GetToken(Tokenizer);
        switch (Token.Type)
        {
            case Token_EndOfStream:
            {
                Parsing = false;
            }break;
            case Token_String:
            {
                if (TokenEquals(Token, "asset"))
                {
					ParseAsset(Tokenizer);
                }
                // this identifies the scene to be shown at load time, but can be left undefined too
                else if (TokenEquals(Token, "scene"))
                {
                    AssertToken(Tokenizer, Token_Colon);
                    Token = GetToken(Tokenizer);
                    Assert(TokenEquals(Token,"0"));
                }
                else if (TokenEquals(Token, "scenes"))
                {
                    SceneStart = Token;
                    SkipToEndOfBracketScope(Tokenizer);
                }
                else if (TokenEquals(Token, "nodes"))
                {
                    NodeStart = Token;
                    SkipToEndOfBracketScope(Tokenizer);
                }
                else if (TokenEquals(Token, "meshes"))
                {
                    MeshStart = Token;
                    SkipToEndOfBracketScope(Tokenizer);
                }
                else if (TokenEquals(Token, "accessors"))
                {
                    AccessorStart = Token;
                    SkipToEndOfBracketScope(Tokenizer);
                }
                else if (TokenEquals(Token, "materials"))
                {
                    while (Token.Type != Token_CloseBracket)
                    {
                        Token = GetToken(Tokenizer);
                        if (Token.Type == Token_OpenBrace)
                        {
                            gltf_material* Material = PushStruct(Temp->Arena, gltf_material);
                            *Material = ParseMaterial(Tokenizer);
                            AddToList(&MaterialList, Material);
                        }
                    }
                }
                else if (TokenEquals(Token, "bufferViews"))
                {
                    BufferViewStart = Token;
                    SkipToEndOfBracketScope(Tokenizer);
                }
                else if (TokenEquals(Token, "buffers"))
                {
                    BufferStart = Token;
                    SkipToEndOfBracketScope(Tokenizer);
                }
            }
        };
    }
    
    token Token = BufferStart;
    Tokenizer->At = Token.Text-1;
    while (Token.Type != Token_CloseBracket)
    {
        Token = GetToken(Tokenizer);
        if (Token.Type == Token_OpenBrace)
        {
            gltf_buffer* Buffer = PushStruct(Temp->Arena, gltf_buffer);
            *Buffer = ParseBuffer(Tokenizer, Bin);
            AddToList(&BufferList, Buffer);
        }
    }
    
    Token = BufferViewStart;
    Tokenizer->At = Token.Text-1;
    while (Token.Type != Token_CloseBracket)
    {
        Token = GetToken(Tokenizer);
        if (Token.Type == Token_OpenBrace)
        {
            gltf_buffer_view* BufferView = PushStruct(Temp->Arena, gltf_buffer_view);
            *BufferView = ParseBufferView(Tokenizer, &BufferList);
            AddToList(&BufferViewList, BufferView);
        }
    }
    
    Token = AccessorStart;
    Tokenizer->At = Token.Text-1;
    while (Token.Type != Token_CloseBracket)
    {
        Token = GetToken(Tokenizer);
        if (Token.Type == Token_OpenBrace)
        {
            gltf_accessor* Accessor = PushStruct(Temp->Arena, gltf_accessor);
            *Accessor = ParseAccessor(Tokenizer, &BufferViewList);
            AddToList(&AccessorList, Accessor);
        }
    }
    
    Token = MeshStart;
    Tokenizer->At = Token.Text-1;
    while (Token.Type != Token_CloseBracket)
    {
        Token = GetToken(Tokenizer);
        if (Token.Type == Token_OpenBrace)
        {
            gltf_mesh* Mesh = PushStruct(Temp->Arena, gltf_mesh);
            *Mesh = ParseMesh(Tokenizer, &AccessorList, &MaterialList, Temp);
            AddToList(&MeshList, Mesh);
        }
    }
    
    Token = NodeStart;
    Tokenizer->At = Token.Text-1;
    while (Token.Type != Token_CloseBracket)
    {
        Token = GetToken(Tokenizer);
        if (Token.Type == Token_OpenBrace)
        {
            gltf_node* Node = PushStruct(Temp->Arena, gltf_node);
            *Node = ParseNode(Tokenizer, &MeshList);
            AddToList(&NodeList, Node);
        }
    }
    
    // Nodes may have children defined that are not declared until farther down the node array,
    // so we store the child indices declared, then do the ptr matchup in this section
    for (uint32 NodeIndex = 0; 
         NodeIndex < NodeList.Count; 
         NodeIndex++)
    {
        gltf_node* Node = (gltf_node*)GetFromList(&NodeList, NodeIndex);
        if (Node->ChildrenIndicesCount > 0)
        {
            Node->ChildCount = Node->ChildrenIndicesCount;
            Node->Children = CreateList(Node->ChildCount, Temp->Arena);
            for (uint32 ChildIndex = 0; 
                 ChildIndex < Node->ChildCount; 
                 ChildIndex++)
            {
                uint32 NodeIndexOfChild = Node->ChildrenIndices[ChildIndex];
                gltf_node* ChildNode = (gltf_node*)GetFromList(&NodeList, NodeIndexOfChild);
                AddToList(&Node->Children, ChildNode);
            }
        }
    }
    
    gltf_scene Scene = {};
    Token = SceneStart;
    Tokenizer->At = Token.Text-1;
    while (Token.Type != Token_CloseBracket)
    {
        Token = GetToken(Tokenizer);
        if (Token.Type == Token_OpenBrace)
        {
            Scene = ParseScene(Tokenizer, &NodeList);
        }
    }
    
    
    //
    //
    //
    InitializeTransform(MeshAsset);
    SetRootName(MeshAsset, AssetName);
    for (uint32 NodeIndex = 0; 
         NodeIndex < NodeList.Count; 
         NodeIndex++)
    {
        gltf_node* Node = (gltf_node*)GetFromList(&NodeList, NodeIndex);
        if (!Node->Added)
        {
            mesh* Mesh = GltfToYamgine(Node, AssetArena, PlatformAPI);
            SetTransformParent(Mesh, MeshAsset);
        }
        
    }
}
