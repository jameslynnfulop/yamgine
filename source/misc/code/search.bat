@echo off
REM https://technet.microsoft.com/en-us/library/cc732459.aspx

REM searches all files in directory for input. Case insensitive. Will also match subwords. Ex. "if" in "#endif"

call findstr /s /i /n "\<*.%1.*\>" *.*