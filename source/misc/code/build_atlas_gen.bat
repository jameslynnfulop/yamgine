@echo off

REM msdfgen is x86, so that's why we don't want this tool to be part of the regular build script

WHERE cl >nul 2>nul 
IF %ERRORLEVEL% NEQ 0 call "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat"

pushd ..\..\build

REM ..\source\msdfgen\msdfgen-1.4-lib\core\msdfgen.cpp

cl -Od -MTd -nologo -fp:fast -fp:except- -Gm- -EHa- -Zo -Oi -FC -Z7 -EHsc -cgthreads4 -DFMOD_SUPPORT=0 /I"..\source\imgui" /I"..\source\msdfgen\msdfgen-1.4-lib" /I"..\source\msdfgen\msdfgen-1.4-lib\include" ..\source\code\msdf_font_atlas_gen.cpp /link %CommonLinkerFlags% /LIBPATH:"..\source\msdfgen\msdfgen-1.4-lib\lib" freetype.lib 

REM msdf_font_atlas_gen.exe
REM output.bmp

popd