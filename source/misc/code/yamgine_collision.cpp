#include "yamgine_collision_detection.cpp"

inline bool32
ClipCylinder(collision_ray Ray, plane Top, plane Bottom, real32* InDistancePtr, real32* OutDistancePtr)
{
    bool32 Result = false;

    real32 InDistance = *InDistancePtr;
    real32 OutDistance = *OutDistancePtr;

    //
    // Intersect with bottom end-cap
    //
    real32 DC;
    real32 DW;
    DC = (Bottom.A * Ray.Direction.x) + (Bottom.B * Ray.Direction.y) + (Bottom.C * Ray.Direction.z);
    DW = (Bottom.A * Ray.Position.x) + (Bottom.B * Ray.Position.y) + (Bottom.C * Ray.Position.z) + Bottom.D;

    if (DC == 0.0f) // make sure we aren't parallel
    {
        if (DW >= 0.0f)
            return false;
    }
    else
    {
        real32 T = -DW / DC;
        if (DC >= 0.0f) // is far plane
        {
            if (T > InDistance && T < OutDistance)
            {
                OutDistance = T; // hit bottom!
            }
            if (T < InDistance)
                return false; // early out
        }
        else // is near plane
        {
            if (T > InDistance && T < OutDistance)
            {
                InDistance = T; // hit bottom!
            }
            if (T > OutDistance)
                return false; // early out
        }
    }

    //
    // Intersect with top end-cap
    //

    DC = (Top.A * Ray.Direction.x) + (Top.B * Ray.Direction.y) + (Top.C * Ray.Direction.z);
    DW = (Top.A * Ray.Position.x) + (Top.B * Ray.Position.y) + (Top.C * Ray.Position.z) + Top.D;

    if (DC == 0.0f) // make sure we aren't parallel
    {
        if (DW >= 0.0f)
            return false;
    }
    else
    {
        real32 T = -DW / DC;
        if (DC >= 0.0f) // is far plane
        {
            if (T > InDistance && T < OutDistance)
            {
                OutDistance = T; // hit top!
            }
            if (T < InDistance)
                return false; // early out
        }
        else // is near plane
        {
            if (T > InDistance && T < OutDistance)
            {
                InDistance = T; // hit top!
            }
            if (T > OutDistance)
                return false; // early out
        }
    }

    //
    //Postamble
    //
    *InDistancePtr = InDistance;
    *OutDistancePtr = OutDistance;

    if (InDistance < OutDistance)
    {
        Result = true;
    }

    return Result;
}

////////////////////////////////////////
// AABB
////////////////////////////////////////

inline aabb
AABBHalfWidths(v3 Position, v3 HalfWidths)
{
    aabb Result{};
    Result.Position = Position;
    Result.HalfWidths = HalfWidths;

    //Assert(Result.HalfWidths.x > 0);
    //Assert(Result.HalfWidths.y > 0);
    //Assert(Result.HalfWidths.z > 0);

    return Result;
}

inline aabb
AABBFullWidths(v3 Position, v3 Widths)
{
    aabb Result{};
    Result = AABBHalfWidths(Position, Widths * 0.5f);

    return Result;
}

//https://tavianator.com/fast-branchless-raybounding-box-intersections/
//TODO(james): get rid of divides
inline bool32
AABBRayIntersects(aabb AABB, collision_ray Ray)
{
    bool32 Result = false;
    real32 RayAABBDistance = Length(AABB.Position - Ray.Position);

    if (AABB.HalfWidths == v3_ZERO)
    {
        return Result;
    }
    if (RayAABBDistance < Ray.MaxDistance)
    {
        float tMin = -FLT_MAX;
        float tMax = FLT_MAX;

        if (Ray.Direction.x != 0)
        {
            real32 tx1 = (AABB.Position.x - AABB.HalfWidths.x - Ray.Position.x) / Ray.Direction.x;
            real32 tx2 = (AABB.Position.x + AABB.HalfWidths.x - Ray.Position.x) / Ray.Direction.x;

            tMin = Max(tMin, Min(tx1, tx2));
            tMax = Min(tMax, Max(tx1, tx2));
        }

        if (Ray.Direction.y != 0)
        {
            real32 ty1 = (AABB.Position.y - AABB.HalfWidths.y - Ray.Position.y) / Ray.Direction.y;
            real32 ty2 = (AABB.Position.y + AABB.HalfWidths.y - Ray.Position.y) / Ray.Direction.y;

            tMin = Max(tMin, Min(ty1, ty2));
            tMax = Min(tMax, Max(ty1, ty2));
        }

        if (Ray.Direction.z != 0)
        {
            real32 tz1 = (AABB.Position.z - AABB.HalfWidths.z - Ray.Position.z) / Ray.Direction.z;
            real32 tz2 = (AABB.Position.z + AABB.HalfWidths.z - Ray.Position.z) / Ray.Direction.z;

            tMin = Max(tMin, Min(tz1, tz2));
            tMax = Min(tMax, Max(tz1, tz2));
        }

        Result = tMax >= tMin;
    }

    return Result;
}

inline void
UpdateMeshAABBRaw(entity* Entity, bool32 Collision)
{
    mesh_extents WorldExtents;

    float I = FLT_MAX;

    //TODO(james): could probably just store the cardinal direction, not the whole vertex
    WorldExtents.Front = V3(0, 0, -I);
    WorldExtents.Top = V3(0, -I, 0);
    WorldExtents.Right = V3(-I, 0, 0);
    WorldExtents.Back = V3(0, 0, I);
    WorldExtents.Bottom = V3(0, I, 0);
    WorldExtents.Left = V3(I, 0, 0);

    //brute force find extrema
    //NOTE(james): use support function instead of you have a smart one

    mesh* Mesh;
    if (Collision)
    {
        Mesh = Entity->ColliderMesh;
    }
    else
    {
        Mesh = Entity->RenderMesh;
    }

    graphics_buffer* Positions = &Mesh->Vertices;
    for (uint32 VertexIndex = 0;
         VertexIndex < Mesh->Vertices.Count;
         VertexIndex++)
    {
        v3 Vertex = MatrixByPosition(&Entity->ColliderMeshInstanceSpace,
                                     GET_V3(Positions, VertexIndex));

        if (Vertex.z > WorldExtents.Front.z)
        {
            WorldExtents.Front = Vertex;
        }
        if (Vertex.y > WorldExtents.Top.y)
        {
            WorldExtents.Top = Vertex;
        }
        if (Vertex.x > WorldExtents.Right.x)
        {
            WorldExtents.Right = Vertex;
        }
        if (Vertex.z < WorldExtents.Back.z)
        {
            WorldExtents.Back = Vertex;
        }
        if (Vertex.y < WorldExtents.Bottom.y)
        {
            WorldExtents.Bottom = Vertex;
        }
        if (Vertex.x < WorldExtents.Left.x)
        {
            WorldExtents.Left = Vertex;
        }
    }

    v3 Widths = {};
    Widths.x = Max(WorldExtents.Right.x, WorldExtents.Left.x) - Min(WorldExtents.Right.x, WorldExtents.Left.x);
    Widths.y = Max(WorldExtents.Top.y, WorldExtents.Bottom.y) - Min(WorldExtents.Top.y, WorldExtents.Bottom.y);
    Widths.z = Max(WorldExtents.Front.z, WorldExtents.Back.z) - Min(WorldExtents.Front.z, WorldExtents.Back.z);

    v3 Position = {};
    Position.x = Lerp(Max(WorldExtents.Right.x, WorldExtents.Left.x),
                      Min(WorldExtents.Right.x, WorldExtents.Left.x),
                      0.5f);
    Position.y = Lerp(Max(WorldExtents.Top.y, WorldExtents.Bottom.y),
                      Min(WorldExtents.Top.y, WorldExtents.Bottom.y),
                      0.5f);
    Position.z = Lerp(Max(WorldExtents.Front.z, WorldExtents.Back.z),
                      Min(WorldExtents.Front.z, WorldExtents.Back.z),
                      0.5f);

    aabb Result = AABBFullWidths(Position, Widths);
    if (Collision)
    {
        Entity->ColliderAABB = Result;
    }
    else
    {
        Entity->RenderAABB = Result;
    }
}

inline void
UpdateCollisionMeshAABB(entity* Entity)
{
    Assert(Entity->ColliderType == ColliderType_Mesh);
    Assert(Entity->ColliderMesh != NULL);
    UpdateMeshAABBRaw(Entity, true);
}

inline void
UpdateRendererMeshAABB(entity* Entity)
{
    UpdateMeshAABBRaw(Entity, false);
}

inline void
UpdateSphereAABB(entity* Entity)
{
    Assert(Entity->ColliderType == ColliderType_Sphere);

    aabb Result;

    Result.Position = Entity->Position;
    Result.HalfWidths = V3(Entity->SphereColliderRadius, Entity->SphereColliderRadius, Entity->SphereColliderRadius);

    Entity->ColliderAABB = Result;
}

inline void
UpdateCapsuleAABB(entity* Entity)
{
    Assert(Entity->ColliderType == ColliderType_Capsule);

    aabb Result;

    Result.Position = Entity->Position;

    v3 TopNode;
    v3 BottomNode;
    GetCapsuleNodes(Entity, &TopNode, &BottomNode);
    v3 Diff = TopNode - BottomNode;

    Result.HalfWidths = V3(AbsoluteValue(Diff.x * 0.5f) + Entity->CapsuleColliderRadius,
                           AbsoluteValue(Diff.y * 0.5f) + Entity->CapsuleColliderRadius,
                           AbsoluteValue(Diff.z * 0.5f) + Entity->CapsuleColliderRadius);

    Entity->ColliderAABB = Result;
}

inline void
UpdateCylinderAABB(entity* Entity)
{
    Assert(Entity->ColliderType == ColliderType_Cylinder);

    aabb Result;

    Result.Position = Entity->Position;

    real32 HalfHeight = (Entity->CylinderColliderHeight / 2);

    v3 Top = GetWorldPositionFromLocalOffset(Entity, V3(0, HalfHeight, 0));
    v3 Bottom = GetWorldPositionFromLocalOffset(Entity, V3(0, -HalfHeight, 0));
    v3 Diff = Top - Bottom;

    v3 HalfWidths;
    //TODO(james): this is incorrect, box is a little too large. Didn't feel like figuring out how to do it correctly
    HalfWidths.x = AbsoluteValue(Diff.x * 0.5f) + Entity->CylinderColliderRadius;
    HalfWidths.y = AbsoluteValue(Diff.y * 0.5f) + Entity->CylinderColliderRadius;
    HalfWidths.z = AbsoluteValue(Diff.z * 0.5f) + Entity->CylinderColliderRadius;
    Result.HalfWidths = HalfWidths;

    Entity->ColliderAABB = Result;
}

#if 0
//http://www.gamasutra.com/view/feature/131424/pool_hall_lessons_fast_accurate_.php?page=2
inline bool32
SphereStaticSphereDynamicIntersects(entity* Static, entity* Dynamic, contact_info* Contact)
{
    bool32 Result = false;
    
    real32 DistanceBetween = Distance(Static->Position, Dynamic->Position);
    real32 SumRadii = Static->SphereColliderRadius + Dynamic->SphereColliderRadius;
    real32 DistanceBetweenMinusRadii = DistanceBetween - SumRadii;
    
    //early out, if there's no chance for two to come within intersection range
    if (Length(Dynamic->LinearVelocity) > DistanceBetweenMinusRadii)
    {
        v3 MoveDirection = Normalize(Dynamic->LinearVelocity);
        v3 DynamicToStaticDirection = Static->Position - Dynamic->Position;
        
        real32 D = Inner(MoveDirection, DynamicToStaticDirection);
        
        //make sure moving in direction of static sphere
        if (D > 0)
        {
            real32 F = (DistanceBetween * DistanceBetween) - (D * D);
            real32 SumRadiiSquared = SumRadii * SumRadii;
            
            //
            if (F < SumRadiiSquared)
            {
                real32 T = SumRadiiSquared - F;
                if (T > 0)
                {
                    real32 DistanceToCollision = D - SquareRoot(T);
                    
                    real32 VelocityMagnitude = Length(Dynamic->LinearVelocity);
                    
                    if (VelocityMagnitude > DistanceToCollision)
                    {
                        //Collided!!! 
                        DynamicToStaticDirection = Normalize(DynamicToStaticDirection);//TODO(james):rename this, i dont like reusing variables like this
                        real32 PenetrationDepth = VelocityMagnitude - DistanceToCollision;
                        v3 ContactPoint = Dynamic->Position + (DynamicToStaticDirection * Dynamic->SphereColliderRadius);
                        
                        Contact->Normal = DynamicToStaticDirection;
                        Contact->PenetrationDepth = PenetrationDepth;
                        //Contact->PointWorldMe = Contact
                        /*
                        Dynamic->ContactInfo.Normal = DynamicToStaticDirection;
                        Dynamic->ContactInfo.PenetrationDepth = PenetrationDepth;
                        Dynamic->ContactInfo.PointWorld = ContactPoint;

                        Static->ContactInfo.Normal = -DynamicToStaticDirection;
                        Static->ContactInfo.PenetrationDepth = PenetrationDepth;
                        Static->ContactInfo.PointWorld = ContactPoint;
                        
                        Dynamic->ContactInfo.Other = &Static->ContactInfo;
                        Static->ContactInfo.Other = &Dynamic->ContactInfo;
                        */
                        Result = true;
                    }
                }
            }
        }
    }
    
    return Result;
}
#endif

/*
inline bool32
SphereSphereRigidbody(entity* A, entity* B, contact_info* Info)
{
    bool32 Result = false;
    
    Assert(A->ColliderType == ColliderType_sphere);
    Assert(B->ColliderType == ColliderType_sphere);
    
    
}
*/

inline bool32
SphereRayIntersects(v3 Position, real32 Radius, collision_ray Ray, raycast_info* Info)
{
    bool32 Result = false;

    float t0, t1; // solutions for t if the ray intersects
    float t;

    // analytic solution
    v3 L = Ray.Position - Position;
    real32 A = Inner(Ray.Direction, Ray.Direction);
    real32 B = 2 * Inner(Ray.Direction, L);
    real32 C = Inner(L, L) - (Radius * Radius);
    if (SolveQuadratic(A, B, C, &t0, &t1))
    {
        if (t0 > 0 || t1 > 0)
        {
            // We want the least positive t

            if (t0 < 0)
            {
                t = t1;
            }
            else if (t1 < 0)
            {
                t = t0;
            }
            else if (t0 < t1)
            {
                t = t0;
            }
            else
            {
                t = t1;
            }

            Result = true;

            Info->Position = Ray.Position + (Ray.Direction * t);
            Info->Normal = Normalize(Ray.Position - Position);
        }
    }

    return Result;
}

inline bool32
SphereRayIntersects(entity* Sphere, collision_ray Ray, raycast_info* Info)
{
    bool32 Result = SphereRayIntersects(Sphere->Position, Sphere->SphereColliderRadius, Ray, Info);
    Info->Entity = Sphere;
    return Result;
}

//GPU Gems IV page 356, Intersecting a Ray with a Cylinder
inline bool32
CylinderRayIntersects(v3 Position, v3 Up, real32 Radius, real32 Height,
                      collision_ray Ray, raycast_info* Info)
{
    bool32 Result = false;

    Assert(ApproximatelyEqual(Length(Up), 1.0f));
    Assert(Radius > 0);
    Assert(Height > 0);

    v3 n = Cross(Ray.Direction, Up);
    v3 RayBaseToCylinderBase = Ray.Position - Position; // RC
    real32 LengthN = Length(n);
    if (LengthN == 0) // cylinder and ray are parallel
    {
        real32 Dist = Inner(RayBaseToCylinderBase, Up); //d
        v3 D = RayBaseToCylinderBase - (Dist * Up);
        Dist = Length(D);
        if (Dist <= Radius) // hit!
        {
            real32 IntersectionDistance = (Length(RayBaseToCylinderBase) - (Height / 2));
            Info->Position = Ray.Position + (Ray.Direction * IntersectionDistance);
            Info->Normal = -Ray.Direction;
            Result = true;
        }
    }
    else
    {
        n = Normalize(n);
        real32 Dist = AbsoluteValue(Inner(RayBaseToCylinderBase, n)); // d
        if (Dist <= Radius) // hit!
        {
            v3 O = Cross(RayBaseToCylinderBase, Up);
            real32 t = -Inner(O, n) / LengthN;
            O = Normalize(Cross(n, Up));

            real32 s = AbsoluteValue(SquareRoot(Radius * Radius - Dist * Dist) / Inner(Ray.Direction, O));
            real32 InDistance = t - s;
            real32 OutDistance = t + s;

            //final answer for an infinite cylinder
            v3 InfiniteCylinderSideIntersectionPosition = Ray.Position + (Ray.Direction * InDistance);

            //
            // Handle End Caps
            //

            v3 TopCapPosition = Position + (Up * (Height / 2));
            v3 BottomCapPosition = Position + (-Up * (Height / 2));

            plane Top = PlaneFromPositionAndNormal(TopCapPosition, Up);
            plane Bottom = PlaneFromPositionAndNormal(BottomCapPosition, -Up);
            if (ClipCylinder(Ray, Top, Bottom, &InDistance, &OutDistance))
            {
                Info->Position = Ray.Position + (Ray.Direction * InDistance);
                //TODO(james): Normal

                Result = true;
            }
        }
    }

    return Result;
}

inline bool32
CylinderRayIntersects(entity* Cylinder, collision_ray Ray, raycast_info* Info)
{
    /*
    CylinderRayIntersects(v3 Position, v3 Up, real32 Radius, real32 Height, 
    collision_ray Ray, raycast_info* Info)
    */
    Assert(Cylinder->ColliderType == ColliderType_Cylinder);
    bool32 Result = CylinderRayIntersects(Cylinder->Position, Cylinder->Up,
                                          Cylinder->CylinderColliderRadius, Cylinder->CylinderColliderHeight,
                                          Ray, Info);
    return Result;
}

inline bool32
CapsuleRayIntersects(entity* Capsule, collision_ray Ray, raycast_info* Info)
{
    Assert(Capsule->ColliderType == ColliderType_Capsule);

    bool32 Result = false;

    //TODO(james): intersect test against the end spheres and the middle cylinder, take closest point
    real32 HalfHeight = (Capsule->CapsuleColliderHeight / 2) - Capsule->CapsuleColliderRadius;
    v3 TopSpherePos = GetWorldPositionFromLocalOffset(Capsule, V3(0, HalfHeight, 0));
    v3 BottomSpherePos = GetWorldPositionFromLocalOffset(Capsule, V3(0, -HalfHeight, 0));

    raycast_info TopSphereInfo = {};
    TopSphereInfo.Position = v3_MAX;
    bool32 TopSphereHit = SphereRayIntersects(TopSpherePos, Capsule->CapsuleColliderRadius,
                                              Ray, &TopSphereInfo);

    raycast_info BottomSphereInfo = {};
    BottomSphereInfo.Position = v3_MAX;
    bool32 BottomSphereHit = SphereRayIntersects(BottomSpherePos, Capsule->CapsuleColliderRadius,
                                                 Ray, &TopSphereInfo);

    raycast_info CylinderInfo = {};
    CylinderInfo.Position = v3_MAX;
    bool32 CylinderHit = CylinderRayIntersects(Capsule->Position, Capsule->Up,
                                               Capsule->CapsuleColliderRadius,
                                               Capsule->CapsuleColliderHeight - (Capsule->CapsuleColliderRadius * 2),
                                               Ray, &CylinderInfo);

    if (TopSphereHit || BottomSphereHit || CylinderHit)
    {
        Result = true;
        real32 TopSphereDistance = Distance(TopSphereInfo.Position, Ray.Position);
        real32 BottomSphereDistance = Distance(BottomSphereInfo.Position, Ray.Position);
        real32 CylinderDistance = Distance(CylinderInfo.Position, Ray.Position);

        real32 ClosestPoint = Min3(TopSphereDistance, BottomSphereDistance, CylinderDistance);

        if (ClosestPoint == TopSphereDistance)
        {
            *Info = TopSphereInfo;
        }
        else if (ClosestPoint == BottomSphereDistance)
        {
            *Info = BottomSphereInfo;
        }
        else if (ClosestPoint == CylinderDistance)
        {
            *Info = CylinderInfo;
        }
    }

    return Result;
}

/*
NOTE(james): this is my sort of translated psuedo code from 
Ray Casting against General Convex with Application to Continuous Detection
by GINO VAN DEN BERGEN

s = ray position
lambda = ray direction
r = ray distance

x = hit position
n = hit normal

v = simplex point
P = simplex



Algorithm 1

HitDistance = 0;
HitPosition = RayPosition;
Normal = 0;
Vertex = �the point of mesh closest to HitPosition�;
while not �x is close enough to c� do
begin
    Normal = HitPosition - c;
    if Dot(Normal, RayDirection) >= 0 then return false
    else
    begin
        HitDistance = HitDistance - Square(Absolute(Normal))/Dot(Normal, RayDirection);
        HitPosition = HitPosition + RayDirection * HitDistance;
        Vertex = �the point of C closest to HitPosition�
    end
end;
return true



conv() = point of simplex closest to v3(0,0,0)?
v() = point closest to object origin?
v(conv()) is closest to the CSO origin, so v3(0,0,0)

Algorithm 3

HitDistance = 0;
HitPosition = RayPosition;
Normal = 0;
SearchDirection = RayPosition - RandomVertex�;
P[] = �NULL;
while LengthSq(SearchDirection) > (Epsilon*Epsilon) do
begin
    p = FindSupportDirection(Direction); {{ v is a normal of C at p }}
    w = RayPosition - p;
    if Dot(Direction, w) > 0 then
    begin
        if Dot(VertexPosition, RayDirection) = 0 then return false
        else
        begin
            HitDistance = HitDistance - Dot(Support, w) / Dot(Support, RayDirection)
            HitPosition = RayPosition + HitDistance * RayDirection;
            Normal = SearchDirection
        end
    end;
    Y[] = P UNION {p};
    Direction = v(conv({x} - Y ));    v()
    P[] = �smallest X ? Y such that v ? conv({x} - X)� //this seems to correlate to our buildSimplex routine
end;
return true 

*/

/*
TODO(james): The intersection is detecting okay, but the collision point and normal is all wrong.
There's something wrong in the search direction determation i think
*/
#if 0
internal bool32
MeshRayIntersectsGJK(render_list* RenderList, entity* MeshEntry, collision_ray* Ray, raycast_info* Info)
{
	bool32 Result = true;

    real32 HitDistance = 0;
    v3 HitPosition = Ray->Position;
    v3 HitNormal = {};
    v3 SearchDirection = Normalize(Ray->Position - MeshEntry->Mesh->Vertices[0]);

    simplex_point Simplex[4];
    simplex_point InitialPoint = {};
    InitialPoint.Dilation = Ray->Position - MeshEntry->Mesh->Vertices[0];
    Simplex[0] = InitialPoint;
    
    uint32 TestSimplexSize = stb_arr_len(Simplex);

    real32 Epsilon = 0.0001f;

    while (LengthSq(SearchDirection) > (Epsilon * Epsilon))
    {
        v3 NewVertex = ClosestPointToDirectionMesh(MeshEntry, SearchDirection);
        v3 NewVertexToRayStart = Ray->Position - NewVertex;
        real32 VDotW = Inner(SearchDirection, NewVertexToRayStart);
        real32 VDotR = Inner(SearchDirection, Ray->Direction);
        if (VDotW > 0)
        {
            if (VDotR >= 0)
            {
                //fail
                Result = false;
                break;
            }
            else
            {
                HitDistance = HitDistance - (VDotW / VDotR);//is this negative??
                HitPosition = Ray->Position + (Ray->Direction * HitDistance);
                HitNormal = SearchDirection;
            }
		}
        uint32 TestSimplexSize2 = stb_arr_len(Simplex);
         

        simplex_point Point = {};
        Point.Dilation = NewVertex;
        stb_arr_push(Simplex, Point);
        
        
        
        uint32 SimplexSize = stb_arr_len(Simplex);
        /*
        if (SimplexSize == 2)
        {
            if (Simplex[0].Dilation == Simplex[1].Dilation)
            {
				break;
			}
		}
        else if (SimplexSize == 3)
        {
            if (Simplex[0].Dilation == Simplex[2].Dilation || 
                Simplex[0].Dilation == Simplex[1].Dilation ||
                Simplex[1].Dilation == Simplex[2].Dilation)
            {
				break;
			}
		}
        else if (SimplexSize == 4)
        {
            if (Simplex[0].Dilation == Simplex[1].Dilation || 
                Simplex[0].Dilation == Simplex[2].Dilation ||
                Simplex[0].Dilation == Simplex[3].Dilation ||
                Simplex[1].Dilation == Simplex[2].Dilation || 
                Simplex[1].Dilation == Simplex[2].Dilation ||
                Simplex[1].Dilation == Simplex[3].Dilation ||
                Simplex[2].Dilation == Simplex[3].Dilation)
            {
                break;
            }
        }
        */
        v3 BogusSearchDirection = {};
        GJK_DoSimplex(RenderList, Simplex,  &SearchDirection);
		/*
        v3 SmallestSearchDirection = V3(FLT_MAX, FLT_MAX, FLT_MAX);
        for (int i = 0; i < stb_arr_len(Simplex); i++)
        {
            v3 NewSearchDirection = Ray->Position - Simplex[i].Dilation;
            if (Length(NewSearchDirection) < Length(SmallestSearchDirection))
            {
				SmallestSearchDirection = NewSearchDirection;
			}
        }
        SearchDirection = SmallestSearchDirection;*/
    }
    
    Info->Position = HitPosition;
    Info->Normal = HitNormal;
 
    return Result; 
}
#endif

inline v3
TrianglePosition(triangle Triangle)
{
    v3 Result = {};

    Result = ToCartesian(V3(1, 1, 1), Triangle); //(Triangle.A + Triangle.B + Triangle.C) * 0.33333f;

    return Result;
}
//NOTE(james): plane is one sided
//NOTE(james): could fail if they are parallel, point is too far away, or plane/ray angle is greater than 90
inline bool32
ProjectRayOnPlane(collision_ray Ray, v3 PlanePosition, v3 PlaneNormal, v3* IntersectionPoint, bool32 OneSided)
{
    Assert(Ray.MaxDistance > 0);
    Assert(PlaneNormal != v3_ZERO);

    bool32 Result = false;

    float RayPlaneAngle = Inner(PlaneNormal, Ray.Direction);
    if ((OneSided && RayPlaneAngle < -0.00001f) || AbsoluteValue(RayPlaneAngle) > 0.0001f) //nearly parallel
    {
        v3 RayToPlaneDirection = PlanePosition - Ray.Position;
        float PointDistance = Inner(RayToPlaneDirection, PlaneNormal) / RayPlaneAngle;
        if (PointDistance >= 0 && PointDistance < Ray.MaxDistance)
        {
            *IntersectionPoint = Ray.Position + (Ray.Direction * PointDistance);
            Result = true;
        }
    }

    return Result;
}

//https://www.cs.princeton.edu/courses/archive/fall00/cs426/lectures/raycast/sld020.htm
/*NOTE(james):
   A. first see where the ray lays on a plane defined by the triangle
   B. Make sure point lies within triangle bounds
*/
internal bool32
TriangleRayIntersect(triangle Tri, collision_ray Ray, raycast_info* Info)
{
    bool32 Result = false;

    v3 IntersectionPoint = {};
#if 0
    if (ProjectRayOnPlane(Ray, TrianglePosition(Triangle), TriangleNormal(Triangle), /*out*/&IntersectionPoint))
    {
        v3 BarycentricCoords = ToBarycentric(IntersectionPoint, Triangle);
        if (Is01(BarycentricCoords.x) &&
            Is01(BarycentricCoords.y) &&
            Is01(BarycentricCoords.z))
        {
			Result = true;
            Info->Position = IntersectionPoint;
            Info->Normal = Reflect(Ray.Direction, TriangleNormal(Triangle));
		}
    }
#endif

    v3 v0v1 = Tri.B - Tri.A;
    v3 v0v2 = Tri.C - Tri.A;
    v3 pvec = Cross(Ray.Direction, v0v2);
    float det = Inner(v0v1, pvec);

    // if the determinant is negative the triangle is backfacing
    // if the determinant is close to 0, the ray misses the triangle
    if (AbsoluteValue(det) > 0.000001f)
    {
        float invDet = 1 / det;

        v3 tvec = Ray.Position - Tri.A;
        v3 qvec = Cross(tvec, v0v1);
        float u = Inner(tvec, pvec) * invDet;
        float v = Inner(Ray.Direction, qvec) * invDet;

        if (u > 0 && u < 1 && v > 0 && u + v < 1)
        {
            float t = Inner(v0v2, qvec) * invDet;
            if (t > 0)
            {
                v3 HitPosition = Ray.Position + (Ray.Direction * t);
                Info->Position = HitPosition;
                Result = true;
            }
        }
    }

    return Result;
}

/*
TODO(james): This currently cannot do backface culling, I think it needs to all happen in 
local space, so we need World To Local, which would be inverse matrix of LocalToWorld 
*/
internal bool32
MeshRayIntersectsBrute(mesh* Mesh, entity* Entity, collision_ray Ray, /*out*/ raycast_info* Info)
{
    bool32 Result = false;

    raycast_info LatestInfo = {};

    matrix4 WorldToLocal = InverseMatrix(Entity->ColliderMeshInstanceSpace);
    v3 WorldToLocalRayPosition = MatrixByPosition(&WorldToLocal, Ray.Position);
    v3 WorldToLocalRayDirection = MatrixByDirection(&WorldToLocal, Ray.Direction);

    collision_ray LocalRay;
    LocalRay.Position = WorldToLocalRayPosition;
    LocalRay.Direction = WorldToLocalRayDirection;
    LocalRay.Direction = Normalize(LocalRay.Direction);
    LocalRay.MaxDistance = Ray.MaxDistance;

    real32 ClosestDistance = FLT_MAX;
    graphics_buffer* Vertices = &Mesh->Vertices;
    for (uint32 TriangleIndex = 0;
         TriangleIndex < Mesh->Vertices.Count;
         TriangleIndex)
    {
//TODO(james): can't we use the calculated world-space vertices here?
        triangle Tri = {};
        Tri.A = GET_V3(Vertices, TriangleIndex++);
        Tri.B = GET_V3(Vertices, TriangleIndex++);
        Tri.C = GET_V3(Vertices, TriangleIndex++);
        //Tri.A = MatrixByPosition(&MeshEntry.MeshInstanceSpace, MeshEntry.Mesh->Vertices[TriangleIndex++]);
        //Tri.B = MatrixByPosition(&MeshEntry.MeshInstanceSpace, MeshEntry.Mesh->Vertices[TriangleIndex++]);
        //Tri.C = MatrixByPosition(&MeshEntry.MeshInstanceSpace, MeshEntry.Mesh->Vertices[TriangleIndex++]);
        //DrawCube(RenderList, Tri.A, V3(0.1f,0.1f,0.1f), v4_GREEN);
        //DrawCube(RenderList, Tri.B, V3(0.1f,0.1f,0.1f), v4_GREEN);
        //DrawCube(RenderList, Tri.C, V3(0.1f,0.1f,0.1f), v4_GREEN);
        if (TriangleRayIntersect(Tri, LocalRay, &LatestInfo))
        {
            Result = true;
            //NOTE(james): not sure if this is necessary
            float CollisionDistance = Length(LatestInfo.Position - LocalRay.Position);
            if (CollisionDistance < ClosestDistance)
            {
                *Info = LatestInfo;
                Info->Position = MatrixByPosition(&Entity->ColliderMeshInstanceSpace, Info->Position);
                Info->Normal = MatrixByDirection(&Entity->ColliderMeshInstanceSpace, Info->Normal);

                ClosestDistance = CollisionDistance;
            }
        }
    }

    if (Result)
    {
        Info->Entity = Entity;
    }

    return Result;
}

internal bool32
MeshRayIntersects(mesh* Mesh, entity* MeshEntity, collision_ray Ray, raycast_info* Info)
{
    bool32 Result;

    Result = MeshRayIntersectsBrute(Mesh, MeshEntity, Ray, Info);

    return Result;
}

#if 0
//returns distance to nearest point on face
inline real32 
ClosestDistanceOnMeshToPoint(entity* MeshEntity, v3 Point, 
                         triangle* IntersectedFace)
{
    real32 Result = FLT_MAX;
    
    //v3 ClosestNormal = v3_ZERO;
    //v3 PointOnClosestFace = v3_ZERO;
    
    triangle ClosestTriangle;
    
    for (uint32 FaceIndex = 0;
         FaceIndex < MeshEntity->ColliderMesh->VertexCount;
        FaceIndex)
    {    
        v3 A = MatrixByPosition(&MeshEntity->LocalToWorld, MeshEntity->ColliderMesh->Vertices[FaceIndex++]);
        v3 B = MatrixByPosition(&MeshEntity->LocalToWorld, MeshEntity->ColliderMesh->Vertices[FaceIndex++]);
        v3 C = MatrixByPosition(&MeshEntity->LocalToWorld, MeshEntity->ColliderMesh->Vertices[FaceIndex++]);
        triangle Tri = Triangle(A,B,C);

        real32 FaceDistance = PointDistanceFromFace(Point, Tri);
        if (FaceDistance < Result)
        {
            //v3 NewNormal = TriangleNormal(Tri);
            Result = FaceDistance;
            //ClosestNormal = NewNormal;
            PointOnClosestFace = A;
            ClosestTriangle = Tri;
        }
    }

    //*NormalResult = ClosestNormal;
    //*MeshCollisionPoint = PointOnPlaneClosestToPoint(A, ClosestNormal, Point);
    *IntersectedFace = ClosestTriangle;

    //Assert(*NormalResult != v3_ZERO);

    return Result;
}

internal bool32
SphereMeshIntersects(entity* Sphere, entity* Mesh)
{
    Assert(Sphere->ColliderType == ColliderType_sphere);
    Assert(Mesh->ColliderType == ColliderType_mesh);
    bool32 Result = false;
    v3 NormalResult;//normal of mesh face
    matrix4 WorldToLocalMesh = InverseMatrix(Mesh->LocalToWorld);
    triangle ClosestFace;
    real32 Closest = ClosestDistanceOnMeshToPoint(Mesh, Sphere->LocalPosition 
                                                  , &ClosestFace);
    if (Closest < Sphere->SphereColliderRadius)
    {
        v3 Normal = TriangleNormal(ClosestFace);
        
        
        Sphere->ContactInfo.PenetrationDepth = Sphere->SphereColliderRadius - Closest;
        Sphere->ContactInfo.Normal = -NormalResult;
        Sphere->ContactInfo.Position = Sphere->LocalPosition + (-NormalResult * Sphere->SphereColliderRadius);
        Result = true;
    }
    
    return Result;
}
#endif

inline void
AddCollisionRule(collision_group* Group, collision_layer A, collision_layer B, bool32 Collides)
{
    // NOTE(james): I fear indexing collisions with this system.
    // NOTE(james): Should we support 1 way collisions at this level?
    Assert(!Group->CollisionRules[A * CollisionLayer_Count + B]);
    Assert(!Group->CollisionRules[B * CollisionLayer_Count + A]);

    Group->CollisionRules[A * CollisionLayer_Count + B] = Collides;
    Group->CollisionRules[B * CollisionLayer_Count + A] = Collides;
}

internal void
DrawAllAABBs(collision_group* Group, render_list* RenderList, v4 Color)
{
    linked_list_member* Member = Group->Entries.FirstMember;
    while (Member != NULL)
    {
        YamProf(Broadphase);
        entity* Entity = (entity*)Member->Payload;
        DrawAABB(RenderList, &Entity->ColliderAABB, Color);
        Member = Member->NextNode;
    }
}

internal void
DrawAllRendererAABBs(linked_list* EntityMaster, render_list* RenderList, v4 Color)
{
    linked_list_member* Member = EntityMaster->FirstMember;
    do
    {
        entity* Entity = (entity*)Member->Payload;
        YamProf(Broadphase);
        if (LengthSq(Entity->RenderAABB.HalfWidths) > 0)
        {
            DrawAABB(RenderList, &Entity->RenderAABB, Color);
        }

        Member = Member->NextNode;
    } while (Member != NULL);
}

internal void
UpdateCollisionAABBs(collision_group* Group, bool32 UpdateStatics)
{
    YamProf(UpdateCollisionAABBs);
    CheckForLoops(Group->Entries.FirstMember);
    uint32 Count = 0;
    linked_list_member* Member = Group->Entries.FirstMember;
    while (Member != NULL)
    {
        ++Count;
        Assert(Count < 1000); //sometimes we loop into infinity? This assert counter should be more than the entity count
        entity* Entity = (entity*)Member->Payload;
        Assert(LengthSq(Entity->LocalScale) > 0);

        Entity->CollidedThisFrame = false;

        if (Entity->Enabled)
        {
            if (Entity->ColliderType == ColliderType_Sphere)
            {
                UpdateSphereAABB(Entity);
            }
            else if (Entity->ColliderType == ColliderType_Mesh)
            {
                bool32 Update = (!UpdateStatics) ? !Entity->IsStatic : true;
                if (Entity->ColliderMesh != NULL && Update)
                {
                    Entity->ColliderMeshInstanceSpace = Entity->LocalToWorld * Entity->ColliderMesh->LocalToWorld;
                    UpdateCollisionMeshAABB(Entity);
#if 1 //update worldspace vertices at start
                    if (Entity->WorldspaceVertices == NULL)
                    {
                        PushAndUpdateWorldspaceVertices(Entity, &Group->PermanentColliderVertexArena);
                    }
                    else
                    {
                        UpdateWorldspaceVertices(Entity);
                    }
#endif
                    //TEMP
                    Entity->ColliderAABB.HalfWidths += V3(0.05f, 0.05f, 0.05f);
                }
            }
            else if (Entity->ColliderType == ColliderType_Capsule)
            {
                UpdateCapsuleAABB(Entity);
            }
            else if (Entity->ColliderType == ColliderType_Cylinder)
            {
                UpdateCylinderAABB(Entity);
            }
            else
            {
                InvalidCodePath;
            }
        }
        
        
        Member = Member->NextNode;
    }
}

internal void
UpdateRendererAABBs(linked_list* EntityMaster, bool32 UpdateStatics)
{
    YamProf(UpdateRendererAABBs);
    linked_list_member* Member = EntityMaster->FirstMember;
    while (Member != NULL)
    {
        entity* Entity = (entity*)Member->Payload;
        Entity->CollidedThisFrame = false;

        if (Entity->RenderMesh != NULL)
        {
            if (Entity->IsStatic)
            {
                if (UpdateStatics)
                {
                    Entity->ColliderMeshInstanceSpace = Entity->LocalToWorld * Entity->RenderMesh->LocalToWorld;
                    UpdateRendererMeshAABB(Entity);
                }
            }
            else
            {
                Entity->ColliderMeshInstanceSpace = Entity->LocalToWorld * Entity->RenderMesh->LocalToWorld;
                UpdateRendererMeshAABB(Entity);
            }
        }
        Member = Member->NextNode;
    }
}

internal void
CollisionResponse(entity* Entity, uint32 ConfigIndex, contact_info* Contact)
{
    // apply gravity

    if (Entity->CollidedThisFrame && !Entity->IsTrigger && !Entity->IsStatic)
    {
        rb_config* Config = &Entity->Configs[ConfigIndex];

        //push out so they are abutting
        //MoveLocal(Entity, -Entity->Contact.Normal * Entity->Contact.PenetrationDepth);

        v3 AToContactPoint = Contact->PointWorldA - Config->Position;

        v3 VelocityR = Config->LinearVelocity;
        if (!Entity->LockRotation)
        {
            Assert(0); //NOTE(james): need to work on rotation response still
            VelocityR += Cross(Config->AngularVelocity, AToContactPoint);
        }
        //Assert(VelocityR.x == 0);
        Assert(Entity->CoefficientOfRestitution > 0); //TODO(james): 0 will make an rb freeze inside another rb, should be pushed out at least
        real32 Bounciness = Entity->CoefficientOfRestitution; //0 to 1
        real32 RelativeVelocity = Inner(VelocityR, -Contact->NormalA);
        if (RelativeVelocity <= 0) //NOTE(james):positive would mean moving INTO the opposing object
        {
            real32 ImpulseNumerator = -(1 + Bounciness) * Inner(VelocityR, -Contact->NormalA);

            real32 InverseMassSum = (1 / Entity->Mass); // + (1/Contact->B->Mass);
            v3 ImpulseDenominatorA = Config->InverseWorldInertiaTensor * TripleCrossProduct(AToContactPoint, -Contact->NormalA, AToContactPoint);
            //v3 DenomB = Contact->B->InverseInertiaTensor * TripleCrossProduct(BToContactPoint, Contact->Normal, BToContactPoint);

            real32 ImpulseDenominator = 1;
            if (!Entity->LockRotation)
            {
                ImpulseDenominator = InverseMassSum + Inner(ImpulseDenominatorA, -Contact->NormalA);
            }

            Assert(ApproximatelyEqual(Length(Contact->NormalA), 1.0f));
            v3 Impulse = (ImpulseNumerator / ImpulseDenominator) * -Contact->NormalA;
            //Assert(Impulse.y > 0);
            Config->LinearVelocity += (1 / Entity->Mass) * Impulse;

            if (!Entity->LockRotation)
            {
                Config->AngularMomentum += Cross(AToContactPoint, Impulse);

                Config->AngularVelocity = Config->InverseWorldInertiaTensor * Config->AngularMomentum;
            }
        }
        else
        {
            //Assert(0);//Loop again?
        }
    }
}

void ComputeForces(collision_group* Group, entity* Entity, uint32 Configuration)
{
    Entity->Configs[Configuration].Force.y = 0;
    Entity->Configs[Configuration].Torque = v3_ZERO;
    if (Entity->UseGravity)
    {
        Entity->Configs[Configuration].Force.y += Group->Gravity / (1 / Entity->Mass);
    }
}

void CheckForTeleportation(entity* Entity, uint32 ConfigIndex)
{
    if (Entity->IsTrigger)
    {
        return;
    }
    Assert(ConfigIndex == 0 || ConfigIndex == 1);
    rb_config* Config = &Entity->Configs[ConfigIndex];
    Assert(Config->LocalPosition != -v3_ONE); // NOTE(james): flag that its actually been simulated once

    real32 MoveDelta = Length(Entity->LastCollisionStepPosition - Config->LocalPosition);
    real32 MaxMoveDelta = Length(Config->LinearVelocity) * (FIXED_TIME_STEP_DELTA * 2);
    Assert(MoveDelta <= MaxMoveDelta + 0.5f); //TODO(james): shouldnt need 0.5f
}

void Integrate(entity* Entity, uint32 ConfigSource, uint32 ConfigTarget, real32 DeltaTime)
{
    rb_config* Source = &Entity->Configs[ConfigSource];
    rb_config* Target = &Entity->Configs[ConfigTarget];

    //
    // linear
    //

    Target->LinearVelocity = Source->LinearVelocity + (DeltaTime * (1 / Entity->Mass)) * Source->Force;
    Target->LinearVelocity.x *= 1 - (DeltaTime * Entity->XZFriction);
    Target->LinearVelocity.z *= 1 - (DeltaTime * Entity->XZFriction);

    if (!Source->IsResting)
    {
        CheckForTeleportation(Entity, Entity->SourceConfig);
        v3 MoveDelta = (DeltaTime * (Source->LinearVelocity));
        SetLocalPosition(Target, Source->LocalPosition + MoveDelta);
        CheckForTeleportation(Entity, Entity->TargetConfig);
        int x = 0;
    }
    else
    {
        Target->LinearVelocity = v3_ZERO;
        Target->IsResting = true;
    }

    if (!Entity->LockRotation)
    {
        //
        // angular
        //
        if (Source->AngularVelocity != v3_ZERO)
        {
            v3 AngularVelocityDelta = Source->AngularVelocity * DeltaTime;
            real32 AngularVelocityDeltaLength = Length(AngularVelocityDelta);
            v4 Spin = ToV4((AngularVelocityDelta * (1 / AngularVelocityDeltaLength)) * Sin(AngularVelocityDeltaLength / 2),
                           Cos(AngularVelocityDeltaLength / 2)); //quaternion
            Spin = Normalize(Spin);
            Target->LocalRotation = (CombineQuaternions(Source->LocalRotation, Spin));
            Target->LocalRotation = Normalize(Target->LocalRotation);

            //Target->LocalPosition += v3_UP;
            //Source->AngularVelocity = v3_ZERO;
            //Source->AngularMomentum = v3_ZERO;
        }

        Target->AngularMomentum = Source->AngularMomentum + DeltaTime * Source->Torque;

        matrix3 RotationMat = QuaternionToMatrix3(Target->LocalRotation);
        Target->InverseWorldInertiaTensor = RotationMat * Entity->InverseBodyInertiaTensor * Transpose(RotationMat);

        Target->AngularVelocity = Target->InverseWorldInertiaTensor * Target->AngularMomentum;
    }
}

internal void
CollisionStep(render_list* RenderList, collision_group* Group,
              linked_list* EntityMaster)
{
    YamProf(CollisionStep);

    // clear all contacts
    linked_list_member* CollisionMember = Group->Entries.FirstMember;
    while (CollisionMember != NULL)
    {
        entity* Entity = (entity*)CollisionMember->Payload;

        if (Entity->PhysicsUpdate != NULL)
        {
            Entity->PhysicsUpdate(Group, Group->GameState, Entity);
        }

        if (!Entity->IsStatic && Entity->Enabled && !Entity->DeletionPending)
        {
            PushAndUpdateWorldspaceVertices(Entity, &Group->TransientColliderVertexArena);
        }

        CollisionMember = CollisionMember->NextNode;
    }

    // start chris hecker sim loop here http://chrishecker.com/images/e/e7/Gdmphys3.pdf
    CollisionMember = Group->Entries.FirstMember;
    while (CollisionMember != NULL)
    {
        entity* Entity = (entity*)CollisionMember->Payload;

        if (!Entity->IsStatic && !Entity->IsTrigger && Entity->Enabled && !Entity->DeletionPending)
        {
            YamProf(RigidbodyCollision);

            rb_config* SourceConfig = &Entity->Configs[Entity->SourceConfig];
            rb_config* TargetConfig = &Entity->Configs[Entity->TargetConfig];

            if (SourceConfig->LocalPosition == -v3_ONE)
            {
                SourceConfig->LocalPosition = Entity->LocalPosition;
                SourceConfig->LocalRotation = Entity->LocalRotation;
                SourceConfig->LocalScale = Entity->LocalScale;
                SourceConfig->IsDirty = true;
                ResolveLocalToWorld(SourceConfig);
            }

            if (TargetConfig->LocalPosition == -v3_ONE)
            {
                *TargetConfig = *SourceConfig;
            }

            real64 CurrentTime = 0;
            real64 TargetTime = FIXED_TIME_STEP_DELTA;

            CheckForTeleportation(Entity, Entity->SourceConfig);
            CheckForTeleportation(Entity, Entity->TargetConfig);

            uint32 Iterations = 0;
            while (CurrentTime < FIXED_TIME_STEP_DELTA)
            {
                SourceConfig = &Entity->Configs[Entity->SourceConfig];
                TargetConfig = &Entity->Configs[Entity->TargetConfig];

                CheckForTeleportation(Entity, Entity->SourceConfig);
                CheckForTeleportation(Entity, Entity->TargetConfig);

                ComputeForces(Group, Entity, Entity->SourceConfig);

                Integrate(Entity, Entity->SourceConfig, Entity->TargetConfig,
                          TargetTime - CurrentTime);

                SetDirty(&Entity->Configs[Entity->SourceConfig]);
                ResolveLocalToWorld(&Entity->Configs[Entity->SourceConfig]);

                SetDirty(&Entity->Configs[Entity->TargetConfig]);
                ResolveLocalToWorld(&Entity->Configs[Entity->TargetConfig]);

                if (Entity->ColliderType == ColliderType_Mesh)
                {
                    Entity->ColliderMeshInstanceSpace = Entity->Configs[Entity->TargetConfig].LocalToWorld * Entity->ColliderMesh->LocalToWorld;
                    UpdateWorldspaceVertices(Entity);
                }

                //float CorrectDepth = (0.5f - TargetConfig->LocalPosition.y);
                bool32 Collided = false;
                all_contacts AllContacts = {};
                CollisionDetection(Group, RenderList,
                                   Entity, Entity->TargetConfig, &AllContacts);
                //Assert(Entity->ContactCount < 2);

                real32 PenetrationDepth = 0;
                for (uint32 ContactIndex = 0;
                     ContactIndex < AllContacts.Count;
                     ++ContactIndex)
                {
                    contact_info* Contact = &AllContacts.Contacts[ContactIndex];

                    if (!Contact->IsTriggerContact)
                    {
                        if (Collided)
                        {
                            //Assert(AbsoluteValue(CorrectDepth - Entity->Contacts[ContactIndex].PenetrationDepth) < FLT_EPSILON * 10);
                            //bool32 Collided = CollisionDetection(Group, RenderList,
                            //                                     Entity, Entity->TargetConfig,
                            //&Contact);
                        }

                        // if we are trying to move away from a collision, then invalidate it.
                        // this should prevent us from getting stuck in walls and stuff

                        v3 U = Contact->PointWorldA - TargetConfig->Position;
                        v3 Velocity = TargetConfig->LinearVelocity + Cross(TargetConfig->AngularVelocity, U);
                        real32 RelativeVelocity = Inner(-Contact->NormalA, Velocity);
                        if (RelativeVelocity >= 0)
                        {
                            Contact->PenetrationDepth = -1;
                        }

                        // take the largest pen
                        if ((Contact->PenetrationDepth > PenetrationDepth || PenetrationDepth == 0) && Contact->PenetrationDepth > 0)
                        {
                            PenetrationDepth = Contact->PenetrationDepth;
                        }
                    }
                }

                for (uint32 ContactIndex = 0;
                     ContactIndex < AllContacts.Count;
                     ++ContactIndex)
                {
                    contact_info* Contact = &AllContacts.Contacts[ContactIndex];
                    if (Contact->PenetrationDepth != -1 && !Contact->IsTriggerContact)
                    {
                        Collided = true;
                    }
                }

                //NOTE(james): we should be colliding every frame here if we are at rest,
                //so if we dont it means whatever we were resting on has been moved
                //this will probably have to become more complicated when considering multiple objects
                if (Entity->Configs[Entity->SourceConfig].IsResting)
                {
                    if (Collided)
                    {
                        //TODO(james): CollisionStay Callback?
                        PenetrationDepth = 0;
                        Collided = false;
                    }
                    else
                    {
                        Entity->Configs[Entity->SourceConfig].IsResting = false;
                        Entity->Configs[Entity->TargetConfig].IsResting = false;
                    }
                }

#define COLLISION_RESPONSE
#ifdef COLLISION_RESPONSE
                if (PenetrationDepth > OUTER_DEPTH_EPSILON) //interpenetrated
                {
                    // subdivide deltatime
                    TargetTime = (CurrentTime + TargetTime) / 2;
                    //Assert(Iterations != 0);
                    //Assert(AbsoluteValue(TargetTime - CurrentTime) > DBL_EPSILON);
                    //Assert(Iterations < 200);

                    //we are hitting these asserts a lot with bullets so lets just
                    // put in a hacky workaround
                    if ((TargetTime - CurrentTime) <= DBL_EPSILON || Iterations > 100)
                    {

                        v3 SeparationDirection = -Entity->Configs[0].LinearVelocity;
                        MoveWorld(&Entity->Configs[0], Normalize(SeparationDirection) * 0.2f);
                        MoveWorld(&Entity->Configs[1], Normalize(SeparationDirection) * 0.2f);

                        //Entity->DeletionPending = true;
                        CurrentTime = TargetTime;

                        MyConsole.AddLog("Problem! %s", Entity->Path.Text);
                        break;
                    }
                    Iterations++;
                }
                else
#endif
                {
                    if (Collided)
                    {
//
// Collision Reponse, collision fell within acceptable bounds
//
#ifdef COLLISION_RESPONSE
                        uint32 Counter = 0;
                        bool Colliding = false;
                        do
                        {
                            for (uint32 ContactIndex = 0;
                                 ContactIndex < AllContacts.Count;
                                 ++ContactIndex)
                            {
                                contact_info* Contact = &AllContacts.Contacts[ContactIndex];
                                if (Contact->PenetrationDepth != -1)
                                {
                                    CollisionResponse(Entity, Entity->TargetConfig, Contact);

                                    if (Entity->OnCollisionHit)
                                    {
                                        Entity->OnCollisionHit(Group->GameState, Entity, *Contact);
                                    }
                                    // TODO(james): This is dumb
                                    if (Contact->B->OnCollisionHit)
                                    {
                                        contact_info FlippedInfo;
                                        FlippedInfo.A = Contact->B;
                                        FlippedInfo.B = Contact->A;

                                        FlippedInfo.PointWorldA = Contact->PointWorldB;
                                        FlippedInfo.PointWorldB = Contact->PointWorldA;

                                        FlippedInfo.PointLocalA = Contact->PointLocalB;
                                        FlippedInfo.PointLocalB = Contact->PointLocalA;

                                        FlippedInfo.NormalA = Contact->NormalB;
                                        FlippedInfo.NormalB = Contact->NormalA;

                                        Contact->B->OnCollisionHit(Group->GameState, FlippedInfo.A, FlippedInfo);
                                    }
                                }
                            }

                            all_contacts PostResponseContacts = {};
                            Colliding = CollisionDetection(Group, RenderList,
                                                           Entity, Entity->TargetConfig, &PostResponseContacts);
                            Entity->LastCollisionTime = Group->CurrentTime + CurrentTime;
                            for (uint32 ContactIndex = 0;
                                 ContactIndex < PostResponseContacts.Count;
                                 ++ContactIndex)
                            {
                                contact_info* Contact = &PostResponseContacts.Contacts[ContactIndex];
                                v3 U = Contact->PointWorldA - TargetConfig->Position;
                                v3 Velocity = TargetConfig->LinearVelocity + Cross(TargetConfig->AngularVelocity, U);
                                real32 RelativeVelocity = Inner(-Contact->NormalA, Velocity);

                                if (Contact->PenetrationDepth <= OUTER_DEPTH_EPSILON && RelativeVelocity >= 0)
                                {

                                    Colliding = false;

                                    if (Length(Velocity) < RESTING_VELOCITY)
                                    {
                                        TargetConfig->IsResting = true;
                                        SourceConfig->IsResting = true;
                                        TargetConfig->LinearVelocity = v3_ZERO;
                                        SourceConfig->LinearVelocity = v3_ZERO;
                                        *SourceConfig = *TargetConfig;
                                    }
                                }
                                else
                                {
                                    //Assert(0);
                                }
                            }

                            Counter++;
                        } while (Colliding && Counter < 100);

                        Assert(Counter < 100);

//CollidedAtAll = true;
#endif
                    }

                    //made successful step
                    CurrentTime = TargetTime;
                    TargetTime = FIXED_TIME_STEP_DELTA;

                    Iterations++;

                    // update velocities and positions
                    Entity->SourceConfig = Entity->SourceConfig ? 0 : 1;
                    Entity->TargetConfig = Entity->TargetConfig ? 0 : 1;
                }
            }

            Entity->Configs[Entity->SourceConfig].AngularVelocity *= 0.9f;
            Entity->Configs[Entity->SourceConfig].AngularMomentum *= 0.9f;
            Entity->Configs[Entity->TargetConfig].AngularVelocity *= 0.9f;
            Entity->Configs[Entity->TargetConfig].AngularMomentum *= 0.9f;
        }
        else if (Entity->IsTrigger && Entity->OnTriggerHit != NULL)
        {
            YamProf(TriggerCollision);

            //NOTE(james): Don't really need rigidbodies here, so I'm just hardcoding in the use of
            //the first config
            Entity->Configs[0].LocalToWorld = Entity->LocalToWorld;

            UpdateWorldspaceVertices(Entity);

            all_contacts TriggerContacts = {};
            CollisionDetection(Group, RenderList, Entity, 0, &TriggerContacts);

            for (uint32 ContactIndex = 0;
                 ContactIndex < TriggerContacts.Count;
                 ++ContactIndex)
            {
                if (TriggerContacts.Contacts[ContactIndex].PenetrationDepth != 0)
                {
                    Entity->OnTriggerHit(Group->GameState, Entity, TriggerContacts.Contacts[ContactIndex]);
                }
            }
        }

#ifndef COLLISION_RESPONSE
        Entity->Configs[Entity->SourceConfig].LocalPosition = -v3_ONE;
        Entity->Configs[Entity->TargetConfig].LocalPosition = -v3_ONE;
#endif
        CollisionMember = CollisionMember->NextNode;
    }
}

internal void
ResolveCollision(render_list* RenderList, collision_group* Group,
                 bool32 BroadphaseViz, bool32 NarrowphaseViz, linked_list* EntityMaster)
{
    /*
    1. update mesh instance matrices
    2. generate AABB's
    3. do broadphase detection with AABB's
    4. narrowphase detection off pairs from broadphase
    5. Rigidbody resolution off narrowphase
    */
    YamProf(Collision);

    Group->RenderList = RenderList;
    Group->BroadphaseViz = BroadphaseViz;
    Group->NarrowphaseViz = NarrowphaseViz;

    ZeroSize(Group->TransientColliderVertexArena.Used, Group->TransientColliderVertexArena.Base);
    Group->TransientColliderVertexArena.Used = 0;

    // clamp number of iterations that can occur in one go
    if (Group->FrameDeltaTime > FIXED_TIME_STEP_DELTA * 10)
    {
        Group->FrameDeltaTime = FIXED_TIME_STEP_DELTA * 10;
    }

    Group->TimeAccumulator += Group->FrameDeltaTime;

    uint32 StepsTaken = 0;
#ifdef COLLISION_RESPONSE
    while (Group->TimeAccumulator >= FIXED_TIME_STEP_DELTA)
#endif
    {
        //TODO(james): make sure that performing a collision step takes less computational time than
        //its covering
        CollisionStep(RenderList, Group, EntityMaster);

        StepsTaken++;

        Group->CurrentTime += FIXED_TIME_STEP_DELTA;
        Group->TimeAccumulator -= FIXED_TIME_STEP_DELTA;

        // move up the entire step
        if (Group->TimeAccumulator >= FIXED_TIME_STEP_DELTA)
        {
            linked_list_member* CollisionMember = Group->Entries.FirstMember;
            while (CollisionMember != NULL)
            {
                entity* Entity = (entity*)CollisionMember->Payload;
#ifdef COLLISION_RESPONSE
                if (!Entity->IsStatic && !Entity->IsTrigger)
                {
                    // make sure rb didn't make a huge jump in one step
                    CheckForTeleportation(Entity, Entity->SourceConfig);

                    SetLocalPosition(Entity, Entity->Configs[Entity->SourceConfig].LocalPosition, false);
                    SetLocalRotation(Entity, Entity->Configs[Entity->SourceConfig].LocalRotation, false);
                    ResolveLocalToWorld(Entity);

                    CheckForTeleportation(Entity, Entity->SourceConfig);

                    Entity->LastCollisionStepPosition = Entity->Configs[Entity->SourceConfig].LocalPosition;

                    Assert(AbsoluteValue(Entity->LocalPosition.y) < 1000);
                }
#endif
                CollisionMember = CollisionMember->NextNode;
            }
        }
    }

    real64 Alpha = Group->TimeAccumulator / FIXED_TIME_STEP_DELTA;
#ifdef COLLISION_RESPONSE
    Assert(Alpha > 0 && Alpha < 1);
#endif

    linked_list_member* CollisionMember = Group->Entries.FirstMember;
    while (CollisionMember != NULL)
    {
        entity* Entity = (entity*)CollisionMember->Payload;

#ifdef COLLISION_RESPONSE
        if (!Entity->IsStatic && Entity->Enabled && !Entity->DeletionPending && !Entity->IsTrigger)
        {
            v3 SourceConfigPos = Entity->Configs[Entity->SourceConfig].LocalPosition;

            //NOTE(james): theres a chance a new object was created, and within the same game-logic frame no
            //collision step occurred, so this makes sure we don't try to update anything that hasn't been
            //simulated at least once
            if (SourceConfigPos != -v3_ONE)
            {
                CheckForTeleportation(Entity, Entity->SourceConfig);
                v3 LerpedPos = Lerp(Entity->LastCollisionStepPosition,
                                    SourceConfigPos,
                                    (real32)Alpha);

                SetLocalPosition(Entity, LerpedPos, false);
                CheckForTeleportation(Entity, Entity->SourceConfig);

                Assert(AbsoluteValue(Entity->LocalPosition.y) < 1000);

                //TODO(james): Slerp!
                v4 LerpedRot = Lerp(Entity->LocalRotation, Entity->Configs[Entity->SourceConfig].LocalRotation, Alpha);
                SetLocalRotation(Entity, LerpedRot, false);

                ResolveLocalToWorld(Entity);

                Entity->LastCollisionStepPosition = Entity->Configs[Entity->SourceConfig].LocalPosition;
            }
        }
#endif
        CollisionMember = CollisionMember->NextNode;
    }
}

internal void
InitCollision(collision_group* Group)
{
    linked_list_member* Member = Group->Entries.FirstMember;
    while (Member != NULL)
    {
        entity* Entity = (entity*)Member->Payload;

        Entity->Configs[0].LocalPosition = Entity->LocalPosition;
        SetDirty(&Entity->Configs[0]);
        ResolveLocalToWorld(&Entity->Configs[0]);

        Entity->Configs[1].LocalRotation = Entity->LocalRotation;
        SetDirty(&Entity->Configs[1]);
        ResolveLocalToWorld(&Entity->Configs[1]);

        Entity->LastCollisionStepPosition = Entity->LocalPosition;

        Member = Member->NextNode;
    }

    Group->CurrentTime = 0;
    Group->TimeAccumulator = 0;
}

inline void
AddCollisionEntity(collision_group* Group, entity* Entity,
                   bool32 UseGravity, bool32 IsTrigger,
                   collision_layer CollisionLayer)
{
    Entity->CollisionGroupMember = AddToList(&Group->Entries, Entity);

    Entity->Configs[0].LocalPosition = -v3_ONE;
    Entity->Configs[0].LocalRotation = Entity->LocalRotation;
    Entity->Configs[0].LocalScale = Entity->LocalScale;
    Entity->Configs[1] = Entity->Configs[0];

    Entity->CollidedThisFrame = false;
    Entity->UseGravity = UseGravity;
    Entity->IsTrigger = IsTrigger;
    Entity->CollisionLayer = CollisionLayer;

    Entity->LastCollisionStepPosition = Entity->LocalPosition;

    Entity->SourceConfig = 0;
    Entity->TargetConfig = 1;

    Entity->Mass = 1; //TODO(james): mass

    real32 T = 1.0f / 12.0f;
    v3 ScaleSquared = Hadamard(Entity->LocalScale, Entity->LocalScale);
    real32 W = ScaleSquared.x;
    real32 H = ScaleSquared.y;
    real32 D = ScaleSquared.z;

    real32 M = 1; //Entity->Mass;
    Entity->InverseBodyInertiaTensor = //for a cube
        {
          1 / (T * M * (H + D)), 0, 0,
          0, 1 / (T * M * (W + D)), 0,
          0, 0, 1 / (T * M * (W + H))
        };

    //Assert(Entity->Parent == NULL);//TODO(james): applies only to rigidbodies

    //TODO(james): ensure an entity isn't instantiated while already colliding with something?
}

inline void
AddColliderMesh(collision_group* Group, entity* Entity, mesh* Mesh,
                bool32 UseGravity, bool32 IsTrigger,
                collision_layer CollisionLayer = CollisionLayer_Default)
{
    AddCollisionEntity(Group, Entity, UseGravity, IsTrigger, CollisionLayer);
    Entity->ColliderMesh = Mesh;
    Entity->ColliderType = ColliderType_Mesh;
    SetString(&Entity->ColliderMeshPath, Mesh->Path);

    //https://en.wikipedia.org/wiki/List_of_moments_of_inertia
    real32 T = 1.0f / 12.0f;
    v3 ScaleSquared = Hadamard(Entity->LocalScale, Entity->LocalScale);
    real32 W = ScaleSquared.x;
    real32 H = ScaleSquared.y;
    real32 D = ScaleSquared.z;

    real32 M = 1; //Entity->Mass;
    // NOTE(james): this is a special orthogonal matrix, meaning its not a reflection
    // and A * Transpose(A) == 1
    Entity->InverseBodyInertiaTensor = //for a cube
        {
          1 / (T * M * (H + D)), 0, 0,
          0, 1 / (T * M * (W + D)), 0,
          0, 0, 1 / (T * M * (W + H))
        };
    //Copy(sizeof(Tensor), (void*)Tensor, (void*)Entity->InverseInertiaTensor);
    //Entity->InertiaTensor = Tensor;
}

inline void
AddColliderSphere(collision_group* Group, entity* Entity, real32 SphereRadius,
                  bool32 UseGravity, bool32 IsTrigger,
                  collision_layer CollisionLayer = CollisionLayer_Default)
{
    AddCollisionEntity(Group, Entity, UseGravity, IsTrigger, CollisionLayer);
    Entity->SphereColliderRadius = SphereRadius;
    Entity->ColliderType = ColliderType_Sphere;
}

inline void
AddColliderCapsule(collision_group* Group, entity* Entity, real32 Radius, real32 Height,
                   bool32 UseGravity, bool32 IsTrigger,
                   collision_layer CollisionLayer = CollisionLayer_Default)
{
    AddCollisionEntity(Group, Entity, UseGravity, IsTrigger, CollisionLayer);
    Entity->CapsuleColliderRadius = Radius;
    Entity->CapsuleColliderHeight = Height;
    Entity->ColliderType = ColliderType_Capsule;
}

inline void
AddColliderCylinder(collision_group* Group, entity* Entity, real32 Radius, real32 Height,
                    bool32 UseGravity, bool32 IsTrigger,
                    collision_layer CollisionLayer = CollisionLayer_Default)
{
    AddCollisionEntity(Group, Entity, UseGravity, IsTrigger, CollisionLayer);
    Entity->CylinderColliderRadius = Radius;
    Entity->CylinderColliderHeight = Height;
    Entity->ColliderType = ColliderType_Cylinder;
}

inline void
RemoveEntityFromCollisionList(collision_group* Group, linked_list_member* Member)
{
    RemoveFromList(&Group->Entries, Member);
}

//
// Collision System Queries
//
internal bool32
RendererRaycast(linked_list* EntityMaster, raycast_info* RaycastInfo,
                v3 Position, v3 Direction, real32 MaxDistance,
                collision_layer CollisionLayer = CollisionLayer_Default)
{
    YamProf(RendererRaycast);

    bool32 Result = false;
    *RaycastInfo = {};

    collision_ray Ray;
    Ray.Position = Position;
    Ray.Direction = Direction;
    Ray.Direction = Normalize(Ray.Direction);
    Ray.MaxDistance = MaxDistance;

    real32 ClosestDistance = FLT_MAX;
    raycast_info ClosestInfo = {};

    linked_list_member* Member = NULL;
    entity* Entity = NULL;
    while (PROGRESS_LINKED_LIST(EntityMaster, Member, Entity))
    {
        raycast_info LatestInfo = {};
        bool32 Collided = false;

        if (Entity->RenderMesh != NULL && Entity->Enabled)
        {
            if (AABBRayIntersects(Entity->RenderAABB, Ray))
            {
                if (MeshRayIntersects(Entity->RenderMesh, Entity, Ray, &LatestInfo))
                {
                    real32 DistanceToCollision = Distance(Ray.Position, LatestInfo.Position);
                    if (DistanceToCollision < ClosestDistance)
                    {
                        ClosestInfo = LatestInfo;
                        ClosestDistance = DistanceToCollision;
                        Result = true;
                    }
                }
            }
        }
    }

    if (Result)
    {
        *RaycastInfo = ClosestInfo;
    }

    return Result;
}

//NOTE(james): use TargetCollisionLayer if you only want to raycast against a single collision layer
internal bool32
CollisionRaycast(collision_group* Group, raycast_info* RaycastInfo,
                 v3 Position, v3 Direction, real32 MaxDistance = 1000, collision_layer RaycastCollisionLayer = CollisionLayer_Default, collision_layer TargetCollisionLayer = CollisionLayer_Count)
{
    YamProf(CollisionRaycast);

    bool32 Result = false;

    collision_ray Ray;
    Ray.Position = Position;
    Ray.Direction = Direction;
    Ray.Direction = Normalize(Ray.Direction);
    Ray.MaxDistance = MaxDistance;

    real32 ClosestDistance = FLT_MAX;
    raycast_info ClosestInfo = {};

    linked_list_member* Member = NULL;
    entity* Entity = NULL;
    while (PROGRESS_LINKED_LIST(&Group->Entries, Member, Entity))
    {
        bool32 PassesLayerCheck = false;
        if (TargetCollisionLayer != CollisionLayer_Count)
        {
            PassesLayerCheck = Entity->CollisionLayer == TargetCollisionLayer;
        }
        else
        {
            PassesLayerCheck = CheckLayerCollision(Group, Entity->CollisionLayer, RaycastCollisionLayer);
        }

        if (PassesLayerCheck && Entity->Enabled)
        {

            raycast_info LatestInfo = {};
            bool32 Collided = false;

            switch (Entity->ColliderType)
            {
                case ColliderType_Mesh:
                {

                    //broadphase
                    if (AABBRayIntersects(Entity->ColliderAABB, Ray))
                    {
                        //DrawAABB(RenderList,&Entity->AABB, v4_GREEN);
                        //narrowphase
                        Collided = MeshRayIntersects(Entity->ColliderMesh, Entity, Ray, &LatestInfo);
                    }
                }
                break;

                case ColliderType_Sphere:
                {

                    //broad
                    if (AABBRayIntersects(Entity->ColliderAABB, Ray))
                    {
                        //narrow
                        Collided = SphereRayIntersects(Entity, Ray, &LatestInfo);
                    }
                }
                break;

                case ColliderType_Capsule:
                {
                    if (AABBRayIntersects(Entity->ColliderAABB, Ray))
                    {
                        Collided = CapsuleRayIntersects(Entity, Ray, &LatestInfo);
                    }
                }
                break;

                case ColliderType_Cylinder:
                {
                    if (AABBRayIntersects(Entity->ColliderAABB, Ray))
                    {
                        Collided = CylinderRayIntersects(Entity, Ray, &LatestInfo);
                    }
                }
                break;

                    InvalidDefaultCase;
            }

            if (Collided)
            {
                real32 DistanceToCollision = Distance(Ray.Position, LatestInfo.Position);
                if (DistanceToCollision < ClosestDistance)
                {
                    ClosestInfo = LatestInfo;
                    ClosestDistance = DistanceToCollision;
                    Result = true;
                }
            }
        }
    }

    if (Result)
    {
        //DrawLine(RenderList, Position, Position + (Direction * MaxDistance), v4_GREEN);
        *RaycastInfo = ClosestInfo;
    }
    else
    {
        //DrawLine(RenderList, Position, Position + (Direction * MaxDistance), v4_BLACK);
    }

    return Result;
}
