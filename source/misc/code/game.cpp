
#include "imgui.cpp"
#include "imgui_draw.cpp"
#include "imgui_internal.h"

#include "game.h"

global_variable platform_work_queue* WorkQueue;
//#include "imgui_demo.cpp"

// NOTE(imgui):For the console example, here we are using a more C++ like approach of declaring a class to hold the data and the functions.
struct YamgineConsole
{
    char InputBuf[256];
    ImVector<char*> Items;
    bool ScrollToBottom;
    ImVector<char*> History;
    int HistoryPos; // -1: new line, 0..History.Size-1 browsing history.
    ImVector<const char*> Commands;
    YamgineConsole()
    {
        ClearLog();
        memset(InputBuf, 0, sizeof(InputBuf));
        HistoryPos = -1;
        Commands.push_back("HELP");
        Commands.push_back("HISTORY");
        Commands.push_back("CLEAR");
        Commands.push_back("CLASSIFY"); // "classify" is here to provide an example of "C"+[tab] completing to "CL" and displaying matches.
        AddLog("Yamgine is go.");
    }
    ~YamgineConsole()
    {
        ClearLog();
        for (int i = 0; i < History.Size; i++)
            free(History[i]);
    }
    // Portable helpers
    static int Stricmp(const char* str1, const char* str2)
    {
        int d;
        while ((d = toupper(*str2) - toupper(*str1)) == 0 && *str1)
        {
            str1++;
            str2++;
        }
        return d;
    }
    static int Strnicmp(const char* str1, const char* str2, int n)
    {
        int d = 0;
        while (n > 0 && (d = toupper(*str2) - toupper(*str1)) == 0 && *str1)
        {
            str1++;
            str2++;
            n--;
        }
        return d;
    }
    static char* Strdup(const char* str)
    {
        size_t len = strlen(str) + 1;
        void* buff = malloc(len);
        return (char*)memcpy(buff, (const void*)str, len);
    }
    void ClearLog()
    {
        for (int i = 0; i < Items.Size; i++)
            free(Items[i]);
        Items.clear();
        ScrollToBottom = true;
    }
    void AddLog(const char* fmt, ...) IM_PRINTFARGS(2)
    {
        char buf[1024];
        va_list args;
        va_start(args, fmt);
        vsnprintf(buf, IM_ARRAYSIZE(buf), fmt, args);
        buf[IM_ARRAYSIZE(buf) - 1] = 0;
        va_end(args);
        Items.push_back(Strdup(buf));
        ScrollToBottom = true;
    }
    
    void PrintV3(v3 In)
    {
        AddLog("v3: {%f, %f, %f}", In.x, In.y, In.z);
    }
    
    void PrintV4(v4 In)
    {
        AddLog("v4: {%f, %f, %f, %f}", In.x, In.y, In.z, In.w);
    }
    
    void PrintMatrix(matrix4* M)
    {
        AddLog("Matrix:\n%f, %f, %f, %f\n%f, %f, %f, %f\n%f, %f, %f, %f\n%f, %f, %f, %f\n",
               M->Row0.x, M->Row0.y, M->Row0.z, M->Row0.w,
               M->Row1.x, M->Row1.y, M->Row1.z, M->Row1.w,
               M->Row2.x, M->Row2.y, M->Row2.z, M->Row2.w,
               M->Row3.x, M->Row3.y, M->Row3.z, M->Row3.w);
    }
    
    void Draw(const char* title, bool* p_open)
    {
        ImGui::TextWrapped("Enter 'HELP' for help, press TAB to use text completion.");
        // TODO: display items starting from the bottom
        if (ImGui::SmallButton("Add Dummy Text"))
        {
            AddLog("%d some text", Items.Size);
            AddLog("some more text");
            AddLog("display very important message here!");
        }
        ImGui::SameLine();
        if (ImGui::SmallButton("Add Dummy Error"))
            AddLog("[error] something went wrong");
        ImGui::SameLine();
        if (ImGui::SmallButton("Clear"))
            ClearLog();
        ImGui::SameLine();
        if (ImGui::SmallButton("Scroll to bottom"))
            ScrollToBottom = true;
        //static float t = 0.0f; if (ImGui::GetTime() - t > 0.02f) { t = ImGui::GetTime(); AddLog("Spam %f", t); }
        ImGui::Separator();
        ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0, 0));
        static ImGuiTextFilter filter;
        filter.Draw("Filter (\"incl,-excl\") (\"error\")", 180);
        ImGui::PopStyleVar();
        ImGui::Separator();
        ImGui::BeginChild("ScrollingRegion", ImVec2(0, -ImGui::GetItemsLineHeightWithSpacing()), false, ImGuiWindowFlags_HorizontalScrollbar);
        if (ImGui::BeginPopupContextWindow())
        {
            if (ImGui::Selectable("Clear"))
                ClearLog();
            ImGui::EndPopup();
        }
        // Display every line as a separate entry so we can change their color or add custom widgets. If you only want raw text you can use ImGui::TextUnformatted(log.begin(), log.end());
        // NB- if you have thousands of entries this approach may be too inefficient and may require user-side clipping to only process visible items.
        // You can seek and display only the lines that are visible using the ImGuiListClipper helper, if your elements are evenly spaced and you have cheap random access to the elements.
        // To use the clipper we could replace the 'for (int i = 0; i < Items.Size; i++)' loop with:
        //     ImGuiListClipper clipper(Items.Size);
        //     while (clipper.Step())
        //         for (int i = clipper.DisplayStart; i < clipper.DisplayEnd; i++)
        // However take note that you can not use this code as is if a filter is active because it breaks the 'cheap random-access' property. We would need random-access on the post-filtered list.
        // A typical application wanting coarse clipping and filtering may want to pre-compute an array of indices that passed the filtering test, recomputing this array when user changes the filter,
        // and appending newly elements as they are inserted. This is left as a task to the user until we can manage to improve this example code!
        // If your items are of variable size you may want to implement code similar to what ImGuiListClipper does. Or split your data into fixed height items to allow random-seeking into your list.
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 1)); // Tighten spacing
        for (int i = 0; i < Items.Size; i++)
        {
            const char* item = Items[i];
            if (!filter.PassFilter(item))
                continue;
            ImVec4 col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f); // A better implementation may store a type per-item. For the sample let's just parse the text.
            if (strstr(item, "[error]"))
                col = ImColor(1.0f, 0.4f, 0.4f, 1.0f);
            else if (strncmp(item, "# ", 2) == 0)
                col = ImColor(1.0f, 0.78f, 0.58f, 1.0f);
            ImGui::PushStyleColor(ImGuiCol_Text, col);
            ImGui::TextUnformatted(item);
            ImGui::PopStyleColor();
        }
        if (ScrollToBottom)
            ImGui::SetScrollHere();
        ScrollToBottom = false;
        ImGui::PopStyleVar();
        ImGui::EndChild();
        ImGui::Separator();
#if 0
        // Command-line
        if (ImGui::InputText("Input", InputBuf, IM_ARRAYSIZE(InputBuf), ImGuiInputTextFlags_EnterReturnsTrue | ImGuiInputTextFlags_CallbackCompletion | ImGuiInputTextFlags_CallbackHistory, &TextEditCallbackStub, (void*)this))
        {
            char* input_end = InputBuf + strlen(InputBuf);
            while (input_end > InputBuf && input_end[-1] == ' ')
                input_end--;
            *input_end = 0;
            if (InputBuf[0])
                ExecCommand(InputBuf);
            strcpy(InputBuf, "");
        }
        // Demonstrate keeping auto focus on the input box
        if (ImGui::IsItemHovered() || (ImGui::IsRootWindowOrAnyChildFocused() && !ImGui::IsAnyItemActive() && !ImGui::IsMouseClicked(0)))
            ImGui::SetKeyboardFocusHere(-1); // Auto focus previous widget
#endif
    }
    void ExecCommand(const char* command_line)
    {
        AddLog("# %s\n", command_line);
        // Insert into history. First find match and delete it so it can be pushed to the back. This isn't trying to be smart or optimal.
        HistoryPos = -1;
        for (int i = History.Size - 1; i >= 0; i--)
            if (Stricmp(History[i], command_line) == 0)
        {
            free(History[i]);
            History.erase(History.begin() + i);
            break;
        }
        History.push_back(Strdup(command_line));
        // Process command
        if (Stricmp(command_line, "CLEAR") == 0)
        {
            ClearLog();
        }
        else if (Stricmp(command_line, "HELP") == 0)
        {
            AddLog("Commands:");
            for (int i = 0; i < Commands.Size; i++)
                AddLog("- %s", Commands[i]);
        }
        else if (Stricmp(command_line, "HISTORY") == 0)
        {
            for (int i = History.Size >= 10 ? History.Size - 10 : 0; i < History.Size; i++)
                AddLog("%3d: %s\n", i, History[i]);
        }
        else
        {
            AddLog("Unknown command: '%s'\n", command_line);
        }
    }
    static int TextEditCallbackStub(ImGuiTextEditCallbackData* data) // In C++11 you are better off using lambdas for this sort of forwarding callbacks
    {
        YamgineConsole* console = (YamgineConsole*)data->UserData;
        return console->TextEditCallback(data);
    }
    int TextEditCallback(ImGuiTextEditCallbackData* data)
    {
        //AddLog("cursor: %d, selection: %d-%d", data->CursorPos, data->SelectionStart, data->SelectionEnd);
        switch (data->EventFlag)
        {
            case ImGuiInputTextFlags_CallbackCompletion:
            {
                // Example of TEXT COMPLETION
                // Locate beginning of current word
                const char* word_end = data->Buf + data->CursorPos;
                const char* word_start = word_end;
                while (word_start > data->Buf)
                {
                    const char c = word_start[-1];
                    if (c == ' ' || c == '\t' || c == ',' || c == ';')
                        break;
                    word_start--;
                }
                // Build a list of candidates
                ImVector<const char*> candidates;
                for (int i = 0; i < Commands.Size; i++)
                    if (Strnicmp(Commands[i], word_start, (int)(word_end - word_start)) == 0)
                    candidates.push_back(Commands[i]);
                if (candidates.Size == 0)
                {
                    // No match
                    AddLog("No match for \"%.*s\"!\n", (int)(word_end - word_start), word_start);
                }
                else if (candidates.Size == 1)
                {
                    // Single match. Delete the beginning of the word and replace it entirely so we've got nice casing
                    data->DeleteChars((int)(word_start - data->Buf), (int)(word_end - word_start));
                    data->InsertChars(data->CursorPos, candidates[0]);
                    data->InsertChars(data->CursorPos, " ");
                }
                else
                {
                    // Multiple matches. Complete as much as we can, so inputing "C" will complete to "CL" and display "CLEAR" and "CLASSIFY"
                    int match_len = (int)(word_end - word_start);
                    for (;;)
                    {
                        int c = 0;
                        bool all_candidates_matches = true;
                        for (int i = 0; i < candidates.Size && all_candidates_matches; i++)
                            if (i == 0)
                            c = toupper(candidates[i][match_len]);
                        else if (c != toupper(candidates[i][match_len]))
                            all_candidates_matches = false;
                        if (!all_candidates_matches)
                            break;
                        match_len++;
                    }
                    if (match_len > 0)
                    {
                        data->DeleteChars((int)(word_start - data->Buf), (int)(word_end - word_start));
                        data->InsertChars(data->CursorPos, candidates[0], candidates[0] + match_len);
                    }
                    // List matches
                    AddLog("Possible matches:\n");
                    for (int i = 0; i < candidates.Size; i++)
                        AddLog("- %s\n", candidates[i]);
                }
                break;
            }
            case ImGuiInputTextFlags_CallbackHistory:
            {
                // Example of HISTORY
                const int prev_history_pos = HistoryPos;
                if (data->EventKey == ImGuiKey_UpArrow)
                {
                    if (HistoryPos == -1)
                        HistoryPos = History.Size - 1;
                    else if (HistoryPos > 0)
                        HistoryPos--;
                }
                else if (data->EventKey == ImGuiKey_DownArrow)
                {
                    if (HistoryPos != -1)
                        if (++HistoryPos >= History.Size)
                        HistoryPos = -1;
                }
                // A better implementation would preserve the data on the current input line along with cursor position.
                if (prev_history_pos != HistoryPos)
                {
                    data->CursorPos = data->SelectionStart = data->SelectionEnd = data->BufTextLen = (int)snprintf(data->Buf, (size_t)data->BufSize, "%s", (HistoryPos >= 0) ? History[HistoryPos] : "");
                    data->BufDirty = true;
                }
            }
        }
        return 0;
    }
};

global_variable YamgineConsole MyConsole;

#include "yamgine_render_list.cpp"

#include "yamgine_texture_loading.cpp"


#include "yamgine_collision.cpp"
#include "yamgine_entity.cpp"
#include "yamgine_serialization.cpp"
#include "yamgine_geometry.cpp"

#include "yamgine_random.cpp"
#include "yamgine_gui.cpp"

#include "gltf_loader.cpp"

v3 RandomPointOnCircleEdgeNearLast(v3 CirclePos, v3 CircleNormal, real32 CircleRadius, v3 LastPos, real32 NearRadius)
{
    v3 Result = RandomPointOnCircleEdge(CirclePos, CircleNormal, CircleRadius);
    
    bool32 Looking = true;
    while (Looking)
    {
        if (Distance(Result, LastPos) < NearRadius)
        {
            Looking = false;
        }
        else
        {
            Result = RandomPointOnCircleEdge(CirclePos, CircleNormal, CircleRadius);
        }
    }
    return Result;
}


#include "game_editor.cpp"
#include "yamgine_network.cpp"

//TODO(james): just pass name to CreateFbo
void SetFboDebugNames(fbo* Fbo, char* Name)
{
    CopyString(Name, Fbo->DebugName, 32);
    if (Fbo->HasDepth)
    {
        CopyString(Name, Fbo->DepthTexture.DebugName, 32);   
        AppendString_(Fbo->DepthTexture.DebugName, 32, " D", 2);
    }
    
    CopyString(Name, Fbo->Texture.DebugName, 32);
    AppendString_(Fbo->Texture.DebugName, 32, " D", 2);
}


void SavePlayerSettings(player_settings* Settings, platform_api* PlatformAPI)
{
    Settings->WindowPosition = PlatformAPI->GetWindowPosition().Min;
    
    STRING(File, 1000);
    SerializeStruct(ArrayCount(MembersOf_player_settings), MembersOf_player_settings, (void*)Settings, 0, "player_settings", "PlayerSettings", &File);
    
    PlatformAPI->WriteBinaryFile("serialization/player_settings.txt", (uint8*)File.Text, StringLength(File.Text));
}

void LoadPlayerSettings(player_settings* Settings, temporary_memory* Temp, platform_api* PlatformAPI)
{
    char* PlayerSettingsFile = LoadTextFile("serialization/player_settings.txt", Temp, PlatformAPI);
    
    tokenizer Tokenizer = {};
    Tokenizer.At = PlayerSettingsFile;
    
    GetToken(&Tokenizer); // struct type
    GetToken(&Tokenizer); // struct identifier
    
    GetToken(&Tokenizer); // equals sign
    DeserializeStruct(&Tokenizer, Settings, 
                      MembersOf_player_settings, ArrayCount(MembersOf_player_settings));
}

void UpdateAndDrawPlayerSettings(render_list* RenderList, game_input* Input, 
                                 game_state* GameState, platform_api* PlatformAPI)
{
    //background dimmer
    DrawColorRect(RenderList,  RectMinDim(V2(0,0),Input->ApplicationDim), v4_RED * 0.9f);
    
    v2 MousePos = Input->Controllers[0].MousePosition;
    MousePos += RenderList->Viewport.Min;
    bool32 MouseClick = Input->Controllers[0].LeftMouseClick.DownThisFrame;
    bool32 MouseHold = Input->Controllers[0].LeftMouseClick.IsDown;
    player_settings* PlayerSettings = &GameState->OptionsPlayerSettings;
    
    v2 BasePos = V2(850, 300);
    real32 YSpacing = 50;
    real32 ControlsDistanceX = 400;
    
    
    //quit button
    {
        rectangle2 SaveButton = RectMinDim(BasePos, V2(300, 50));
        SaveButton.Min -= V2(10,15);
        if (DrawButton(RenderList, SaveButton, Input))
        {
#if YAMGINE_EDITOR
            GameState->QuitRequested = true;
#else
            Input->QuitRequested = true;
#endif
        }
        v2 SaveButtonText = BasePos;
        DrawText(RenderList, "Quit Game", SaveButtonText.x, SaveButtonText.y, &GameState->Font, 1.5);
    }
    
    //save button
    {
        rectangle2 SaveButton = RectMinDim(BasePos+V2(0,YSpacing*2), V2(300, 50));
        SaveButton.Min -= V2(10,15);
        if (DrawButton(RenderList,SaveButton, Input))
        {
            if (GameState->PlayerSettings.Vsync != GameState->OptionsPlayerSettings.Vsync)
            {
                PlatformAPI->SetVsync(GameState->OptionsPlayerSettings.Vsync);
            }
            
            if (GameState->PlayerSettings.Fullscreen != GameState->OptionsPlayerSettings.Fullscreen)
            {
                PlatformAPI->SetFullscreen(GameState->OptionsPlayerSettings.Fullscreen);
            }
            
            GameState->PlayerSettings = GameState->OptionsPlayerSettings;
            SavePlayerSettings(PlayerSettings, PlatformAPI);
        }
        v2 SaveButtonText = BasePos+V2(0,YSpacing*2);
        DrawText(RenderList, "Save Settings", SaveButtonText.x, SaveButtonText.y, &GameState->Font, 1.5);
    }
    
    
    // mouse sensitivity
    {
        v2 MouseSensitivityTextPos = BasePos + V2(0, YSpacing*4);
        DrawText(RenderList, "Mouse sensitivity", MouseSensitivityTextPos.x, MouseSensitivityTextPos.y,  &GameState->Font, 1.5);
        
        v2 MouseSensitivitySliderPos = MouseSensitivityTextPos + V2(ControlsDistanceX, 0);
        real32 SliderWidth = 150;
        real32 SliderHeight = 40;
        rectangle2 SliderRect = RectMinDim(MouseSensitivitySliderPos, V2(SliderWidth, SliderHeight));
        
        DrawSlider(RenderList, &PlayerSettings->MouseSensitivity, 0.001f, 0.5f, SliderRect, Input);
        
    }
    
    // vsync
    {
        v2 VsyncPos = BasePos + V2(0, YSpacing * 5);
        DrawText(RenderList, "Vsync", VsyncPos.x, VsyncPos.y, &GameState->Font, 1.5f);
        
        rectangle2 ButtonRect = RectMinDim(VsyncPos + V2(ControlsDistanceX*0.75f, 0), V2(40, 40));
        if (DrawToggle(RenderList, &PlayerSettings->Vsync, ButtonRect, Input))
        {
            
        }
    }
    
    // full screen
    {
        v2 FullscreenPos = BasePos + V2(0, YSpacing * 6);
        DrawText(RenderList, "Full screen", FullscreenPos.x, FullscreenPos.y, &GameState->Font, 1.5);
        
        rectangle2 ButtonRect = RectMinDim(FullscreenPos + V2(ControlsDistanceX*0.75f, 0), V2(40, 40));
        if (DrawToggle(RenderList, &PlayerSettings->Fullscreen, ButtonRect, Input))
        {
            
        }
    }
}


/*
//asynchronously called when certain events happen to your audio
FMOD_RESULT F_CALLBACK DestroyOnStopCallback(FMOD_STUDIO_EVENT_CALLBACK_TYPE Type, 
FMOD_STUDIO_EVENTINSTANCE* Event, 
void *Parameters)
{
if (Type == FMOD_STUDIO_EVENT_CALLBACK_STOPPED)
{

}

return FMOD_OK;
}
*/

void UpdateInputAxis(game_controller* Controller)
{
    if (Controller->MoveRight.IsDown)
    {
        Controller->MoveHorizontalAxis = 1;
    }
    else if (Controller->MoveLeft.IsDown)
    {
        Controller->MoveHorizontalAxis = -1;
    }
    else
    {
        Controller->MoveHorizontalAxis = 0;
    }
    
    if (Controller->MoveForward.IsDown)
    {
        Controller->MoveForwardAxis = -1;
    }
    else if (Controller->MoveBack.IsDown)
    {
        Controller->MoveForwardAxis = 1;
    }
    else
    {
        Controller->MoveForwardAxis = 0;
    }
    
    if (Controller->MoveUp.IsDown)
    {
        Controller->MoveVerticalAxis = 1;
    }
    else if (Controller->MoveDown.IsDown)
    {
        Controller->MoveVerticalAxis = -1;
    }
    else
    {
        Controller->MoveVerticalAxis = 0;
    }
}

void YamProfilingNode(yam_profiling_scope* Node)
{
    ImGuiTreeNodeFlags Flags = ImGuiTreeNodeFlags_DefaultOpen;
    if (!Node->Child)
    {
        Flags |= ImGuiTreeNodeFlags_Leaf;
    }
    bool Result = ImGui::TreeNodeEx(Node->Name, Flags);
    ImGui::NextColumn();
    ImGui::Text("%f", Node->TotalTime);
    ImGui::NextColumn();
    ImGui::Text("%f", Node->TotalTimeSelf);
    ImGui::NextColumn();
    ImGui::Text("%d", Node->EntryCount);
    ImGui::NextColumn();
    
    if (Result)
    {
        yam_profiling_scope* Child = Node->Child;
        while (Child != NULL)
        {
            YamProfilingNode(Child);
            Child = Child->NextSibling;
        }
        ImGui::TreePop();
    }
}
void YamProfilingRender(yam_profiling_report* Report)
{
    ImGui::Columns(4);
    
    ImGui::Separator();
    
    ImGui::Text("Names");
    ImGui::NextColumn();
    ImGui::Text("Total Time");
    ImGui::NextColumn();
    ImGui::Text("Total Time Self");
    ImGui::NextColumn();
    ImGui::Text("Hits");
    ImGui::NextColumn();
    
    ImGui::Separator();
    
    YamProfilingNode(&Report->InternalScope);
    YamProfilingNode(&Report->Global);
    
    if (Report->SkippedFrame)
    {
        MyConsole.AddLog("Skipped frame!");
    }
}

//#define LOAD_FBX

#ifdef LOAD_FBX
#include "yamgine_fbx_loading.cpp"
#endif
void LoadCustomAssets(render_assets* Assets, memory_arena* AssetArena, temporary_memory* Temp, platform_api* PlatformAPI)
{
    YamProf(LoadCustomAssets);
    loaded_assets* LoadedAssets = &Assets->LoadedAssets;
    
    LoadGltf("media/gltf/Cube/Cube.gltf", "media/gltf/Cube/Cube.bin", "CUBE", &LoadedAssets->GltfCube, AssetArena, Temp, PlatformAPI);
    AddModelAsset(Assets, &LoadedAssets->GltfCube);
    
    int x,y,n;
    unsigned char *data = stbi_load("media/gltf/Cube/Cube_BaseColor.png", &x, &y, &n, 0);
    
    LoadGltf("media/gltf/Box/Box.gltf", "media/gltf/Box/Box0.bin", "BOX", &LoadedAssets->GltfBox, AssetArena, Temp, PlatformAPI);
    AddModelAsset(Assets, &LoadedAssets->GltfBox);
    
    LoadGltf("media/gltf/Triangle/Triangle.gltf", "media/gltf/Triangle/simpleTriangle.bin", "TRI", &LoadedAssets->Triangle, AssetArena, Temp, PlatformAPI);
    AddModelAsset(Assets, &LoadedAssets->Triangle);
    
    //load model
#ifdef LOAD_FBX
    FbxManager* MyFbxManager = FbxManager::Create();
    
    /*
    TODO(james): sword_original.fbx doesn't work because the geometric translation isn't being handled correctly
    TODO(james): exporting from Blender with Forward: Z+ doesn't load right. Check out sword_blender_zpos.fbx
    */
    LoadModel(MyFbxManager, &LoadedAssets->Sword, "media/sword_blender_zpos_working.fbx", 0.01f,
              AssetArena, PlatformAPI);
    LoadModel(MyFbxManager, &LoadedAssets->Cractus, "media/cactus/cactus.fbx", 1,
              AssetArena, PlatformAPI);
    LoadModel(MyFbxManager, &LoadedAssets->Sphere, "media/sphere_control_point_normals.fbx", 0.01f,
              AssetArena, PlatformAPI);
    LoadModel(MyFbxManager, &LoadedAssets->Capsule, "media/Capsule.fbx", 0.01f,
              AssetArena, PlatformAPI);
    
    SerializeModel(&LoadedAssets->Sword, PlatformAPI);
    SerializeModel(&LoadedAssets->Sphere, PlatformAPI);
    SerializeModel(&LoadedAssets->Cractus, PlatformAPI);
    SerializeModel(&LoadedAssets->Capsule, PlatformAPI);
#endif
    //DeserializeModel("serialization/models/sword_blender_exported", &LoadedAssets->Sword,
    //                                 AssetArena, PlatformAPI);
    
#if 0
    {
        YamProf(sword);
        DeserializeModel("serialization/models/sword_blender_zpos_working", &LoadedAssets->Sword,
                         AssetArena, PlatformAPI);
    }
    {
        YamProf(cactus);
        DeserializeModel("serialization/models/cactus", &LoadedAssets->Cractus,
                         AssetArena, PlatformAPI);
    }
#endif
    {
        YamProf(Sphere);
        DeserializeModel("serialization/models/sphere_control_point_normals", &LoadedAssets->Sphere,
                         AssetArena, Temp, PlatformAPI);
    }
    
    DeserializeModel("serialization/models/capsule", &LoadedAssets->Capsule,
                     AssetArena, Temp, PlatformAPI);
    
    //AddModelAsset(Assets, &LoadedAssets->Sword);
    
    AddModelAsset(Assets, &LoadedAssets->Sphere);
    AddMeshAsset(Assets, LoadedAssets->Sphere.Child);
    
    //AddModelAsset(Assets, &LoadedAssets->Cractus);
    AddModelAsset(Assets, &LoadedAssets->Capsule);
    
#ifdef LOAD_FBX
    MyFbxManager->Destroy();
#endif
}

#if FMOD_SUPPORT
//TODO(james): compile these out on deploy builds
void (*Common_Private_Error)(FMOD_RESULT, const char*, int);

#define ERRCHECK(_result) ERRCHECK_fn(_result, __FILE__, __LINE__)
void ERRCHECK_fn(FMOD_RESULT result, const char* file, int line)
{
    if (result != FMOD_OK)
    {
        if (Common_Private_Error)
        {
            Common_Private_Error(result, file, line);
        }
        Assert(0);
        //TODO(james): logging
        //Common_Fatal("%s(%d): FMOD error %d - %s", file, line, result, FMOD_ErrorString(result));
    }
}
#endif

////////////////////////////////////////////////////
// IMGUI
////////////////////////////////////////////////////

void ImGuiSetup(imgui_font_info* FontInfo)
{
    YamProf(ImGuiSetupGame);
    FontInfo->GetIOPtr = (imgui_getio_pointer*)ImGui::GetIO;
    
    ImGuiIO& IO = FontInfo->GetIOPtr();
    IO.Fonts->GetTexDataAsRGBA32(&FontInfo->Pixels, &FontInfo->Width, &FontInfo->Height);
}

#if FMOD_SUPPORT
FMOD_VECTOR V3ToFmodVector(v3 A)
{
    FMOD_VECTOR Result;
    Result.x = A.x;
    Result.y = A.y;
    Result.z = A.z;
    return Result;
}
#endif

void ImGuiNewFrame(v2 ScreenDim, game_controller* Controller, float DeltaTime)
{
    ImGuiIO& io = ImGui::GetIO();
    
    io.DisplaySize = ImVec2(ScreenDim.x, ScreenDim.y);
    io.DisplayFramebufferScale = ImVec2(1, 1); //ImVec2(w > 0 ? ((float)display_w / w) : 0, h > 0 ? ((float)display_h / h) : 0);
    
    //setup time step
    io.DeltaTime = DeltaTime;
    
    // ImGui is Y down for mouse coords, but Y up for window positions
    io.MousePos = ImVec2(Controller->MousePosition.x, ScreenDim.y - Controller->MousePosition.y);
    io.MouseWheel = Controller->MouseWheelDelta * 0.01f;
    
    io.MouseDown[0] = (Controller->LeftMouseClick.IsDown) ? true : false;
    
    char* InputCharacter = &Controller->InputCharacters[0];
    while (*InputCharacter != '\0')
    {
        io.AddInputCharacter(*InputCharacter);
        InputCharacter++;
    }
    
    for (int i = 0; i < 512; i++)
    {
        io.KeysDown[i] = Controller->KeyboardState[i];
    }
    io.KeyCtrl = io.KeysDown[L_CONTROL] || io.KeysDown[R_CONTROL];
    io.KeyShift = io.KeysDown[L_SHIFT] || io.KeysDown[R_SHIFT];
    io.KeyAlt = io.KeysDown[L_ALT] || io.KeysDown[R_ALT];
    io.KeySuper = io.KeysDown[L_SUPER] || io.KeysDown[R_SUPER];
    //TODO(james): hide mouse cursor if ImGui is drawing it
    
    ImGui::NewFrame();
}

////////////////////////////////////////
// Modes
////////////////////////////////////////

void SaveGameState(game_and_editor_memory* Memory)
{
    YamProf(SaveGameState);
    
    Copy((Memory->GameState.EntityMaster.MemoryEntriesCount-1) * sizeof(entity),
         Memory->GameState.EntityMaster.MemberBodyMemory,
         Memory->PreservedGameState.EntityMaster.MemberBodyMemory);
    
    Copy(Memory->GameState.GameArena.Size, Memory->GameState.GameArena.Base, Memory->PreservedGameStateArena.Base);
    
    Memory->PreservedGameState = Memory->GameState;
    
    Memory->CollisionEntriesMemberCount = CountListMembers(Memory->GameState.CollisionGroup.Entries.FirstMember);
    CopyLinkedList(&Memory->GameState.CollisionGroup.Entries,
                   &Memory->PreservedCollisionEntries,
                   Memory->GameState.CollisionGroup.Entries.MemberMemory,
                   Memory->PreservedCollisionEntriesMemory);
}

void LoadGameState(game_and_editor_memory* Memory)
{
    entity KeepEditorCamera = *Memory->EditorState.EditorCamera;
    
    *Memory->EditorState.EditorCamera = KeepEditorCamera;
    
    Copy(Memory->GameState.GameArena.Size, Memory->PreservedGameStateArena.Base, Memory->GameState.GameArena.Base);
    
    CopyLinkedList(&Memory->PreservedCollisionEntries,
                   &Memory->GameState.CollisionGroup.Entries,
                   Memory->PreservedCollisionEntriesMemory,
                   Memory->GameState.CollisionGroup.Entries.MemberMemory);
    
    uint32 LoadedCount = CountListMembers(Memory->GameState.CollisionGroup.Entries.FirstMember);
    Assert(Memory->CollisionEntriesMemberCount == LoadedCount);
    
    
    Memory->GameState = Memory->PreservedGameState;
}

void SetForce(entity* Entity, v3 Force)
{
    
    rb_config* Config = &Entity->Configs[Entity->SourceConfig];
    Config->Force = Force;
}

PHYSICS_UPDATE(PlayerPhysicsUpdate)
{
    v3 InputDirection = v3_ZERO;
    InputDirection.x = GameState->MoveDirectionXZ.x; //+ 1;
    InputDirection.z = GameState->MoveDirectionXZ.y;
    InputDirection = Normalize(InputDirection);
    InputDirection *= 100;
    
    rb_config* Config = &Entity->Configs[Entity->SourceConfig];
    //v2 LinearMomentumXZ = V2(Configuration->Force.x,Configuration->Force.z);
    
    //if (Length(LinearMomentumXZ) < 1)
    {
        Config->Force.x = InputDirection.x;
        Config->Force.z = InputDirection.z;
    }
    
    //if (InputDirection.x == 0)
    {
        Config->LinearVelocity.x *= 1-(FIXED_TIME_STEP_DELTA * 12);
    }
    
    //if (InputDirection.z == 0)
    {
        Config->LinearVelocity.z *= 1-(FIXED_TIME_STEP_DELTA * 12);
    }
    
    //
    // jumping
    //
    if (GameState->JumpHit && AbsoluteValue(Config->LinearVelocity.y) < 2 && CollisionGroup->CurrentTime - Entity->LastCollisionTime < 0.1 && CollisionGroup->CurrentTime - Entity->LastJumpTime > 0.3f)
    {
        GameState->JumpHit = false;
        Config->LinearVelocity.y += 5;
#if FMOD_SUPPORT
        ERRCHECK(GameState->Fmod->API.Studio_EventInstance_Start(GameState->JumpInstance));
#endif
        Entity->LastJumpTime = CollisionGroup->CurrentTime;
    }
    
    //
    // Stepping sound
    // 
    real32 VelocityXZ = Length(V2(Config->LinearVelocity.x, Config->LinearVelocity.z));
    
    if (VelocityXZ > GameState->WalkingVelocityThreshold && CollisionGroup->CurrentTime - Entity->LastCollisionTime < 0.1)
    {
        real32 MoveSpeed = InverseLerp(GameState->WalkingVelocityThreshold, GameState->RunningVelocityThreshold, VelocityXZ);
        real32 NextStepTime = GameState->LastStepTime + Lerp(GameState->WalkingStepFrequency,
                                                             GameState->RunningStepFrequency,
                                                             MoveSpeed);
        
        if (CollisionGroup->CurrentTime >= NextStepTime)
        {
#if FMOD_SUPPORT
            ERRCHECK(GameState->Fmod->API.Studio_EventInstance_Start(GameState->StepInstance));
#endif
            GameState->LastStepTime = CollisionGroup->CurrentTime;
        }
    }
}

ON_TRIGGER_HIT(BulletTriggerHit)
{
    Entity->DeletionPending = true;
    
    //MyConsole.AddLog("Bullet Collision: %s", Contact.Other->Path.Text);
    
    if (Contact.B->EntityType == EntityType_EnemyShooter || Contact.B->EntityType == EntityType_EnemyChaser)
    {
        Contact.B->LastHitTime = GameState->ElapsedTime;
        //TODO(james): we want to remove this as a valid collision obj, but can't delete
        // something while we are iterating through the list its part of
        // (these callbacks happen in CollisionStep)
        Contact.B->IsTrigger = true;
        
#if FMOD_SUPPORT
        FMOD_3D_ATTRIBUTES Attributes = {};
        Attributes.position = V3ToFmodVector(Contact.B->Position);
        
        ERRCHECK(GameState->Fmod->API.Studio_EventInstance_Set3DAttributes(Contact.B->DeathInstance, &Attributes));
        ERRCHECK(GameState->Fmod->API.Studio_EventInstance_Start(Contact.B->DeathInstance));
#endif
    }
    
#if FMOD_SUPPORT
    if (Entity->ShootInstance != NULL)
    {
        ERRCHECK(GameState->Fmod->API.Studio_EventInstance_Stop(Entity->ShootInstance, FMOD_STUDIO_STOP_ALLOWFADEOUT));
    }
#endif
    
}


void GameCameraOrbit(entity* Player, matrix4* Camera, game_controller* Controller, float DeltaTime, real32 MouseSensitivity, float currentTime)
{
    real32 OrbitDistance = 5;
    
    real32 OrbitSpeed = 0.1f;
    currentTime *= OrbitSpeed;
    v3 Position = V3(Sin(currentTime) * OrbitDistance, 1, Cos(currentTime) * OrbitDistance);
    SetLocalRotation(Player, AngleAxisToQuaternion(fmod(-currentTime, 2 * PI), v3_UP));
    SetPosition(Player, Position);
    ResolveLocalToWorld(Player);
    
    v3 xaxis = Player->Right;
    v3 yaxis = Player->Up;
    v3 zaxis = Player->Forward;
    
    v3 eye = Player->Position;
    
    // Create a 4x4 view matrix from the right, up, forward and eye position vectors
    matrix4 viewMatrix = {
        V4(xaxis.x, yaxis.x, zaxis.x, 0),
        V4(xaxis.y, yaxis.y, zaxis.y, 0),
        V4(xaxis.z, yaxis.z, zaxis.z, 0),
        V4(-Inner(xaxis, eye), -Inner(yaxis, eye), -Inner(zaxis, eye), 1)
    };
    viewMatrix = Transpose(viewMatrix);
    *Camera = viewMatrix;
    
}

void Swap(particle* A, particle* B)
{
    particle Temp = *A;
    *A = *B;
    *B = Temp;
}

void Heapify(particle* Particles, int32 Count, int32 Index)
{
    int32 Largest = Index;
    
    int32 Left = 2 * Index + 1;
    int32 Right = 2 * Index + 2;
    
    if (Left < Count && Particles[Left].DistanceFromCamera > Particles[Largest].DistanceFromCamera)
    {
        Largest = Left;
    }
    
    if (Right < Count && Particles[Right].DistanceFromCamera > Particles[Largest].DistanceFromCamera)
    {
        Largest = Right;
    }
    
    if (Largest != Index)
    {
        Swap(&Particles[Index], &Particles[Largest]);
        
        Heapify(Particles, Count, Largest);
    }
}

void HeapSort(particle* Particles, int32 Count)
{
    for (int32 Index = Count / 2 - 1;
         Index >= 0;
         Index--)
    {
        Heapify(Particles, Count, Index);
    }
    
    for (int32 Index = Count-1; 
         Index >= 0; 
         Index--)
    {
        Swap(&Particles[0], &Particles[Index]);
        Heapify(Particles, Index, 0);
    }
}

void UpdateParticles(game_state* GameState, game_input* Input, platform_api* PlatformAPI)
{
    
    uint32 ParticlesToCreateThisFrame = 4;
    bool32 ParticleCountMaxed = GameState->Particles.Buffer.Count >= MAX_PARTICLE_COUNT - 1;
    
    
    //
    // Update particles
    //
    
    for (uint32 i = 0; i < MAX_PARTICLE_COUNT; i++)
    {
        particle* P = &GameState->Particles.CpuParticles[i];
        if (P->IsAlive)
        {
            P->TimeAlive += Input->DeltaTime;
            P->Position += P->Velocity * Input->DeltaTime;
            real32 ProgressToDeath = InverseLerp(0, P->AllottedLifespan, P->TimeAlive);
            P->Color.a = Lerp(1, 0, ProgressToDeath);
            
            P->DistanceFromCamera = Distance(GameState->PlayerCamera->LocalPosition, 
                                             P->Position);
            if (P->TimeAlive >= P->AllottedLifespan)
            {
                P->IsAlive = false;
                *P = {};
            }
        }
        else if (ParticlesToCreateThisFrame > 0 && !ParticleCountMaxed)
        {
            P->StartingColor = RandomColor();
            P->Color = P->StartingColor;
            P->AllottedLifespan = RandomRange(1, 3);
            P->Velocity = GenRandomOnUnitSphere() * RandomRange(1, 4);
            P->IsAlive = true;
            ParticlesToCreateThisFrame--;
        }
    }
    
    // sort particles
    HeapSort(GameState->Particles.CpuParticles, MAX_PARTICLE_COUNT);
    
    
    gpu_particle* GpuParticles = (gpu_particle*)GameState->Particles.Buffer.Data;
    
    
    // clear gpu particles from last frame
    for (uint32 i = 0; i < GameState->Particles.Buffer.Count; i++)
    {
        GpuParticles[i] = {};
    }
    
    // fill gpu particles where its full
    uint32 GpuParticlesCount = 0;
    for (uint32 i = 0; i < MAX_PARTICLE_COUNT; i++)
    {
        particle* P = &GameState->Particles.CpuParticles[i];
        if (P->IsAlive)
        {
            GpuParticles[GpuParticlesCount].Position = P->Position;
            GpuParticles[GpuParticlesCount].Color = P->Color;
            GpuParticlesCount++;
        }
    }
    GameState->Particles.Buffer.Count = GpuParticlesCount;
    
    PlatformAPI->FillBuffer(&GameState->Particles.Buffer);
}

//#define DEBUG_START_WITH_GUN
void GameMode(game_and_editor_memory* Memory, 
              memory_arena* TransientArena,
              platform_api* PlatformAPI,
              render_list* RenderList,
              RENDERDOC_API_1_1_1* RenderDocAPI,
              game_input* Input, game_playback PlaybackState)
{
    YamProf(GameMode);
    
    game_state* GameState = &Memory->GameState;
    
    if (GameState->GameplayStartTrigger)
    {
        //TODO(james): these should be reseting when Play/Stop?!?!
        GameState->ElapsedTime = 0;
        GameState->LastFlipTime = 0;
        
        InitCollision(&GameState->CollisionGroup);
        GameState->CollisionGroup.GameState = GameState;
        
        GameState->GameplayStartTrigger = false;
    }
    
    RenderList->Assets->Lines.Vertices.Count = 0;
    RenderList->Assets->Lines.Elements.Count = 0;
    RenderList->Assets->Lines.Colors.Count = 0;
    
    bool32 GamePlaying = (PlaybackState == GamePlayback_Playing);
    
    if (GamePlaying)
    {
        UpdateParticles(GameState, Input, PlatformAPI);
        if (!GameState->DisplayingOptionsMenu)
        {
            GameState->ElapsedTime += Input->DeltaTime;
        }
    }
    
    //
    // filling depthmap
    //
    FboBegin(RenderList, &GameState->Depthmap);
    ClearDepth(RenderList);
    CullFaceMode(RenderList, CullFaceMode_Front);
    
    matrix4 LightView = CameraLookAt(GameState->DirectionalLight->Right, 
                                     GameState->DirectionalLight->Up, 
                                     GameState->DirectionalLight->Forward, 
                                     GameState->PlayerCamera->Position);
    
    // TODO(james): as the depth increases, the difference between near and far plane,
    // the precision of the depth map lessens, maybe the shadow map needs to be range
    // adjusted after creation?
    matrix4 LightProjection = Ortho(-2, 2, -2, 2, -2, 2);
    
    SetViewProjection(RenderList, LightView, LightProjection);
    
    RenderList->Light.LightMatrix = LightProjection * LightView;
    RenderList->Light.LightDir = GameState->DirectionalLight->Forward;
    RenderList->Light.LightColor = v4_WHITE;
    RenderList->Light.Shadowmap = &GameState->Depthmap;
    
    {
        linked_list_member* Member = GameState->EntityMaster.FirstMember;
        while (Member != NULL)
        {
            entity* Entity = (entity*)Member->Payload;
            Member = Member->NextNode;
            if (!Entity->EditorOnly && Entity->Material.CastShadows && Entity->RenderMesh != NULL)
            {
                DrawEntityToDepthmap(RenderList, Entity,  GameState->DirectionalLight);
                
            }
        } 
    }
    CullFaceMode(RenderList, CullFaceMode_Back);
    FboEnd(RenderList);
    
    v2 ViewportDim = GetDim(RenderList->Viewport);
    RenderList->CurrentProjection = Perspective(67, (ViewportDim.x / ViewportDim.y), 0.1f, 100.0f);
    
    //DrawAllAABBs(&GameState->CollisionGroup, RenderList, v4_WHITE );
    
    if (!Memory->EditorMode) 
    {
        if (GamePlaying)
        {
            if (Input->Controllers[0].Quit.DownThisFrame)
            {
                GameState->DisplayingOptionsMenu = true;
                Input->Controllers[0].Quit.DownThisFrame =false;//NOTE(james): so we don't turn it off later down the function
                PlatformAPI->ShowCursor();
            }
            
            if (!Memory->ShowUI)// should hold right mouse in this case?
            {
                PlayerFPSMovement(GameState->PlayerCamera, &RenderList->CurrentView,
                                  &Input->Controllers[0], Input->DeltaTime);
                PlayerFPSRotation(GameState->PlayerCamera,
                                  &Input->Controllers[0], Input->DeltaTime, 
                                  GameState->PlayerSettings.MouseSensitivity);
            }
            
            v3 MoveDirection = v3_ZERO;
            
            MoveDirection += GameState->PlayerCamera->Forward * Input->Controllers[0].MoveForwardAxis;
            MoveDirection += GameState->PlayerCamera->Right * Input->Controllers[0].MoveHorizontalAxis;
            
            GameState->MoveDirectionXZ = V2(0,0);
            GameState->MoveDirectionXZ.x = MoveDirection.x;
            GameState->MoveDirectionXZ.y = MoveDirection.z;
            GameState->JumpHit = Input->Controllers[0].Spacebar.IsDown;
            
            
            GameState->CollisionGroup.FrameDeltaTime = Input->DeltaTime;
            
            UpdateCollisionAABBs(&GameState->CollisionGroup, false);
            
            ResolveCollision(RenderList, &GameState->CollisionGroup,false, false, &GameState->EntityMaster);
            
        }
        RenderList->CurrentView = GameState->PlayerCamera->LocalToWorld;
    }
    else
    {
        EditorCam(Memory, TransientArena,
                  PlatformAPI,
                  RenderDocAPI,
                  RenderList, Input);
        RenderList->CurrentView = Memory->EditorState.EditorCamera->LocalToWorld;
    }
    
    // Resolve pending deletions, do them all in one shot
    linked_list_member* _Member = NULL;
    entity* _Entity = NULL;
    while (PROGRESS_LINKED_LIST(&GameState->EntityMaster, _Member, _Entity))
    {
        if (_Entity->DeletionPending)
        {
            DeleteEntity(_Entity, &GameState->EntityMaster, &GameState->CollisionGroup);
        }
    }
    
    ImGuiMatrix4(RenderList->CurrentView);
    
    SetViewProjection(RenderList, RenderList->CurrentView, RenderList->CurrentProjection);
    RenderList->CameraForward = GameState->PlayerCamera->Forward;
    
    GameState->PlayerViewProjection = RenderList->CurrentViewProjection;
    
#if FMOD_SUPPORT
    FMOD_3D_ATTRIBUTES PlayerAttributes = { { 0 } }; //init to zero?
    PlayerAttributes.position = V3ToFmodVector(GameState->Player->Position);
    PlayerAttributes.velocity = V3ToFmodVector(GameState->Player->Configs[GameState->Player->SourceConfig].LinearVelocity);
    PlayerAttributes.forward = V3ToFmodVector(-GameState->PlayerCamera->Forward);
    PlayerAttributes.up = V3ToFmodVector(GameState->PlayerCamera->Up);
    int PlayerIndex = 0; // TODO(james): adjust for multiplayer
    
    Fmod->API.Studio_System_SetListenerAttributes(Fmod->StudioSystem, PlayerIndex, &PlayerAttributes);
#endif
    
    //
    // drawing for real
    //
    // TODO(james): loop all entities, pushing any with collision stuff
    FboBegin(RenderList, &GameState->GameFbo);
    ClearColor(RenderList, v4_GREY);
    ClearDepth(RenderList);
    {
        YamProf(DrawEntities);
        linked_list_member* Member = GameState->EntityMaster.FirstMember;
        while (Member != NULL)
        {
            entity* Entity = (entity*)Member->Payload;
            Member = Member->NextNode;
            if (!Entity->EditorOnly)
            {
                DrawEntity(RenderList, Entity, GameState->ElapsedTime);
            }
        } 
    }
    
    EditorMode(Memory, TransientArena,
               PlatformAPI,
               RenderDocAPI,
               RenderList, Input);
    
    DrawAllLines(RenderList, PlatformAPI);
    
    //
    // draw particles
    //
    if (GameState->Particles.Buffer.Count > 0)
        DrawParticles(RenderList, &GameState->Particles.Buffer, GameState->PlayerCamera);
    
    
    if (Memory->EditorMode)
    {
        //
        // Xray'd Stuff
        //
        ClearDepth(RenderList);
        
        DrawEditor3dOverlays(Memory, RenderList);
        FillEditorPostProcessOverlays(Memory, RenderList);
    }
    
    //
    // 2D Stuff
    //
    matrix4 Identity = IdentityMatrix();
    matrix4 OrthoProjection = {
        2.0f / ViewportDim.x, 0.0f, 0.0f, -1,
        0.0f, 2.0f / ViewportDim.y, 0.0f, -1,
        0.0f, 0.0f, -1.0f, 0,
        0, 0, 0, 1.0f,
    };
    
    SetViewProjection(RenderList, Identity, OrthoProjection);
    if (GameState->DisplayingOptionsMenu)
    {
        UpdateAndDrawPlayerSettings(RenderList, Input, GameState, PlatformAPI);
        
        if (Input->Controllers[0].Quit.DownThisFrame)
        {
            GameState->DisplayingOptionsMenu = false;
            PlatformAPI->HideCursor(GetCenter(RenderList->Viewport));
            //reset any non-saved changes that occurred
            GameState->OptionsPlayerSettings = GameState->PlayerSettings;
        }
    }
    
    char FrameDuration[10];
    stbsp_snprintf(FrameDuration, 10, "%.3f", Input->DeltaTime * 1000);
    
    //DrawText(RenderList, FrameDuration, 0, 0, &RenderList->Assets->Shaders.FontShader, &GameState->Font, 1);
    if (Input->IsRecording)
    {
        real32 YPos = 0;//RenderList->Viewport.Max.y - 20;
        //DrawRect(RenderList, GameState->ParticleTexture, RectMinDim(0,YPos,20,20), RectMinDim(0,0,1,1),v4_RED);
    }
    else if (Input->IsPlayingBack)
    {
        real32 YPos = 0;//RenderList->Viewport.Max.y - 20;
        DrawRect(RenderList, GameState->ParticleTexture, RectMinDim(0,YPos,20,20), RectMinDim(0,0,1,1),v4_GREEN);
    }
    //DrawRect(RenderList, GameState->ParticleTexture, RectMinDim(0,100,20,20), RectMinDim(0,0,1,1),v4_WHITE);
    
#if FMOD_SUPPORT
    ERRCHECK(Fmod->API.Studio_System_Update(Fmod->StudioSystem));
#endif
    
    FboEnd(RenderList);//gamefbo
    
    FboBegin(RenderList, &GameState->GameAAResolved);
    {
        DrawRectMultisampled(RenderList, &GameState->GameFbo.Texture);
    }
    FboEnd(RenderList);
    
    FboBegin(RenderList, &GameState->FinalGameFbo);
    {
        ClearColor(RenderList, v4_RED);
        DrawRect(RenderList, &GameState->GameAAResolved.Texture, TextureRect(&GameState->GameAAResolved.Texture));
        DrawEditorPostProcessOverlays(Memory, RenderList);
        //DrawRectMultisampled(RenderList, &GameState->GameFbo.Texture);
        //DrawRect(RenderList, &GameState->PostProcessA.Texture, TextureRect(&GameState->PostProcessA.Texture));
    }
    FboEnd(RenderList);
    
    DrawRect(RenderList, &GameState->FinalGameFbo.Texture,
             TextureRect(&GameState->FinalGameFbo.Texture));
}

////////////////////////////////////////////////////
// MAIN
////////////////////////////////////////////////////
extern "C" GAME_UPDATE_AND_RENDER(GameUpdateAndRender)
{
    v2 a1 = V2(-1,0);
    v2 a2 = V2(1,0);
    
    v2 b1 = V2(0,-1);
    v2 b2 = V2(0,1);
    
    
    game_and_editor_memory* GameAndEditorMemory = (game_and_editor_memory*)Memory->PermanentStorage;
    
    memory_arena TransientArena;
    InitializeArena(&TransientArena, Memory->TransientStorageSize, (uint8*)Memory->TransientStorage);
    
    WorkQueue = Memory->WorkQueue;
    platform_api* PlatformAPI = &Memory->PlatformAPI;
    
    if (!GameAndEditorMemory->GameAndEditorMemoryInitialized)
    {
        YamProfiling = Memory->YamProfiling;
        YamProfBegin(GameStart);
        
        game_state* GameState = &GameAndEditorMemory->GameState;
        editor_state* EditorState = &GameAndEditorMemory->EditorState;
        
        memory_arena TotalArena;
        InitializeArena(&TotalArena, Memory->PermanentStorageSize - sizeof(game_and_editor_memory),
                        (uint8*)Memory->PermanentStorage + sizeof(game_and_editor_memory));
        
        SubArena(&GameState->GameArena, &TotalArena, Megabytes(1), AlignNoClear(4));
        SubArena(&GameAndEditorMemory->PreservedGameStateArena, &TotalArena, Megabytes(1), AlignNoClear(4));
        
        InitLinkedList(&GameState->EntityMaster, &GameState->GameArena, 200, true, sizeof(entity));
        InitLinkedList(&GameAndEditorMemory->PreservedGameState.EntityMaster, &GameAndEditorMemory->PreservedGameStateArena, 200, true, sizeof(entity));
        
        SubArena(&EditorState->UndoStackMemory, &TotalArena, Megabytes(16), AlignNoClear(4));
        
        //InitLinkedList(&GameAndEditorMemory->PreservedEntityMaster, &TotalArena, 200, true, sizeof(entity));
        
        memory_arena CollisionArena;//for linked list that contains all collision objects
        
        memory_arena VisualAssetArena;
        
        {
            YamProf(SubArena);
            SubArena(&CollisionArena, &TotalArena, Megabytes(1), AlignNoClear(4));
            SubArena(&GameState->CollisionGroup.PermanentColliderVertexArena, &TotalArena, 
                     Megabytes(1), AlignNoClear(4));
            SubArena(&GameState->CollisionGroup.TransientColliderVertexArena, &TotalArena, 
                     Megabytes(1), AlignNoClear(4));
            
            SubArena(&VisualAssetArena, &TotalArena, GetArenaSizeRemaining(&TotalArena, AlignNoClear(4)));
        }
        
        
        temporary_memory TempMem = BeginTemporaryMemory(&TransientArena);
        
        GameAndEditorMemory->EditorState.ImGuiFontInfo = {};
        ImGuiSetup(&GameAndEditorMemory->EditorState.ImGuiFontInfo);
        PlatformAPI->ImGuiSetup(&GameAndEditorMemory->EditorState.ImGuiFontInfo);
        
        EditorState->EditName.Text = EditorState->__EditNameMemory;
        EditorState->EditName.Size = 128;
        EditorState->TranslationSnappingInterval = 0.5f;
        EditorState->RotationSnappingInterval = PI / 10;
        EditorState->WorldSpaceDragging = false;
        EditorState->RotateDragger = true;
        EditorState->WorldSpaceRotating = true;
        EditorState->GamePlaybackState = GamePlayback_Stopped;
        EditorState->UndoStackPosition = -1;
        
        GameState->SimulationSpeed = 1;
        
        InitLinkedList(&GameState->CollisionGroup.Entries, &CollisionArena, 160);
        GameAndEditorMemory->PreservedCollisionEntriesMemory = PushArray(&CollisionArena, GameState->CollisionGroup.Entries.MemoryEntriesCount, linked_list_member);
        
        ZeroArray(CollisionLayer_Count * CollisionLayer_Count, GameState->CollisionGroup.CollisionRules);
        // self colliding
        AddCollisionRule(&GameState->CollisionGroup, CollisionLayer_Default, CollisionLayer_Default, true);
        
        AddCollisionRule(&GameState->CollisionGroup,
                         CollisionLayer_PlayerBullet, CollisionLayer_Enemy, true);
        AddCollisionRule(&GameState->CollisionGroup,
                         CollisionLayer_PlayerBullet, CollisionLayer_Default, true);
        AddCollisionRule(&GameState->CollisionGroup,
                         CollisionLayer_PlayerBullet, CollisionLayer_BulletKillZone, true);
        
        AddCollisionRule(&GameState->CollisionGroup, CollisionLayer_Player, CollisionLayer_Default, true);
        
        AddCollisionRule(&GameState->CollisionGroup,
                         CollisionLayer_EnemyBullet, CollisionLayer_BulletKillZone, true);
        AddCollisionRule(&GameState->CollisionGroup,
                         CollisionLayer_EnemyBullet, CollisionLayer_Default, true);
        AddCollisionRule(&GameState->CollisionGroup,
                         CollisionLayer_EnemyBullet, CollisionLayer_Player, true);
        
        AddCollisionRule(&GameState->CollisionGroup,
                         CollisionLayer_Enemy, CollisionLayer_Player, true);
        AddCollisionRule(&GameState->CollisionGroup,
                         CollisionLayer_Enemy, CollisionLayer_Default, true);
        
#if YAMGINE_CLIENT
        InitializeClientNetworking(&GameState->NetworkState, &Memory->PlatformAPI);
#endif
        
#if YAMGINE_SERVER
        InitializeServerNetworking(&GameState->NetworkState, &Memory->PlatformAPI);
#endif
        
        /*
        EditorState->EditorCamera = CreateEntity(GameState->EntityMaster, "EditorCamera");
        EditorState->EditorCamera->ShowInSceneGraph = false;
        EditorState->EditorCamera->SerializeToFile = true;
        SetLocalPosition(EditorState->EditorCamera, V3(0,3,-5));
        */
        
        PlatformAPI->CreateBuffer(&GameState->Particles.Buffer, CHANNEL_PARTICLE_CENTER, CHANNEL_PARTICLE_CENTER_WIDTH + CHANNEL_PARTICLE_COLOR_WIDTH,  MeshResidency_Stream);
        GameState->Particles.Buffer.Data= PushArray(&VisualAssetArena, MAX_PARTICLE_COUNT, gpu_particle);
        GameState->Particles.Buffer.Count = 0;
        
        GameState->RenderAssets = {};
        InitLinkedList(&GameState->RenderAssets.Meshes, &VisualAssetArena, 32);
        InitLinkedList(&GameState->RenderAssets.Models, &VisualAssetArena, 32);
        LoadStandardAssets(&GameState->RenderAssets, &VisualAssetArena, &Memory->PlatformAPI);
        
        GameState->RenderAssets.Lines.IsIndexedMesh = true;
        PlatformAPI->CreateBuffer(&GameState->RenderAssets.Lines.Vertices, CHANNEL_POSITION, CHANNEL_POSITION_WIDTH,  MeshResidency_Stream);
        GameState->RenderAssets.Lines.Vertices.Data = PushArray(&VisualAssetArena, MAX_LINE_VERTS, v3);
        PlatformAPI->CreateBuffer(&GameState->RenderAssets.Lines.Elements, CHANNEL_ELEMENT, CHANNEL_ELEMENT_WIDTH,  MeshResidency_Stream);
        GameState->RenderAssets.Lines.Elements.Data =PushArray(&VisualAssetArena, MAX_LINE_VERTS * 2, uint32);
        PlatformAPI->CreateBuffer(&GameState->RenderAssets.Lines.Colors, CHANNEL_COLOR, CHANNEL_COLOR_WIDTH,  MeshResidency_Stream);
        GameState->RenderAssets.Lines.Colors.Data = PushArray(&VisualAssetArena, MAX_LINE_VERTS, v4);
        
        
        GameState->CollisionGroup.ColliderCube = GameState->RenderAssets.StandardAssets.ColliderCube;
        
        LoadCustomAssets(&GameState->RenderAssets,
                         &VisualAssetArena, &TempMem,
                         &Memory->PlatformAPI);
        GameState->RenderAssets.StandardAssets.Sphere = (mesh*)GameState->RenderAssets.LoadedAssets.Sphere.Child;
        
        YamProfBegin(LoadTextures);
        
        GameState->Big1Texture = LoadTexture("media/1.bmp",
                                             &VisualAssetArena, &GameState->RenderAssets, &TempMem, &Memory->PlatformAPI);
        
        GameState->Big2Texture = LoadTexture("media/2.bmp",
                                             &VisualAssetArena, &GameState->RenderAssets, &TempMem, &Memory->PlatformAPI);
        
        GameState->Big3Texture = LoadTexture("media/3.bmp",
                                             &VisualAssetArena, &GameState->RenderAssets, &TempMem, &Memory->PlatformAPI);
        
        GameState->Big4Texture = LoadTexture("media/4.bmp",
                                             &VisualAssetArena, &GameState->RenderAssets, &TempMem, &Memory->PlatformAPI);
        
        GameState->ParticleTexture = LoadTexture("media/circle.bmp",
                                                 &VisualAssetArena, &GameState->RenderAssets, &TempMem, &Memory->PlatformAPI);
        GameState->SplashScreenTexture = LoadTexture("media/splash_screen.bmp",
                                                     &VisualAssetArena, &GameState->RenderAssets, &TempMem, &Memory->PlatformAPI);
        
        LoadFont(&GameState->Font, &VisualAssetArena, &GameState->RenderAssets, &TempMem, &Memory->PlatformAPI);
        
        PlatformAPI->CompleteAllWork(WorkQueue);
        
        YamProfEnd();
        
        GameState->GameFbo = PlatformAPI->CreateFbo(1920, 1080, 
                                                    FboOptions_Color|FboOptions_Depth|FboOptions_Multisample, "BaseGame");
        GameState->FullScreenFbos[GameState->FullScreenFboCount++] = &GameState->GameFbo;
        
        GameState->GameAAResolved = PlatformAPI->CreateFbo(1920, 1080, FboOptions_Color, "Game AA Resolved");
        AddTexture(&GameState->RenderAssets, &GameState->GameAAResolved.Texture);
        GameState->FullScreenFbos[GameState->FullScreenFboCount++] = &GameState->GameAAResolved;
        
        GameState->FinalGameFbo = PlatformAPI->CreateFbo(1920, 1080, FboOptions_Color, "Final game fbo");
        AddTexture(&GameState->RenderAssets, &GameState->FinalGameFbo.Texture);
        GameState->FullScreenFbos[GameState->FullScreenFboCount++] = &GameState->FinalGameFbo;
        
        GameState->PostProcessA = PlatformAPI->CreateFbo(1920, 1080, FboOptions_Color|FboOptions_Alpha, "Post process A");
        AddTexture(&GameState->RenderAssets, &GameState->PostProcessA.Texture);
        GameState->FullScreenFbos[GameState->FullScreenFboCount++] = &GameState->PostProcessA;
        
        GameState->PostProcessB = PlatformAPI->CreateFbo(1920, 1080, FboOptions_Color, "Post process B");
        AddTexture(&GameState->RenderAssets, &GameState->PostProcessB.Texture);
        GameState->FullScreenFbos[GameState->FullScreenFboCount++] = &GameState->PostProcessB;
        
        EditorState->SelectionFrame = PlatformAPI->CreateFbo(1920, 1080, FboOptions_Color|FboOptions_Alpha, "Editor Selection Frame");
        AddTexture(&GameState->RenderAssets, &EditorState->SelectionFrame.Texture);
        
        GameState->Depthmap = PlatformAPI->CreateFbo(2048, 2048, FboOptions_Depth, "Game Lighting depthmap");
        AddTexture(&GameState->RenderAssets, &GameState->Depthmap.DepthTexture);
        
        char* Header = LoadTextFile("../source/code/shaders/shader_uniforms.h", &TempMem, &Memory->PlatformAPI);
        
        
        // name of file in source/shaders
        // shader_uniforms.h
        // preprocessor defs, for enabling/disabling proper uniforms for this shader/shader variant
        // destination folder/name for generated shaders
        // size of the uniform block
        // temporary memoryk/
        // platform api
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.TintedTexture,
            "textured_rect", Header, 
            "#define TEXTURED_RECT_UNIFORMS\n", 
            "shaders/generated/textured_rect",  
            sizeof(textured_rect_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.TintedTexture);
        
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.Glyph,
            "msdf_glyph", Header,
            "#define TEXTURED_RECT_UNIFORMS\n", 
            "shaders/generated/msdf_glyph",
            sizeof(textured_rect_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.Glyph);
        
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.MultisampleRect,
            "multisample_texture_rect", Header, 
            "#define MULTISAMPLE_TEXTURE_RECT_UNIFORMS\n", 
            "shaders/generated/multisample_texture_rect",
            sizeof(multisample_texture_rect_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.MultisampleRect);
        
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.MultisampleRectDepth,
            "multisample_depth_rect", Header, 
            "#define MULTISAMPLE_TEXTURE_RECT_UNIFORMS\n", 
            "shaders/generated/multisample_depth_rect",
            sizeof(multisample_texture_rect_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.MultisampleRectDepth);
        
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.PerVertexColor,
            "basic_line", Header, "#define BASIC_LINE_UNIFORMS\n", 
            "shaders/generated/basic_line",  
            sizeof(line_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.PerVertexColor);
        
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.UniformColor,
            "basic_line_uniform_color", Header,
            "#define BASIC_LINE_UNIFORM_COLOR_UNIFORMS\n", 
            "shaders/generated/basic_line_uniform_color",  
            sizeof(line_uniform_color_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.UniformColor);
        
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.MeshFillDepthmap,
            "fill_depthmap", Header, "#define FILL_DEPTHMAP_UNIFORMS\n", 
            "shaders/generated/fill_depthmap",  
            sizeof(fill_depthmap_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.MeshFillDepthmap);
        
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.MeshRecieveShadows,
            "new_mesh", Header,
            "#define MESH_RECIEVE_SHADOWS_UNIFORMS\n #define TEXTURE 0\n #define RECIEVE_SHADOWS 1\n", 
            "shaders/generated/mesh_recieve_shadows",  
            sizeof(mesh_recieve_shadows_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.MeshRecieveShadows);
        
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.Sobel,
            "sobel_post", Header, "#define SOBEL_UNIFORMS\n", 
            "shaders/generated/sobel",  
            sizeof(sobel_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.Sobel);
        
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.GaussianBlur,
            "gaussian_blur_post", Header, "#define GAUSSIAN_BLUR_UNIFORMS\n", 
            "shaders/generated/gaussian_blur_post",  
            sizeof(gaussian_blur_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.GaussianBlur);
        
        PlatformAPI->CreateShader(
            &GameState->RenderAssets.Shaders.Particles,
            "particles", Header, "#define BASIC_LINE_UNIFORMS\n", 
            "shaders/generated/particles",  
            sizeof(gaussian_blur_uniforms), 
            &TempMem, &Memory->PlatformAPI);
        PlatformAPI->WatchForFileChanges(&GameState->RenderAssets.Shaders.Particles);
#if 0
        GameList->LinesMesh = CreateDynamicLinesMesh(&VisualAssetArena, &Memory->PlatformAPI);
        GameList->LinesVertsCount = 0;
#endif
        
        //GameState->DynamicQuad = CreateQuadMesh(MeshResidency_Stream, &VisualAssetArena, &Memory->PlatformAPI);
        
        //GameState->RenderAssets.LoadedAssets.Cractus.Material.Texture = GameState->TestTga;
        //LoadModel(MyFbxManager, &GameState->Sphere, "media/GipsyGuitar.fbx",
        //    &VisualAssetArena, &GameState->TransformArena, &Memory->PlatformAPI);
        
        //give models custom transforms
        //GameState->Sword = CreateEntity(GameState->EntityMaster, "Sword");
        //GameState->Sword->RenderModel = &GameState->RenderAssets.LoadedAssets.Sword;
        
        EditorState->EntityMaster = &GameState->EntityMaster;
        EditorState->CollisionGroup = &GameState->CollisionGroup;
        EditorState->RenderAssets = &GameState->RenderAssets;
        EditorState->GameWindowForcedAspectRatio = 1920.0f/1080.0f;
        
        GameState->CollisionGroup.Gravity = -9.81f * 0.001f; //TODO(james): expose this in editor
        
        CreateCoordinateSystemHandle(EditorState);
        
        if (PlatformAPI->FileExists("serialization/player_settings.txt"))
        {
            LoadPlayerSettings(&GameState->PlayerSettings, &TempMem, &Memory->PlatformAPI);
        }
        else
        {
            GameState->PlayerSettings.MouseSensitivity = 0.1f;
            GameState->PlayerSettings.Vsync = true;
            GameState->PlayerSettings.Fullscreen = false;
            GameState->PlayerSettings.WindowPosition = V2(0,0);
            
            SavePlayerSettings(&GameState->PlayerSettings, &Memory->PlatformAPI);
        }
        
        if (!IsInRectangle(RectMinDim(v2_ZERO, Input->ScreenDim), GameState->PlayerSettings.WindowPosition))
        {
            GameState->PlayerSettings.WindowPosition =v2_ZERO;
        }
        
        GameState->OptionsPlayerSettings = GameState->PlayerSettings;
        PlatformAPI->SetVsync(GameState->PlayerSettings.Vsync);
        PlatformAPI->SetFullscreen(GameState->PlayerSettings.Fullscreen);
        GameState->PlayerSettings.WindowPosition.y = 0;
        rectangle2 DesiredWindowRect = RectMinDim(GameState->PlayerSettings.WindowPosition, Input->ApplicationDim);
        
        PlatformAPI->SetWindowPosition(DesiredWindowRect);
        
        if (PlatformAPI->FileExists("serialization/editor_settings.txt"))
        {
            FindAllScenes(EditorState, PlatformAPI);
            LoadEditorSettings(&EditorState->EditorSettings, &TempMem, &Memory->PlatformAPI);
            for (uint32 SceneIndex = 0;
                 SceneIndex < EditorState->SceneCount;
                 SceneIndex++)
            {
                if (StringsAreEqualAbsolute(EditorState->Scenes[SceneIndex].Name, EditorState->EditorSettings.LastOpenScene))
                {
                    EditorState->CurrentScene = &EditorState->Scenes[SceneIndex];
                    break;
                }
            }
        }
        else
        {
            FindAllScenes(EditorState, PlatformAPI);
            EditorState->CurrentScene = &EditorState->Scenes[0];
        }
        
        
        LoadScene(EditorState->CurrentScene, GameState, EditorState, &TempMem, PlatformAPI);
        GameState->Player = FindEntity(&GameState->EntityMaster, "/Player");
        Assert(GameState->Player);
        SetDirty(GameState->Player);
        ResolveLocalToWorld(GameState->Player);
        
        
        //TODO(james): make this part of the deserialization process, ptr is lost on reload
        GameState->Player->PhysicsUpdate = PlayerPhysicsUpdate;
        
        GameState->DirectionalLight = FindEntity(&GameState->EntityMaster, "/DirectionalLight");
        
        
#if FMOD_SUPPORT
        GameState->Fmod = &Memory->Fmod;
        ERRCHECK(
            Memory->Fmod.API.Studio_EventDescription_CreateInstance(
            Memory->Fmod.Events.MouseClick, &GameState->Player->ShootInstance));
        
        ERRCHECK(
            Memory->Fmod.API.Studio_EventDescription_CreateInstance(
            Memory->Fmod.Events.PlayerDeath, &GameState->PlayerDeathInstance));
        
        ERRCHECK(Memory->Fmod.API.Studio_EventDescription_CreateInstance(Memory->Fmod.Events.Music,
                                                                         &GameState->MusicInstance));
        
        ERRCHECK(Memory->Fmod.API.Studio_EventDescription_CreateInstance(
            Memory->Fmod.Events.PlayerJump, &GameState->JumpInstance));
        
        ERRCHECK(Memory->Fmod.API.Studio_EventDescription_CreateInstance(
            Memory->Fmod.Events.Step, &GameState->StepInstance));
        
        ERRCHECK(Memory->Fmod.API.Studio_EventDescription_CreateInstance(
            Memory->Fmod.Events.WeaponPickup, &GameState->WeaponPickupInstance));
#endif
        
        GameState->WalkingStepFrequency = 0.7f;
        GameState->RunningStepFrequency = 0.25f;
        
        GameState->WalkingVelocityThreshold = 0.1f;
        GameState->RunningVelocityThreshold = 6.5f;
        
        GameState->PlayerCamera = FindEntity(&GameState->EntityMaster, "/Player/PlayerCamera");
        Assert(GameState->PlayerCamera);
        //TODO(james): it'd be good if these weren't necessary... just load the right stuff!
        PlayerFPSMovement(GameState->PlayerCamera, &GameList->CurrentView,
                          &Input->Controllers[0], 0);
        PlayerFPSRotation(GameState->PlayerCamera, &Input->Controllers[0], 0, 0);
        
        EditorState->EditorCamera = FindEntity(&GameState->EntityMaster, "/EditorCamera");
        PlayerFPSRotation(EditorState->EditorCamera, &Input->Controllers[0], 0, 0);
        
#if !YAMGINE_EDITOR //start playing immediately
        GameAndEditorMemory->GameState.GameplayStartTrigger = true;
        GameAndEditorMemory->GamePlaying = true;
        
#if FMOD_SUPPORT
        ERRCHECK(Memory->Fmod.API.Studio_EventInstance_Start(GameAndEditorMemory->GameState.MusicInstance));
#endif
#endif
        PlatformAPI->CompleteAllWork(Memory->WorkQueue);
        //
        // Postamble
        //
        SaveGameState(GameAndEditorMemory);
        
        GameAndEditorMemory->GameAndEditorMemoryInitialized = true;
        
        EndTemporaryMemory(TempMem);
        
        YamProfEnd(); ////////////////////////////////////////////////
    } //End of Init
    
    //////////////////////////////////////
    //
    // GAME LOOP
    //
    //////////////////////////////////////
    YamProfiling = Memory->YamProfiling;
    YamProf(DATE_AND_RENDER); //TODO(james): UPDATE_AND_RENDER conflicts with Win32Tick, causes problems currently
    
    GameAndEditorMemory->ElapsedTimeSinceAppStart += Input->DeltaTime;
    
    if (Memory->ExecutableReloaded)
    {
        ImGuiSetup(&GameAndEditorMemory->EditorState.ImGuiFontInfo);
        PlatformAPI->ImGuiSetup(&GameAndEditorMemory->EditorState.ImGuiFontInfo);
    }
    
    GameList->Assets = &GameAndEditorMemory->GameState.RenderAssets;
    
    game_state* GameState = &GameAndEditorMemory->GameState;
    editor_state* EditorState = &GameAndEditorMemory->EditorState;
    
    shader* ChangedFile = PlatformAPI->DumpChangedFileNotifications();
    while (ChangedFile != NULL)
    {
        temporary_memory TempMem = BeginTemporaryMemory(&TransientArena);
        char* Header = LoadTextFile("../source/code/shaders/shader_uniforms.h", &TempMem, &Memory->PlatformAPI);
        
        PlatformAPI->CreateShader(
            ChangedFile,
            ChangedFile->FileName.Text, Header,
            ChangedFile->PreprocessorHeader.Text, 
            ChangedFile->CompiledPath.Text, 
            ChangedFile->UniformsStructSize, 
            &TempMem, &Memory->PlatformAPI);
        
        ChangedFile = PlatformAPI->DumpChangedFileNotifications();
        EndTemporaryMemory(TempMem);
    }
    
    UpdateInputAxis(&Input->Controllers[0]);
    
    if (EditorState->GamePlaybackState == GamePlayback_Stopped && Input->Controllers[0].Quit.IsDown)
    {
        Input->QuitRequested = true;
        SavePlayerSettings(&GameState->PlayerSettings, &Memory->PlatformAPI);
        SaveEditorSettings(EditorState, &Memory->PlatformAPI);
    }
    
    Assert(Input->ApplicationDim.x > 0);
    Assert(Input->ApplicationDim.y > 0);
    ImGuiNewFrame(Input->ApplicationDim, &Input->Controllers[0], Input->DeltaTime);
    
    if (Input->Controllers[0].ToggleGUI.DownThisFrame)
    {
        if (GameAndEditorMemory->EditorMode)
        {
            GameAndEditorMemory->EditorMode = false;
        }
        else
        {
            PlatformAPI->ShowCursor();
            GameAndEditorMemory->EditorMode = true;
        }
    }
    
    if (Input->Controllers[0].TabKey.DownThisFrame)
    {
        if (GameAndEditorMemory->ShowUI)
        {
            PlatformAPI->HideCursor(Input->Controllers[0].MousePosition);
            GameAndEditorMemory->ShowUI = false;
        }
        else
        {
            PlatformAPI->ShowCursor();
            GameAndEditorMemory->ShowUI = true;
        }
    }
    
    if ((Input->Controllers[0].PlayPause.DownThisFrame) || GameState->QuitRequested)
    {
        if (EditorState->GamePlaybackState == GamePlayback_Stopped)
        {
            EditorState->GamePlaybackState = GamePlayback_Playing;
        }
        else if (EditorState->GamePlaybackState == GamePlayback_Playing)
        {
            EditorState->GamePlaybackState = GamePlayback_Stopped;
        }
        else
        {
            Assert(0);// came into here while paused?!
        }
        
        if (EditorState->GamePlaybackState == GamePlayback_Playing)
        {
            SaveGameState(GameAndEditorMemory);
            
            GameState->GameplayStartTrigger = true;
            
#if FMOD_SUPPORT
            ERRCHECK(GameState->Fmod->API.Studio_EventInstance_Start(GameState->MusicInstance));
#endif
            
#if WWISE_SUPPORT
            /*
            AKRESULT RegResult = Memory->Wwise.API.RegisterGameObj((AkGameObjectID)GameState->Player,
                                                                   "Player",
                                                                   0x01
                                                                   );
            AkPlayingID PostEventResult= WwisePostEvent(&Memory->Wwise.API, AK::EVENTS::IM_START, (AkGameObjectID)GameState->Player);
            AKRESULT SetStateResult = Memory->Wwise.API.SetState(AK::STATES::MUSIC::GROUP, AK::STATES::MUSIC::STATE::EXPLORING);
            */
#endif
        }
        else
        {
#if FMOD_SUPPORT
            if (GameState->MusicInstance != NULL)
            {
                ERRCHECK(GameState->Fmod->API.Studio_EventInstance_Stop(GameState->MusicInstance, FMOD_STUDIO_STOP_ALLOWFADEOUT));
            }
#endif
            LoadGameState(GameAndEditorMemory);
            PlatformAPI->ShowCursor();
        }
    }
    
    GameList->Viewport = RectMinDim(V2(0, 0), V2(Input->ApplicationDim.x, Input->ApplicationDim.y));
    
    v2 ViewportDim = GetDim(GameList->Viewport);
    if (GameState->FinalGameFbo.Texture.Width != ViewportDim.x || 
        GameState->FinalGameFbo.Texture.Height != ViewportDim.y)
    {
        for (uint32 FboIndex = 0; FboIndex < GameState->FullScreenFboCount; FboIndex++)
        {
            PlatformAPI->ResizeFbo(GameState->FullScreenFbos[FboIndex], ViewportDim.x, ViewportDim.y);
        }
        
        PlatformAPI->ResizeFbo(&EditorState->SelectionFrame, 
                               Input->ApplicationDim.x, 
                               Input->ApplicationDim.y);
    }
    
    GameMode(GameAndEditorMemory, &TransientArena, &Memory->PlatformAPI,
             GameList, Memory->RenderDocAPI, Input, 
             EditorState->GamePlaybackState);
    
    if (GameAndEditorMemory->ShowUI)
    {
        if (ImGui::Begin("Yam Profiler"))
        {
            local_persist bool IsOn = false;
            ImGui::Checkbox("Start", &IsOn);
            if (IsOn)
            {
                if (Memory->YamReportStart)
                    YamProfilingRender(Memory->YamReportStart);
            }
            else
            {
                if (Memory->YamReport)
                    YamProfilingRender(Memory->YamReport);
            }
        }
        ImGui::End();
        
        if (ImGui::Begin("Console"))
        {
            bool Opened = true;
            MyConsole.Draw("Yamgine Log", &Opened);
        }
        ImGui::End();
        
        BeginStatusBar();
        if (MyConsole.Items.size() > 0)
        {
            ImGui::Text(MyConsole.Items[MyConsole.Items.size()-1]);
        }
        EndStatusBar();
        
        Clicked = Input->Controllers[0].LeftMouseClick.IsDown;
        bool open = true;
        ImGui::SetNextWindowPos(ImVec2(0, 20));
        ImGui::SetNextWindowSize(ImVec2(300, Input->ApplicationDim.y*0.5f-40));
        
        if (ImGui::Begin("Asset Browser",&open,ImVec2(0,0), 0.3f,ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoSavedSettings))
        {
            AssetBrowser(GameAndEditorMemory, &TransientArena, &Memory->PlatformAPI, GameList, Input);
        }
        ImGui::End();
        
        ImGui::SetNextWindowPos(ImVec2(0, 20+Input->ApplicationDim.y*0.5f));
        ImGui::SetNextWindowSize(ImVec2(300, Input->ApplicationDim.y*0.5f-40));
        if (ImGui::Begin("Scene Graph",&open,ImVec2(0,0), 0.3f,ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoSavedSettings))
        {
            SceneGraphGui(&GameState->EntityMaster, &EditorState->SelectedEntity);
        }
        ImGui::End();
        
        
        
        GameList->Viewport = RectMinDim(V2(0,0),Input->ApplicationDim);
        
        ImGui::SetNextWindowPos(ImVec2(Input->ApplicationDim.x-300, 20));
        ImGui::SetNextWindowSize(ImVec2(300, Input->ApplicationDim.y-40));
        if (ImGui::Begin("Properties",&open,ImVec2(0,0), 0.3f,ImGuiWindowFlags_NoResize|ImGuiWindowFlags_NoMove|ImGuiWindowFlags_NoSavedSettings))
        {
            if (EditorState->SelectedEntity)
            {
                EntityPropertiesGui(EditorState, &GameState->RenderAssets, &GameState->CollisionGroup);
            }
        }
        ImGui::End();
    }
    
    if (ImGui::Begin("General"))
    {
        ImGui::Checkbox("Is Recording", &Input->IsRecording);
#if 0
        ImGui::Checkbox("Draw Broadphase Debug", &GameState->DrawBroadphaseViz);
        ImGui::Checkbox("Draw Narrowphase Debug", &GameState->DrawNarrowphaseViz);
        ImGui::Checkbox("Flip Entity Draw Order", &EditorState->FlipDrawEntityOrder);
        ImGui::SliderFloat("Simulation Speed", &GameState->SimulationSpeed, 0, 5);
        
        entity* Player = GameState->Player;
        v3 FullVelocity = Player->Configs[Player->SourceConfig].LinearVelocity;
        v2 VelocityXZ = V2(FullVelocity.x, FullVelocity.z);
        real32 Vel = Length(VelocityXZ);
        ImGui::SliderFloat("Player Velocity", &Vel, 0, 10);
        ImGui::Separator();
        ImGui::SliderFloat("Walking Velocity Threshold", &GameState->WalkingVelocityThreshold, 0, 10);
        ImGui::SliderFloat("Running Velocity Threshold", &GameState->RunningVelocityThreshold, 0, 10);
        ImGui::SliderFloat("Walking Step Frequency", &GameState->WalkingStepFrequency, 0, 2);
        ImGui::SliderFloat("Running Step Frequency", &GameState->RunningStepFrequency, 0, 2);
#endif
        
        
        
        if (EditorState->GamePlaybackState == GamePlayback_Playing)
        {
            ImGui::Text("Playback State: Playing");
        }
        if (EditorState->GamePlaybackState == GamePlayback_Paused)
        {
            ImGui::Text("Playback State: Paused");
        }
        if (EditorState->GamePlaybackState == GamePlayback_Stopped)
        {
            ImGui::Text("Playback State: Stopped");
        }
        
        ImGui::Checkbox("Editor Mode", &GameAndEditorMemory->EditorMode);
        ImGui::Checkbox("Show UI", &GameAndEditorMemory->ShowUI);
    }
    ImGui::End();
    
    
    DrawImGui(GameList);
}
