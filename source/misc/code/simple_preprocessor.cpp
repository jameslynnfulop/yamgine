/* ========================================================================
   $File: $
   $Date: $
   $Revision: $
   $Creator: Casey Muratori $
   $Notice: (C) Copyright 2015 by Molly Rocket, Inc. All Rights Reserved. $
   ======================================================================== */

#include <memory.h>
#include <stdio.h>
#include <stdlib.h>

struct meta_struct
{
    char* Name;
    meta_struct* Next;
};

static meta_struct* FirstMetaStruct;

static char* ReadEntireFileIntoMemoryAndNullTerminate(char* FileName)
{
    char* Result = 0;

    FILE* File = fopen(FileName, "r");
    if (File)
    {
        fseek(File, 0, SEEK_END);
        size_t FileSize = ftell(File);
        fseek(File, 0, SEEK_SET);

        Result = (char*)malloc(FileSize + 1);
        fread(Result, FileSize, 1, File);
        Result[FileSize] = 0;

        fclose(File);
    }

    return (Result);
}

enum token_type
{
    Token_Unknown,

    Token_OpenParen,
    Token_CloseParen,
    Token_Colon,
    Token_Semicolon,
    Token_Asterisk,
    Token_OpenBracket,
    Token_CloseBracket,
    Token_OpenBrace,
    Token_CloseBrace,
    Token_Equals,

    Token_String,
    Token_Identifier,

    Token_EndOfStream,
};

struct token
{
    token_type Type;

    size_t TextLength;
    char* Text;
};

struct tokenizer
{
    char* At;
};

inline bool IsEndOfLine(char C)
{
    bool Result = ((C == '\n') || (C == '\r'));

    return (Result);
}

inline bool IsWhitespace(char C)
{
    bool Result = ((C == ' ') || (C == '\t') || (C == '\v') || (C == '\f') || IsEndOfLine(C));

    return (Result);
}

inline bool IsAlpha(char C)
{
    bool Result = (((C >= 'a') && (C <= 'z')) || ((C >= 'A') && (C <= 'Z')) || C == '_');

    return (Result);
}

inline bool IsNumber(char C)
{
    bool Result = ((C >= '0') && (C <= '9'));

    return (Result);
}

inline bool TokenEquals(token Token, char* Match)
{
    char* At = Match;
    for (int Index = 0; Index < Token.TextLength; ++Index, ++At)
    {
        if ((*At == 0) || (Token.Text[Index] != *At))
        {
            return (false);
        }
    }

    bool Result = (*At == 0);
    return (Result);
}

static void EatAllWhitespace(tokenizer* Tokenizer)
{
    for (;;)
    {
        if (IsWhitespace(Tokenizer->At[0]))
        {
            ++Tokenizer->At;
        }
        else if ((Tokenizer->At[0] == '/') && (Tokenizer->At[1] == '/'))
        {
            Tokenizer->At += 2;
            while (Tokenizer->At[0] && !IsEndOfLine(Tokenizer->At[0]))
            {
                ++Tokenizer->At;
            }
        }
        else if ((Tokenizer->At[0] == '/') && (Tokenizer->At[1] == '*'))
        {
            Tokenizer->At += 2;
            while (Tokenizer->At[0] && !((Tokenizer->At[0] == '*') && (Tokenizer->At[1] == '/')))
            {
                ++Tokenizer->At;
            }

            if (Tokenizer->At[0] == '*')
            {
                Tokenizer->At += 2;
            }
        }
        else
        {
            break;
        }
    }
}

static token GetToken(tokenizer* Tokenizer)
{
    EatAllWhitespace(Tokenizer);

    token Token = {};
    Token.TextLength = 1;
    Token.Text = Tokenizer->At;
    char C = Tokenizer->At[0];
    ++Tokenizer->At;
    switch (C)
    {
        case '\0':
        {
            Token.Type = Token_EndOfStream;
        }
        break;

        case '(':
        {
            Token.Type = Token_OpenParen;
        }
        break;
        case ')':
        {
            Token.Type = Token_CloseParen;
        }
        break;
        case ':':
        {
            Token.Type = Token_Colon;
        }
        break;
        case ';':
        {
            Token.Type = Token_Semicolon;
        }
        break;
        case '*':
        {
            Token.Type = Token_Asterisk;
        }
        break;
        case '[':
        {
            Token.Type = Token_OpenBracket;
        }
        break;
        case ']':
        {
            Token.Type = Token_CloseBracket;
        }
        break;
        case '{':
        {
            Token.Type = Token_OpenBrace;
        }
        break;
        case '}':
        {
            Token.Type = Token_CloseBrace;
        }
        break;
        case '=':
        {
            Token.Type = Token_Equals;
        }
        break;

        case '"':
        {
            Token.Type = Token_String;

            Token.Text = Tokenizer->At;

            while (Tokenizer->At[0] && Tokenizer->At[0] != '"')
            {
                if ((Tokenizer->At[0] == '\\') && Tokenizer->At[1])
                {
                    ++Tokenizer->At;
                }
                ++Tokenizer->At;
            }

            Token.TextLength = Tokenizer->At - Token.Text;
            if (Tokenizer->At[0] == '"')
            {
                ++Tokenizer->At;
            }
        }
        break;

        default:
        {
            if (IsAlpha(C))
            {
                Token.Type = Token_Identifier;

                while (IsAlpha(Tokenizer->At[0]) || IsNumber(Tokenizer->At[0]) || (Tokenizer->At[0] == '_'))
                {
                    ++Tokenizer->At;
                }

                Token.TextLength = Tokenizer->At - Token.Text;
            }
#if 0
            else if(IsNumeric(C))
            {
                ParseNumber();
            }
#endif
            else
            {
                Token.Type = Token_Unknown;
            }
        }
        break;
    }

    return (Token);
}

static bool RequireToken(tokenizer* Tokenizer, token_type DesiredType)
{
    token Token = GetToken(Tokenizer);
    bool Result = (Token.Type == DesiredType);
    return (Result);
}

static bool ParseIntrospectionParams(tokenizer* Tokenizer)
{
    bool Valid = true;

    for (;;)
    {
        token Token = GetToken(Tokenizer);
        if (TokenEquals(Token, "IGNORED"))
        {
            Valid = false;
            break;
        }

        if ((Token.Type == Token_CloseParen) || (Token.Type == Token_EndOfStream))
        {
            break;
        }
    }

    return (Valid);
}

static void ParseMember(tokenizer* Tokenizer, token StructTypeToken,
                        token MemberTypeToken, token BaseTypeToken)
{
#if 1
    bool Parsing = true;
    bool IsPointer = false;
    while (Parsing)
    {
        token Token = GetToken(Tokenizer);
        switch (Token.Type)
        {
            case Token_Asterisk:
            {
                IsPointer = true;
            }
            break;

            case Token_Identifier:
            {
                char NoBase[8] = "no_base";
                if (BaseTypeToken.TextLength == 0)
                {
                    BaseTypeToken.Text = NoBase;
                    BaseTypeToken.TextLength = 7;
                }
                printf("   {%s, MetaType_%.*s, \"%.*s\", MetaType_%.*s, \"%.*s\", "
                       "\"%.*s\", U32FromPointer(&((%.*s *)0)->%.*s)},\n",
                       IsPointer ? "MetaMemberFlag_IsPointer" : "0",
                       (unsigned int)MemberTypeToken.TextLength, MemberTypeToken.Text,
                       (unsigned int)MemberTypeToken.TextLength, MemberTypeToken.Text,
                       (unsigned int)BaseTypeToken.TextLength, BaseTypeToken.Text,
                       (unsigned int)BaseTypeToken.TextLength, BaseTypeToken.Text,
                       (unsigned int)Token.TextLength, Token.Text,
                       (unsigned int)StructTypeToken.TextLength, StructTypeToken.Text,
                       (unsigned int)Token.TextLength, Token.Text);
            }
            break;

            case Token_Equals:
            {
                Token = GetToken(Tokenizer); // value after equals
                if (Token.Type == Token_OpenBrace)
                {
                    while (Token.Type != Token_CloseBrace)
                        Token = GetToken(Tokenizer);
                }
            }
            break;

            case Token_Semicolon:
            case Token_EndOfStream:
            {

                Parsing = false;
            }
            break;
        }
    }
#else
    token Token = GetToken(Tokenizer);
    switch (Token.Type)
    {
        case Token_Asterisk:
        {
            ParseMember(Tokenizer, Token);
        }
        break;
        case Token_Identifier:
        {
            printf("DEBUG_VALUE(%.*s);\n", Token.TextLength, Token.Text);
        }
        break;
    }
#endif
}

static void ParseStruct(tokenizer* Tokenizer)
{
    bool HasSerializedMembers = false;
    token NameToken = GetToken(Tokenizer);
    token MaybeOpenBrace = GetToken(Tokenizer);
    token BaseTypeToken = {};

    int InitializerScope = 0;

    if (MaybeOpenBrace.Type == Token_Colon)
    {
        BaseTypeToken = GetToken(Tokenizer);
        if (BaseTypeToken.Type != Token_Identifier || !RequireToken(Tokenizer, Token_OpenBrace))
        {
            printf("\nbad struct header\n");
        }
    }
    else if (MaybeOpenBrace.Type != Token_OpenBrace)
    {
        printf("\nbad struct header\n");
    }

    {
        printf("member_definition MembersOf_%.*s[] = \n",
               (unsigned int)NameToken.TextLength, NameToken.Text);
        printf("{\n");
        for (;;)
        {
            token MemberToken = GetToken(Tokenizer);

            if (MemberToken.Type == Token_OpenBrace)
            {
                InitializerScope++;
            }
            if (MemberToken.Type == Token_CloseBrace)
            {
                InitializerScope--;
                if (InitializerScope == -1)
                {
                    break;
                }
            }
            else if (TokenEquals(MemberToken, "serialize"))
            {
                HasSerializedMembers = true;

                GetToken(Tokenizer); // open paren
                token OptionParam = GetToken(Tokenizer); // serialize param
                GetToken(Tokenizer); // close paren

                MemberToken = GetToken(Tokenizer);
                char* ForceUint32 = "uint32";
                if (TokenEquals(OptionParam, "enum"))
                {
                    MemberToken.Text = ForceUint32;
                    MemberToken.TextLength = 8;
                }

                ParseMember(Tokenizer, NameToken, MemberToken, BaseTypeToken);
            }
        }
        printf("};\n");

        meta_struct* Meta = (meta_struct*)malloc(sizeof(meta_struct));
        Meta->Name = (char*)malloc(NameToken.TextLength + 1);
        memcpy(Meta->Name, NameToken.Text, NameToken.TextLength);
        Meta->Name[NameToken.TextLength] = 0;
        Meta->Next = FirstMetaStruct;
        FirstMetaStruct = Meta;

        if (!HasSerializedMembers)
        {
            printf("\nstruct has no serialized members\n");
        }
    }
}

static void ParseIntrospectable(tokenizer* Tokenizer)
{
    if (RequireToken(Tokenizer, Token_OpenParen))
    {
        if (ParseIntrospectionParams(Tokenizer))
        {
            token TypeToken = GetToken(Tokenizer);
            if (TokenEquals(TypeToken, "struct"))
            {
                ParseStruct(Tokenizer);
            }
            else if (TokenEquals(TypeToken, "typedef"))
            {
                token TypeToken2 = GetToken(Tokenizer);
                if (TokenEquals(TypeToken2, "struct"))
                {
                    ParseStruct(Tokenizer);
                }
                else
                {
                    fprintf(stderr, "ERROR: Introspection is only supported for typedef "
                                    "structs right now :(\n");
                }
            }
            else
            {
                fprintf(stderr, "ERROR: Introspection is only supported for structs "
                                "right now :(\n");
            }
        }
    }
    else
    {
        fprintf(stderr, "ERROR: Missing parentheses.\n");
    }
}

#define MAX_ENUM_MEMBERS 20
static void EnumToString(tokenizer* Tokenizer)
{
    token EnumToken = GetToken(Tokenizer);
    if (TokenEquals(EnumToken, "enum"))
    {
        token EnumName = GetToken(Tokenizer);
        
        char* NameString = (char*)calloc(1, EnumName.TextLength + 1);
        memcpy(NameString, EnumName.Text, EnumName.TextLength);
        printf("static char* %s_strings[%i] = {\n", NameString, MAX_ENUM_MEMBERS);
        
         int EnumMemberCount = 0;
        if (RequireToken(Tokenizer, Token_OpenBrace))
        {
            bool ParsingEnum = true;
            while (ParsingEnum)
            {
                EnumMemberCount++;
                if (EnumMemberCount > MAX_ENUM_MEMBERS)
                {
                    fprintf(stderr, "ERROR: more enum members than we hacked for!\n");
                }
                token EnumMember = GetToken(Tokenizer);
                
                NameString = (char*)calloc(1, EnumMember.TextLength + 1);
                memcpy(NameString, EnumMember.Text, EnumMember.TextLength);
                printf("\"%s\"", NameString);
                
                token CommaOrClosingBrace = GetToken(Tokenizer);
                if (CommaOrClosingBrace.Type == Token_CloseBrace)
                {
                    ParsingEnum = false;
                    printf("\n};\n");
                }
                else
                {
                    printf(",\n");
                }
            }
            
        }
        else
        {
            fprintf(stderr, "ERROR: Mising open brace.\n");
        }
    }
    else
    {
        fprintf(stderr, "ERROR: Mising \"enum\".\n");
    }
    
}

int main(int ArgCount, char** Args)
{
    char* FileNames[] = { "platform_agnostic.h", "yamgine_entity.h", "yamgine_gui.h", "yamgine_render_list.h", "game_editor.h" };
    for (int FileIndex = 0;
         FileIndex < (sizeof(FileNames) / sizeof(FileNames[0]));
         ++FileIndex)
    {
        char* FileContents = ReadEntireFileIntoMemoryAndNullTerminate(FileNames[FileIndex]);

        tokenizer Tokenizer = {};
        Tokenizer.At = FileContents;

        bool Parsing = true;
        while (Parsing)
        {
            token Token = GetToken(&Tokenizer);
            switch (Token.Type)
            {
                case Token_EndOfStream:
                {
                    Parsing = false;
                }
                break;

                case Token_Unknown:
                {
                }
                break;

                case Token_Identifier:
                {
                    if (TokenEquals(Token, "introspect"))
                    {
                        ParseIntrospectable(&Tokenizer);
                    }
                    if (TokenEquals(Token, "meta_string_enum"))
                    {
                        EnumToString(&Tokenizer);
                    }
                }
                break;

                default:
                {
                    //                printf("%d: %.*s\n", Token.Type, Token.TextLength,
                    //                Token.Text);
                }
                break;
            }
        }
    }

    printf("#define META_SERIALIZE_STRUCT(MemberPtr, NextIndentLevel, TypeName, "
           "StructName, File) \\\n");
    for (meta_struct* Meta = FirstMetaStruct;
         Meta;
         Meta = Meta->Next)
    {
        printf("    case MetaType_%s: {SerializeStruct(ArrayCount(MembersOf_%s), "
               "MembersOf_%s, MemberPtr, (NextIndentLevel), TypeName, StructName, "
               "File);} break; %s\n",
               Meta->Name, Meta->Name, Meta->Name, Meta->Next ? "\\" : "");
    }

    printf("#define META_DELTA_SERIALIZE_STRUCT(MemberPtr, DefaultStructPtr, NextIndentLevel, TypeName, "
           "StructName, File) \\\n");
    for (meta_struct* Meta = FirstMetaStruct;
         Meta;
         Meta = Meta->Next)
    {
        printf("    case MetaType_%s: {DeltaSerializeStruct(ArrayCount(MembersOf_%s), "
               "MembersOf_%s, MemberPtr, DefaultStructPtr, (NextIndentLevel), TypeName, StructName, "
               "File);} break; %s\n",
               Meta->Name, Meta->Name, Meta->Name, Meta->Next ? "\\" : "");
    }
}