#include "yamgine_serialization_shared.cpp"

void SerializeEntity(char* EntityFolderPath, entity* Entity, platform_api* PlatformAPI)
{
    // path changed since being loaded, so we have to delete existing serialization info
    // NOTE(james): only necessary if we are doing delta serialization, not nuke/save system
    /*if (!StringsAreEqual(Entity->SerializedPath, "") && 
        !StringsAreEqual(Entity->Path, Entity->SerializedPath))
    {
        CSTRING_INIT(PrependEntityPath, 128, "serialization/entities");
        AppendString(PrependEntityPath, Entity->SerializedPath, 128);
        PlatformAPI->DeleteDirectory(PrependEntityPath);
    }
    */
    SetString(&Entity->SerializedPath, Entity->Path);

    if (Entity->RenderMesh != NULL && StringLength(Entity->RenderMeshPath.Text) == 0)
    {
        Assert(0);
    }

    STRING(File, 2048);

    entity DefaultEntity = {};
    InitializeEntity(&DefaultEntity, true);

    DeltaSerializeStruct(ArrayCount(MembersOf_entity), MembersOf_entity,
                         (void*)Entity, (void*)&DefaultEntity,
                         0,
                         "entity", GetName(Entity), &File);

    STRING(FileName, 255);
    AppendStringEx(&FileName, "%s/%s/%s.ent", EntityFolderPath, Entity->Path.Text, GetName(Entity));

    PlatformAPI->WriteBinaryFile(FileName.Text,
                                 (uint8*)File.Text,
                                 StringLength(File.Text));
}

//
// Deserialization
//
void DeserializeEntity(entity* Entity, char* EntityFilePath, temporary_memory* Temp, platform_api* PlatformAPI)
{
    void* FileRaw = LoadTextFile(EntityFilePath, Temp, PlatformAPI);
    
    char* File = (char*)FileRaw;
    tokenizer Tokenizer = {};
    Tokenizer.At = &File[0];

    GetToken(&Tokenizer); // struct type
    GetToken(&Tokenizer); // struct identifier

    GetToken(&Tokenizer); // equals sign

    DeserializeStruct(&Tokenizer, Entity, MembersOf_entity, ArrayCount(MembersOf_entity));

    SetString(&Entity->SerializedPath, Entity->Path);

    SetDirty(Entity);
    ResolveLocalToWorld(Entity);
}

void DeserializeEntities_Internal(linked_list* EntityMaster,
                         collision_group* CollisionGroup,
                         render_assets* RenderAssets,
                         char* Path, //folder to look in, Ex. "serialization/entities"
                         entity* Parent,
                         temporary_memory* Temp,
                         platform_api* PlatformAPI)
{
    YamProf(DeserializeEntities);

    entity* Entity = NULL;
    file File = {};
    find_handle FindHandle = {};
    if (PlatformAPI->GetFirstFileInDirectory(Path, &File, &FindHandle))
    {
        //TODO(james): this sucks. If we only want one file from a directory,
        // the find_handle doesn't get closed until it fails. 
        file DummyFile = {};
        bool32 DummyNext = PlatformAPI->GetNextFile(&DummyFile, &FindHandle);
        Assert(!DummyNext);//Only one file per directory
        
        //Ex. serialization/entities/box/box.ent
        STRING(ChildFilePath, 255);
        AppendStringEx(&ChildFilePath, "%s/%s", Path, File.Name.Text);

        void* Memory = LoadTextFile(ChildFilePath.Text, Temp, PlatformAPI);

        //Ex. entity.ent -> entity
        STRING(EntityName, 128);
        AppendSubstring(&EntityName, File.Name.Text, FindFirstOf(File.Name.Text, '.'));

        STRING(EntityPath, 255);
        if (Parent != NULL)
        {
            AppendStringEx(&EntityPath, "%s/%s", Parent->Path.Text, EntityName.Text);
        }
        else
        {
            AppendStringEx(&EntityPath, "/%s", EntityName.Text);
        }

        bool32 IsNew = false;
        Entity = CreateOrFindEntity(EntityMaster, EntityPath.Text, Parent, &IsNew);

        DeserializeEntity(Entity, ChildFilePath.Text, Temp, PlatformAPI);

        // collision info
        if (Entity->ColliderType != ColliderType_None)
        {
            if (IsNew)
            {
                AddCollisionEntity(CollisionGroup, Entity,
                                   Entity->UseGravity, Entity->IsTrigger, Entity->CollisionLayer);
            }

            if (Entity->ColliderMeshPath.Text)
            {
                Entity->ColliderMesh = FindMeshByPath(RenderAssets, Entity->ColliderMeshPath.Text);
            }
        }

        // rendering info
        if (StringLength(Entity->RenderMeshPath.Text) > 0)
        {
            Entity->RenderMesh = FindMeshByPath(RenderAssets, Entity->RenderMeshPath.Text);
            if (Entity->RenderMesh == NULL)
            {
                MyConsole.AddLog("Serialization: couldn't find mesh asset for path");
                MyConsole.AddLog(Entity->RenderMeshPath.Text);
            }
        }
    }

    directory Directory = {};
    find_handle FindHandleDir = {};
    if (PlatformAPI->GetFirstSubdirectory(Path, &Directory, &FindHandleDir))
    {
        do
        {
            STRING(ChildPath, 255);
            AppendStringEx(&ChildPath, "%s/%s", Path, Directory.Name.Text);
            DeserializeEntities_Internal(EntityMaster, CollisionGroup, RenderAssets, ChildPath.Text, Entity, Temp, PlatformAPI);
        } while (PlatformAPI->GetNextSubdirectory(&Directory, &FindHandleDir));
    }
    
    Assert(!FindHandleDir.IsOpen);
    Assert(!FindHandle.IsOpen);
}

void DeserializeEntities(linked_list* EntityMaster,
                         collision_group* CollisionGroup,
                         render_assets* RenderAssets,
                         char* Path, //folder to look in, Ex. "serialization/entities"
                         temporary_memory* Temp,
                         platform_api* PlatformAPI)
{
    DeserializeEntities_Internal(EntityMaster,CollisionGroup,RenderAssets,
                             Path,NULL,Temp,PlatformAPI);
}
//
//
// MODEL / MESH SERIALIZATION
//
//

void SerializeMesh(mesh* Mesh, platform_api* PlatformAPI)
{
    STRING(FileName, 255);

    AppendStringEx(&FileName, "serialization/models%s/%s", Mesh->Path.Text, GetName(Mesh));

    //
    // vertices
    //
    STRING(VerticesFileName, 255);
    AppendString(&VerticesFileName, FileName.Text);
    AppendString(&VerticesFileName, ".vertices");

    PlatformAPI->WriteBinaryFile(VerticesFileName.Text,
                                 (uint8*)Mesh->Vertices.Data,
                                 Mesh->Vertices.Count * sizeof(v3));

    //
    // normals
    //
    if (IsValidBuffer(&Mesh->Normals))
    {
        STRING(NormalsFileName, 255);
        AppendString(&NormalsFileName, FileName.Text);
        AppendString(&NormalsFileName, ".normals");
        
        PlatformAPI->WriteBinaryFile(NormalsFileName.Text,
                                     (uint8*)Mesh->Normals.Data,
                                     Mesh->Normals.Count * sizeof(v3));
    }
    

    //
    // elements
    //
    if (Mesh->IsIndexedMesh)
    {
        Assert(IsValidBuffer(&Mesh->Elements));
        STRING(ElementsFileName, 255);
        AppendString(&ElementsFileName, FileName.Text);
        AppendString(&ElementsFileName, ".elements");

        PlatformAPI->WriteBinaryFile(ElementsFileName.Text,
                                     (uint8*)Mesh->Elements.Data,
                                     Mesh->Elements.Count * sizeof(uint32));
    }

    //
    // UVs
    //
    if (IsValidBuffer(&Mesh->UVs))
    {
        STRING(UVsFileName, 255);
        AppendString(&UVsFileName, FileName.Text);
        AppendString(&UVsFileName, ".uvs");
        PlatformAPI->WriteBinaryFile(UVsFileName.Text,
                                     (uint8*)Mesh->UVs.Data,
                                     Mesh->UVs.Count * sizeof(v2));
    }

    //
    // mesh struct
    //
    mesh DefaultMesh = mesh();

    STRING(MeshInfoFile, 1024);
    DeltaSerializeStruct(ArrayCount(MembersOf_mesh), MembersOf_mesh,
                         Mesh, &DefaultMesh, 0,
                         "mesh", GetName(Mesh), &MeshInfoFile);

    STRING(MeshInfoFileName, 255);
    AppendString(&MeshInfoFileName, FileName.Text);
    AppendString(&MeshInfoFileName, ".mesh");
    PlatformAPI->WriteBinaryFile(MeshInfoFileName.Text,
                                 (uint8*)MeshInfoFile.Text,
                                 StringLength(MeshInfoFile.Text));

    //
    // imported material
    //
}

internal void
DeserializeMesh(mesh* Mesh, char* ModelPath, memory_arena* VisualAssetsArena,
                temporary_memory* Temp,platform_api* PlatformAPI)
{
    YamProf(DeserializeMesh);
    
    //
    //load mesh info
    //
    STRING(MeshInfoFileName, 255);
    AppendString(&MeshInfoFileName, ModelPath);
    AppendString(&MeshInfoFileName, ".mesh");

    void* MeshInfoFileRaw = LoadTextFile(MeshInfoFileName.Text, Temp, PlatformAPI);

    char* MeshInfoFile = (char*)MeshInfoFileRaw;
    tokenizer Tokenizer = {};
    Tokenizer.At = &MeshInfoFile[0];

    GetToken(&Tokenizer); // struct type
    GetToken(&Tokenizer); // struct identifier

    GetToken(&Tokenizer); // equals sign

    InitializeTransform(Mesh);
    DeserializeStruct(&Tokenizer, Mesh, MembersOf_mesh, ArrayCount(MembersOf_mesh));

    //
    // Fix up Path strings?
    //

    Mesh->MeshType = MeshType_Triangles;
    SetString(&Mesh->Path, Mesh->SerializedPath);

    //
    // load vertices
    //

    STRING(VerticesFileName, 255);
    AppendString(&VerticesFileName, ModelPath);
    AppendString(&VerticesFileName, ".vertices");

    uint64 VerticesFileSize;
    void* VerticesFileRaw = LoadBinaryFile(VerticesFileName.Text, Temp, PlatformAPI, &VerticesFileSize);
    void* LoadedVertices = PushCopy(VisualAssetsArena, VerticesFileSize, VerticesFileRaw);
    //AddBufferToMesh(Mesh, Vertices);
    Mesh->Vertices.Data = LoadedVertices;
    Mesh->Vertices.Count = VerticesFileSize / 3;

    //
    // load normals
    //
    STRING(NormalsFileName, 255);
    AppendString(&NormalsFileName, ModelPath);
    AppendString(&NormalsFileName, ".normals");

    if (PlatformAPI->FileExists(NormalsFileName.Text))
    {
        uint64 NormalsFileSize;
        void* NormalsFileRaw = LoadBinaryFile(NormalsFileName.Text, Temp, PlatformAPI, &NormalsFileSize);
        if (NormalsFileRaw)
        {
            void* LoadedNormals = PushCopy(VisualAssetsArena, NormalsFileSize, NormalsFileRaw);
            Mesh->Normals.Data = LoadedNormals;
            Mesh->Normals.Count = NormalsFileSize / 3;
        }
        
    }
    
    //
    // load indices
    //
    if (Mesh->IsIndexedMesh)
    {
        STRING(ElementsFileName, 255);
        AppendString(&ElementsFileName, ModelPath);
        AppendString(&ElementsFileName, ".elements");

        uint64 ElementsFileSize;
        void* ElementsFileRaw = LoadBinaryFile(ElementsFileName.Text, Temp, PlatformAPI, &ElementsFileSize);
        void* LoadedElements = PushCopy(VisualAssetsArena, ElementsFileSize, ElementsFileRaw);
        
        Mesh->Elements.Data = LoadedElements;
        Mesh->Elements.Count = ElementsFileSize;
    }

    //
    // load UVs
    //
    STRING(UVsFileName, 255);
        AppendString(&UVsFileName, ModelPath);
        AppendString(&UVsFileName, ".uvs");

    if (PlatformAPI->FileExists(UVsFileName.Text))
    {
        uint64 UVsFileSize;
        void* UVsFileRaw = LoadBinaryFile(UVsFileName.Text, Temp, PlatformAPI, &UVsFileSize);
        if (UVsFileRaw)
        {
            void* LoadedUVs = PushCopy(VisualAssetsArena, UVsFileSize, UVsFileRaw);
            Mesh->UVs.Data = LoadedUVs;
            Mesh->UVs.Count = UVsFileSize;
            
            PlatformAPI->CreateBuffer(&Mesh->UVs, CHANNEL_UV, CHANNEL_UV_WIDTH, MeshResidency_Static);
            PlatformAPI->FillBuffer(&Mesh->UVs);
            
            Assert(!Mesh->IsIndexedMesh);
        }
        
    }
    
    SetDirty(Mesh);
    ResolveLocalToWorld(Mesh);
}

void DeserializeModel(char* Path, mesh* Model, memory_arena* VisualAssetsArena, temporary_memory* Temp, platform_api* PlatformAPI)
{
    YamProf(DeserializeModel);
    //ExtractFolderName(Path, GetName(Entity), 255);

    file File = {};
    find_handle FindHandle = {};
    if (PlatformAPI->GetFirstFileInDirectory(Path, &File, &FindHandle))
    {
        //looks like meshname.mesh
        char* ChildEntity = File.Name.Text;

        STRING(ChildFilePath, 255);

        AppendString(&ChildFilePath, Path);
        AppendString(&ChildFilePath, "/");
        AppendSubstring(&ChildFilePath, ChildEntity, FindLastOf(ChildEntity, '.'));

        //mesh* Mesh = PushStruct(VisualAssetsArena, mesh);
        DeserializeMesh(Model, ChildFilePath.Text, VisualAssetsArena, Temp, PlatformAPI);
        SetDirty(Model);
        ResolveLocalToWorld(Model);
        //SetTransformParent(Mesh, Model);
    }

    directory Directory = {};
    find_handle FindHandleDir = {};
    if (PlatformAPI->GetFirstSubdirectory(Path, &Directory, &FindHandleDir))
    {
        do
        {
            char* ChildDirectory = Directory.Name.Text;

            STRING(ChildPath, 255);
            AppendStringEx(&ChildPath, "%s/%s", Path, ChildDirectory);

            mesh* Child = PushStruct(VisualAssetsArena, mesh);
            InitializeTransform(Child);
            DeserializeModel(ChildPath.Text, Child, VisualAssetsArena, Temp, PlatformAPI);
            SetTransformParent(Child, Model);
        } while (PlatformAPI->GetNextSubdirectory(&Directory, &FindHandleDir));
    }
}

void SerializeModel(mesh* Mesh, platform_api* PlatformAPI)
{
    SerializeMesh(Mesh, PlatformAPI);

    if (Mesh->Child != NULL)
    {
        mesh* Child = (mesh*)Mesh->Child;
        do
        {
            SerializeModel(Child, PlatformAPI);
            Child = (mesh*)Child->NextSibling;
        } while (Child != NULL);
    }
}
