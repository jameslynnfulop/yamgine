#if !defined(YAMGINE_RENDER_LIST_H)

#include "yamgine_linked_list.h"

/* TODO(james):

-I think when we have multiple cameras, I think they will all have separate render groups
-For any position based drawing, maybe we should do AABB frustom intersection checking before adding them to list?

*/

#define CPP
#include "shaders/shader_uniforms.h"

#define GET_V3(buffer, index)(((v3*)(buffer->Data))[index])
struct graphics_buffer
{
    void* Data;
    uint32 Handle=MAX_UINT32;
     uint32 Channel;
    uint32 CountPerEntry;// things per entry, so 3 for v3, 2 for v2...
    mesh_residency Residency;
    
    // count of members, actual size of buf would be CountPerEntry * Count, assuming a 4 byte entry
    uint32 Count;
};


#define BUFFER_COUNT 5
introspect(category: "math") struct mesh : transform
{
    serialize("param") bool32 IsIndexedMesh = false;
    mesh_type MeshType = MeshType_Triangles;
    
    //union 
    //{
        struct
        {
            graphics_buffer Vertices;
            graphics_buffer Normals;
            graphics_buffer Colors;
            graphics_buffer Elements;
            graphics_buffer UVs;
        };
        //graphics_buffer Buffers[BUFFER_COUNT];
    //};
    //mesh_extents Extents;//NOTE(james): seems like a good thing to cache with collision obj for frames where transform didn't change
    material* ImportedMaterial = NULL;
};

bool32 
IsValidBuffer(graphics_buffer* Buffer)
{
    bool32 Result = false;
    if (Buffer->Count > 0 && Buffer->Handle != MAX_UINT32)
    {
        Result = true;
    }
    return Result;
}

enum render_blend_type
{
    RenderBlendType_none,
    RenderBlendType_one_minus_src
    //RenderBlendTypes_additive,
    //RenderBlendTypes_multiply
};

enum render_group_entry_type
{
    RenderGroupEntryType_render_entry_color_clear,
    RenderGroupEntryType_render_entry_depth_clear,
    
    RenderGroupEntryType_render_entry_disable_depth,
    RenderGroupEntryType_render_entry_enable_depth,

    RenderGroupEntryType_render_entry_imgui,

    RenderGroupEntryType_render_entry_fbo_begin,
    RenderGroupEntryType_render_entry_fbo_end,

    RenderGroupEntryType_render_entry_set_view_projection,
    
    RenderGroupEntryType_render_entry_use_mesh,
    RenderGroupEntryType_render_entry_use_shader,
    RenderGroupEntryType_render_entry_set_texture,
    
    RenderGroupEntryType_render_entry_draw_mesh,
    
    RenderGroupEntryType_render_entry_set_blend,
    
        RenderGroupEntryType_render_entry_set_line_width,
    
    RenderGroupEntryType_render_entry_set_cull_face_mode,
    
    RenderGroupEntryType_render_entry_use_particles,
    RenderGroupEntryType_render_entry_draw_particles
};

enum render_mode
{
    RenderMode_Wireframe,
    RenderMode_Fill,
    RenderMode_Point
};

enum cull_face_mode
{
    CullFaceMode_Front,
    CullFaceMode_Back,
    CullFaceMode_FrontAndBack,
    CullFaceMode_None
};

struct render_group_entry_header
{
    render_group_entry_type Type;
};

struct render_entry_color_clear
{
    v4 Color;
};

struct render_entry_depth_clear
{
    uint32 Dummy;
};

struct render_entry_enable_depth
{
    uint32 Dummy;
};

struct render_entry_disable_depth
{
    uint32 Dummy;
};

struct render_entry_set_cull_face_mode
{
    cull_face_mode CullFaceMode;
};

struct render_entry_set_view_projection
{
    global_uniforms GlobalUniforms;
};

struct render_entry_imgui
{
    ImDrawData* DrawData;
    v2 DisplaySize;
    v2 DisplayFramebufferScale;
};

struct render_entry_fbo_begin
{
    fbo* Fbo;
};

struct render_entry_fbo_end
{
    uint32 Dummy;
};

struct render_entry_use_mesh
{
    mesh* Mesh;
};

struct render_entry_use_shader
{
    shader* Shader;
    void* Uniforms;
};

struct render_entry_set_texture
{
    texture* Texture;
    uint32 Slot;
    bool32 IsShadowmap;
};

struct render_entry_draw_mesh
{
    mesh* Mesh;
    render_mode RenderMode;
};

struct render_entry_set_blend
{
    render_blend_type BlendType;
};

struct render_entry_set_line_width
{
    float LineWidth;
};

struct render_entry_use_particles
{
    mesh* BaseMesh;
    graphics_buffer* ParticleCenters;
};

struct render_entry_draw_particles
{
    mesh* BaseMesh;
    graphics_buffer* ParticleCenters;
};

//
// ASSETS
//

struct standard_assets
{
    mesh* Cube;
    mesh* ColliderCube;
    mesh* Tetrahedron;
    mesh* Pyramid;
    mesh* Sphere;
    mesh* Quad;
    mesh* QuadVertices;
    mesh* Grid;
    
    mesh* CircleOutline;
};

struct loaded_assets
{
    mesh Sword;
    mesh Cractus;
    mesh Sphere;
    mesh Capsule;
    mesh Triangle;
    
    mesh GltfBox;
    mesh GltfCube;
};

struct shaders
{
    shader TintedTexture;
    shader Glyph;
    shader MultisampleRect;
    shader MultisampleRectDepth;
    shader UniformColor;
    
    shader MeshFillDepthmap;
    shader MeshRecieveShadows;
    
    shader Sobel;
    shader GaussianBlur;
    shader Stenciling;
    
    shader PerVertexColor;
    
    shader Particles;
};

#define MAX_ASSET_TEXTURES 32
#define MAX_LINE_VERTS 1000
struct render_assets
{
    standard_assets StandardAssets;
    loaded_assets LoadedAssets;

    linked_list Meshes;
    linked_list Models; //top level mesh hierarchies

    uint32 TextureCount;
    texture* Textures[MAX_ASSET_TEXTURES];
    
    shaders Shaders;
    
    mesh Lines;
};

#define YAMGINE_RENDER_LIST_H
#endif