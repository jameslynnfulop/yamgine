#include "platform_win32.h"

#define STB_SPRINTF_IMPLEMENTATION
#include "stb_printf.h"

#if WWISE_SUPPORT
#include "wwise_impl.cpp"
#endif

global_variable char DLL_FILE_NAME[] = "yamgine.dll";

global_variable char LOCK_FILE_NAME[] = "lock.tmp";

global_variable char GL_DLL_FILE_NAME[] = "renderer_opengl.dll";
global_variable char GL_LOCK_FILE_NAME[] = "gl_lock.tmp";

global_variable platform_api Platform;

// Input Stubs
// NOTE(james): this is just a way of faking to the compiler that we have this function
// NOTE(casey): XInputGetState
#define X_INPUT_GET_STATE(name) DWORD WINAPI name(DWORD dwUserIndex, XINPUT_STATE* pState)
typedef X_INPUT_GET_STATE(x_input_get_state);
X_INPUT_GET_STATE(XInputGetStateStub)
{
    return (ERROR_DEVICE_NOT_CONNECTED);
}
global_variable x_input_get_state* XInputGetState_ = XInputGetStateStub;
#define XInputGetState XInputGetState_

//icon
#include "resource.h"

// Wine Support?
typedef void APIENTRY _glXGetProcAddressARB(const GLubyte* procName);
global_variable _glXGetProcAddressARB* glXGetProcAddressARB;

//
// Windows Graphics Layer stubs
//

#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092
#define WGL_CONTEXT_LAYER_PLANE_ARB 0x2093
#define WGL_CONTEXT_FLAGS_ARB 0x2094
#define WGL_CONTEXT_PROFILE_MASK_ARB 0x9126

#define WGL_CONTEXT_DEBUG_BIT_ARB 0x0001
#define WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x0002

#define WGL_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001
#define WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB 0x00000002

#define WGL_FULL_ACCELERATION_ARB 0x2027
#define WGL_SUPPORT_OPENGL_ARB 0x2010

#define WGL_DEPTH_BITS_ARB 0x2022
#define WGL_STENCIL_BITS_ARB 0x2023

//NOTE(james):lets you create an elevated OpenGL context
typedef HGLRC WINAPI wgl_create_context_attribs_arb(HDC hDC, HGLRC hShareContext,
                                                    const int* attribList);

//NOTE(james): VSync
typedef BOOL WINAPI wgl_swap_interval_ext(int interval);
global_variable wgl_swap_interval_ext* wglSwapIntervalEXT;

//NOTE(james): SRGB
#define WGL_DRAW_TO_WINDOW_ARB 0x2001
#define WGL_ACCELERATION_ARB 0x2003
#define WGL_SUPPORT_OPENGL_ARB 0x2010
#define WGL_DOUBLE_BUFFER_ARB 0x2011
#define WGL_PIXEL_TYPE_ARB 0x2013

#define WGL_TYPE_RGBA_ARB 0x202B
#define WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB 0x20A9

typedef BOOL WINAPI wgl_choose_pixel_format_arb(HDC hdc,
                                                const int* piAttribIList,
                                                const FLOAT* pfAttribFList,
                                                UINT nMaxFormats,
                                                int* piFormats,
                                                UINT* nNumFormats);
global_variable wgl_choose_pixel_format_arb* wglChoosePixelFormatARB;

typedef const char* WINAPI wgl_get_extensions_string_ext(void);
global_variable wgl_get_extensions_string_ext* wglGetExtensionsStringEXT;

global_variable bool32 OpenGLSupportsSRGBFramebuffer;

//INFO(james): Multisampling
#define WGL_SAMPLE_BUFFERS_ARB 0x2041
#define WGL_SAMPLES_ARB 0x2042
global_variable bool32 OpenGLSupportsMultisampling;

global_variable gl_code* GlobalGLCode;
global_variable gl_state* GlobalGLState;

global_variable win32_state* GlobalWin32State;

PLATFORM_QUERY_TIME(Win32QueryTime)
{
    LARGE_INTEGER freq;
    LARGE_INTEGER time;

    BOOL ok = QueryPerformanceFrequency(&freq);
    assert(ok == TRUE);

    freq.QuadPart = freq.QuadPart;

    ok = QueryPerformanceCounter(&time);
    assert(ok == TRUE);

    return (real64)time.QuadPart / (real64)freq.QuadPart;
}

PLATFORM_GET_TIMESTAMP(Win32GetTimestamp)
{
    LARGE_INTEGER time;

    BOOL ok = QueryPerformanceCounter(&time);
    assert(ok == TRUE);

    return time.QuadPart;
}

global_variable yam_profiling MasterYamProfiling;

//
// End stubs
//

void DebugConsole(const char* input)
{
    OutputDebugStringA(input);
}

void LoadLibraries()
{
    // TODO(casey): Test this on Windows 8
    HMODULE XInputLibrary = LoadLibraryA("xinput1_4.dll");
    if (!XInputLibrary)
    {
        // TODO(casey): Diagnostic
        XInputLibrary = LoadLibraryA("xinput9_1_0.dll");
    }

    if (!XInputLibrary)
    {
        // TODO(casey): Diagnostic
        XInputLibrary = LoadLibraryA("xinput1_3.dll");
    }
    XInputGetState = (x_input_get_state*)GetProcAddress(XInputLibrary, "XInputGetState");
    if (!XInputGetState)
    {
        XInputGetState = XInputGetStateStub;
    }

    //NOTE(james): Probably do fmod in here as well? according to msdn, we are loading
    //xinput via explicit linking. We could maybe do fmod with implicit linking
    //https://msdn.microsoft.com/en-us/library/d14wsce5.aspx
}

void LoadRenderDoc(game_memory* Memory)
{
    HMODULE RenderDoc = LoadLibrary("renderdoc.dll");
    pRENDERDOC_GetAPI RenderDocGetAPIEntryFunction = (pRENDERDOC_GetAPI)GetProcAddress(RenderDoc, "RENDERDOC_GetAPI");

    int LoadedRenderDocAPI = RenderDocGetAPIEntryFunction(eRENDERDOC_API_Version_1_1_1,
                                                          (void**)&Memory->RenderDocAPI);
    Assert(LoadedRenderDocAPI);
}

inline FILETIME
Win32GetLastWriteTime(char* Filename)
{
    FILETIME LastWriteTime = {};

    WIN32_FILE_ATTRIBUTE_DATA Data;
    if (GetFileAttributesEx(Filename, GetFileExInfoStandard, &Data))
    {
        LastWriteTime = Data.ftLastWriteTime;
    }

    return (LastWriteTime);
}

PLATFORM_ALLOCATE_MEMORY(Win32AllocateMemory)
{
    void* Result = VirtualAlloc(0, Size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

    return (Result);
}

PLATFORM_DEALLOCATE_MEMORY(Win32DeallocateMemory)
{
    if (Memory)
    {
        VirtualFree(Memory, 0, MEM_RELEASE);
    }
}

internal v2
Win32GetWindowDimension(HWND Window)
{
    v2 Result = {};

    RECT ClientRect;
    GetClientRect(Window, &ClientRect);
    Result.x = (real32)ClientRect.right - ClientRect.left;
    Result.y = (real32)ClientRect.bottom - ClientRect.top;

    return Result;
}

global_variable HWND GlobalWindow;
PLATFORM_GET_WINDOW_POSITION(Win32GetWindowPosition)
{
    RECT WindowRect;
    GetWindowRect(GlobalWindow, &WindowRect);
    rectangle2 Result = {(real32)WindowRect.left, (real32)WindowRect.top, 
        (real32)WindowRect.right, (real32)WindowRect.bottom};

    return Result;
}

////////////////////////////////////////
// File System API
////////////////////////////////////////

// NOTE(james): returns something like c:\\build
inline void
Win32GetExecutableLocation(char* Buffer)
{
    HMODULE hModule = GetModuleHandleA(NULL);

    char PathWithName[MAX_PATH];
    GetModuleFileNameA(hModule, PathWithName, MAX_PATH); // NOTE(james): comes back like c:\\build\\executable.exe

    int LastBackslashLocation = C_FindLastOf(PathWithName, '\\');
    C_SubString(PathWithName, 0, LastBackslashLocation, Buffer, MAX_PATH);
}

// NOTE(james): returns something like c:\\build\\input.txt
inline void
Win32BuildEXEPathFileName(char* FileName, string* Dest)
{
    Win32GetExecutableLocation(Dest->Text);

    C_AppendString(Dest, "\\");
    C_AppendString(Dest, FileName);
}

PLATFORM_FILE_EXISTS(Win32FileExists)
{
    bool32 Result = false;
    if (GetFileAttributes(Path) != INVALID_FILE_ATTRIBUTES)
    {
        Result = true;
    }

    return Result;
}

PLATFORM_READ_BINARY_FILE(Win32ReadBinaryFile)
{
    HANDLE FileHandle = CreateFileA(FileName, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    DWORD LastError = GetLastError();
    Assert(LastError != ERROR_FILE_NOT_FOUND);

    Assert(LastError != ERROR_PATH_NOT_FOUND);
    Assert(LastError != ERROR_SHARING_VIOLATION);

    LARGE_INTEGER FileSize;
    bool32 FileSizeSuccess = (bool32)GetFileSizeEx(FileHandle, &FileSize);
    Assert(FileSizeSuccess);
    Assert(FileSize.QuadPart < 2147483647); //LONG_MAX

    DWORD ReadBytes; //not used
    bool32 ReadSuccess = (bool32)ReadFile(FileHandle, Memory, FileSize.LowPart, &ReadBytes, NULL);
    LastError = GetLastError();
    Assert(ReadSuccess);
    Assert(ReadBytes == FileSize.LowPart);

    CloseHandle(FileHandle);
}

bool32 CheckDirectoryExists(char* Path)
{
    bool32 Result = false;
    DWORD Attributes = GetFileAttributes(Path);

    // NOTE(james): attributes are different values bitwise-OR'd together
    // however, INVALID_FILE_ATTRIBUTES is 0xFFFFFFFF so it will OR as true
    // if we dont check for it
    if (Attributes == INVALID_FILE_ATTRIBUTES)
    {
    }
    else if ((Attributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
    {
        Result = true;
    }

    return Result;
}

void FulfillRequestedPath(char* FileName)
{
    STRING(FilePath, MAX_PATH);
    C_AppendSubstring(&FilePath, FileName, C_FindLastOf(FileName, '/'));
    if (!CheckDirectoryExists(FilePath.Text))
    {
        int NextSlashIndex = 0;
        do
        {
            uint32 SlashIndex = C_FindFirstOf(FilePath.Text + (sizeof(char) * (NextSlashIndex)), '/');

            // NOTE(james): path is not terminated with slash, so we need this for the final dir
            if (SlashIndex == -1)
            {
                NextSlashIndex = C_StringLength(FilePath.Text);
            }
            else
            {
                NextSlashIndex += SlashIndex;
            }

            STRING(DirectoryToCreate, MAX_PATH);
            C_AppendSubstring(&DirectoryToCreate, FilePath.Text, NextSlashIndex);
            ++NextSlashIndex; // skip past this slash on next search

            CreateDirectoryA(DirectoryToCreate.Text, NULL);

        } while (!CheckDirectoryExists(FilePath.Text));
    }
}

//if not provided an absolute path, then the path is relative to the executable location
//if provided a path with directories that don't exist, those directories are created
PLATFORM_WRITE_BINARY_FILE(Win32WriteBinaryFile)
{
    bool32 Result;

    FulfillRequestedPath(FileName);

    HANDLE FileHandle = CreateFileA(FileName, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);

    DWORD WrittenBytes; //not used
    Result = (bool32)WriteFile(FileHandle, Base, (DWORD)Size, &WrittenBytes, NULL);
    Assert(Result);

    CloseHandle(FileHandle);

    return Result;
}

// TODO(james): may want to swap to FindFirstChangeNotification, 
// example:  https://msdn.microsoft.com/en-us/library/aa365261(v=vs.85).aspx
// use 0 in WaitForMultipleObjects so it returns immediately

//void name(char* AbsoluteFilePath)
PLATFORM_WATCH_FOR_FILE_CHANGES(Win32WatchForFileChanges)
{
    GlobalWin32State->WatchedFiles[GlobalWin32State->WatchedFilesCount++] = Shader;
    Assert(GlobalWin32State->WatchedFilesCount < 16);
    
    HANDLE FileHandle = (HANDLE)Shader->SourceFileHandle;
    
    bool FileTimeSuccess = GetFileTime(FileHandle,NULL,NULL,&GlobalWin32State->LastWriteTimes[GlobalWin32State->WatchedFilesCount-1]);
    Assert(FileTimeSuccess);
}

bool32 CheckFileForChanges(HANDLE FileHandle, uint32 FileTimesIndex)
{
    bool32 Result = false;
    
    FILETIME NewTime = {};
    if (FileHandle != INVALID_HANDLE_VALUE)
    {
        bool FileTimeSuccess = GetFileTime(FileHandle,NULL,NULL,&NewTime);
        if (CompareFileTime(&NewTime, &GlobalWin32State->LastWriteTimes[FileTimesIndex]) > 0)
        {
            Result = true;
            GlobalWin32State->LastWriteTimes[FileTimesIndex] = NewTime;
        }
        if (!FileTimeSuccess)
        {
            DWORD LastError = GetLastError();
            Assert(0);
        }
    }
    
    return Result;
}

//shader* name()
PLATFORM_DUMP_CHANGED_FILE_NOTIFICATIONS(Win32DumpChangedFileNotifications)
{
    shader* Result = NULL;
    
    for (uint32 FileIndex = 0; 
         FileIndex < GlobalWin32State->WatchedFilesCount; 
         FileIndex++)
    {
        if (CheckFileForChanges(GlobalWin32State->WatchedFiles[FileIndex]->SourceFileHandle, FileIndex))
        {
            Result = GlobalWin32State->WatchedFiles[FileIndex];
            CloseHandle(GlobalWin32State->WatchedFiles[FileIndex]->SourceFileHandle);
            break;
        }
    }
    
    return Result;
}

// NOTE(james): this is also how you rename file or directory,
// this might be duplicated into Move and Rename if these are separate actions on other platforms
PLATFORM_MOVE_FILE(Win32MoveFile)
{
    bool32 MoveResult = MoveFile(ExistingName, NewName);
    if (!MoveResult)
    {
        DWORD LastError = GetLastError();
        int dummyToSeeError = 0;
    }
    return MoveResult;
}

PLATFORM_SET_WINDOW_POSITION(Win32SetWindowPosition)
{
    HWND Window = GetActiveWindow();
    v2 Dim = GetDim(Rect);
    SetWindowPos(Window, 0, Rect.Min.x, Rect.Min.y, Dim.x, Dim.y, SWP_NOZORDER);
}

file GetFileInfo(char* FileName, char* Directory)
{
    file Result = {};

    STRING(FullPath, MAX_PATH);
    
    Assert(C_FindFirstOf(FileName, '.') != MAX_UINT32);
    AppendStringEx(&FullPath, "%s/%s", Directory, FileName);

    C_SetString(&Result.Name, FileName);

    
    return Result;
}

PLATFORM_GET_FILE_SIZE(Win32GetFileSize)
{
    HANDLE FileHandle = CreateFile(FilePath, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    
    if (FileHandle == INVALID_HANDLE_VALUE)
    {
        DWORD Error = GetLastError();
        // error 2 == file not found
        // error 32 == sharing violation
        Assert(0);
    }
    
    LARGE_INTEGER FileSize;
    bool32 FileSizeSuccess = (bool32)GetFileSizeEx(FileHandle, &FileSize);
    Assert(FileSizeSuccess);
    Assert(FileSize.QuadPart < 2147483647); //LONG_MAX
    
    CloseHandle(FileHandle);
    
    uint32 Result = FileSize.LowPart;
    return Result;
}

//find_handle FindHandle, file* File
PLATFORM_GET_NEXT_FILE(Win32GetNextFile)
{
    bool32 Result = false;

    /*
    typedef struct _WIN32_FIND_DATA {
  DWORD    dwFileAttributes;
  FILETIME ftCreationTime;
  FILETIME ftLastAccessTime;
  FILETIME ftLastWriteTime;
  DWORD    nFileSizeHigh;
  DWORD    nFileSizeLow;
  DWORD    dwReserved0;
  DWORD    dwReserved1;
  TCHAR    cFileName[MAX_PATH];
  TCHAR    cAlternateFileName[14];
}
    */
    WIN32_FIND_DATA FindData;
    bool32 FoundNext = FindNextFile(FindHandle->Handle, &FindData);

    if (FoundNext)
    {
        //item was a directory, go next entry
        if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            Result = Win32GetNextFile(File, FindHandle);
        }
        else
        {
            *File = GetFileInfo(FindData.cFileName, FindHandle->Directory.Text);
            Result = true;
        }
    }
    
    if (!Result && FindHandle->IsOpen)
    {
        FindClose(FindHandle->Handle);
        FindHandle->IsOpen = false;
    }
    
    return Result;
}

//char* DirectoryPath, file* File, find_handle* FindHandle
PLATFORM_GET_FIRST_FILE_IN_DIRECTORY(Win32GetFirstFileInDirectory)
{
    bool32 Result = false;

    STRING(FullSearchString, MAX_PATH);
    AppendStringEx(&FullSearchString, "%s/*", DirectoryPath);

    C_SetString(&FindHandle->Directory, DirectoryPath);

    WIN32_FIND_DATA FindData;
    FindHandle->Handle = FindFirstFileA(&FullSearchString.Text[0], &FindData);
    FindHandle->IsOpen = true;
    if (FindHandle->Handle != INVALID_HANDLE_VALUE)
    {
        Result = Win32GetNextFile(File, FindHandle);
    }
    
    if (!Result && FindHandle->IsOpen)
    {
        FindClose(FindHandle->Handle);
        FindHandle->IsOpen = false;
    }
    
    return Result;
}

PLATFORM_GET_NEXT_SUBDIRECTORY(Win32GetNextSubdirectory)
{
    bool32 Result = false;

    /*
    typedef struct _WIN32_FIND_DATA {
  DWORD    dwFileAttributes;
  FILETIME ftCreationTime;
  FILETIME ftLastAccessTime;
  FILETIME ftLastWriteTime;
  DWORD    nFileSizeHigh;
  DWORD    nFileSizeLow;
  DWORD    dwReserved0;
  DWORD    dwReserved1;
  TCHAR    cFileName[MAX_PATH];
  TCHAR    cAlternateFileName[14];
}
    */
    WIN32_FIND_DATA FindData;
    bool32 FoundNext = FindNextFile(FindHandle->Handle, &FindData);

    if (FoundNext)
    {
        if (FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            if (!C_StringsAreEqualUnion(FindData.cFileName, ".") && !C_StringsAreEqualUnion(FindData.cFileName, ".."))
            {
                //fill directory struct
                Result = true;
                C_SetString(&Directory->Name, FindData.cFileName);
            }
            else
            {
                Result = Win32GetNextSubdirectory(Directory, FindHandle);
            }
        }
        else
        {
            Result = Win32GetNextSubdirectory(Directory, FindHandle);
        }
    }
    
    if (!Result && FindHandle->IsOpen)
    {
        FindClose(FindHandle->Handle);
        FindHandle->IsOpen = false;
    }
    
    return Result;
}

// char* DirectoryPath, file* File, find_handle* FindHandle
PLATFORM_GET_FIRST_SUBDIRECTORY(Win32GetFirstSubdirectory)
{
    bool32 Result = false;

    char FullSearchString[MAX_PATH];
    C_CopyString(&FullSearchString[0], MAX_PATH ,DirectoryPath);
    C_AppendString_(&FullSearchString[0], MAX_PATH, "/*", C_StringLength("/*"));

    C_CopyString(&FindHandle->Directory.Text[0], MAX_PATH, DirectoryPath);

    WIN32_FIND_DATA FindData;
    FindHandle->Handle = FindFirstFileA(&FullSearchString[0], &FindData);
    if (FindHandle->Handle != INVALID_HANDLE_VALUE)
    {
        FindHandle->IsOpen = true;
        Result = Win32GetNextSubdirectory(Directory, FindHandle);
    }
    
    return Result;
}

PLATFORM_DELETE_DIRECTORY(Win32DeleteDirectory)
{
    directory Directory = {};
    find_handle FindHandleDir = {};
    if (Win32GetFirstSubdirectory(DirectoryName, &Directory, &FindHandleDir))
    {
        do
        {
            STRING(PathToDir, MAX_PATH);
            AppendStringEx(&PathToDir, "%s/%s", DirectoryName, Directory.Name.Text);
            Win32DeleteDirectory(PathToDir.Text);
        } while (Win32GetNextSubdirectory(&Directory, &FindHandleDir));
    }

    file File = {};
    find_handle FindHandle = {};
    if (Win32GetFirstFileInDirectory(DirectoryName, &File, &FindHandle))
    {
        do
        {
            STRING(PathToFile, MAX_PATH);
            AppendStringEx(&PathToFile, "%s/%s", DirectoryName, File.Name.Text);
            BOOL DeleteResult = DeleteFile(PathToFile.Text);
            Assert(DeleteResult);
        } while (Win32GetNextFile(&File, &FindHandle));
    }

    BOOL DeleteDirectoryResult = RemoveDirectory(DirectoryName);
    Assert(DeleteDirectoryResult);
}

PLATFORM_CREATE_DIRECTORY(Win32CreateDirectory)
{
    CreateDirectoryA(Path, NULL);
}

//
// Threading stuff
//
internal void
Win32AddEntry(platform_work_queue *Queue, platform_work_queue_callback *Callback, void *Data)
{
    // TODO(casey): Switch to InterlockedCompareExchange eventually
    // so that any thread can add?
    uint32 NewNextEntryToWrite = (Queue->NextEntryToWrite + 1) % ArrayCount(Queue->Entries);
    Assert(NewNextEntryToWrite != Queue->NextEntryToRead);
    platform_work_queue_entry *Entry = Queue->Entries + Queue->NextEntryToWrite;
    Entry->Callback = Callback;
    Entry->Data = Data;
    ++Queue->CompletionGoal;
    _WriteBarrier();
    Queue->NextEntryToWrite = NewNextEntryToWrite;
    ReleaseSemaphore(Queue->SemaphoreHandle, 1, 0);
}

internal bool32
Win32DoNextWorkQueueEntry(platform_work_queue *Queue)
{
    bool32 WeShouldSleep = false;
    
    uint32 OriginalNextEntryToRead = Queue->NextEntryToRead;
    uint32 NewNextEntryToRead = (OriginalNextEntryToRead + 1) % ArrayCount(Queue->Entries);
    if(OriginalNextEntryToRead != Queue->NextEntryToWrite)
    {
        uint32 Index = InterlockedCompareExchange((LONG volatile *)&Queue->NextEntryToRead,
                                                  NewNextEntryToRead,
                                                  OriginalNextEntryToRead);
        if(Index == OriginalNextEntryToRead)
        {
            platform_work_queue_entry Entry = Queue->Entries[Index];
            Entry.Callback(Queue, Entry.Data);
            InterlockedIncrement((LONG volatile *)&Queue->CompletionCount);
        }
    }
    else
    {
        WeShouldSleep = true;
    }
    
    return(WeShouldSleep);
}

internal void
Win32CompleteAllWork(platform_work_queue* WorkQueue)
{
    while(WorkQueue->CompletionGoal != WorkQueue->CompletionCount)
    {
        Win32DoNextWorkQueueEntry(WorkQueue);
        //NOTE(james): switch to this if you want all work happening on worker threads 
        //while main sleeps good for debugging
        //Sleep(100);
    }
    
    WorkQueue->CompletionGoal = 0;
    WorkQueue->CompletionCount = 0;
}

DWORD WINAPI
ThreadProc(LPVOID lpParameter)
{
    win32_thread_startup *Thread = (win32_thread_startup *)lpParameter;
    platform_work_queue *Queue = Thread->Queue;
    
    uint32 TestThreadID = GetThreadID();
    Assert(TestThreadID == GetCurrentThreadId());
    
    wglMakeCurrent(Queue->WindowDC, Thread->GLContext);
    
    for(;;)
    {
        if(Win32DoNextWorkQueueEntry(Queue))
        {
            WaitForSingleObjectEx(Queue->SemaphoreHandle, INFINITE, FALSE);
        }
    }
    
    //    return(0);
}

internal void
Win32MakeQueue(platform_work_queue *Queue, uint32 ThreadCount, 
               win32_thread_startup *Startups, HDC WindowDC)
{
    Queue->CompletionGoal = 0;
    Queue->CompletionCount = 0;
    
    Queue->NextEntryToWrite = 0;
    Queue->NextEntryToRead = 0;
    
    uint32 InitialCount = 0;
    Queue->SemaphoreHandle = CreateSemaphoreEx(0,
                                               InitialCount,
                                               ThreadCount,
                                               0, 0, SEMAPHORE_ALL_ACCESS);
    for(uint32 ThreadIndex = 0;
        ThreadIndex < ThreadCount;
        ++ThreadIndex)
    {
        win32_thread_startup *Startup = Startups + ThreadIndex;
        Startup->Queue = Queue;
        
        DWORD ThreadID;
        HANDLE ThreadHandle = CreateThread(0, 0, ThreadProc, Startup, 0, &ThreadID);
        CloseHandle(ThreadHandle);
    }
}


////////////////////////////////////////////////////
// Gameplay Recording/Playback
////////////////////////////////////////////////////

internal void
Win32GetInputFileLocation(bool32 InputStream, int SlotIndex,
                          string* Dest)
{
    char Temp[64];
    wsprintf(Temp, "loop_edit_%d_%s.hmi", SlotIndex, InputStream ? "input" : "state");
    Win32BuildEXEPathFileName(Temp, Dest);
}

internal win32_replay_buffer*
Win32GetReplayBuffer(win32_state* State, int unsigned Index)
{
    Assert(Index > 0);
    Assert(Index < ArrayCount(State->ReplayBuffers));
    win32_replay_buffer* Result = &State->ReplayBuffers[Index];
    return (Result);
}

/*
NOTE(james): we only copy out the memory block at the beginning. Since the only state 
change is from input, by going back to that first game state and replaying the input,
It'll be as if we re-did the whole thing
*/
internal void
Win32BeginRecordingInput(win32_state* State, int InputRecordingIndex)
{
    win32_replay_buffer* ReplayBuffer = Win32GetReplayBuffer(State, InputRecordingIndex);
    if (ReplayBuffer->MemoryBlock)
    {
        State->InputRecordingIndex = InputRecordingIndex;

        STRING(FileName, MAX_PATH);
        Win32GetInputFileLocation(true, InputRecordingIndex, &FileName);
        State->RecordingHandle = CreateFileA(FileName.Text, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);

#if 0
        LARGE_INTEGER FilePosition;
        FilePosition.QuadPart = State->TotalSize;
        SetFilePointerEx(State->RecordingHandle, FilePosition, 0, FILE_BEGIN);
#endif

        CopyMemory(ReplayBuffer->MemoryBlock, State->GameMemoryBlock, State->TotalSize);
    }
}

internal void
Win32EndRecordingInput(win32_state* State)
{
    CloseHandle(State->RecordingHandle);
    State->InputRecordingIndex = 0;
}

internal void
Win32BeginInputPlayBack(win32_state* State, int InputPlayingIndex)
{
    win32_replay_buffer* ReplayBuffer = Win32GetReplayBuffer(State, InputPlayingIndex);
    if (ReplayBuffer->MemoryBlock)
    {
        State->InputPlayingIndex = InputPlayingIndex;

        STRING(FileName, MAX_PATH);
        Win32GetInputFileLocation(true, InputPlayingIndex, &FileName);
        State->PlaybackHandle = CreateFileA(FileName.Text, GENERIC_READ, 0, 0, OPEN_EXISTING, 0, 0);

#if 0
        LARGE_INTEGER FilePosition;
        FilePosition.QuadPart = State->TotalSize;
        SetFilePointerEx(State->PlaybackHandle, FilePosition, 0, FILE_BEGIN);
#endif

        CopyMemory(State->GameMemoryBlock, ReplayBuffer->MemoryBlock, State->TotalSize);
    }
}

internal void
Win32EndInputPlayBack(win32_state* State)
{
    CloseHandle(State->PlaybackHandle);
    State->InputPlayingIndex = 0;
}

internal void
Win32RecordInput(win32_state* State, game_input* NewInput)
{
    DWORD BytesWritten;
    WriteFile(State->RecordingHandle, NewInput, sizeof(*NewInput), &BytesWritten, 0);
}

internal void
Win32PlayBackInput(win32_state* State, game_input* NewInput)
{
    DWORD BytesRead = 0;
    if (ReadFile(State->PlaybackHandle, NewInput, sizeof(*NewInput), &BytesRead, 0))
    {
        if (BytesRead == 0)
        {
            // NOTE(casey): We've hit the end of the stream, go back to the beginning
            int PlayingIndex = State->InputPlayingIndex;
            Win32EndInputPlayBack(State);
            Win32BeginInputPlayBack(State, PlayingIndex);
            ReadFile(State->PlaybackHandle, NewInput, sizeof(*NewInput), &BytesRead, 0);
        }
    }
}

////////////////////////////////////////////////////
// INPUT
////////////////////////////////////////////////////
// list from https://handmade.network/forums/t/2011-keyboard_inputs_-_scancodes,_raw_input,_text_input,_key_names
enum Scancode
{

    sc_escape = 0x01,
    sc_1 = 0x02,
    sc_2 = 0x03,
    sc_3 = 0x04,
    sc_4 = 0x05,
    sc_5 = 0x06,
    sc_6 = 0x07,
    sc_7 = 0x08,
    sc_8 = 0x09,
    sc_9 = 0x0A,
    sc_0 = 0x0B,
    sc_minus = 0x0C,
    sc_equals = 0x0D,
    sc_backspace = 0x0E,
    sc_tab = 0x0F,
    sc_q = 0x10,
    sc_w = 0x11,
    sc_e = 0x12,
    sc_r = 0x13,
    sc_t = 0x14,
    sc_y = 0x15,
    sc_u = 0x16,
    sc_i = 0x17,
    sc_o = 0x18,
    sc_p = 0x19,
    sc_bracketLeft = 0x1A,
    sc_bracketRight = 0x1B,
    sc_enter = 0x1C,
    sc_controlLeft = 0x1D,
    sc_a = 0x1E,
    sc_s = 0x1F,
    sc_d = 0x20,
    sc_f = 0x21,
    sc_g = 0x22,
    sc_h = 0x23,
    sc_j = 0x24,
    sc_k = 0x25,
    sc_l = 0x26,
    sc_semicolon = 0x27,
    sc_apostrophe = 0x28,
    sc_grave = 0x29,
    sc_shiftLeft = 0x2A,
    sc_backslash = 0x2B,
    sc_z = 0x2C,
    sc_x = 0x2D,
    sc_c = 0x2E,
    sc_v = 0x2F,
    sc_b = 0x30,
    sc_n = 0x31,
    sc_m = 0x32,
    sc_comma = 0x33,
    sc_preiod = 0x34,
    sc_slash = 0x35,
    sc_shiftRight = 0x36,
    sc_numpad_multiply = 0x37,
    sc_altLeft = 0x38,
    sc_space = 0x39,
    sc_capsLock = 0x3A,
    sc_f1 = 0x3B,
    sc_f2 = 0x3C,
    sc_f3 = 0x3D,
    sc_f4 = 0x3E,
    sc_f5 = 0x3F,
    sc_f6 = 0x40,
    sc_f7 = 0x41,
    sc_f8 = 0x42,
    sc_f9 = 0x43,
    sc_f10 = 0x44,
    sc_numLock = 0x45,
    sc_scrollLock = 0x46,
    sc_numpad_7 = 0x47,
    sc_numpad_8 = 0x48,
    sc_numpad_9 = 0x49,
    sc_numpad_minus = 0x4A,
    sc_numpad_4 = 0x4B,
    sc_numpad_5 = 0x4C,
    sc_numpad_6 = 0x4D,
    sc_numpad_plus = 0x4E,
    sc_numpad_1 = 0x4F,
    sc_numpad_2 = 0x50,
    sc_numpad_3 = 0x51,
    sc_numpad_0 = 0x52,
    sc_numpad_period = 0x53,
    sc_alt_printScreen = 0x54, /* Alt + print screen. MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54. */
    sc_bracketAngle = 0x56, /* Key between the left shift and Z. */
    sc_f11 = 0x57,
    sc_f12 = 0x58,
    sc_oem_1 = 0x5a, /* VK_OEM_WSCTRL */
    sc_oem_2 = 0x5b, /* VK_OEM_FINISH */
    sc_oem_3 = 0x5c, /* VK_OEM_JUMP */
    sc_eraseEOF = 0x5d,
    sc_oem_4 = 0x5e, /* VK_OEM_BACKTAB */
    sc_oem_5 = 0x5f, /* VK_OEM_AUTO */
    sc_zoom = 0x62,
    sc_help = 0x63,
    sc_f13 = 0x64,
    sc_f14 = 0x65,
    sc_f15 = 0x66,
    sc_f16 = 0x67,
    sc_f17 = 0x68,
    sc_f18 = 0x69,
    sc_f19 = 0x6a,
    sc_f20 = 0x6b,
    sc_f21 = 0x6c,
    sc_f22 = 0x6d,
    sc_f23 = 0x6e,
    sc_oem_6 = 0x6f, /* VK_OEM_PA3 */
    sc_katakana = 0x70,
    sc_oem_7 = 0x71, /* VK_OEM_RESET */
    sc_f24 = 0x76,
    sc_sbcschar = 0x77,
    sc_convert = 0x79,
    sc_nonconvert = 0x7B, /* VK_OEM_PA1 */

    sc_media_previous = 0xE010,
    sc_media_next = 0xE019,
    sc_numpad_enter = 0xE01C,
    sc_controlRight = 0xE01D,
    sc_volume_mute = 0xE020,
    sc_launch_app2 = 0xE021,
    sc_media_play = 0xE022,
    sc_media_stop = 0xE024,
    sc_volume_down = 0xE02E,
    sc_volume_up = 0xE030,
    sc_browser_home = 0xE032,
    sc_numpad_divide = 0xE035,
    sc_printScreen = 0xE037,
    /*
    sc_printScreen:
    - make: 0xE02A 0xE037
    - break: 0xE0B7 0xE0AA
    - MapVirtualKeyEx( VK_SNAPSHOT, MAPVK_VK_TO_VSC_EX, 0 ) returns scancode 0x54;
    - There is no VK_KEYDOWN with VK_SNAPSHOT.
    */
    sc_altRight = 0xE038,
    sc_cancel = 0xE046, /* CTRL + Pause */
    sc_home = 0xE047,
    sc_arrowUp = 0xE048,
    sc_pageUp = 0xE049,
    sc_arrowLeft = 0xE04B,
    sc_arrowRight = 0xE04D,
    sc_end = 0xE04F,
    sc_arrowDown = 0xE050,
    sc_pageDown = 0xE051,
    sc_insert = 0xE052,
    sc_delete = 0xE053,
    sc_metaLeft = 0xE05B,
    sc_metaRight = 0xE05C,
    sc_application = 0xE05D,
    sc_power = 0xE05E,
    sc_sleep = 0xE05F,
    sc_wake = 0xE063,
    sc_browser_search = 0xE065,
    sc_browser_favorites = 0xE066,
    sc_browser_refresh = 0xE067,
    sc_browser_stop = 0xE068,
    sc_browser_forward = 0xE069,
    sc_browser_back = 0xE06A,
    sc_launch_app1 = 0xE06B,
    sc_launch_email = 0xE06C,
    sc_launch_media = 0xE06D,

    sc_pause = 0xE11D45,
    /*
    sc_pause:
    - make: 0xE11D 45 0xE19D C5
    - make in raw input: 0xE11D 0x45
    - break: none
    - No repeat when you hold the key down
    - There are no break so I don't know how the key down/up is expected to work. Raw input sends "keydown" and "keyup" messages, and it appears that the keyup message is sent directly after the keydown message (you can't hold the key down) so depending on when GetMessage or PeekMessage will return messages, you may get both a keydown and keyup message "at the same time". If you use VK messages most of the time you only get keydown messages, but some times you get keyup messages too.
    - when pressed at the same time as one or both control keys, generates a 0xE046 (sc_cancel) and the string for that scancode is "break".
    */
};

internal void
Win32ProcessKeyboardMessage(game_button_state* NewState, uint32 Event)
{
    if (!NewState->IsDown && Event == WM_KEYDOWN)
    {
        NewState->IsDown = true;
        NewState->DownThisFrame = true;
    }
    if (Event == WM_KEYUP)
    {
        if (NewState->IsDown)
        {
            NewState->UpThisFrame = true;
        }
        NewState->IsDown = false;
    }
}

uint8 CharacterToVKey(char Input)
{
    uint8 VKey;

    //TODO(james): other keyboard layouts
    uint16 FullResult = VkKeyScan(Input);
    //NOTE(james): top order bits contain modifier information about the character,
    //which we aren't interested in here
    VKey = (uint8)(FullResult & 0xFF);

    return VKey;
}

//TODO(james): Multiple keyboards/mice
//TODO(james): localization
//TODO(james): GetAsyncState, so any button down/up trigger calls will definitely happen
internal void
Win32ProcessPendingMessages(HWND Window, win32_state* Win32State, game_controller* KeyboardController)
{
    // Run the message loop.

    MSG Message;

    while (PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
    {
        switch (Message.message)
        {
            case WM_QUIT:
            {
                Win32State->GlobalRunning = false;
            }
            break;

            case WM_GESTURENOTIFY:
            {
                int x=0;
            }
            break;
            
            case WM_GESTURE:
            {
                int x=0;
            }
            break;

			case WM_MOUSEWHEEL:
			{
				int x = 0;
			}break;
            
            case WM_TOUCH:
            {
                int x=0;
            }
            break;
            
            //TODO(james): split input into its own function
            case WM_INPUT:
            {
                //is there any data? if so, how much?
                uint32_t dwSize;
                if (GetRawInputData((HRAWINPUT)Message.lParam,
                                    RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER))
                    == -1)
                {
                    break;
                }

                //TODO(james):switch to custom alloc
                LPBYTE lpb = new BYTE[dwSize];
                if (lpb == NULL)
                {
                    break;
                }
                //actually copy out input data
                if (GetRawInputData((HRAWINPUT)Message.lParam,
                                    RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER))
                    != dwSize)
                {
                    delete[] lpb;//TODO(james): yooo
                    break;
                }

                PRAWINPUT raw = (PRAWINPUT)lpb;

                if (raw->header.dwType == RIM_TYPEKEYBOARD)
                {
                    uint32 Event = raw->data.keyboard.Message;

                    uint32 VKey = raw->data.keyboard.VKey;
                    uint32 ScanCode = raw->data.keyboard.MakeCode;

                    GetKeyState(0); // TODO(james): why
                    BYTE KeyboardState[256];
                    GetKeyboardState(KeyboardState);

                    char AsciiOut[16] = {};
                    //AsciiOut[15] = '\0';
                    int ToAsciiResult = ToAsciiEx(VKey, ScanCode, KeyboardState, (LPWORD)&AsciiOut, 16, 0);
                    //bool32 HighBitOn = (ScanCode) >> 31;
                    if (ToAsciiResult)
                    {
                        if (GetAsyncKeyState(VKey) & 0x8000)
                        {
                            C_CopyString(KeyboardController->InputCharacters, 16, AsciiOut);
                        }
                        else
                        {
                            //DebugConsole("UP");
                        }
                    }

                    for (int i = 0; i < 256; i++)
                    {
                        if (GetAsyncKeyState(i) & 0x8000)
                            KeyboardController->KeyboardState[i] = true;
                    }

                    //TODO(james): will this handle multiple keys in one frame?
                    if (ScanCode == sc_w)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->MoveForward, Event);
                    }
                    if (ScanCode == sc_a)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->MoveLeft, Event);
                    }
                    if (ScanCode == sc_s)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->MoveBack, Event);
                    }
                    if (ScanCode == sc_d)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->MoveRight, Event);
                    }
                    if (ScanCode == sc_x)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->MoveUp, Event);
                    }
                    if (VK_SPACE == VKey)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->MoveDown, Event);
                    }
                    if (VKey == VK_ESCAPE)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->Quit, Event);
                    }
                    if (VKey == VK_UP)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->ArrowKeyUp, Event);
                    }
                    if (VKey == VK_DOWN)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->ArrowKeyDown, Event);
                    }
                    if (VKey == VK_LEFT)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->ArrowKeyLeft, Event);
                    }
                    if (VKey == VK_RIGHT)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->ArrowKeyRight, Event);
                    }
                    if (ScanCode == sc_e)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->Action1, Event);
                    }
                    if (ScanCode == sc_r)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->Action2, Event);
                    }
                    if (ScanCode == sc_f)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->Action3, Event);
                    }
                    if (ScanCode == sc_q)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->Action4, Event);
                    }
                    if (VKey == VK_F5)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->PlayPause, Event);
                    }
                    if (VKey == VK_F11)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->ToggleFullscreen, Event);
                    }
                    if (VKey == VK_OEM_3) //"`~" key
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->ToggleGUI, Event);
                    }
                    if (VKey == VK_DELETE)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->DeleteKey, Event);
                    }
                    if (VKey == VK_CONTROL)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->CtrlKey, Event);
                    }
                    if (ScanCode == sc_c)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->CopyKey, Event);
                    }
                    if (ScanCode == sc_v)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->PasteKey, Event);
                    }
                    if (ScanCode == sc_z)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->UndoKey, Event);
                    }
                    if (VKey == VK_SPACE)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->Spacebar, Event);
                    }

                    if (VKey == VK_TAB)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->TabKey, Event);
                    }

                    //DebugConsole("Unknown key");
                    TranslateMessage(&Message); //TODO(james): is this necessary?
                    DispatchMessage(&Message);
                }
                else if (raw->header.dwType == RIM_TYPEMOUSE)
                {
                    //TODO(james) do a bounds check here, that the action occurre
                    //while the mouse was in the window range

                    // NOTE(james): USBUTTONFLAGS
                    USHORT Event = raw->data.mouse.usButtonFlags;
                    if (Event & RI_MOUSE_LEFT_BUTTON_DOWN)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->LeftMouseClick, WM_KEYDOWN);
                        //DebugConsole("Left mouse down");
                    }

                    if (Event & RI_MOUSE_LEFT_BUTTON_UP)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->LeftMouseClick, WM_KEYUP);
                    }

                    if (Event & RI_MOUSE_RIGHT_BUTTON_DOWN)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->RightMouseClick, WM_KEYDOWN);
                    }

                    if (Event & RI_MOUSE_RIGHT_BUTTON_UP)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->RightMouseClick, WM_KEYUP);
                    }
                    
                    if (Event & RI_MOUSE_BUTTON_3_UP)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->MiddleMouseClick, WM_KEYUP);
                    }
                    
                    if (Event & RI_MOUSE_BUTTON_3_DOWN)
                    {
                        Win32ProcessKeyboardMessage(&KeyboardController->MiddleMouseClick, WM_KEYDOWN);
                    }

                    if (Event & RI_MOUSE_WHEEL)
                    {
                        KeyboardController->MouseWheelDelta = ((short)(unsigned short)raw->data.mouse.usButtonData);
                    }

                    //
                    // NOTE(james): USFLAGS
                    //
                    uint16 MouseState = raw->data.mouse.usFlags;
                    if (MouseState & MOUSE_ATTRIBUTES_CHANGED)
                    {
                    }

                    if (MouseState & MOUSE_MOVE_RELATIVE || MouseState == 0) // MOUSE_MOVE_RELATIVE is 0
                    {
                        KeyboardController->MouseDeltaX += raw->data.mouse.lLastX;
                        KeyboardController->MouseDeltaY += raw->data.mouse.lLastY;
                    }

                    if (MouseState & MOUSE_MOVE_ABSOLUTE)
                    {
                        int x = 0;
                    }

                    if (MouseState & MOUSE_VIRTUAL_DESKTOP)
                    {
                    }
                }
                else
                {
                    DebugConsole("Unknown input");
                    TranslateMessage(&Message); //TODO(james): is this necessary?
                    DispatchMessage(&Message);
                }
                delete[] lpb; // free this now

                break;
            } //end of WM_INPUT

            default:
            {
                TranslateMessage(&Message); //TODO(james): is this necessary?
                DispatchMessageA(&Message);
            }
            break;

        } //end of Switch

    } //end message queue
    POINT CursorPoint;
    GetCursorPos(&CursorPoint);
    ScreenToClient(Window, &CursorPoint);
    // NOTE(james): windows is Y down, so here we flip it
    v2 Dim = Win32GetWindowDimension(Window);
    KeyboardController->MousePosition = V2(CursorPoint.x, Dim.y - CursorPoint.y);
}

#if 0
internal void
Win32ProcessXInputDigitalButton(DWORD XInputButtonState,
                                game_button_state *OldState, DWORD ButtonBit,
                                game_button_state *NewState)
{
    NewState->EndedDown = ((XInputButtonState & ButtonBit) == ButtonBit);
    NewState->HalfTransitionCount = (OldState->EndedDown != NewState->EndedDown) ? 1 : 0;
}

internal real32
Win32ProcessXInputStickValue(SHORT Value, SHORT DeadZoneThreshold)
{
    real32 Result = 0;

    if(Value < -DeadZoneThreshold)
    {
        Result = (real32)((Value + DeadZoneThreshold) / (32768.0f - DeadZoneThreshold));
    }
    else if(Value > DeadZoneThreshold)
    {
        Result = (real32)((Value - DeadZoneThreshold) / (32767.0f - DeadZoneThreshold));
    }

    return(Result);
}
#endif

////////////////////////////////////////////////////
// LOAD GAME CODE
////////////////////////////////////////////////////
void FreeGameCode(game_code* GameCode)
{
    if (GameCode->LoadedDLL)
    {
        BOOL FreeResult = FreeLibrary(GameCode->LoadedDLL);
        Assert(FreeResult);
        GameCode->LoadedDLL = 0;
    }
    GameCode->UpdateAndRender = 0;
}

bool LoadGameCode(game_code* GameCode)
{
    bool Result = false;

    WIN32_FILE_ATTRIBUTE_DATA Ignored; //NOTE(james): not used for anything
    if (!GetFileAttributesEx(LOCK_FILE_NAME, GetFileExInfoStandard, &Ignored)) //NOTE(james): is the lock file there?
    {
        GameCode->LastWriteTime = Win32GetLastWriteTime(DLL_FILE_NAME);

#if YAMGINE_DEBUG
        uint32 RandomNumber = (uint32)Win32GetTimestamp();
        STRING(RandomNumberString, 32);
        ToString(&RandomNumberString, RandomNumber);

        STRING(TempDllFileName, MAX_PATH);
        C_AppendSubstring(&TempDllFileName, DLL_FILE_NAME, C_FindLastOf(DLL_FILE_NAME, '.'));

        AppendStringEx(&TempDllFileName, "_%s.dll", RandomNumberString.Text);

        //NOTE(james): we do this so yamgine.dll can be replaced safely on the next recompile
        bool32 CopyResult = CopyFile(DLL_FILE_NAME, TempDllFileName.Text, FALSE);
        DWORD LastError = GetLastError();
        Assert(CopyResult);

        GameCode->LoadedDLL = LoadLibrary(TempDllFileName.Text);
#else
        GameCode->LoadedDLL = LoadLibrary(DLL_FILE_NAME);
#endif

        if (GameCode->LoadedDLL)
        {

            //NOTE(james): GetProcAddress gives a generic function pointer, so we cast it
            //to the kind of function pointer we want
            GameCode->UpdateAndRender = (game_update_and_render*)
                GetProcAddress(GameCode->LoadedDLL, "GameUpdateAndRender");

            if (!GameCode->UpdateAndRender)
            {
                Assert(0);
            }

            Result = true;
        }
        else
        {
            InvalidCodePath;
        }
    }

    return Result;
}

////////////////////////////////////////////////////
// OPENGL
////////////////////////////////////////////////////
void FreeOpenGLCode(gl_code* GLCode)
{
    if (GLCode->LoadedDLL)
    {
        FreeLibrary(GLCode->LoadedDLL);
        GLCode->LoadedDLL = 0;
    }
    GLCode->OpenGLInit = 0;
    GLCode->OpenGLExit = 0;
    GLCode->OpenGLImGuiLoadFonts = 0;
    GLCode->OpenGLRenderCommands = 0;
}

bool LoadOpenGLCode(gl_code* GLCode)
{
    bool Result = false;

    WIN32_FILE_ATTRIBUTE_DATA Ignored; //NOTE(james): not used for anything
    if (!GetFileAttributesEx(GL_LOCK_FILE_NAME, GetFileExInfoStandard, &Ignored)) //NOTE(james): is the lock file there?
    {
        GLCode->LastWriteTime = Win32GetLastWriteTime(GL_DLL_FILE_NAME);

#if YAMGINE_DEBUG
        //NOTE(james): we do this so renderer_opengl.dll can be replaced safely on the next recompile
        uint32 RandomNumber = (uint32)rand();
        STRING(RandomNumberString, 32);
        ToString(&RandomNumberString, RandomNumber);

        STRING(TempDllFileName, MAX_PATH);
        C_AppendSubstring(&TempDllFileName, GL_DLL_FILE_NAME, C_FindLastOf(GL_DLL_FILE_NAME, '.'));

        AppendStringEx(&TempDllFileName, "_%s.dll", RandomNumberString.Text);

        bool32 CopyResult = CopyFile(GL_DLL_FILE_NAME, TempDllFileName.Text, FALSE);
        DWORD LastError = GetLastError();
        Assert(CopyResult);

        GLCode->LoadedDLL = LoadLibrary(TempDllFileName.Text);
#else
        GLCode->LoadedDLL = LoadLibrary(GL_DLL_FILE_NAME);
#endif
        if (GLCode->LoadedDLL)
        {

            //NOTE(james): GetProcAddress gives a generic function pointer, so we cast it
            //to the kind of function pointer we want
            GLCode->OpenGLInit = (opengl_init*)
                GetProcAddress(GLCode->LoadedDLL, "OpenGLInit");

            GLCode->OpenGLExit = (opengl_exit*)
                GetProcAddress(GLCode->LoadedDLL, "OpenGLExit");

            GLCode->OpenGLImGuiLoadFonts = (opengl_imgui_loadfonts*)
                GetProcAddress(GLCode->LoadedDLL, "OpenGLImGuiLoadFonts");

            GLCode->OpenGLRenderCommands = (opengl_render_commands*)
                GetProcAddress(GLCode->LoadedDLL, "OpenGLRenderCommands");

            GLCode->OpenGLCreateBuffer = (opengl_create_buffer*)
                GetProcAddress(GLCode->LoadedDLL, "OpenGLCreateBuffer");
            
            GLCode->OpenGLFillBuffer = (opengl_fill_buffer*)
                GetProcAddress(GLCode->LoadedDLL, "OpenGLFillBuffer");

            GLCode->OpenGLAllocateTexture = (opengl_allocate_texture*)
                GetProcAddress(GLCode->LoadedDLL, "OpenGLAllocateTexture");

            GLCode->OpenGLCreateFbo = (opengl_create_fbo*)
                GetProcAddress(GLCode->LoadedDLL, "OpenGLCreateFbo");

            GLCode->OpenGLResizeFbo = (opengl_resize_fbo*)
                GetProcAddress(GLCode->LoadedDLL, "OpenGLResizeFbo");
            
            GLCode->OpenGLCreateShader = (opengl_create_shader*)
                GetProcAddress(GLCode->LoadedDLL, "OpenGLCreateShader");

            //Assert(GLCode->OpenGLInit);
            
            Result = true;
        }
        else
        {
            InvalidCodePath;
        }
    }

    return Result;
}

// NOTE(james): these correspond to the basic stub window used for boostrapping I think
int Win32OpenGLAttribs[] = {
    WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
    WGL_CONTEXT_MINOR_VERSION_ARB, 3,
    WGL_CONTEXT_FLAGS_ARB, 0 // NOTE(casey): Enable for testing WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB
#if YAMGINE_DEBUG
    | WGL_CONTEXT_DEBUG_BIT_ARB
#endif
    //|WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB
    ,
    WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,//WGL_CONTEXT_COMPATIBILITY_PROFILE_BIT_ARB
    0,
};

internal void
Win32SetPixelFormat(HDC WindowDC)
{
    YamProf(Win32SetPixelFormat);
    int SuggestedPixelFormatIndex = 0;
    GLuint ExtendedPick = 0;

    // NOTE(james): second pass
    if (wglChoosePixelFormatARB)
    {
        YamProf(Win32ChoosePixelFormatARB);
        int IntAttribList[] = {
            WGL_DRAW_TO_WINDOW_ARB,
            GL_TRUE,
            WGL_ACCELERATION_ARB,
            WGL_FULL_ACCELERATION_ARB,
            WGL_SUPPORT_OPENGL_ARB,
            GL_TRUE,
            WGL_DOUBLE_BUFFER_ARB, //NOTE(james): Casey was having trouble with this while streaming
            GL_TRUE,
            WGL_PIXEL_TYPE_ARB,
            WGL_TYPE_RGBA_ARB,
            WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB,
            GL_TRUE,
            WGL_SAMPLE_BUFFERS_ARB,
            1, //must be set to one to enable antialiasing
            WGL_SAMPLES_ARB,
            16, //4x, 8x, 16x, etc
            WGL_DEPTH_BITS_ARB,
            24,
            WGL_STENCIL_BITS_ARB,
            8,
            0,
        };

        if (!OpenGLSupportsSRGBFramebuffer)
        {
            IntAttribList[10] = 0; //NOTE(james): set WGL_FRAMEBUFFER_SRGB_CAPABLE_ARB to 0 in above array
        }

        if (!OpenGLSupportsMultisampling)
        {
            IntAttribList[11] = 0;
            IntAttribList[12] = 0;
        }

        wglChoosePixelFormatARB(WindowDC, IntAttribList, 0, 1,
                                &SuggestedPixelFormatIndex, &ExtendedPick);
    }

    // NOTE(james): first pass
    if (!ExtendedPick)
    {
        YamProf(Win32ChoosePixelFormatLegacy);
        PIXELFORMATDESCRIPTOR DesiredPixelFormat = {
            sizeof(PIXELFORMATDESCRIPTOR), //  size of this pfd
            1, // version number
            PFD_DRAW_TO_WINDOW | // support window
                PFD_SUPPORT_OPENGL | // support OpenGL
                PFD_DOUBLEBUFFER, // double buffered
            PFD_TYPE_RGBA, // RGBA type
            24, // 24-bit color depth
            0,
            0, 0, 0, 0, 0, // color bits ignored
            0, // no alpha buffer
            0, // shift bit ignored
            0, // no accumulation buffer
            0, 0, 0, 0, // accum bits ignored
            32, // 32-bit z-buffer
            0, // no stencil buffer
            0, // no auxiliary buffer
            PFD_MAIN_PLANE, // main layer
            0, // reserved
            0, 0, 0 // layer masks ignored
        };

        SuggestedPixelFormatIndex = ChoosePixelFormat(WindowDC, &DesiredPixelFormat);
        Assert(SuggestedPixelFormatIndex > 0);

        int Err = GetLastError();
        Assert(Err != 128); // NOTE(james): indicates bad graphics driver? Happened on my old laptop
    }

    PIXELFORMATDESCRIPTOR SuggestedPixelFormat;
    DescribePixelFormat(WindowDC, SuggestedPixelFormatIndex,
                        sizeof(SuggestedPixelFormat), &SuggestedPixelFormat);
    BOOL SetPixelFormatResult = SetPixelFormat(WindowDC, SuggestedPixelFormatIndex, &SuggestedPixelFormat);
    Assert(SetPixelFormatResult);
}

internal HGLRC
Win32InitOpenGL(HDC InitialWindowDC, gl_state* GLState, gl_code* GLCode, 
                platform_work_queue* Queue, win32_thread_startup* WorkStartups)
{
    YamProf(Win32InitOpenGL);

    wgl_create_context_attribs_arb* wglCreateContextAttribsARB = NULL;

    WNDCLASSA WindowClass = {};

    WindowClass.lpfnWndProc = DefWindowProcA;
    WindowClass.hInstance = GetModuleHandle(0);
    WindowClass.lpszClassName = "YamgineWGLLoader";

    if (RegisterClassA(&WindowClass))
    {
        YamProf(Win32_WGLLoader);
        HWND BootloaderWindow = CreateWindowExA(
            0,
            WindowClass.lpszClassName,
            "Yamgine",
            0,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            CW_USEDEFAULT,
            0,
            0,
            WindowClass.hInstance,
            0);

        HDC BootloaderWindowDC = GetDC(BootloaderWindow);
        Win32SetPixelFormat(BootloaderWindowDC);
        HGLRC OpenGLRC = wglCreateContext(BootloaderWindowDC);
        if (wglMakeCurrent(BootloaderWindowDC, OpenGLRC))
        {
            wglChoosePixelFormatARB = (wgl_choose_pixel_format_arb*)wglGetProcAddress("wglChoosePixelFormatARB");
            DWORD Error = GetLastError();
            if (Error == 127)
            {
                Assert(0); // function wasn't found, might be a graphics driver problem
            }
            wglCreateContextAttribsARB = (wgl_create_context_attribs_arb*)wglGetProcAddress("wglCreateContextAttribsARB");
            wglSwapIntervalEXT = (wgl_swap_interval_ext*)wglGetProcAddress("wglSwapIntervalEXT");
            wglGetExtensionsStringEXT = (wgl_get_extensions_string_ext*)wglGetProcAddress("wglGetExtensionsStringEXT");

            if (wglGetExtensionsStringEXT)
            {
                YamProf(Win32WglExtensionReading);
                char* Extensions = (char*)wglGetExtensionsStringEXT();
                char* At = Extensions;
                while (*At)
                {
                    while (IsWhitespace(*At))
                    {
                        ++At;
                    }
                    char* End = At;
                    while (*End && !IsWhitespace(*End))
                    {
                        ++End;
                    }

                    uintptr Count = End - At;

                    if (0)
                    {
                    }
                    else if (C_StringsAreEqualUnion(At, "WGL_EXT_framebuffer_sRGB"))
                    {
                        OpenGLSupportsSRGBFramebuffer = true;
                    }
                    else if (C_StringsAreEqualUnion(At, "WGL_ARB_multisample"))
                    {
                        OpenGLSupportsMultisampling = true;
                    }

                    At = End;
                }
            }

            wglMakeCurrent(0, 0);
        }

        wglDeleteContext(OpenGLRC);
        ReleaseDC(BootloaderWindow, BootloaderWindowDC);
        DestroyWindow(BootloaderWindow);
        UnregisterClass(WindowClass.lpszClassName, GetModuleHandle(0));
    }

    bool32 IsModernContext = true;
    HGLRC OpenGLRCMain = 0;
    if (wglCreateContextAttribsARB)
    {
        Win32SetPixelFormat(InitialWindowDC);
        OpenGLRCMain= wglCreateContextAttribsARB(InitialWindowDC, 0, Win32OpenGLAttribs);
        for (int i = 0; i < WORKER_THREAD_COUNT; i++)
        {
            HGLRC WorkerContext = wglCreateContextAttribsARB(InitialWindowDC, OpenGLRCMain, 
                                                             Win32OpenGLAttribs);
            WorkStartups[i].GLContext = WorkerContext;
        }
        Queue->WindowDC = InitialWindowDC;
        DWORD Error = GetLastError();
        int x = 0;
    }

    if (wglMakeCurrent(InitialWindowDC, OpenGLRCMain))
    {
        void* TempMemory = Win32AllocateMemory(Megabytes(2));
        
        *GLState = GLCode->OpenGLInit(IsModernContext, OpenGLSupportsSRGBFramebuffer, 
                                      TempMemory, Megabytes(2), 
                                      YamProfiling, &Platform);
        
        Win32DeallocateMemory(TempMemory);
    }
    else
    {
        Assert(0);
    }

    return (OpenGLRCMain);
}
////////////////////////////////////////////////////
// FMOD
////////////////////////////////////////////////////

#if FMOD_SUPPORT
void (*Common_Private_Error)(FMOD_RESULT, const char*, int);

#define ERRCHECK(_result) ERRCHECK_fn(_result, __FILE__, __LINE__)
void ERRCHECK_fn(FMOD_RESULT result, const char* file, int line)
{
    if (result != FMOD_OK)
    {
        if (Common_Private_Error)
        {
            Common_Private_Error(result, file, line);
        }
        Assert(0);
        //TODO(james): logging
        //Common_Fatal("%s(%d): FMOD error %d - %s", file, line, result, FMOD_ErrorString(result));
    }
}

void InitFmod(FMOD* Fmod, char* ExecutableFolder)
{
    CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED);

    STRING(LogFilePath, 255);
    SetString(&LogFilePath, ExecutableFolder); // just copying the folder dir in
    AppendString(&LogFilePath, "\\fmod_output.log");

#if YAMGINE_DEBUG
    ERRCHECK(FMOD_Debug_Initialize(FMOD_DEBUG_LEVEL_LOG, FMOD_DEBUG_MODE_FILE, NULL, LogFilePath.Text));
#endif
    void* ExtraDriverData = NULL; //TODO(james): research what this is
    ERRCHECK(FMOD_Studio_System_Create(&Fmod->StudioSystem, FMOD_VERSION));

    FMOD_SYSTEM* FmodLowLevelSystem = NULL;
    ERRCHECK(FMOD_Studio_System_GetLowLevelSystem(Fmod->StudioSystem, &FmodLowLevelSystem));
    ERRCHECK(FMOD_System_SetSoftwareFormat(FmodLowLevelSystem, 0, FMOD_SPEAKERMODE_DEFAULT, 0));

    ERRCHECK(FMOD_Studio_System_Initialize(Fmod->StudioSystem,
                                           32,
                                           FMOD_STUDIO_INIT_NORMAL
                                               | FMOD_STUDIO_INIT_LIVEUPDATE, //should probably be in some IFDEF
                                           FMOD_INIT_NORMAL | FMOD_INIT_3D_RIGHTHANDED,
                                           ExtraDriverData));

    FMOD_STUDIO_SYSTEM* StudioSystem = Fmod->StudioSystem;

    STRING(MasterBankPath, MAX_PATH);
    SetString(&MasterBankPath, ExecutableFolder);
    AppendString(&MasterBankPath, "\\media\\audio\\Master Bank.bank");
    FMOD_STUDIO_BANK* MasterBank = NULL;
    ERRCHECK(FMOD_Studio_System_LoadBankFile(StudioSystem, MasterBankPath.Text, FMOD_STUDIO_LOAD_BANK_NORMAL, &MasterBank));

    STRING(MasterBankStringPath, MAX_PATH);
    SetString(&MasterBankStringPath, ExecutableFolder);
    AppendString(&MasterBankStringPath, "\\media\\audio\\Master Bank.strings.bank");
    FMOD_STUDIO_BANK* StringsBank = NULL;
    ERRCHECK(FMOD_Studio_System_LoadBankFile(StudioSystem, MasterBankStringPath.Text, FMOD_STUDIO_LOAD_BANK_NORMAL, &StringsBank));

    //get all events
    ERRCHECK(FMOD_Studio_System_GetEvent(StudioSystem, "event:/Music", &Fmod->Events.Music));
    ERRCHECK(FMOD_Studio_System_GetEvent(StudioSystem, "event:/Bullet_Fire", &Fmod->Events.MouseClick));
    ERRCHECK(FMOD_Studio_System_GetEvent(StudioSystem, "event:/Player_Jump", &Fmod->Events.PlayerJump));
    ERRCHECK(FMOD_Studio_System_GetEvent(StudioSystem, "event:/Weapon_Pickup", &Fmod->Events.WeaponPickup));
    ERRCHECK(FMOD_Studio_System_GetEvent(StudioSystem, "event:/Enemy_Fire", &Fmod->Events.EnemyFire));
    ERRCHECK(FMOD_Studio_System_GetEvent(StudioSystem, "event:/Player_Footsteps_Default", &Fmod->Events.Step));
    ERRCHECK(FMOD_Studio_System_GetEvent(StudioSystem, "event:/Bullet_Travel", &Fmod->Events.BulletTravel));
    ERRCHECK(FMOD_Studio_System_GetEvent(StudioSystem, "event:/Player_Death", &Fmod->Events.PlayerDeath));
    ERRCHECK(FMOD_Studio_System_GetEvent(StudioSystem, "event:/Enemy_Death", &Fmod->Events.EnemyDeath));

    //get all api
    //system
    Fmod->API.Studio_System_GetEvent = &FMOD_Studio_System_GetEvent;
    Fmod->API.Studio_System_Update = &FMOD_Studio_System_Update;
    Fmod->API.Studio_System_SetListenerAttributes = &FMOD_Studio_System_SetListenerAttributes;

    //event description
    Fmod->API.Studio_EventDescription_CreateInstance = &FMOD_Studio_EventDescription_CreateInstance;

    //event instance
    Fmod->API.Studio_EventInstance_Set3DAttributes = &FMOD_Studio_EventInstance_Set3DAttributes;
    Fmod->API.Studio_EventInstance_Start = &FMOD_Studio_EventInstance_Start;
    Fmod->API.Studio_EventInstance_Stop = &FMOD_Studio_EventInstance_Stop;
    Fmod->API.Studio_EventInstance_SetParameterValue = &FMOD_Studio_EventInstance_SetParameterValue;
}
#endif
////////////////////////////////////////////////////
// IMGUI
////////////////////////////////////////////////////

PLATFORM_IMGUI_SETUP(Win32ImGuiSetup)
{
    YamProf(Win32ImGuiSetup);
    ImGuiIO& IO = FontInfo->GetIOPtr();
    //IO.ImeWindowHandle = Window;
    IO.KeyMap[ImGuiKey_Tab] = VK_TAB;
    IO.KeyMap[ImGuiKey_LeftArrow] = VK_LEFT;
    IO.KeyMap[ImGuiKey_RightArrow] = VK_RIGHT;
    IO.KeyMap[ImGuiKey_UpArrow] = VK_UP;
    IO.KeyMap[ImGuiKey_DownArrow] = VK_DOWN;
    IO.KeyMap[ImGuiKey_PageUp] = VK_PRIOR;
    IO.KeyMap[ImGuiKey_PageDown] = VK_NEXT;
    IO.KeyMap[ImGuiKey_Home] = VK_HOME;
    IO.KeyMap[ImGuiKey_End] = VK_END;
    IO.KeyMap[ImGuiKey_Delete] = VK_DELETE;
    IO.KeyMap[ImGuiKey_Backspace] = VK_BACK;
    IO.KeyMap[ImGuiKey_Enter] = VK_RETURN;
    IO.KeyMap[ImGuiKey_Escape] = VK_ESCAPE;
    IO.KeyMap[ImGuiKey_A] = 0x41;
    IO.KeyMap[ImGuiKey_C] = 0x43;
    IO.KeyMap[ImGuiKey_V] = 0x56;
    IO.KeyMap[ImGuiKey_X] = 0x58;
    IO.KeyMap[ImGuiKey_Y] = 0x59;
    IO.KeyMap[ImGuiKey_Z] = 0x5A;
    GlobalGLCode->OpenGLImGuiLoadFonts(GlobalGLState, FontInfo);
}

PLATFORM_CREATE_BUFFER(Win32CreateBuffer)
{
    GlobalGLCode->OpenGLCreateBuffer(&GlobalGLState->gl, Buffer, BufferChannel, ChannelWidth, Residency);
}

PLATFORM_FILL_BUFFER(Win32FillBuffer)
{
    YamProf(PlatformFillBuffer);
    GlobalGLCode->OpenGLFillBuffer(&GlobalGLState->gl, Buffer);
 }

PLATFORM_ALLOCATE_TEXTURE(Win32AllocateTexture)
{
    GlobalGLCode->OpenGLAllocateTexture(&GlobalGLState->gl, Texture);
}

PLATFORM_CREATE_FBO(Win32CreateFbo)
{
    return GlobalGLCode->OpenGLCreateFbo(&GlobalGLState->gl, Width, Height, FboOptions, DebugName);
}

PLATFORM_RESIZE_FBO(Win32ResizeFbo)
{
    return GlobalGLCode->OpenGLResizeFbo(&GlobalGLState->gl, Fbo, Width, Height);
}

PLATFORM_CREATE_SHADER(Win32CreateShader)
{
    return GlobalGLCode->OpenGLCreateShader(&GlobalGLState->gl, Shader, MetashaderFilePath, Header,ShaderPreproc, DestinationFolder,  UniformStructSize, Temp, API);
}

PLATFORM_CREATE_SOCKET(Win32CreateSocket)
{
    network_socket Result = {};

    Result.Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    Assert(Result.Socket != INVALID_SOCKET);

    u_long NonBlocking = 1;
    int NonBlockingResult = ioctlsocket(Result.Socket, FIONBIO, &NonBlocking);
    Assert(NonBlockingResult == 0);

    return Result;
}

sockaddr_in BuildSockAddr(char* IP, uint16 Port)
{
    IN_ADDR IPAddr = {};
    if (C_StringLength(IP) > 0)
    {
        int ConversionResult = inet_pton(AF_INET, IP, &IPAddr);
        Assert(ConversionResult == 1);
    }

    sockaddr_in Address = {};
    Address.sin_family = AF_INET;

    if (C_StringLength(IP) > 0)
    {
        Address.sin_addr = IPAddr;
    }
    else
    {
        Address.sin_addr.s_addr = INADDR_ANY;
    }
    Address.sin_port = htons(Port); //send port

    return Address;
}

//pass empty IP string to bind to INADDR_ANY
PLATFORM_BIND_SOCKET(Win32BindSocket)
{
    sockaddr_in Address = BuildSockAddr(IP, Port);

    int BindResult = bind(Socket->Socket, (SOCKADDR*)&Address, sizeof(Address));
    if (BindResult == -1)
    {
        int Error = WSAGetLastError();
        Assert(0);
    }
    Assert(BindResult == 0);

    Socket->IsBound = true;
}

PLATFORM_SENDTO(Win32SendTo)
{
    YamProf(Win32Socket_SendTo);
    Assert(Socket.IsBound);

    sockaddr_in Address = BuildSockAddr(DestinationIP, DestinationPort);

    uint32 PayloadSize = C_StringLength(Payload);
    uint32 SentAmount = sendto(Socket.Socket, Payload, PayloadSize, 0, (SOCKADDR*)&Address, sizeof(sockaddr_in));
    Assert(PayloadSize == SentAmount);
}

PLATFORM_RECVFROM(Win32RecvFrom)
{
    YamProf(Win32Socket_RecvFrom);
    Assert(Socket.IsBound);

    sockaddr_in FromAddr;
    int FromAddrSize = sizeof(sockaddr_in); //TODO(james): when will I need this?
    uint32 BytesRecieved = recvfrom(Socket.Socket,
                                    &OutputBuffer[0], OutputBufferSize,
                                    0, //flags
                                    (SOCKADDR*)&FromAddr, &FromAddrSize);

    if (BytesRecieved == SOCKET_ERROR)
    {
        BytesRecieved = 0;
        int Error = WSAGetLastError();

        //NOTE(james): some errors are non-fatal, so we should just continue like normal
        //NOTE(james): WSAEWOULDBLOCK = Resource unavailable. Returned when trying to read from a non-blocking socket that has no data queued
        if (Error != WSAEWOULDBLOCK)
        {
            Assert(0);
        }
    }

    inet_ntop(AF_INET, &FromAddr.sin_addr, SenderIP, 16); //convert IP to string format
    *SenderPort = FromAddr.sin_port;

    return BytesRecieved;
}

//NOTE(james): ShowCursor API is weird, calling it with true/false increments/decrements a counter.
// <0 hides cursor. This bool check gets us "normal" toggling behavior,
//value will never get above 1 or below 0
PLATFORM_SHOW_CURSOR(Win32ShowCursor)
{
    if (!GlobalWin32State->ShowCursor)
    {
        ShowCursor(true);
    }
    GlobalWin32State->ShowCursor = true;
}

global_variable WINDOWPLACEMENT GlobalWindowPlacement = {};
PLATFORM_HIDE_CURSOR(Win32HideCursor)
{
    RECT windowRect;
    
    // Get the window in screen space
    GetWindowRect( GlobalWindow, &windowRect );
    POINT CursorPoint;
    GetCursorPos(&CursorPoint);
    bool isInRect = PtInRect( &windowRect, CursorPoint );
    
    if (isInRect)
    {
        if (GlobalWin32State->ShowCursor)
        {
            ShowCursor(false);
        }
        GlobalWin32State->ShowCursor = false;
        GlobalWin32State->HideCursorPos = HidePos;
    }
}

PLATFORM_SET_VSYNC(Win32SetVsync)
{
    if (wglSwapIntervalEXT)
    {
        if (VsyncOn)
        {
            wglSwapIntervalEXT(1);
        }
        else
        {
            wglSwapIntervalEXT(0);
        }
    }
}

PLATFORM_SET_FULLSCREEN(Win32SetFullscreen)
{
    // NOTE(casey): This follows Raymond Chen's prescription
    // for fullscreen toggling, see:
    // http://blogs.msdn.com/b/oldnewthing/archive/2010/04/12/9994016.aspx
    DWORD Style = GetWindowLong(GlobalWindow, GWL_STYLE);

    if (Style & WS_OVERLAPPEDWINDOW && FullscreenOn)
    {
        MONITORINFO MonitorInfo = { sizeof(MonitorInfo) };
        if (GetWindowPlacement(GlobalWindow, &GlobalWindowPlacement) && GetMonitorInfo(MonitorFromWindow(GlobalWindow, MONITOR_DEFAULTTOPRIMARY), &MonitorInfo))
        {
            SetWindowLong(GlobalWindow, GWL_STYLE, Style & ~WS_OVERLAPPEDWINDOW);
            SetWindowPos(GlobalWindow, HWND_TOP,
                         MonitorInfo.rcMonitor.left, MonitorInfo.rcMonitor.top,
                         MonitorInfo.rcMonitor.right - MonitorInfo.rcMonitor.left,
                         MonitorInfo.rcMonitor.bottom - MonitorInfo.rcMonitor.top,
                         SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
        }
    }
    else if (!FullscreenOn)
    {
        SetWindowLong(GlobalWindow, GWL_STYLE, Style | WS_OVERLAPPEDWINDOW);
        SetWindowPlacement(GlobalWindow, &GlobalWindowPlacement);
        SetWindowPos(GlobalWindow, 0, 0, 0, 0, 0,
                     SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
    }
}

#if 0
#define STACK_WALK_64(name) BOOL name(\
_In_ DWORD MachineType,                                               \
                                      \
_In_ HANDLE hProcess,                                                 \
                                      \
_In_ HANDLE hThread,                                                  \
                                      \
_Inout_ LPSTACKFRAME64 StackFrame,                                    \
                                      \
_Inout_ PVOID ContextRecord,                                          \
                                      \
_In_opt_ PREAD_PROCESS_MEMORY_ROUTINE64 ReadMemoryRoutine,            \
                                      \
_In_opt_ PFUNCTION_TABLE_ACCESS_ROUTINE64 FunctionTableAccessRoutine, \
                                      \
_In_opt_ PGET_MODULE_BASE_ROUTINE64 GetModuleBaseRoutine,             \
                                      \
_In_opt_ PTRANSLATE_ADDRESS_ROUTINE64 TranslateAddress\
)\

typedef STACK_WALK_64(stack_walk_64);
#endif

void CaptureStacktrace()
{
#if 0
    //TODO(james): I should use dll distributed with alongside game instead of using one given by Windows?
    HMODULE DbgHelp = LoadLibrary("dbghelp.dll");
    stack_walk_64* StackWalk64 = (stack_walk_64*)GetProcAddress(DbgHelp, "StackWalk64");


    STACKFRAME64 Stack[64] = {};
    STRING(StacktraceString, 10000);

    CONTEXT ContextRecord = {};

    //EnterCriticalSection( &DbgHelpLock );
    RtlCaptureContext(&ContextRecord);

    BOOL SymInitResult = SymInitialize(GetCurrentProcess(), NULL, true);
    bool StackWalkResult = true;
    uint32 StackCount = 0;

    STACKFRAME64 CurrentStackFrame = {}; //this is both an "in" and an "out" for StackWalk64

    HANDLE Process = GetCurrentProcess();
    HANDLE Thread = GetCurrentThread();
    while (StackWalkResult && StackCount < 64)
    {

        StackWalkResult = StackWalk64(IMAGE_FILE_MACHINE_AMD64,
                                      Process,
                                      Thread,
                                      &CurrentStackFrame,
                                      &ContextRecord,
                                      NULL,
                                      SymFunctionTableAccess64,
                                      SymGetModuleBase64,
                                      NULL);

        Stack[StackCount++] = CurrentStackFrame;
    }

    AppendString(&StacktraceString, "BEGIN STACKTRACE\r\n\r\n");
    for (uint32 StackIndex = StackCount - 1;
         StackIndex > 0;
         --StackIndex) //for (uint32 Index = 0; Index < StackCount; Index++)
    {
        struct symbol_name_pair
        {
            IMAGEHLP_SYMBOL64 Symbol = {};
            char SymbolName[255] = {};
        };

        symbol_name_pair SymbolNamePair = {};
        SymbolNamePair.Symbol.MaxNameLength = 255;

        BOOL GetSymResult = SymGetSymFromAddr64(Process, Stack[StackIndex].AddrPC.Offset, NULL, &SymbolNamePair.Symbol);
        char Tabs[128] = {};
        for (uint32 TabIndex = 0;
             TabIndex < StackIndex - 1;
             ++TabIndex)
        {
            Tabs[TabIndex * 2] = '-';
            Tabs[(TabIndex * 2) + 1] = '-';
        }

        if (StringLength(Tabs) > 0)
        {
            AppendString(&StacktraceString, (char*)&Tabs[0]);
        }
        AppendString(&StacktraceString, (char*)&SymbolNamePair.Symbol.Name[0]);

        AppendString(&StacktraceString, "\r\n");
        int x = 0;
    }

    //LeaveCriticalSection( &DbgHelpLock );
    Win32WriteBinaryFile("crash/stacktrace.txt", (uint8*)StacktraceString.Text, StringLength(StacktraceString.Text));

    MessageBox(
        GetActiveWindow(),
        "Crash!\n\nPlease send \"crash\" folder to jameslynnfulop@gmail.com with a description of what happened.",
        "Yamgine",
        MB_OK);
#endif
}

LONG ErrorCatcher(LPEXCEPTION_POINTERS p)
{
    CaptureStacktrace();
    return EXCEPTION_EXECUTE_HANDLER;
}

////////////////////////////////////////////////////
// MAIN
////////////////////////////////////////////////////

LRESULT CALLBACK WindowProc(HWND Window, UINT uMsg, WPARAM wParam, LPARAM lParam);

int CALLBACK
WinMain(HINSTANCE Instance,
        HINSTANCE PrevInstance,
        LPSTR CommandLine,
        int ShowCode)
{
#if !YAMGINE_DEBUG // enables our custom crash hooks
    SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOGPFAULTERRORBOX);
    SetUnhandledExceptionFilter((LPTOP_LEVEL_EXCEPTION_FILTER)&ErrorCatcher);
#endif
    
    LARGE_INTEGER Frequency;
    QueryPerformanceFrequency(&Frequency);
    YamProfiling = &MasterYamProfiling;

    YamProfiling->PerformanceFrequency = Frequency.QuadPart;
    YamProfiling->GetTimestamp = Win32GetTimestamp;
    InitProfiler();
    YamProfBegin(Win32Start);

    LARGE_INTEGER SeedCRandom;
    QueryPerformanceCounter(&SeedCRandom);
    srand(SeedCRandom.QuadPart);

    LoadLibraries();

    gl_code GLCode;
    
    Assert(LoadOpenGLCode(&GLCode));
    GlobalGLCode = &GLCode;

#if YAMGINE_DEBUG
    LPVOID BaseAddress = (LPVOID)Terabytes(2);
#else
    LPVOID BaseAddress = 0;
#endif

    game_memory GameMemory = {};
    GameMemory.PermanentStorageSize = Megabytes(256);
    GameMemory.TransientStorageSize = Megabytes(128);
    
    #if 0
    LoadRenderDoc(&GameMemory);
    GameMemory.RenderDocAPI->MaskOverlayBits(eRENDERDOC_Overlay_None,eRENDERDOC_Overlay_None);
    #endif

    win32_state Win32State = {};
    Win32State.ShowCursor = true;
    GlobalWin32State = &Win32State;
    Win32State.TotalSize = GameMemory.PermanentStorageSize + GameMemory.TransientStorageSize;
    Win32State.GameMemoryBlock = VirtualAlloc(BaseAddress, Win32State.TotalSize,
                                              MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);

    GameMemory.PermanentStorage = Win32State.GameMemoryBlock;
    GameMemory.TransientStorage = ((uint8*)GameMemory.PermanentStorage) + GameMemory.PermanentStorageSize;

    //api functions
    GameMemory.PlatformAPI.ImGuiSetup = Win32ImGuiSetup;
    GameMemory.PlatformAPI.FileExists = Win32FileExists;
    GameMemory.PlatformAPI.GetFirstFileInDirectory = Win32GetFirstFileInDirectory;
    GameMemory.PlatformAPI.GetNextFile = Win32GetNextFile;
    GameMemory.PlatformAPI.GetNextSubdirectory = Win32GetNextSubdirectory;
    GameMemory.PlatformAPI.GetFirstSubdirectory = Win32GetFirstSubdirectory;
    GameMemory.PlatformAPI.ReadBinaryFile = Win32ReadBinaryFile;
    GameMemory.PlatformAPI.WriteBinaryFile = Win32WriteBinaryFile;
    GameMemory.PlatformAPI.CreateDirectory = Win32CreateDirectory;
        GameMemory.PlatformAPI.DeleteDirectory = Win32DeleteDirectory;
    GameMemory.PlatformAPI.MoveFile = Win32MoveFile;
    GameMemory.PlatformAPI.QueryTime = Win32QueryTime;
    GameMemory.PlatformAPI.GetFileSize = Win32GetFileSize;
    GameMemory.PlatformAPI.WatchForFileChanges = Win32WatchForFileChanges;
    GameMemory.PlatformAPI.DumpChangedFileNotifications = Win32DumpChangedFileNotifications;

    GameMemory.PlatformAPI.SetWindowPosition = Win32SetWindowPosition;
    GameMemory.PlatformAPI.GetWindowPosition = Win32GetWindowPosition;
    
    //renderer specific stuff
    GameMemory.PlatformAPI.CreateBuffer = Win32CreateBuffer;
    GameMemory.PlatformAPI.FillBuffer = Win32FillBuffer;
    GameMemory.PlatformAPI.AllocateTexture = Win32AllocateTexture;
    GameMemory.PlatformAPI.CreateFbo = Win32CreateFbo;
    GameMemory.PlatformAPI.ResizeFbo = Win32ResizeFbo;
    GameMemory.PlatformAPI.CreateShader = Win32CreateShader;

    //networking
    GameMemory.PlatformAPI.CreateSocket = Win32CreateSocket;
    GameMemory.PlatformAPI.BindSocket = Win32BindSocket;
    GameMemory.PlatformAPI.RecvFrom = Win32RecvFrom;
    GameMemory.PlatformAPI.SendTo = Win32SendTo;

    //window management
    GameMemory.PlatformAPI.HideCursor = Win32HideCursor;
    GameMemory.PlatformAPI.ShowCursor = Win32ShowCursor;

    GameMemory.PlatformAPI.SetVsync = Win32SetVsync;
    GameMemory.PlatformAPI.SetFullscreen = Win32SetFullscreen;
    
    GameMemory.PlatformAPI.AddEntry = Win32AddEntry;
    GameMemory.PlatformAPI.CompleteAllWork = Win32CompleteAllWork;

    Platform = GameMemory.PlatformAPI;

    game_code GameCode;
    LoadGameCode(&GameCode);

    //icon loading tutorial http://www.winprog.org/tutorial/menus.html
    HICON MyIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(MY_ICON));
    HCURSOR SystemCursor = LoadCursor(NULL, IDC_ARROW);

    WNDCLASSA WindowClass = {};
    WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    WindowClass.lpfnWndProc = WindowProc;
    WindowClass.hInstance = Instance;
    WindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    WindowClass.lpszClassName = "YamgineWindowClass";
    WindowClass.hIcon = MyIcon;
    WindowClass.hCursor = SystemCursor;

    ATOM RegisterResult = RegisterClass(&WindowClass);
    Assert(RegisterResult);

    Win32GetExecutableLocation(GameMemory.ExecutableFolder);

#if FMOD_SUPPORT
    InitFmod(&GameMemory.Fmod, GameMemory.ExecutableFolder);
#endif

#if WWISE_SUPPORT
    WwiseInit(&GameMemory.Wwise, GameMemory.ExecutableFolder);
    WwiseLoadBanks();
#endif

    HWND Window = CreateWindowExA(
        0, // Optional window styles.
        WindowClass.lpszClassName, // Window class
        "Yamgine", // Window text
        WS_OVERLAPPEDWINDOW | WS_MAXIMIZE, // Window style

        // Size and position
        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,

        0, // Parent window
        0, // Menu
        Instance, // Instance handle
        0 // Additional application data
        );

    if (Window == NULL)
    {
        //TODO(james):error message
        DWORD Error = GetLastError();
        return 0;
    }

    GlobalWindow = Window;

    memory_arena YamProfilingReportArena = {};
    YamProfilingReportArena.Size = Kilobytes(50);
    YamProfilingReportArena.Base = (uint8*)Win32AllocateMemory(Kilobytes(50));

    memory_arena YamProfilingReportStartArena = {};
    YamProfilingReportStartArena.Size = Kilobytes(50);
    YamProfilingReportStartArena.Base = (uint8*)Win32AllocateMemory(Kilobytes(50));

    int MonitorRefreshHz = 60;
    HDC RefreshDC = GetDC(Window);
    int Win32RefreshRate = GetDeviceCaps(RefreshDC, VREFRESH);
    ReleaseDC(Window, RefreshDC);
    if (Win32RefreshRate > 1)
    {
        MonitorRefreshHz = Win32RefreshRate;
    }
    real32 GameUpdateHz = (real32)(MonitorRefreshHz);
    real32 TargetSecondsPerFrame = 1.0f / (real32)GameUpdateHz; //TODO(james): use this value for detecting missed frames
    
    win32_thread_startup WorkStartups[WORKER_THREAD_COUNT] = {};
    platform_work_queue WorkQueue = {};
    
    HDC OpenGLDC = GetDC(Window);
    HGLRC OpenGLRC = 0;
    gl_state GLState = {};
    OpenGLRC = Win32InitOpenGL(OpenGLDC, /*out*/ &GLState, &GLCode, &WorkQueue, WorkStartups);
    GlobalGLState = &GLState;
    
    Win32MakeQueue(&WorkQueue, ArrayCount(WorkStartups), WorkStartups, OpenGLDC);
    GameMemory.WorkQueue = &WorkQueue;
    
    uint32 GameDefaultLayerBufferSize = Megabytes(8);
    void* GameDefaultLayerBuffer = Win32AllocateMemory(GameDefaultLayerBufferSize);

    game_input NewInput = {};
    game_input OldInput = {};

    ShowWindow(Window, SW_MAXIMIZE);
    UpdateWindow(Window);
    

    LARGE_INTEGER LastFrameTime;
    LARGE_INTEGER CurrentFrameTime;

    QueryPerformanceCounter(&LastFrameTime);
    QueryPerformanceCounter(&CurrentFrameTime);

#if YAMGINE_DEBUG
    for (int ReplayIndex = 1;
         ReplayIndex < ArrayCount(Win32State.ReplayBuffers);
         ++ReplayIndex)
    {
        win32_replay_buffer* ReplayBuffer = &Win32State.ReplayBuffers[ReplayIndex];

        // TODO(casey): Recording system still seems to take too long
        // on record start - find out what Windows is doing and if
        // we can speed up / defer some of that processing.

        Win32GetInputFileLocation(false, ReplayIndex, &ReplayBuffer->FileName);

        ReplayBuffer->FileHandle = CreateFileA(ReplayBuffer->FileName.Text,
                                               GENERIC_WRITE | GENERIC_READ, 0, 0, CREATE_ALWAYS, 0, 0);

        LARGE_INTEGER MaxSize;
        MaxSize.QuadPart = Win32State.TotalSize;
        ReplayBuffer->MemoryMap = CreateFileMapping(ReplayBuffer->FileHandle, 0, PAGE_READWRITE,
                                                    MaxSize.HighPart, MaxSize.LowPart, 0);

        ReplayBuffer->MemoryBlock = MapViewOfFile(ReplayBuffer->MemoryMap, FILE_MAP_ALL_ACCESS,
                                                  0, 0, Win32State.TotalSize);
        if (ReplayBuffer->MemoryBlock)
        {
        }
        else
        {
            // TODO(casey): Diagnostic
        }
    }
#endif

    v2 ScreenDim = V2(GetSystemMetrics(SM_CXVIRTUALSCREEN),
                      GetSystemMetrics(SM_CYVIRTUALSCREEN));
    
//
// Sockets Startup
//
#if 0
    WSADATA StartupData;
    int StartupResult = WSAStartup(MAKEWORD(2, 2), &StartupData);
    Assert(StartupResult == 0);
#endif

    render_list GameList = {};

    Win32State.GlobalRunning = true;

    YamProfEnd();

    //
    // GAME LOOP
    //
    while (Win32State.GlobalRunning)
    {
        YamProfBegin(Win32Tick);
        
        YamProfBegin(Win32FrameTime);
        YamProfiling->FrameTime = YamProfiling->CurrentScope;
        
        if (Win32State.ShowCursor)
        {
            Assert(ClipCursor(NULL));
        }
        else
        {
            if (GetFocus() == Window)
            {
                v2 Dim = Win32GetWindowDimension(Window);
                v2 Pos = Win32GetWindowPosition().Min;

                v2 FlippedYHideCursorPos = Win32State.HideCursorPos;
                FlippedYHideCursorPos.y = Dim.y - FlippedYHideCursorPos.y;
                v2 DesiredMousePos = Pos + FlippedYHideCursorPos;

                // NOTE(james): this confines the mouse to a small rectangle at the center of the app
                RECT Rect = {};
                Rect.left = DesiredMousePos.x - 1;
                Rect.right = DesiredMousePos.x + 1;
                Rect.bottom = DesiredMousePos.y + 1;
                Rect.top = DesiredMousePos.y - 1;
                Assert(ClipCursor(&Rect));

                // NOTE(james): if the mouse is showing, it got set to visible when app focus was lost,
                // so set it to invisible again when we regain focus
                CURSORINFO pci = {};
                pci.cbSize = sizeof(CURSORINFO);
                GetCursorInfo(&pci);
                if (pci.flags == 1)
                {
                    ShowCursor(false);
                }
            }
            else
            {
                // NOTE(james): our window is no longer focused, so stop clipping and make it so the cursor
                // still appears when hovering over the window
                Assert(ClipCursor(NULL)); //stop clipping

                CURSORINFO pci = {};
                pci.cbSize = sizeof(CURSORINFO);
                GetCursorInfo(&pci);
                if (pci.flags == 0) //check if hidden
                {
                    ShowCursor(true);
                }
            }
        }

        //
        // Input
        //
        LastFrameTime = CurrentFrameTime;
        QueryPerformanceCounter(&CurrentFrameTime);
        real64 FrameDuration = ((real64)CurrentFrameTime.QuadPart - (real64)LastFrameTime.QuadPart) / (real64)Frequency.QuadPart;

        SetWindowText(Window, "Yam 3D");

        NewInput = {};
        NewInput.ScreenDim = ScreenDim;

        if (GetFocus() == Window)
        {
            for (int ControllerIndex = 0;
                 ControllerIndex < ArrayCount(NewInput.Controllers);
                 ControllerIndex++)
            {
                for (int ButtonStateIndex = 0;
                     ButtonStateIndex < ArrayCount(NewInput.Controllers[ControllerIndex].Buttons);
                     ++ButtonStateIndex)
                {
                    NewInput.Controllers[ControllerIndex].Buttons[ButtonStateIndex].IsDown = OldInput.Controllers[ControllerIndex].Buttons[ButtonStateIndex].IsDown;
                }
            }
        }

        //NOTE(james): if we hit the debugger or something, the FrameDuration becomes huge since the last frame flip was just before the debugger hit. Passing a massive DeltaTime can cause problems down the pipe so lets just cap it at something kind of reasonable
        if (FrameDuration > 1)
        {
            NewInput.DeltaTime = 1;
        }
        else
        {
            NewInput.DeltaTime = FrameDuration;
        }

        if (GetFocus() == Window)
        {
            NewInput.ApplicationDim = Win32GetWindowDimension(Window);
        }
        else
        {
            NewInput.ApplicationDim = OldInput.ApplicationDim;
        }
        
        
NewInput.IsRecording = OldInput.IsRecording;
        if (NewInput.IsPlayingBack)NewInput.IsRecording = false;
        
        // Reload game DLL if its been recompiled
        GameMemory.ExecutableReloaded = false;
        FILETIME LatestWriteTime = Win32GetLastWriteTime(DLL_FILE_NAME);
        if (CompareFileTime(&LatestWriteTime, &GameCode.LastWriteTime) != 0)
        {
            FreeGameCode(&GameCode);
            bool LoadedSuccessfully = false;
            while (!LoadedSuccessfully)
            {
                LoadedSuccessfully = LoadGameCode(&GameCode);
                GameMemory.ExecutableReloaded = true;
            }
        }

        FILETIME LatestGLWriteTime = Win32GetLastWriteTime(GL_DLL_FILE_NAME);
        if (CompareFileTime(&LatestGLWriteTime, &GLCode.LastWriteTime) != 0)
        {
            FreeOpenGLCode(&GLCode);
            bool LoadedSuccessfully = false;
            while (!LoadedSuccessfully)
            {
                LoadedSuccessfully = LoadOpenGLCode(&GLCode);
            }
        }

        Win32ProcessPendingMessages(Window, &Win32State, &NewInput.Controllers[0]);

        XINPUT_STATE XboxState = {};
        DWORD result = XInputGetState(0, &XboxState);
        if (result == ERROR_SUCCESS)
        {
            if (XboxState.Gamepad.wButtons & XINPUT_GAMEPAD_A)
            {
                DebugConsole("Controller A");
            }
        }

        GameList.PushBufferBase = (uint8*)GameDefaultLayerBuffer;
        GameList.PushBufferUsed = 0;
        GameList.MaxPushBufferSize = GameDefaultLayerBufferSize;
        GameList.PushBufferElementCount = 0;

/*
        if not recording, record
        if recording, stop and playback
        if playing, stop playing
        */
#if YAMGINE_DEBUG
        if (NewInput.Controllers[0].Action4.DownThisFrame)
        {
            if (Win32State.InputPlayingIndex == 0)
            {
                if (Win32State.InputRecordingIndex == 0)
                {
                    Win32BeginRecordingInput(&Win32State, 1);
                    NewInput.IsRecording = true;
                }
                else
                {
                    Win32EndRecordingInput(&Win32State);
                    NewInput.IsRecording = false;
                    Win32BeginInputPlayBack(&Win32State, 1);
                    NewInput.IsPlayingBack = true;
                }
            }
            else
            {
                Win32EndInputPlayBack(&Win32State);
                NewInput.IsPlayingBack = false;

                for (int ControllerIndex = 0;
                     ControllerIndex < ArrayCount(NewInput.Controllers);
                     ControllerIndex++)
                {
                    for (int ButtonStateIndex = 0;
                         ButtonStateIndex < ArrayCount(NewInput.Controllers[ControllerIndex].Buttons);
                         ++ButtonStateIndex)
                    {
                        NewInput.Controllers[ControllerIndex].Buttons[ButtonStateIndex].IsDown = false;
                    }
                }
            }
        }

        if (Win32State.InputRecordingIndex)
        {
            Win32RecordInput(&Win32State, &NewInput);
        }

        if (Win32State.InputPlayingIndex)
        {
            game_input Temp = NewInput;
            Win32PlayBackInput(&Win32State, &NewInput);
            /*
            for(uint32 MouseButtonIndex = 0;
                MouseButtonIndex < PlatformMouseButton_Count;
                ++MouseButtonIndex)
            {
                NewInput->MouseButtons[MouseButtonIndex] = Temp.MouseButtons[MouseButtonIndex];
            }
            NewInput->MouseX = Temp.MouseX;
            NewInput->MouseY = Temp.MouseY;
            NewInput->MouseZ = Temp.MouseZ;
            */
            NewInput.IsPlayingBack = true;
        }
        if (NewInput.IsPlayingBack)NewInput.IsRecording = false;
#endif
        GameMemory.YamProfiling = YamProfiling;
        GameCode.UpdateAndRender(&GameMemory, &GameList, &NewInput);

#if WWISE_SUPPORT
        AK::SoundEngine::RenderAudio();
#endif

        if (NewInput.ApplicationDim.x > 0 && NewInput.ApplicationDim.y > 0)
        {
            YamProf(Win32Rendering);
            GLCode.OpenGLRenderCommands(&GameList, &GLState, 
                                        GameMemory.TransientStorage, GameMemory.TransientStorageSize,
                                        YamProfiling, &Platform);
        }

        YamProfEnd(); //frame time
        {
            YamProf(SwapBuffers);
            HDC DeviceContext = GetDC(Window);
            bool32 Success = SwapBuffers(DeviceContext); //TODO(james):this should probably be in the  GL layer or in an #ifdef
            ReleaseDC(Window, DeviceContext);
        }

        if (NewInput.QuitRequested)
        {
            Win32State.GlobalRunning = false;
        }

        OldInput = NewInput;
        YamProfEnd(); //win32 tick
        YamProfEnd(); //__global

        ZeroSize(YamProfilingReportArena.Size, YamProfilingReportArena.Base);
        YamProfilingReportArena.Used = 0;

        GameMemory.YamReport = CreateReport(&YamProfilingReportArena);
        if (!GameMemory.YamReportStart)
        {
            GameMemory.YamReportStart = CreateReport(&YamProfilingReportStartArena);
        }

        UpdateProfiling();
    } //end game loop

    GLCode.OpenGLExit(&GLState);
    WSACleanup();

#if FMOD_SUPPORT
    CoUninitialize();
#endif

#if WWISE_SUPPORT
    WwiseTerminate();
#endif

    return 0;
}

LRESULT CALLBACK WindowProc(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam)
{
    LRESULT Result = 0;

    switch (Message)
    {
        case WM_CREATE:
        {

            //NOTE(james): only place i could actually find a list for usUsagePage and usUsage
            //http://www.codeproject.com/Articles/297312/Minimal-Key-Logger-using-RAWINPUT

            // register interest in raw data here
            RAWINPUTDEVICE rid[2];

            //keyboard
            //NOTE(james): when registed with NOLEGACY, media keys stopped working
            rid[0].dwFlags = NULL;
            rid[0].usUsagePage = 0x01;
            rid[0].usUsage = 0x06;
            rid[0].hwndTarget = Window;

            //mouse
            //NOTE(james):if you set RIDEV_NOLEGACY here,
            //you won't be able drag the window or close it normally
            rid[1].dwFlags = 0;
            rid[1].usUsagePage = 0x01;
            rid[1].usUsage = 0x02;
            rid[1].hwndTarget = Window;


            if (!RegisterRawInputDevices(rid, 2, sizeof(RAWINPUTDEVICE)))
            {
                Assert(0);
            }

        }break;

        case WM_CLOSE:
        {
            PostQuitMessage(0);
            return 0;
        }break;

        case WM_DESTROY:
        {
            PostQuitMessage(0);
            return 0;
        }break;

        case WM_SIZE:
        {
            int x = 0;
        }break;

        case WM_PAINT:
        {
            PAINTSTRUCT Paint;
            HDC DeviceContext = BeginPaint(Window, &Paint);
            EndPaint(Window, &Paint);
        }break;
        
        case WM_GESTURENOTIFY:
        {
            int x = 0;
        }break;
        
        case WM_GESTURE:
        {
            int x = 0;
        }break;
        
        case WM_TOUCH:
        {
            int x = 0;
        }break;
        
        case WM_MOUSEWHEEL:
        {
            int x = 0;// TOOD(james): getting trackpad two finger scrolls here
        }break;

        default:
        {
            Result = DefWindowProcA(Window, Message, WParam, LParam);
        }
    }
    return Result;
}
