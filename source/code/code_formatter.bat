@echo off
find -name '*yamgine_*' -o -name 'renderer_*' -o -name 'platform_*' -o -name simple_preprocessor.cpp | xargs clang-format -i
del *.TMP