@echo off

REM use --xml-version=2 to find error id's if you need to add a new suppression

call "C:\Program Files\Cppcheck\cppcheck"  --quiet --force --inline-suppr --enable=warning  --inconclusive --std=posix --suppress=missingInclude --suppress=unusedFunction --suppress=unreadVariable --suppress=nullPointerRedundantCheck .