#include "yamgine_tokenizer.h"

#define MAX_SHADER_SIZE 6000

void GenerateShader(char* Filename, char* Options, char* Header, char* OutputName, temporary_memory* Temp, platform_api* PlatformAPI)
{
    char* FileContents = (char*)LoadTextFile(Filename, Temp, PlatformAPI);

    tokenizer Tokenizer = {};
    Tokenizer.At = FileContents;
    Tokenizer.EnableMinusToken = true;

    STRING(Vert, MAX_SHADER_SIZE);
    AppendStringEx(&Vert, "// VERT SHADER %s\n\n#version 430\n%s%s", Filename, Options, Header);

    STRING(Frag, MAX_SHADER_SIZE);
    AppendStringEx(&Frag, "// FRAG SHADER %s\n\n#version 430\n%s%s", Filename, Options, Header);

    /*
        char Uniforms[MAX_SHADER_SIZE] = {};
        uint32 UniformCount;
        char* UniformLocations[32];
*/
    bool WritingVert = true;
    bool BeganWritingVertMain = false;
    bool ParsedVertToFragStruct = false;

    string* CurrentShader = &Vert;

    bool Parsing = true;

    int BraceDepth = 0;
    token Token = GetToken(&Tokenizer);
    bool32 NextCharIsSpace = false;
    while (Parsing)
    {
        if (*Tokenizer.At == ' ')
        {
            NextCharIsSpace = true;
        }
        else
        {
            NextCharIsSpace = false;
        }
        token NextToken = GetToken(&Tokenizer);

        switch (Token.Type)
        {
            case Token_EndOfStream:
            {
                Parsing = false;
            }
            break;

            case Token_Unknown:
            {
            }
            break;

            case Token_OpenBracket:
            {
                BraceDepth++;
                C_AppendString(CurrentShader, "[");
            }
            break;
            
            case Token_CloseBracket:
            {
                BraceDepth--;
                C_AppendString(CurrentShader, "]");
            }
            break;
            
            case Token_OpenBrace:
            {
                BraceDepth++;
                C_AppendString(CurrentShader, "{\n");
                Indent(CurrentShader, BraceDepth);
            }
            break;

            case Token_CloseBrace:
            {
                BraceDepth--;
                Indent(CurrentShader, BraceDepth);
                C_AppendString(CurrentShader, "}\n");

                if (BraceDepth == 0 && BeganWritingVertMain && WritingVert)
                {
                    BeganWritingVertMain = false;
                    WritingVert = false;
                    CurrentShader = &Frag;
                }
            }
            break;

            case Token_Semicolon:
            {
                C_AppendString(CurrentShader, ";\n");
            }
            break;

            case Token_Equals:
            {
                C_AppendString(CurrentShader, "=");
                if (NextToken.Type == Token_Identifier)
                {
                    C_AppendString(CurrentShader, " ");
                }
            }
            break;

            case Token_LessThan:
            {
                C_AppendString(CurrentShader, "<");
                if (NextToken.Type == Token_Identifier)
                {
                    C_AppendString(CurrentShader, " ");
                }
            }
            break;

            case Token_GreaterThan:
            {
                C_AppendString(CurrentShader, ">");
                if (NextToken.Type == Token_Identifier)
                {
                    C_AppendString(CurrentShader, " ");
                }
            }
            break;

            case Token_Comma:
            {
                C_AppendString(CurrentShader, ",");
            }
            break;

            case Token_Period:
            {
                C_AppendString(CurrentShader, ".");
            }
            break;

            case Token_Plus:
            {
				if (NextToken.Type == Token_Equals || NextToken.Type == Token_Plus)
				{
                    C_AppendString(CurrentShader, "+");
				}
				else
				{
                    C_AppendString(CurrentShader, "+ ");
				}
            }
            break;

            case Token_Minus:
            {
                if (NextCharIsSpace)
				{
                    C_AppendString(CurrentShader, "- ");
				}
				else
				{
                    C_AppendString(CurrentShader, "-");
				}
            }
            break;

            case Token_Asterisk:
            {
                if (NextToken.Type == Token_Equals || NextToken.Type == Token_Asterisk)
                {
                    C_AppendString(CurrentShader, "*");
                }
                else
                {
                    C_AppendString(CurrentShader, "* ");
                }
            }
            break;

            case Token_Divide:
            {
                if (NextToken.Type == Token_Equals || NextToken.Type == Token_Divide)
                {
                    C_AppendString(CurrentShader, "/");
                }
                else
                {
                    C_AppendString(CurrentShader, "/ ");
                }
            }
            break;

            case Token_OpenParen:
            {
                C_AppendString(CurrentShader, "(");
            }
            break;

            case Token_CloseParen:
            {
                C_AppendString(CurrentShader, ")");
            }
            break;

            case Token_Number:
            {
                C_AppendSubstring(CurrentShader, Token.Text, (uint32)Token.TextLength);
            }
            break;

            case Token_Pound:
            {
                if (TokenEquals(NextToken, "ifdef"))
                {
                    C_AppendString(CurrentShader, "#ifdef ");

                    token PoundId = GetToken(&Tokenizer);
                    C_AppendSubstring(CurrentShader, PoundId.Text, (uint32)PoundId.TextLength);
                    C_AppendString(CurrentShader, "\n");
                }
                else if (TokenEquals(NextToken, "define"))
                {
                    C_AppendString(CurrentShader, "#define ");

                    token PoundId = GetToken(&Tokenizer);
                    C_AppendSubstring(CurrentShader, PoundId.Text, (uint32)PoundId.TextLength);
                    C_AppendString(CurrentShader, "\n");
                }
                else if (TokenEquals(NextToken, "endif"))
                {
                    C_AppendString(CurrentShader, "#endif\n");
                }
                else if (TokenEquals(NextToken, "else"))
                {
                    C_AppendString(CurrentShader, "#else\n");
                }
                else if (TokenEquals(NextToken, "elif"))
                {
                    C_AppendString(CurrentShader, "#elif ");

                    token PoundId = GetToken(&Tokenizer);
                    C_AppendSubstring(CurrentShader, PoundId.Text, (uint32)PoundId.TextLength);
                    C_AppendString(CurrentShader, "\n");
                }
                else if (TokenEquals(NextToken, "if"))
                {
                    C_AppendString(CurrentShader, "#if");

                    token RestOfLine = GetRestOfLine(&Tokenizer);
                    C_AppendSubstring(CurrentShader, RestOfLine.Text, (uint32)RestOfLine.TextLength);
                    C_AppendString(CurrentShader, "\n");
                }
                else
                {
                    InvalidCodePath;
                }
                NextToken = GetToken(&Tokenizer);
            }
            break;

            case Token_Identifier:
            {
                if (TokenEquals(Token, "VERT_TO_FRAG"))
                {
                    if (!ParsedVertToFragStruct) //convert VERT_TO_FRAG struct
                    {
                        Assert(NextToken.Type == Token_OpenBrace); //open brace
                        token Type = GetToken(&Tokenizer);
                        while (Type.Type != Token_CloseBrace)
                        {
                            if (Type.Type == Token_Identifier)
                            {
                                token Identifier = GetToken(&Tokenizer);
                                Assert(Identifier.Type == Token_Identifier);

                                C_AppendString(&Vert, "out ");
                                C_AppendSubstring(&Vert, Type.Text, (uint32)Type.TextLength);
                                C_AppendString(&Vert, " _");
                                C_AppendSubstring(&Vert, Identifier.Text, (uint32)Identifier.TextLength);
                                C_AppendString(&Vert, ";\n");

                                C_AppendString(&Frag, "in ");
                                C_AppendSubstring(&Frag, Type.Text, (uint32)Type.TextLength);
                                C_AppendString(&Frag, " _");
                                C_AppendSubstring(&Frag, Identifier.Text, (uint32)Identifier.TextLength);
                                C_AppendString(&Frag, ";\n");

                                AssertToken(&Tokenizer, Token_Semicolon);

                                Type = GetToken(&Tokenizer);
                            }
                            else if (Type.Type == Token_Pound)
                            {
                                token PoundType = GetToken(&Tokenizer);

                                if (TokenEquals(PoundType, "ifdef"))
                                {
                                    C_AppendString(&Vert, "#ifdef ");
                                    C_AppendString(&Frag, "#ifdef ");

                                    token PoundId = GetToken(&Tokenizer);
                                    C_AppendSubstring(&Vert, PoundId.Text, (uint32)PoundId.TextLength);
                                    C_AppendSubstring(&Frag, PoundId.Text, (uint32)PoundId.TextLength);

                                    C_AppendString(&Vert, "\n");
                                    Indent(&Vert, BraceDepth);
                                    C_AppendString(&Frag, "\n");
                                    Indent(&Frag, BraceDepth);
                                }
                                else if (TokenEquals(PoundType, "if"))
                                {
                                    C_AppendString(&Vert, "#if");
                                    C_AppendString(&Frag, "#if");

                                    token RestOfLine = GetRestOfLine(&Tokenizer);
                                    C_AppendSubstring(&Vert, RestOfLine.Text, (uint32)RestOfLine.TextLength);
                                    C_AppendSubstring(&Frag, RestOfLine.Text, (uint32)RestOfLine.TextLength);

                                    C_AppendString(&Vert, "\n");
                                    Indent(&Vert, BraceDepth);
                                    C_AppendString(&Frag, "\n");
                                    Indent(&Frag, BraceDepth);
                                }
                                else if (TokenEquals(PoundType, "endif"))
                                {
                                    C_AppendString(&Vert, "#endif\n");
                                    Indent(&Vert, BraceDepth);
                                    C_AppendString(&Frag, "#endif\n");
                                    Indent(&Frag, BraceDepth);
                                }
                                else
                                {
                                    InvalidCodePath;
                                }
                                Type = GetToken(&Tokenizer);
                            }
                        }
                        AssertToken(&Tokenizer, Token_Semicolon);
                        ParsedVertToFragStruct = true;
                        NextToken = GetToken(&Tokenizer);
                    }
                    else //convert VERT_TO_FRAG member
                    {
                        if (CurrentShader->Text[C_StringLength(CurrentShader->Text) - 1] == '\n')
                        {
                            Indent(CurrentShader, BraceDepth);
                        }

                        C_AppendString(CurrentShader, "_");
                        Assert(NextToken.Type == Token_Period);
                        NextToken = GetToken(&Tokenizer);
                    }
                }
                else if (TokenEquals(Token, "VERT"))
                {
                    C_AppendString(&Vert, "main");
                    BeganWritingVertMain = true;
                }
                else if (TokenEquals(Token, "FRAG"))
                {
                    C_AppendString(&Frag, "main");
                }
                else
                {
                    if (CurrentShader->Text[C_StringLength(CurrentShader->Text) - 1] == '\n')
                    {
                        Indent(CurrentShader, BraceDepth);
                    }

                    C_AppendSubstring(CurrentShader, Token.Text, (uint32)Token.TextLength);
                    if (NextToken.Type == Token_Identifier)
                        C_AppendString(CurrentShader, " ");
                }
            }
            break;
        }
        Token = NextToken;
    }
    
    //TODO(james): get rid of this assumed output path?
    STRING(VertPath, 1024);
    AppendStringEx(&VertPath, "%s.vert", OutputName);

    PlatformAPI->WriteBinaryFile(VertPath.Text,
                                 (uint8*)Vert.Text,
                                 C_StringLength(Vert.Text));

    STRING(FragPath, 1024);
    AppendStringEx(&FragPath, "%s.frag", OutputName);

    PlatformAPI->WriteBinaryFile(FragPath.Text,
                                 (uint8*)Frag.Text,
                                 C_StringLength(Frag.Text));
}
