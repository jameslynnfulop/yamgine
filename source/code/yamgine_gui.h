#if !defined(YAMGINE_GUI_H)

introspect(category
           : "player") struct player_settings
{
    serialize("param") real32 MouseSensitivity;
    serialize("param") bool32 Vsync;
    serialize("param") bool32 Fullscreen;
    
    serialize("param") v2 WindowPosition;
};

introspect(category
           : "gui") struct glyph_table_entry
{
    serialize("param") char Glyph;
    serialize("param") rectangle2 BoundingBox; //use in font rendering
    serialize("param") real32 Advance; //horizontal space, including "whitespace" around the glyph

    serialize("param") rectangle2 AtlasBox; //location on the atlas texture
};

struct font
{
    texture* TextureAtlas;
    glyph_table_entry* Glyphs;
    uint32 GlyphCount;
};

#define YAMGINE_GUI_H
#endif
