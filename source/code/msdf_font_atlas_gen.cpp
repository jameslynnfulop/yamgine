//#include "msdfgen.h"

#define _USE_MATH_DEFINES
#include <cmath>

#include "msdfgen-ext.h"

#include "core/Vector2.cpp"
#include "core/Bitmap.cpp"
#include "core/Shape.cpp"
#include "core/SignedDistance.cpp"
#include "core/shape-description.cpp"
#include "core/EdgeHolder.cpp"
#include "core/edge-coloring.cpp"
#include "core/edge-segments.cpp"
#include "core/equation-solver.cpp"
#include "core/Contour.cpp"

#include "ext/import-font.cpp"

#include "core/save-bmp.cpp"
#include "ext/save-png.cpp"

#include "core/msdfgen.cpp"

#include "lib/lodepng.cpp"

using namespace msdfgen;

#include "yamgine_shared.h"
#include "yamgine_gui.h"

#include "yamgine_entity.h"
#include "yamgine_meta.h"
#include "yamgine_serialization_shared.cpp"

int main() 
{
    FreetypeHandle *ft = initializeFreetype();
    if (ft) 
    {
        FontHandle *font = loadFont(ft, "Z:\\projects\\going_ham\\build\\javatext.ttf");
        
        if (font) 
        {
            //
            // load everying to atlas
            //
            
            // we want chars [33, 126], so a total of 93 characters
            Bitmap<FloatRGB> atlas(512, 512);
            glyph_table_entry Entries[95];
            uint32 EntryCount = 0;
            
            Shape shape = {};
            double SpaceAdvance = 0;
            loadGlyph(shape, font, ' ', &SpaceAdvance);
            Entries[EntryCount].Glyph = '0';
            Entries[EntryCount].Advance = SpaceAdvance;
            EntryCount++;
            
            for (char glyphCount = 1; 
                 glyphCount < 95; 
                 ++glyphCount)
            {
                int xIndexBottomLeftCorner = glyphCount % 15;
                int yIndexBottomLeftCorner = floor(((float)glyphCount) / ((float)15));
                
                xIndexBottomLeftCorner *= 32;
                yIndexBottomLeftCorner *= 32;
                
                xIndexBottomLeftCorner += (glyphCount % 15);//one pixel border 
                yIndexBottomLeftCorner += floor(((float)glyphCount) / ((float)15));//one pixel border 
                
                Shape shape = {};
                //chars 0 to 32 in ASCII are not visible characters, things like line breaks
                char glyph = (glyphCount - 1)  + 33;
                
                double glyphAdvance = 0;
                if (loadGlyph(shape, font,glyph, &glyphAdvance)) 
                {
                    if (!shape.validate())
                    {
                        InvalidCodePath;
                    }
                    shape.normalize();
                    double left = 100000;
                    double bottom = 100000;
                    double right = -100000;
                    double top = -100000;
                    shape.bounds(left, bottom, right, top);
                    
                    rectangle2 BoundingBox = RectMinMax(V2(left, bottom), V2(right, top));
                    
                    if (left < 0)
                    {
                        left = -left;
                    }
                    else
                    {
                        left = 0;
                    }
                    if (bottom < 0)
                    {
                        bottom = -bottom;
                    }
                    else
                    {
                        bottom = 0;
                    }
                    
                    rectangle2 AtlasBox = RectMinDim(V2((real32)(xIndexBottomLeftCorner)/512.0f, 
                                                        ((real32)yIndexBottomLeftCorner)/512.0f), 
                                                     V2((right+left)/512.0f, 
                                                                                                            (top+bottom)/512.0f));
                    
                    edgeColoringSimple(shape, 3.0);
                    //           image width, height
                    Bitmap<FloatRGB> msdf(32, 32);
                    //                     range, scale, translation
                    generateMSDF(msdf, shape, 4.0, 1.0, Vector2(left, bottom));
                    //saveBmp(msdf, "output.bmp");
                    
                    // Copy To Atlas
                    for (int yIndex = 0; 
                         yIndex < 32; 
                         ++yIndex)
                    {
                        for (int xIndex = 0; 
                             xIndex < 32; 
                             ++xIndex)
                        {
                              FloatRGB pixel = msdf(xIndex, yIndex);
                            int xAtlas = xIndex + xIndexBottomLeftCorner;
                            int yAtlas = yIndex + yIndexBottomLeftCorner;
                            
                            int oneDindex = yAtlas * 512 + xAtlas;
                            if (oneDindex >= 512*512)__debugbreak();
                            atlas(xAtlas, yAtlas) = pixel;
                        }
                    }
                    
                    // create entry
                    glyph_table_entry* Entry = &Entries[EntryCount++];
                    Entry->Glyph = glyph;
                    Entry->BoundingBox = BoundingBox;
                    Entry->Advance = glyphAdvance;
                    Entry->AtlasBox = AtlasBox;
                }
                else
                {
                    InvalidCodePath;
                }
            }
            
            destroyFont(font);
            
            // serialize atlas
            saveBmp(atlas, "glyphs.bmp");
            
            STRING(File, 100000);
            for (int i = 0; i < ArrayCount(Entries); i++)
            {
                glyph_table_entry* Entry = &Entries[i];
                char title[2];
                    title[0] = 'g';
                title[1] = '\0';
                SerializeStruct(ArrayCount(MembersOf_glyph_table_entry), MembersOf_glyph_table_entry, (void*)Entry, 0, "glyph_table_entry", &title[0], &File);
            }
            
            HANDLE FileHandle = CreateFileA("glyphs.table", GENERIC_WRITE, 0, 0, CREATE_ALWAYS, 0, 0);
            
            DWORD WrittenBytes; //not used
            bool32 Result = (bool32)WriteFile(FileHandle, File.Text, StringLength(File), &WrittenBytes, NULL);
            Assert(Result);
            
            CloseHandle(FileHandle);
            
        }
        deinitializeFreetype(ft);
    }
    return 0;
}