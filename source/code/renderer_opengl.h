
#if !defined(RENDERER_OPENGL_H)

#define WIN32_LEAN_AND_MEAN
#include <windows.h> // gl.h depends on this? WTF?
#include "gl/gl.h"

#include "yamgine_shared.h"

/* NOTE(james): 
This file contains commands that are OpenGL only. Platform specific context
creation should occur in the platform layer. Should just need to pass an Init function
to this file
*/

struct opengl_info
{
    bool32 HasModernContext;

    char* Vendor;
    char* Renderer;
    char* Version;
    char* ShadingLanguageVersion;
    char* Extensions;

    bool32 GL_EXT_texture_sRGB;
    bool32 GL_EXT_framebuffer_sRGB;
    bool32 GL_ARB_multisample;

    bool32 GL_ARB_texture_cube_map;
    bool32 GL_ARB_texture_compression;
    bool32 GL_EXT_texture_compression_s3tc;
};

struct gl_imgui_context
{
    GLuint FontTexture;

    GLuint Shader;

    GLuint VaoHandle;
    GLuint VboHandle;
    GLuint ElementsHandle;

    GLint TextureID;
    GLint ProjMtxID;

    GLint LastTexture;
    GLint LastArrayBuffer;

    GLint LastVertexArray;
};

struct mesh_shader
{
    GLint Texture_RecieveShadows_Shader;
    GLint Texture_Shader;

    GLint RecieveShadows_Shader;
    GLint VanillaShader;
};

//global_variable gl_render_contexts GlobalRenderContexts;

typedef char GLchar;
typedef ptrdiff_t GLsizeiptr;
typedef ptrdiff_t GLintptr;

global_variable GLuint OpenGLDefaultInternalTextureFormat;

#define GL_LINES_ADJACENCY 0x000A
#define GL_LINE_STRIP_ADJACENCY 0x000B
#define GL_TRIANGLES_ADJACENCY 0x000C
#define GL_TRIANGLE_STRIP_ADJACENCY 0x000D

#define GL_FRAMEBUFFER_SRGB 0x8DB9
#define GL_SRGB8_ALPHA8 0x8C43

#define GL_BGRA                           0x80E1

#define GL_SHADING_LANGUAGE_VERSION 0x8B8C

//Shaders
#define GL_COMPILE_STATUS 0x8B81
#define GL_LINK_STATUS 0x8B82
#define GL_FRAGMENT_SHADER 0x8B30
#define GL_VERTEX_SHADER 0x8B31

#define GL_NUM_EXTENSIONS                 0x821D
typedef GLubyte *APIENTRY _glGetStringi (GLenum name, GLuint index);

typedef GLuint APIENTRY _glCreateShader(GLenum type);
typedef void APIENTRY _glShaderSource(GLuint shader, GLsizei count, const GLchar* const* string, const GLint* length);
typedef void APIENTRY _glCompileShader(GLuint shader);
typedef void APIENTRY _glGetShaderiv(GLuint shader, GLenum pname, GLint* params);
typedef GLuint APIENTRY _glCreateProgram(void);
typedef void APIENTRY _glAttachShader(GLuint program, GLuint shader);
typedef void APIENTRY _glLinkProgram(GLuint program);
typedef void APIENTRY _glDeleteShader(GLuint shader);
typedef void APIENTRY _glGetShaderInfoLog(GLuint shader, GLsizei maxLength, GLsizei* length, GLchar* infoLog);
typedef void APIENTRY _glCompressedTexImage2D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, const void* data);
typedef void APIENTRY _glGetProgramiv(GLuint program, GLenum pname, GLint* params);
typedef void APIENTRY _glGetProgramInfoLog(GLuint program, GLsizei maxLength, GLsizei* length, GLchar* infoLog);

#define GL_UNIFORM                        0x92E1
#define GL_ACTIVE_RESOURCES               0x92F5
#define GL_BLOCK_INDEX                    0x92FD
#define GL_TYPE                           0x92FA
#define GL_NAME_LENGTH                    0x92F9
#define GL_LOCATION                       0x930E
typedef void APIENTRY _glGetProgramInterfaceiv (GLuint program, GLenum programInterface, GLenum pname, GLint *params);
typedef void APIENTRY _glGetProgramResourceiv (GLuint program, GLenum programInterface, GLuint index, GLsizei propCount, const GLenum *props, GLsizei bufSize, GLsizei *length, GLint *params);
typedef void APIENTRY _glGetProgramResourceName (GLuint program, GLenum programInterface, GLuint index, GLsizei bufSize, GLsizei *length, GLchar *name);

//GL loading/binding/deleting data
#define GL_ARRAY_BUFFER 0x8892
#define GL_STATIC_DRAW 0x88E4
#define GL_STREAM_DRAW 0x88E0
#define GL_DYNAMIC_DRAW 0x88E8
#define GL_ELEMENT_ARRAY_BUFFER 0x8893
#define GL_UNIFORM_BUFFER 0x8A11
typedef void APIENTRY _glGenVertexArrays(GLsizei n, GLuint* arrays);
typedef void APIENTRY _glBindVertexArray(GLuint array);
typedef void APIENTRY _glGenBuffers(GLsizei n, GLuint* buffers);
typedef void APIENTRY _glBindBuffer(GLenum target, GLuint buffer);
typedef void APIENTRY _glBufferData(GLenum target, GLsizeiptr size, const void* data, GLenum usage);
typedef GLint APIENTRY _glGetUniformLocation(GLuint program, const GLchar* name);
typedef GLint APIENTRY _glGetAttribLocation(GLuint program, const GLchar* name);
typedef void APIENTRY _glVertexAttribPointer(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void* pointer);
typedef void APIENTRY _glEnableVertexAttribArray(GLuint index);
typedef void APIENTRY _glDisableVertexAttribArray(GLuint index);
typedef void APIENTRY _glDeleteVertexArrays(GLsizei n, const GLuint* arrays);
typedef void APIENTRY _glDeleteBuffers(GLsizei n, const GLuint* buffers);
typedef GLuint APIENTRY _glGetUniformBlockIndex(GLuint program, const GLchar* uniformBlockName);
typedef void APIENTRY _glUniformBlockBinding(GLuint program, GLuint uniformBlockIndex, GLuint uniformBlockBinding);
typedef void APIENTRY _glBindBufferRange(GLenum target, GLuint index, GLuint buffer, GLintptr offset, GLsizeiptr size);
typedef void APIENTRY _glBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, const void* data);

#define GL_WRITE_ONLY                     0x88B9
typedef void* APIENTRY _glMapBuffer(GLenum target, GLenum access);
typedef GLboolean APIENTRY _glUnmapBuffer(GLenum target);

#define GL_MAP_WRITE_BIT                  0x0002
#define GL_MAP_INVALIDATE_BUFFER_BIT      0x0008
typedef void* APIENTRY _glMapBufferRange(GLenum target, GLintptr offset, GLsizeiptr length, GLbitfield access);
typedef void APIENTRY _glBindVertexBuffer(GLuint bindingindex, GLuint buffer, GLintptr offset, GLintptr stride);
typedef void APIENTRY _glVertexAttribBinding(GLuint attribindex, GLuint bindingindex);
typedef void APIENTRY _glVertexAttribFormat(GLuint attribindex,GLint size,GLenum type,GLboolean normalized,GLuint relativeoffset);
typedef void APIENTRY _glVertexAttribDivisor(GLuint index, GLuint divisor);
typedef void APIENTRY _glVertexBindingDivisor (GLuint bindingindex, GLuint divisor);

// samplers
typedef void APIENTRY _glGenSamplers(GLsizei n, GLuint *samplers);
typedef void APIENTRY _glBindSampler(GLuint unit, GLuint sampler);
typedef void APIENTRY _glSamplerParameteri(GLuint sampler, GLenum pname, GLint param);

//gl using/drawing data

typedef void APIENTRY _glUseProgram(GLuint program);
typedef void APIENTRY _glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat* value);
typedef void APIENTRY _glUniform2f(GLint location, GLfloat in0, GLfloat in1);
typedef void APIENTRY _glUniform3f(GLint location, GLfloat in0, GLfloat in1, GLfloat in2);
typedef void APIENTRY _glUniform4f(GLint location, GLfloat in0, GLfloat in1, GLfloat in2, GLfloat in3);

typedef void APIENTRY _glDrawArraysInstanced(GLenum mode, GLint first, GLsizei count, GLsizei instancecount);

//used by imgui
#define GL_ARRAY_BUFFER_BINDING 0x8894
#define GL_VERTEX_ARRAY_BINDING 0x85B5
#define GL_CURRENT_PROGRAM 0x8B8D
#define GL_ELEMENT_ARRAY_BUFFER_BINDING 0x8895
#define GL_BLEND_EQUATION_RGB 0x8009
#define GL_BLEND_EQUATION_ALPHA 0x883D
#define GL_FUNC_ADD 0x8006
#define GL_TEXTURE0 0x84C0
#define GL_TEXTURE1 0x84C1

typedef void APIENTRY _glBlendEquation(GLenum mode);
typedef void APIENTRY _glActiveTexture(GLenum texture);
typedef void APIENTRY _glBlendEquationSeparate(GLenum modeRGB, GLenum modeAlpha);
typedef void APIENTRY _glUniform1i(GLint location, GLint v0);

//adjacency
#define GL_PRIMITIVE_RESTART 0x8F9D
typedef void APIENTRY _glPrimitiveRestartIndex(GLuint index);

//textures
#define GL_CLAMP_TO_EDGE 0x812F
#define GL_BGR 0x80E0
#define GL_CLAMP_TO_BORDER 0x812D
#define GL_TEXTURE_COMPARE_MODE 0x884C
#define GL_TEXTURE_COMPARE_FUNC           0x884D
#define GL_COMPARE_REF_TO_TEXTURE 0x884E

//texture compression
#define COMPRESSED_RGB_S3TC_DXT1_EXT 0x83F0
#define COMPRESSED_RGBA_S3TC_DXT1_EXT 0x83F1
#define COMPRESSED_RGBA_S3TC_DXT3_EXT 0x83F2
#define COMPRESSED_RGBA_S3TC_DXT5_EXT 0x83F3

//fbos
#define GL_FRAMEBUFFER 0x8D40
#define GL_DEPTH_ATTACHMENT 0x8D00
#define GL_COLOR_ATTACHMENT0 0x8CE0
#define GL_DRAW_FRAMEBUFFER 0x8CA9
typedef void APIENTRY _glGenFramebuffers(GLsizei n, GLuint* framebuffers);
typedef void APIENTRY _glBindFramebuffer(GLenum target, GLuint framebuffer);
typedef void APIENTRY _glFramebufferTexture2D(GLenum target, GLenum attachment, GLenum textarget, GLuint texture, GLint level);
#define GL_FRAMEBUFFER_COMPLETE 0x8CD5
#define GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT 0x8CD6
#define GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT 0x8CD7
#define GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER 0x8CDB
#define GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER 0x8CDC
#define GL_FRAMEBUFFER_UNSUPPORTED 0x8CDD
#define GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE 0x8D56
typedef GLenum APIENTRY _glCheckFramebufferStatus(GLenum target);
#define GL_TEXTURE_2D_MULTISAMPLE 0x9100
typedef void APIENTRY _glTexImage2DMultisample(GLenum target, GLsizei samples, GLenum internalformat, GLsizei width, GLsizei height, GLboolean fixedsamplelocations);
#define GL_DEPTH_COMPONENT16 0x81A5
#define GL_DEPTH_COMPONENT24 0x81A6
#define GL_DEPTH_COMPONENT32 0x81A7

// error messages
#define GL_NO_ERROR 0
#define GL_INVALID_ENUM 0x0500
#define GL_INVALID_VALUE 0x0501
#define GL_INVALID_OPERATION 0x0502
#define GL_STACK_OVERFLOW 0x0503
#define GL_STACK_UNDERFLOW 0x0504
#define GL_OUT_OF_MEMORY 0x0505
#define GL_INVALID_FRAMEBUFFER_OPERATION 0x0506


#define GL_DEBUG_SEVERITY_HIGH            0x9146
#define GL_DEBUG_SEVERITY_MEDIUM          0x9147
#define GL_DEBUG_SEVERITY_LOW             0x9148
#define GL_DEBUG_TYPE_MARKER              0x8268
#define GL_DEBUG_TYPE_PUSH_GROUP          0x8269
#define GL_DEBUG_TYPE_POP_GROUP           0x826A
#define GL_DEBUG_SEVERITY_NOTIFICATION    0x826B

#define DEBUG_SOURCE_API_ARB                              0x8246
#define DEBUG_SOURCE_WINDOW_SYSTEM_ARB                    0x8247
#define DEBUG_SOURCE_SHADER_COMPILER_ARB                  0x8248
#define DEBUG_SOURCE_THIRD_PARTY_ARB                      0x8249
#define DEBUG_SOURCE_APPLICATION_ARB                      0x824A
#define DEBUG_SOURCE_OTHER_ARB                            0x824B

typedef void _glObjectLabel(GLenum identifier, GLuint name, GLsizei length, const GLchar *label);


#define GL_DEBUG_OUTPUT_SYNCHRONOUS       0x8242
#define GL_DEBUG_CALLBACK(Name) void WINAPI Name(GLenum source,GLenum type,GLuint id,GLenum severity,GLsizei length,const GLchar *message,const void *userParam)
typedef GL_DEBUG_CALLBACK(GLDEBUGPROC);

typedef void APIENTRY _glDebugMessageCallbackARB(GLDEBUGPROC *callback, const void *userParam);
#define GL_DEBUG_SOURCE_APPLICATION       0x824A
typedef void APIENTRY _glPushDebugGroup (GLenum source, GLuint id, GLsizei length, const GLchar *message);
typedef void APIENTRY _glPopDebugGroup (void);

struct gl_wrapped
{
    _glCreateShader* glCreateShader;
    _glShaderSource* glShaderSource;
    _glCompileShader* glCompileShader;
    _glGetShaderiv* glGetShaderiv;
    _glCreateProgram* glCreateProgram;
    _glAttachShader* glAttachShader;
    _glLinkProgram* glLinkProgram;
    _glDeleteShader* glDeleteShader;
    _glGetShaderInfoLog* glGetShaderInfoLog;
    _glCompressedTexImage2D* glCompressedTexImage2D;
    _glGenVertexArrays* glGenVertexArrays;
    _glBindVertexArray* glBindVertexArray;
    _glGenBuffers* glGenBuffers;
    _glBindBuffer* glBindBuffer;
    _glBufferData* glBufferData;
    _glGetUniformLocation* glGetUniformLocationUnsafe;
    _glGetAttribLocation* glGetAttribLocationUnsafe;
    _glVertexAttribPointer* glVertexAttribPointer;
    _glEnableVertexAttribArray* glEnableVertexAttribArray;
    _glDisableVertexAttribArray* glDisableVertexAttribArray;
    _glDeleteVertexArrays* glDeleteVertexArrays;
    _glDeleteBuffers* glDeleteBuffers;
    _glUseProgram* glUseProgram;
    _glUniformMatrix4fv* glUniformMatrix4fv;
    _glUniform2f* glUniform2f;
    _glUniform3f* glUniform3f;
    _glUniform4f* glUniform4f;
    _glBlendEquation* glBlendEquation;
    _glActiveTexture* glActiveTexture;
    _glBlendEquationSeparate* glBlendEquationSeparate;
    _glUniform1i* glUniform1i;
    _glPrimitiveRestartIndex* glPrimitiveRestartIndex;
    _glGenFramebuffers* glGenFramebuffers;
    _glBindFramebuffer* glBindFramebuffer;
    _glFramebufferTexture2D* glFramebufferTexture2D;
    _glGetProgramiv* glGetProgramiv;
    _glGetProgramInfoLog* glGetProgramInfoLog;
    _glUniformBlockBinding* glUniformBlockBinding;
    _glGetUniformBlockIndex* glGetUniformBlockIndex;
    _glBindBufferRange* glBindBufferRange;
    _glBufferSubData* glBufferSubData;
    _glCheckFramebufferStatus* glCheckFramebufferStatus;
    _glTexImage2DMultisample* glTexImage2DMultisample;
    _glGetProgramInterfaceiv* glGetProgramInterfaceiv;
    _glGetProgramResourceiv* glGetProgramResourceiv;
    _glGetProgramResourceName* glGetProgramResourceName;
    _glGenSamplers* glGenSamplers;
    _glBindSampler* glBindSampler;
    _glSamplerParameteri* glSamplerParameteri;
    _glMapBuffer* glMapBuffer;
    _glUnmapBuffer* glUnmapBuffer;
    _glMapBufferRange* glMapBufferRange;
    _glBindVertexBuffer* glBindVertexBuffer;
    _glVertexAttribBinding* glVertexAttribBinding;
    _glVertexAttribFormat* glVertexAttribFormat;
    _glDebugMessageCallbackARB* glDebugMessageCallbackARB;
    _glDrawArraysInstanced* glDrawArraysInstanced;
    _glVertexAttribDivisor* glVertexAttribDivisor;
    _glVertexBindingDivisor* glVertexBindingDivisor;
    _glObjectLabel* glObjectLabel;
    _glPushDebugGroup* glPushDebugGroup;
    _glPopDebugGroup* glPopDebugGroup;
};

#define MAX_CHANNELS 16
struct gl_state
{
    gl_imgui_context ImGuiContext;
    gl_wrapped gl;

    GLuint GlobalMatrices;
    
    GLuint RefToTextureSampler;
    uint32 LastSlotBoundToSampler;
    
    GLuint GlobalWithColorVao;
    GLuint GlobalWithoutColorVao;
    
    GLuint ParticlesVao;
};

#define OPENGL_INIT(name) gl_state name(bool32 ModernContext, bool32 FramebufferSupportsSRGB, void* TempMemory, size_t TempMemorySize, yam_profiling* Profiling, platform_api* PlatformAPI)
typedef OPENGL_INIT(opengl_init);

#define OPENGL_EXIT(name) void name(gl_state* State)
typedef OPENGL_EXIT(opengl_exit);

#define OPENGL_IMGUI_LOADFONTS(name) void name(gl_state* State, imgui_font_info* FontInfo)
typedef OPENGL_IMGUI_LOADFONTS(opengl_imgui_loadfonts);

#define OPENGL_RENDER_COMMANDS(name) void name(render_list* List, gl_state* State, void* TempMemory, size_t TempMemorySize, yam_profiling* Profiling,platform_api* API)
typedef OPENGL_RENDER_COMMANDS(opengl_render_commands);

#define OPENGL_CREATE_BUFFER(name) void name(gl_wrapped* gl, graphics_buffer* Buffer, uint32 Channel, uint32 ChannelWidth,  mesh_residency Residency)
typedef OPENGL_CREATE_BUFFER(opengl_create_buffer);

#define OPENGL_FILL_BUFFER(name) void name(gl_wrapped* gl, graphics_buffer* Buffer)
typedef OPENGL_FILL_BUFFER(opengl_fill_buffer);

#define OPENGL_ALLOCATE_TEXTURE(name) void name(gl_wrapped* gl, texture* Texture)
typedef OPENGL_ALLOCATE_TEXTURE(opengl_allocate_texture);

#define OPENGL_CREATE_FBO(name) fbo name(gl_wrapped* gl, uint32 Width, uint32 Height, uint32 FboOptions, char* DebugName)
typedef OPENGL_CREATE_FBO(opengl_create_fbo);

#define OPENGL_RESIZE_FBO(name) void name(gl_wrapped* gl, fbo* Fbo, uint32 Width, uint32 Height)
typedef OPENGL_RESIZE_FBO(opengl_resize_fbo);

#define OPENGL_CREATE_SHADER(name) bool  name(gl_wrapped* gl, shader* Shader, char* MetashaderFilePath, char* Header, char* ShaderPreproc, char* DestinationFolder, uint32 UniformStructSize, temporary_memory* Temp, platform_api* PlatformAPI)
typedef OPENGL_CREATE_SHADER(opengl_create_shader);

#define RENDERER_OPENGL_H
#endif