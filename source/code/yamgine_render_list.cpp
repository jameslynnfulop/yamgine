#include "yamgine_render_list.h"

void AddMeshAsset(render_assets* Assets, transform* TransformToAdd)
{
    Assert(TransformToAdd != NULL);
    AddToList(&Assets->Meshes, TransformToAdd);
}

void AddModelAsset(render_assets* Assets, mesh* TransformToAdd)
{
    Assert(TransformToAdd != NULL);
    Assert(StringLength(TransformToAdd->Path) > 0);
    AddToList(&Assets->Models, TransformToAdd);
    
    if (TransformToAdd->Vertices.Count > 0)
    {
        AddMeshAsset(Assets, TransformToAdd);
    }
    
    if (TransformToAdd->Child != NULL)
    {
        mesh* Child = (mesh*)TransformToAdd->Child;
        do
        {
            AddMeshAsset(Assets, Child);
            Child = (mesh*)Child->NextSibling;
        } while (Child != NULL);
    }
}

void AddTexture(render_assets* Assets, texture* Texture)
{
    //Assert(Texture->Handle != MAX_UINT32);
    
    Assert(Assets->TextureCount < MAX_ASSET_TEXTURES-1);
    
    Assets->Textures[Assets->TextureCount++] = Texture;
}

mesh* FindMeshByPath(render_assets* Assets, char* Path)
{
    mesh* Result = NULL;
    
    linked_list_member* Check = Assets->Meshes.FirstMember;
    do
    {
        transform* Transform = (transform*)Check->Payload;
        if (C_StringsAreEqualAbsolute(Transform->Path, Path))
        {
            Result = (mesh*)Transform;
            break;
        }
        Check = Check->NextNode;
    } while (Check != NULL);
    
    return Result;
}

void AddRenderMesh(entity* Entity, mesh* Mesh)
{
    Entity->RenderMesh = Mesh;
    SetString(&Entity->RenderMeshPath, Mesh->Path);
}

#define PushRenderElement(Layer, type) reinterpret_cast<type*>(PushRenderElement_(Layer, sizeof(type), RenderGroupEntryType_##type))
inline void*
PushRenderElement_(render_list* List, uint32 Size, render_group_entry_type Type)
{
    void* Result = 0;
    
    Size += sizeof(render_group_entry_header);
    
    if ((List->PushBufferUsed + Size) < List->MaxPushBufferSize)
    {
        render_group_entry_header* Header = (render_group_entry_header*)(List->PushBufferBase + List->PushBufferUsed);
        Header->Type = Type;
        Result = (uint8*)Header + sizeof(*Header);
        
        List->PushBufferUsed += Size;
        ++List->PushBufferElementCount;
    }
    else
    {
        InvalidCodePath;
    }
    
    return Result;
}

inline void ClearColor(render_list* List, v4 Color)
{
    render_entry_color_clear* Entry = PushRenderElement(List, render_entry_color_clear);
    if (Entry)
    {
        Entry->Color = Color;
    }
}

inline void ClearDepth(render_list* List)
{
    render_entry_depth_clear* Entry = PushRenderElement(List, render_entry_depth_clear);
    Assert(Entry);
}

inline void DisableDepth(render_list* List)
{
    render_entry_disable_depth* Entry = PushRenderElement(List, render_entry_disable_depth);
    Assert(Entry);
}

inline void EnableDepth(render_list* List)
{
    render_entry_enable_depth* Entry = PushRenderElement(List, render_entry_enable_depth);
    Assert(Entry);
}

inline void CullFaceMode(render_list* List, cull_face_mode CullFaceMode)
{
    render_entry_set_cull_face_mode* Entry = PushRenderElement(List, render_entry_set_cull_face_mode);
    if (Entry)
    {
        Entry->CullFaceMode = CullFaceMode;
    }
}

inline void FboBegin(render_list* List, fbo* Fbo)
{
    render_entry_fbo_begin* Entry = NULL;
    Entry = PushRenderElement(List, render_entry_fbo_begin);
    
    if (Entry)
    {
        Entry->Fbo = Fbo;
    }
}

inline void FboEnd(render_list* List)
{
    PushRenderElement(List, render_entry_fbo_end);
}

inline void
UseMesh(render_list* List, mesh* Mesh)
{
    render_entry_use_mesh* Entry = PushRenderElement(List, render_entry_use_mesh);
    if (Entry)
    {
        Entry->Mesh = Mesh;
    }
}

inline void
UseShader(render_list* List, shader* Shader, void* Uniforms)
{
    Assert(Shader->ProgramHandle != MAX_UINT32);
    Assert(Shader->UniformsStructHandle != MAX_UINT32);
    Assert(Shader->UniformsStructSize != MAX_UINT32);
    uint32 Size = sizeof(render_entry_use_shader) + Shader->UniformsStructSize;
    render_entry_use_shader* Entry = 
        (render_entry_use_shader*)PushRenderElement_(List, Size, 
                                                     RenderGroupEntryType_render_entry_use_shader);
    
    if (Entry)
    {
        Entry->Shader = Shader;
        
        Entry->Uniforms = ((uint8*)Entry) + sizeof(render_entry_use_shader);
        Copy(Shader->UniformsStructSize, Uniforms, Entry->Uniforms);
    }
}


inline void
SetTexture(render_list* List, texture* Texture, uint32 Slot, bool32 IsShadowmap=false)
{
    render_entry_set_texture* Entry = PushRenderElement(List, render_entry_set_texture);
    
    if (Entry)
    {
        Entry->Texture = Texture;
        Entry->Slot = Slot;
        Entry->IsShadowmap = IsShadowmap;
    }
}

inline void
SetBlend(render_list* List, render_blend_type BlendType)
{
    render_entry_set_blend* Entry = PushRenderElement(List, render_entry_set_blend);
    
    if (Entry)
    {
        Entry->BlendType  = BlendType;
    }
}

inline void
SetLineWidth(render_list* List, real32 LineWidth)
{
    render_entry_set_line_width* Entry = PushRenderElement(List, render_entry_set_line_width);
    
    if (Entry)
    {
        Entry->LineWidth = LineWidth;
    }
}

inline void DrawMeshNew(render_list* List, mesh* Mesh, render_mode RenderMode=RenderMode_Fill)
{
    Assert(IsValidBuffer(&Mesh->Vertices));
    
    render_entry_draw_mesh* Entry = PushRenderElement(List, render_entry_draw_mesh);
    if (Entry)
    {
        Entry->Mesh = Mesh;
        Entry->RenderMode = RenderMode;
    }
}

inline void
DrawMesh(render_list* List, transform* Instance, mesh* Mesh, material Material,
         render_mode RenderMode = RenderMode_Fill)
{
    if (Material.Texture != NULL)
    {
        Assert(IsValidBuffer(&Mesh->UVs));
    }
    
    UseMesh(List, Mesh);
    
    ResolveLocalToWorld(Instance);
    ResolveLocalToWorld(Mesh);
    
    matrix4 CanonicalToNormalized = 
    {
        0.5f, 0, 0, 0.5f,
        0,0.5f, 0, 0.5f,
        0, 0, 0.5f, 0.5f,
        0,0,0,1
    };
    
    mesh_recieve_shadows_uniforms Uniforms = {};
    Uniforms.Model = Transpose(Instance->LocalToWorld * Mesh->LocalToWorld);
    Uniforms.LightSpaceMatrix = Transpose(CanonicalToNormalized * List->Light.LightMatrix);
    Uniforms.ObjectColor = Material.Color;
    Uniforms.LightDir = List->Light.LightDir;
    Uniforms.LightColor = List->Light.LightColor.xyz;
    
    UseShader(List, &List->Assets->Shaders.MeshRecieveShadows, (void*)&Uniforms);
    
    SetTexture(List, &List->Light.Shadowmap->DepthTexture, 0, true);
    
    DrawMeshNew(List, Mesh, RenderMode);
}


inline void
DrawSolidColorMesh(render_list* List, transform* Instance, mesh* Mesh, material Material, 
                   render_mode RenderMode = RenderMode_Fill)
{
    if (Material.Texture != NULL)
    {
        Assert(IsValidBuffer(&Mesh->UVs));
    }
    
    UseMesh(List, Mesh);
    
    ResolveLocalToWorld(Instance);
    ResolveLocalToWorld(Mesh);
    
    line_uniform_color_uniforms Uniforms = {};
    Uniforms.Model = Transpose(Instance->LocalToWorld * Mesh->LocalToWorld);
    Uniforms.Color = Material.Color;
    
    UseShader(List, &List->Assets->Shaders.UniformColor, (void*)&Uniforms);
    
    DrawMeshNew(List, Mesh, RenderMode);
}


inline void 
PushLineVertex(render_list* List, v3 Point)
{
    ((v3*)List->Assets->Lines.Vertices.Data)[List->Assets->Lines.Vertices.Count] = Point;
    List->Assets->Lines.Vertices.Count++;
    
    Assert(List->Assets->Lines.Vertices.Count < MAX_LINE_VERTS);
}

inline void 
PushLineColor(render_list* List, v4 Color)
{
    ((v4*)List->Assets->Lines.Colors.Data)[List->Assets->Lines.Colors.Count] = Color;
    List->Assets->Lines.Colors.Count++;
}

inline void 
PushLineElement(render_list* List, uint32 Element)
{
    ((uint32*)List->Assets->Lines.Elements.Data)[List->Assets->Lines.Elements.Count] = Element;
    List->Assets->Lines.Elements.Count++;
    
    Assert(List->Assets->Lines.Vertices.Count < MAX_LINE_VERTS * 2);
}

internal void
DrawLine(render_list* List, v3 PointA, v3 PointB, v4 Color, float LineWidth=0.01f, v3 Normal=v3_ZERO)
{
    if (Normal == v3_ZERO)
    {
        Normal = Cross(Normalize(PointA - PointB), List->CameraForward);
    }
    
    uint32 StartIndex = List->Assets->Lines.Vertices.Count;
    
    PushLineVertex(List, PointA + Normal * LineWidth);
    PushLineColor(List, Color);
    
    PushLineVertex(List, PointA - Normal * LineWidth);
    PushLineColor(List, Color);
    
    PushLineVertex(List, PointB + Normal * LineWidth);
    PushLineColor(List, Color);
    
    PushLineVertex(List, PointB - Normal * LineWidth);
    PushLineColor(List, Color);
    
    PushLineElement(List, StartIndex);
    PushLineElement(List, StartIndex+1);
    PushLineElement(List, StartIndex+2);
    
    PushLineElement(List, StartIndex+1);
    PushLineElement(List, StartIndex+3);
    PushLineElement(List, StartIndex+2);
}

inline void
DrawAllLines(render_list* List, platform_api* PlatformAPI)
{
    if (List->Assets->Lines.Vertices.Count > 0)
    {
        Assert(List->Assets->Lines.Vertices.Count == List->Assets->Lines.Colors.Count);
        
        PlatformAPI->FillBuffer(&List->Assets->Lines.Vertices);
        PlatformAPI->FillBuffer(&List->Assets->Lines.Elements);
        PlatformAPI->FillBuffer(&List->Assets->Lines.Colors);
        
        UseMesh(List, &List->Assets->Lines);
        
        line_uniforms Uniforms;
        Uniforms.Model = IdentityMatrix();
        UseShader(List, &List->Assets->Shaders.PerVertexColor, &Uniforms);
        
        CullFaceMode(List, CullFaceMode_None);
        DrawMeshNew(List, &List->Assets->Lines);
        CullFaceMode(List, CullFaceMode_Back);
    }
}


inline void
DrawLinesMesh(render_list* List, transform* Instance, mesh* Mesh)
{
    Assert(Mesh != nullptr);
    
    ResolveLocalToWorld(Instance);
    ResolveLocalToWorld(Mesh);
    
    UseMesh(List, Mesh);
    
    line_uniforms Uniforms;
    Uniforms.Model = Transpose(Instance->LocalToWorld * Mesh->LocalToWorld);
    UseShader(List, &List->Assets->Shaders.PerVertexColor, &Uniforms);
    
    DrawMeshNew(List, Mesh);
}

inline void
DrawRay(render_list* List, v3 StartPosition, v3 Direction, v4 Color, real32 LineWidth = 0.001f)
{
    DrawLine(List, StartPosition, StartPosition + Direction, Color, LineWidth);
}

void DrawCameraFrustrum(render_list* List, matrix4* ViewProjection, v4 Color)
{
    #if 0
    Assert(0);// this is causing weird overlay in origin coordinate system
    v3 BottomLeftNear = CameraToWorldPosition(*ViewProjection, V2(-1, -1), -1);
    v3 BottomLeftFar = CameraToWorldPosition(*ViewProjection, V2(-1, -1), 1);
    DrawLine(List, BottomLeftNear, BottomLeftFar, Color);
    
    v3 BottomRightNear = CameraToWorldPosition(*ViewProjection, V2(1, -1), -1);
    v3 BottomRightFar = CameraToWorldPosition(*ViewProjection, V2(1, -1), 1);
    DrawLine(List, BottomRightNear, BottomRightFar, Color);
    
    v3 TopLeftNear = CameraToWorldPosition(*ViewProjection, V2(-1, 1), -1);
    v3 TopLeftFar = CameraToWorldPosition(*ViewProjection, V2(-1, 1), 1);
    DrawLine(List, TopLeftNear, TopLeftFar, Color);
    
    v3 TopRightNear = CameraToWorldPosition(*ViewProjection, V2(1, 1), -1);
    v3 TopRightFar = CameraToWorldPosition(*ViewProjection, V2(1, 1), 1);
    DrawLine(List, TopRightNear, TopRightFar, Color);
    
    DrawLine(List, TopLeftNear, TopRightNear, Color);
    DrawLine(List, TopRightNear, BottomRightNear, Color);
    DrawLine(List, BottomLeftNear, BottomRightNear, Color);
    DrawLine(List, BottomLeftNear, TopLeftNear, Color);
    
    DrawLine(List, TopLeftFar, TopRightFar, Color);
    DrawLine(List, TopRightFar, BottomRightFar, Color);
    DrawLine(List, BottomLeftFar, BottomRightFar, Color);
    DrawLine(List, BottomLeftFar, TopLeftFar, Color);
    #endif
}

inline void
DrawCircle(render_list* List, v3 Position, v3 Normal, v3 Perp, real32 Radius, v4 Color)
{
    UseMesh(List, List->Assets->StandardAssets.CircleOutline);
    
    line_uniform_color_uniforms Uniforms;
    v3 Up = Normalize(Cross(Normal, Perp));
    matrix4 LookAtMatrix = ObjectLookAt(Perp, Up, Normal, v3_ZERO);
    v4 Rot = MatrixToQuaternion(LookAtMatrix);
    
    matrix4 PositionMatrix = Translate(Position);
    matrix4 RotationMatrix = QuaternionToMatrixVersion2(Rot);
    Uniforms.Model = Transpose(PositionMatrix * RotationMatrix * CreateScaleMatrix(V3(Radius, Radius, 1)));
    Uniforms.Color = Color;
    UseShader(List, &List->Assets->Shaders.UniformColor, &Uniforms);
    
    DrawMeshNew(List, List->Assets->StandardAssets.CircleOutline);
}

matrix4 Rect2ToMatrix4(rectangle2 Rect, rectangle2 Viewport, bool32 ColumnMajor = false)
{
    v2 ViewportDim = GetDim(Viewport);
    Rect.Min.y = Rect.Min.y;
    Rect.Max.y = Rect.Max.y;
    
    v2 Pos = GetCenter(Rect);
    v2 Scale = GetDim(Rect);
    
    matrix4 Model = {
        Scale.x, 0, 0, 0,
        0, -Scale.y, 0, 0,
        0, 0, 0, 1,
        Pos.x, Pos.y, 0, 1
    };
    return Model;
}

inline void
DrawRect(render_list* List, texture* Texture, 
         rectangle2 DrawingRect, rectangle2 UVRect = RectMinDim(V2(0, 0), V2(1, 1)),
         v4 Color = v4_WHITE)
{
    Assert(!Texture->IsMultisampled);
    
    Assert(UVRect.Min.x >= 0 && UVRect.Min.x <= 1);
    Assert(UVRect.Min.y >= 0 && UVRect.Min.y <= 1);
    Assert(UVRect.Max.x >= 0 && UVRect.Max.x <= 1);
    Assert(UVRect.Max.y >= 0 && UVRect.Max.y <= 1);
    
    UseMesh(List, List->Assets->StandardAssets.Quad);
    
    matrix4 Model = Rect2ToMatrix4(DrawingRect, List->Viewport);
    
    textured_rect_uniforms Uniforms;
    Uniforms.Model = Model;
    
    Uniforms.UVRect = V4(UVRect.Min.x, UVRect.Min.y, UVRect.Max.x, UVRect.Max.y);
    Uniforms.Color = Color;
    
    UseShader(List, &List->Assets->Shaders.TintedTexture, (void*)&Uniforms);
    
    SetTexture(List, Texture, 0);
    
    SetBlend(List, RenderBlendType_one_minus_src);
    DrawMeshNew(List, List->Assets->StandardAssets.Quad);
    SetBlend(List, RenderBlendType_none);
}


inline void
DrawGlyphRect(render_list* List, texture* Texture, 
              rectangle2 DrawingRect, rectangle2 UVRect = RectMinDim(V2(0, 0), V2(1, 1)),
              v4 Color = v4_WHITE)
{
    Assert(!Texture->IsMultisampled);
    
    Assert(UVRect.Min.x >= 0 && UVRect.Min.x <= 1);
    Assert(UVRect.Min.y >= 0 && UVRect.Min.y <= 1);
    Assert(UVRect.Max.x >= 0 && UVRect.Max.x <= 1);
    Assert(UVRect.Max.y >= 0 && UVRect.Max.y <= 1);
    
    UseMesh(List, List->Assets->StandardAssets.Quad);
    
    matrix4 Model = Rect2ToMatrix4(DrawingRect, List->Viewport);
    
    textured_rect_uniforms Uniforms;
    Uniforms.Model = Model;
    
    Uniforms.UVRect = V4(UVRect.Min.x, UVRect.Min.y, UVRect.Max.x, UVRect.Max.y);
    Uniforms.Color = Color;
    
    UseShader(List, &List->Assets->Shaders.Glyph, (void*)&Uniforms);
    
    SetTexture(List, Texture, 0);
    
    SetBlend(List, RenderBlendType_one_minus_src);
    DrawMeshNew(List, List->Assets->StandardAssets.Quad);
    SetBlend(List, RenderBlendType_none);
}

inline void
DrawColorRect(render_list* List, 
              rectangle2 DrawingRect, v4 Color)
{
    UseMesh(List, List->Assets->StandardAssets.Quad);
    
    matrix4 Model = Rect2ToMatrix4(DrawingRect, List->Viewport);
    
    line_uniform_color_uniforms Uniforms;
    Uniforms.Model = Model;
    Uniforms.Color = Color;
    
    UseShader(List, &List->Assets->Shaders.UniformColor, (void*)&Uniforms);
    
    SetBlend(List, RenderBlendType_one_minus_src);
    DrawMeshNew(List, List->Assets->StandardAssets.Quad);
    SetBlend(List, RenderBlendType_none);
}


inline void
DrawSobel(render_list* List, texture* DepthTex)
{
#if 0
    UseMesh(List, List->Assets->StandardAssets.Quad);
    
    rectangle2 DrawingRect = TextureRect(DepthTex);
    matrix4 Model = Rect2ToMatrix4(DrawingRect, List->Viewport);
    
    sobel_uniforms Uniforms = {};
    Uniforms.Model = Model;
    Uniforms.imageHeightFactor = 1.0f / ((real32)DepthTex->Width);
    Uniforms.imageWidthFactor = 1.0f / ((real32)DepthTex->Height);
    
    UseShader(List, &List->Assets->Shaders.Sobel, (void*)&Uniforms);
    
    SetTexture(List, DepthTex, 0);
    
    DrawMeshNew(List, List->Assets->StandardAssets.Quad);
    #endif
}


inline void
DrawGaussian(render_list* List, texture* Texture, real32 Radius, bool32 Horizontal)
{
    UseMesh(List, List->Assets->StandardAssets.Quad);
    
    rectangle2 DrawingRect = TextureRect(Texture);
    
    matrix4 Model = Rect2ToMatrix4(DrawingRect, List->Viewport);
    
    gaussian_blur_uniforms Uniforms = {};
    Uniforms.Model = Model;
    if (Horizontal)
    {
        Uniforms.dir = V2(1,0);
        Uniforms.resolution = Texture->Width;
    }
    else
    {
        Uniforms.dir = V2(0,1);
        Uniforms.resolution = Texture->Height;
    }
    
    Uniforms.radius = Radius;
    
    UseShader(List, &List->Assets->Shaders.GaussianBlur, (void*)&Uniforms);
    
    SetTexture(List, Texture, 0);
    
    DrawMeshNew(List, List->Assets->StandardAssets.Quad);
}

inline void
DrawStencil(render_list* List, texture* Texture, texture* Stencil)
{
    UseMesh(List, List->Assets->StandardAssets.Quad);
    
    rectangle2 DrawingRect = TextureRect(Texture);
    
    matrix4 Model = Rect2ToMatrix4(DrawingRect, List->Viewport);
    
    line_uniforms Uniforms = {};
    Uniforms.Model = Model;
    
    UseShader(List, &List->Assets->Shaders.Stenciling, (void*)&Uniforms);
    
    SetTexture(List, Texture, 0);
    SetTexture(List, Stencil, 1);
    
    
    SetBlend(List, RenderBlendType_one_minus_src);
    DrawMeshNew(List, List->Assets->StandardAssets.Quad);
    SetBlend(List, RenderBlendType_none);
}

inline void
DrawRectMultisampled(render_list* List, texture* Texture)
{
    Assert(Texture->IsMultisampled);
    
    UseMesh(List, List->Assets->StandardAssets.Quad);
    
    rectangle2 DrawingRect = TextureRect(Texture);
    
    v2 ViewportDim = GetDim(List->Viewport);
    DrawingRect.Min.y = ViewportDim.y-DrawingRect.Min.y;
    DrawingRect.Max.y = ViewportDim.y-DrawingRect.Max.y;
    
    v2 Pos = GetCenter(DrawingRect);
    v2 Scale = GetDim(DrawingRect);
    
    matrix4 Model = {
        Scale.x, 0, 0, 0,
        0, Scale.y, 0, 0,
        0, 0, 0, 1,
        Pos.x, Pos.y, 0, 1
    };
    
    multisample_texture_rect_uniforms Uniforms = {};
    Uniforms.Model = Model;
    Uniforms.Dimensions = V2(Texture->Width, Texture->Height);
    
    UseShader(List, &List->Assets->Shaders.MultisampleRect, (void*)&Uniforms);
    
    SetTexture(List, Texture, 0);
    
    SetBlend(List, RenderBlendType_one_minus_src);
    DrawMeshNew(List, List->Assets->StandardAssets.Quad);
    SetBlend(List, RenderBlendType_none);
}

inline void
DrawRectMultisampledDepth(render_list* List, texture* Texture)
{
    Assert(Texture->IsMultisampled);
    
    UseMesh(List, List->Assets->StandardAssets.Quad);
    
    rectangle2 DrawingRect = TextureRect(Texture);
    
    v2 ViewportDim = GetDim(List->Viewport);
    DrawingRect.Min.y = ViewportDim.y-DrawingRect.Min.y;
    DrawingRect.Max.y = ViewportDim.y-DrawingRect.Max.y;
    
    v2 Pos = GetCenter(DrawingRect);
    v2 Scale = GetDim(DrawingRect);
    
    matrix4 Model = {
        Scale.x, 0, 0, 0,
        0, Scale.y, 0, 0,
        0, 0, 0, 1,
        Pos.x, Pos.y, 0, 1
    };
    
    multisample_texture_rect_uniforms Uniforms = {};
    Uniforms.Model = Model;
    Uniforms.Dimensions = V2(Texture->Width, Texture->Height);
    
    UseShader(List, &List->Assets->Shaders.MultisampleRectDepth, (void*)&Uniforms);
    
    SetTexture(List, Texture, 0);
    
    SetBlend(List, RenderBlendType_one_minus_src);
    DrawMeshNew(List, List->Assets->StandardAssets.Quad);
    SetBlend(List, RenderBlendType_none);
}

inline void
DrawImGui(render_list* List)
{
    YamProf(DrawImGui);
    render_entry_imgui* Entry = PushRenderElement(List, render_entry_imgui);
    if (Entry)
    {
        ImGui::Render();
        Entry->DrawData = ImGui::GetDrawData();
        ImGuiIO& io = ImGui::GetIO();
        Entry->DisplaySize = V2(io.DisplaySize.x, io.DisplaySize.y);
        Entry->DisplayFramebufferScale = V2(io.DisplayFramebufferScale.x, io.DisplayFramebufferScale.y);
    }
}

void SetViewProjection(render_list* List, matrix4 View, matrix4 Projection)
{
    render_entry_set_view_projection* Entry = PushRenderElement(List, render_entry_set_view_projection);
    if (Entry)
    {
        List->CurrentView = View;
        List->CurrentProjection = Projection;
        List->CurrentViewProjection = List->CurrentProjection * List->CurrentView;
        
        Entry->GlobalUniforms.View = View;
        Entry->GlobalUniforms.Projection = Projection;
    }
    
}
////////////////////////////////////////
// HELPERS
////////////////////////////////////////

inline void
DrawQuad(render_list* List, transform* Transform, v4 Color,
         render_mode RenderMode = RenderMode_Fill)
{
    material Material = {};
    Material.Color = Color;
    DrawMesh(List, Transform, List->Assets->StandardAssets.Quad, Material, RenderMode);
}

inline void
DrawQuad(render_list* List, v3 Position, v3 Scale, v3 Normal, v4 Color,
         render_mode RenderMode = RenderMode_Fill)
{
    transform Transform = TransformTRSLocal(Position, v4_ZERO_ROTATION, Scale);
    SetForward(&Transform, Normal);
    DrawQuad(List, &Transform, Color, RenderMode);
}

inline void
DrawCube(render_list* List, transform* Transform, v4 Color,
         render_mode RenderMode = RenderMode_Fill)
{
    material Material = {};
    Material.Color = Color;
    DrawMesh(List, Transform, List->Assets->StandardAssets.Cube, Material, RenderMode);
}

inline void
DrawBoxOutline(render_list* List, transform* Transform, v4 Color)
{
    
    v3 BottomLeftBack = MatrixByPosition(&Transform->LocalToWorld, V3(-0.5f, -0.5f, -0.5f));
    v3 BottomRightBack = MatrixByPosition(&Transform->LocalToWorld, V3(0.5f, -0.5f, -0.5f));
    v3 TopLeftBack = MatrixByPosition(&Transform->LocalToWorld, V3(-0.5f, 0.5f, -0.5f));
    v3 TopRightBack = MatrixByPosition(&Transform->LocalToWorld, V3(0.5f, 0.5f, -0.5f));
    v3 BottomLeftFront = MatrixByPosition(&Transform->LocalToWorld, V3(-0.5f, -0.5f, 0.5f));
    v3 BottomRightFront = MatrixByPosition(&Transform->LocalToWorld, V3(0.5f, -0.5f, 0.5f));
    v3 TopLeftFront = MatrixByPosition(&Transform->LocalToWorld, V3(-0.5f, 0.5f, 0.5f));
    v3 TopRightFront = MatrixByPosition(&Transform->LocalToWorld, V3(0.5f, 0.5f, 0.5f));
    
    // X
    DrawLine(List, BottomLeftBack, BottomRightBack, Color, 1);
    DrawLine(List, TopLeftBack, TopRightBack, Color, 1);
    
    DrawLine(List, BottomLeftBack, TopLeftBack, Color, 1);
    DrawLine(List, BottomRightBack, TopRightBack, Color, 1);
    
    // Y
    DrawLine(List, BottomLeftFront, BottomRightFront, Color, 1);
    DrawLine(List, TopLeftFront, TopRightFront, Color, 1);
    
    DrawLine(List, BottomLeftFront, TopLeftFront, Color, 1);
    DrawLine(List, BottomRightFront, TopRightFront, Color, 1);
    
    // Z
    DrawLine(List, BottomLeftBack, BottomLeftFront, Color, 1);
    DrawLine(List, TopLeftBack, TopLeftFront, Color, 1);
    
    DrawLine(List, BottomRightBack, BottomRightFront, Color, 1);
    DrawLine(List, TopRightBack, TopRightFront, Color, 1);
}

inline void
DrawCube(render_list* List, v3 Position, v3 Scale, v4 Color,
         render_mode RenderMode = RenderMode_Fill)
{
    transform Transform = TransformTRSLocal(Position, v4_ZERO_ROTATION, Scale);
    DrawCube(List, &Transform, Color, RenderMode);
}

inline void
DrawTetrahedron(render_list* List, transform* Transform, v4 Color,
                render_mode RenderMode = RenderMode_Fill)
{
    material Material = {};
    Material.Color = Color;
    DrawMesh(List, Transform, List->Assets->StandardAssets.Tetrahedron, Material, RenderMode);
}

inline void
DrawPyramid(render_list* List, transform* Transform,
            v4 Color, render_mode RenderMode = RenderMode_Fill)
{
    material Material = {};
    Material.Color = Color;
    DrawMesh(List, Transform, List->Assets->StandardAssets.Pyramid, Material, RenderMode);
}

#if 0
inline void
DrawArrow(render_list *List, v3 StartPosition, v3 EndPosition, v4 Color)
{
    DrawLine(List, StartPosition, EndPosition, Color);
    
    transform ArrowHeadTransform = TransformTRSLocal(EndPosition, v3_ZERO, v3_ONE * 0.1f);
    //SetRotationLookAt(&ArrowHeadTransform, StartPosition - EndPosition);// TODO(james): should be some kind of AngleAxis
    RotateLocal(&ArrowHeadTransform, V3(-PI/2, 0, 0));
    
    DrawPyramid(List, &ArrowHeadTransform, Color);
}
#endif

inline void
DrawSphere(render_list* List, v3 Position, real32 Radius, v4 Color,
           render_mode RenderMode = RenderMode_Fill)
{
    material Material = {};
    Material.Color = Color;
    transform Transform = TransformTRSLocal(Position, v4_ZERO_ROTATION, V3(Radius, Radius, Radius));
    DrawMesh(List, &Transform, List->Assets->StandardAssets.Sphere, Material, RenderMode);
}

inline void
DrawSphere(render_list* List, transform* Transform, material Material,
           render_mode RenderMode = RenderMode_Fill)
{
    DrawMesh(List, Transform, List->Assets->StandardAssets.Sphere, Material, RenderMode);
}

inline void
DrawSphereSkeleton(render_list* List, v3 Position, real32 Radius, v4 Color)
{
    DrawCircle(List, Position, v3_FORWARD, v3_RIGHT, Radius, Color);
    DrawCircle(List, Position, v3_RIGHT, v3_UP, Radius, Color);
    DrawCircle(List, Position, v3_UP, v3_RIGHT, Radius, Color);
}

inline void
DrawSphereSkeleton(render_list* List, v3 Position, v3 Forward, v3 Right, v3 Up, real32 Radius, v4 Color)
{
    Assert(LengthSq(Forward) > 0);
    Assert(LengthSq(Right) > 0);
    Assert(LengthSq(Up) > 0);
    DrawCircle(List, Position, Forward, Right, Radius, Color);
    DrawCircle(List, Position, Right, Up, Radius, Color);
    DrawCircle(List, Position, Up, Forward, Radius, Color);
}

void DrawMeshNormals(render_list* List, mesh* Mesh)
{
    graphics_buffer* Positions = &Mesh->Vertices;
    graphics_buffer* Normals = &Mesh->Normals;
    
    Assert(Positions->Count == Normals->Count);
    for (uint32 NormalIndex = 0;
         NormalIndex < Normals->Count;
         NormalIndex++)
    {
        v3 Vertex = GET_V3(Positions, NormalIndex) + Mesh->LocalPosition;
        v3 Normal = GET_V3(Normals, NormalIndex) * 0.2f;
        DrawLine(List, Vertex, Vertex + Normal, V4(0, 1, 0, 1.0f));
    }
}

void DrawDebugXYGrid(render_list* List)
{
    v4 GridLineColor = V4(0.2f, 0.2f, 0.2f, 1);
    for (int ZIncrement = -100;
         ZIncrement < 100;
         ZIncrement++)
    {
        DrawLine(List, V3i(-100, ZIncrement, 0), V3i(100, ZIncrement, 0), GridLineColor);
    }
    
    for (int XIncrement = -100;
         XIncrement < 100;
         XIncrement++)
    {
        DrawLine(List, V3i(XIncrement, -100, 0), V3i(XIncrement, 100, 0), GridLineColor);
    }
}

void DrawCoordinateSystem(render_list* List, transform* Transform)
{
    // X Axis
    v4 XAxisColor = V4(1, 0, 0, 1);
    transform XAxisTransform = TransformTRSLocal(V3(0.5f, 0, 0), v4_ZERO_ROTATION, V3(1, 0.1f, 0.1f));
    SetTransformParent(&XAxisTransform, Transform);
    DrawCube(List, &XAxisTransform, XAxisColor);
    
    // Y Axis
    v4 YAxisColor = V4(0, 1, 0, 1);
    transform YAxisTransform = TransformTRSLocal(V3(0, 0.5f, 0), v4_ZERO_ROTATION, V3(0.1f, 1, 0.1f));
    SetTransformParent(&YAxisTransform, Transform);
    DrawCube(List, &YAxisTransform, YAxisColor);
    
    // Z Axis
    v4 ZAxisColor = V4(0, 0, 1, 1);
    transform ZAxisTransform = TransformTRSLocal(V3(0, 0, 0.5f), v4_ZERO_ROTATION, V3(0.1f, 0.1f, 1));
    SetTransformParent(&ZAxisTransform, Transform);
    DrawCube(List, &ZAxisTransform, ZAxisColor);
}

void DrawCoordinateSystemLines(render_list* List, transform* Transform)
{
    DrawRay(List, Transform->Position, Transform->Right, v4_RED);
    DrawRay(List, Transform->Position, Transform->Up, v4_GREEN);
    DrawRay(List, Transform->Position, Transform->Forward, v4_BLUE);
}

void DrawAABB(render_list* List, aabb* A, v4 Color)
{
    transform Transform = TransformTRSLocal(A->Position, v4_ZERO_ROTATION, A->HalfWidths * 2);
    DrawCube(List, &Transform, Color, RenderMode_Wireframe);
}

void DrawCapsuleFrame(render_list* List, transform* Transform, real32 Radius, real32 Height)
{
    Assert(Radius > 0);
    Assert(Height > 0);
    ResolveLocalToWorld(Transform);
    
    real32 HalfHeight = (Height / 2) - Radius;
    
    // could probably get a lot of this info using world up/right/forward vectors and position?
    //top
    DrawSphereSkeleton(List, GetWorldPositionFromLocalOffset(Transform, V3(0, HalfHeight, 0)),
                       Transform->Forward, Transform->Right, Transform->Up, Radius, v4_GREEN);
    //bottom
    DrawSphereSkeleton(List, GetWorldPositionFromLocalOffset(Transform, V3(0, -HalfHeight, 0)),
                       Transform->Forward, Transform->Right, Transform->Up, Radius, v4_GREEN);
    
    //sides
    DrawLine(List,
             GetWorldPositionFromLocalOffset(Transform, V3(0, HalfHeight, Radius)),
             GetWorldPositionFromLocalOffset(Transform, V3(0, -HalfHeight, Radius)),
             v4_GREEN);
    DrawLine(List,
             GetWorldPositionFromLocalOffset(Transform, V3(0, HalfHeight, -Radius)),
             GetWorldPositionFromLocalOffset(Transform, V3(0, -HalfHeight, -Radius)),
             v4_GREEN);
    DrawLine(List,
             GetWorldPositionFromLocalOffset(Transform, V3(Radius, HalfHeight, 0)),
             GetWorldPositionFromLocalOffset(Transform, V3(Radius, -HalfHeight, 0)),
             v4_GREEN);
    DrawLine(List,
             GetWorldPositionFromLocalOffset(Transform, V3(-Radius, HalfHeight, 0)),
             GetWorldPositionFromLocalOffset(Transform, V3(-Radius, -HalfHeight, 0)),
             v4_GREEN);
}

void DrawCylinderFrame(render_list* List, transform* Transform, real32 Radius, real32 Height)
{
    Assert(Radius > 0);
    Assert(Height > 0);
    ResolveLocalToWorld(Transform);
    
    real32 HalfHeight = (Height / 2);
    
    DrawCircle(List, GetWorldPositionFromLocalOffset(Transform, V3(0, HalfHeight, 0)),
               Transform->Up, Transform->Right, Radius, v4_GREEN);
    DrawCircle(List, GetWorldPositionFromLocalOffset(Transform, V3(0, -HalfHeight, 0)),
               -Transform->Up, Transform->Right, Radius, v4_GREEN);
    
    //sides
    DrawLine(List,
             GetWorldPositionFromLocalOffset(Transform, V3(0, HalfHeight, Radius)),
             GetWorldPositionFromLocalOffset(Transform, V3(0, -HalfHeight, Radius)),
             v4_GREEN);
    DrawLine(List,
             GetWorldPositionFromLocalOffset(Transform, V3(0, HalfHeight, -Radius)),
             GetWorldPositionFromLocalOffset(Transform, V3(0, -HalfHeight, -Radius)),
             v4_GREEN);
    DrawLine(List,
             GetWorldPositionFromLocalOffset(Transform, V3(Radius, HalfHeight, 0)),
             GetWorldPositionFromLocalOffset(Transform, V3(Radius, -HalfHeight, 0)),
             v4_GREEN);
    DrawLine(List,
             GetWorldPositionFromLocalOffset(Transform, V3(-Radius, HalfHeight, 0)),
             GetWorldPositionFromLocalOffset(Transform, V3(-Radius, -HalfHeight, 0)),
             v4_GREEN);
}

void DrawEntityToDepthmap(render_list* List, entity* Entity, entity* DirectionalLight)
{
    mesh* Mesh = Entity->RenderMesh;
    transform* Instance = Entity;
    
    ResolveLocalToWorld(Instance);
    ResolveLocalToWorld(Mesh);
    
    UseMesh(List, Mesh);
    
    fill_depthmap_uniforms Uniforms;
    Uniforms.Model = Transpose(Instance->LocalToWorld * Mesh->LocalToWorld);
    UseShader(List, &List->Assets->Shaders.MeshFillDepthmap, &Uniforms);
    
    DrawMeshNew(List, Mesh);
}

void UseParticles(render_list* List, mesh* BaseMesh, graphics_buffer* ParticleCenters)
{
    render_entry_use_particles* Entry = PushRenderElement(List, render_entry_use_particles);
    if (Entry)
    {
        Entry->BaseMesh = BaseMesh;
        Entry->ParticleCenters = ParticleCenters;
    }
}


void DrawParticles_I(render_list* List, mesh* BaseMesh, graphics_buffer* ParticleCenters)
{
    render_entry_draw_particles* Entry = PushRenderElement(List, render_entry_draw_particles);
    if (Entry)
    {
        Entry->BaseMesh = BaseMesh;
        Entry->ParticleCenters = ParticleCenters;
    }
}

void DrawParticles(render_list* List, graphics_buffer* ParticleCenters, transform* Camera)
{
    UseParticles(List, List->Assets->StandardAssets.QuadVertices, ParticleCenters);
    
    fill_depthmap_uniforms Uniforms = {};
    Uniforms.Model = QuaternionToMatrixVersion2(InverseQuaternion(Camera->LocalRotation)) * 
        CreateScaleMatrix(V3(0.1f,0.1f,0.1f));
    UseShader(List, &List->Assets->Shaders.Particles, &Uniforms);
    
    SetBlend(List, RenderBlendType_one_minus_src);
    CullFaceMode(List, CullFaceMode_None);
    DrawParticles_I(List, List->Assets->StandardAssets.QuadVertices, ParticleCenters);
    //TODO(james): assuming here, we should store the current state somewhere
    CullFaceMode(List, CullFaceMode_Back);
    SetBlend(List, RenderBlendType_none);
}