/* NOTE(james): 
This file contains commands that are OpenGL only. Platform specific context
creation should occur in the platform layer. 
*/
#include "renderer_opengl.h"
#include "renderer_opengl_shadergen.cpp"

//Name(GLenum source,GLenum type,GLuint id,GLenum severity,GLsizei length,const GLchar *message,const void *userParam)
GL_DEBUG_CALLBACK(OpenGLDebugCallback)
{
    if (severity != GL_DEBUG_SEVERITY_NOTIFICATION)
    {
        char* ErrorMessage = (char*)message;
        OutputDebugStringA("--------------------------------------------\n");
        switch(source)
        {
            case DEBUG_SOURCE_API_ARB:
            OutputDebugStringA("Error Source: API: ");
            break;
            case DEBUG_SOURCE_SHADER_COMPILER_ARB:
            OutputDebugStringA("Error Source: ShaderCompiler: ");
            break;
        }
        OutputDebugStringA(ErrorMessage);
        OutputDebugStringA("\n");
        if(severity == GL_DEBUG_SEVERITY_LOW ||
           severity == GL_DEBUG_SEVERITY_MEDIUM ||
           severity == GL_DEBUG_SEVERITY_HIGH)
        {
#if 1
            Assert(!"OpenGL Error encountered");
#endif
        }
    }
    
}

GLuint LoadShader(gl_wrapped* gl, char* ShaderSource, int ShaderType)
{
    int ShaderSourceArrayCount = 0;
    
    GLuint Shader = gl->glCreateShader(ShaderType);
    
    gl->glShaderSource(Shader, 1, &ShaderSource, NULL);
    
    GLint SourceAssignError = glGetError();
    gl->glCompileShader(Shader);
    GLint CompileShaderError = glGetError();
    
    GLint Status;
    gl->glGetShaderiv(Shader, GL_COMPILE_STATUS, &Status);
    
    char Log[512];//TODO(james): maybe use temp memory instead?
    int ActualLogLength;
    gl->glGetShaderInfoLog(Shader, 512, &ActualLogLength, Log);
    
    if (ActualLogLength > 0)
    {
        Assert(0); // NOTE(james): might catch warning messages here too
    }
    
    Assert(Status);
    if (!Status)
    {
        Shader = MAX_UINT32;
    }
    
    return Shader;
}

//TODO(james): check out glGetProgramBinary sometime, lets you load precompiled shaders
GLuint LoadShaderProgram(gl_wrapped* gl, char* VertShaderSource, char* FragShaderSource)
{
    GLuint VertexShader = LoadShader(gl, VertShaderSource, GL_VERTEX_SHADER);
    GLuint FragShader = LoadShader(gl, FragShaderSource, GL_FRAGMENT_SHADER);
    
    if (VertexShader != MAX_UINT32 && FragShader != MAX_UINT32)
    {
        GLuint ImageShaderProgram = gl->glCreateProgram();
        gl->glAttachShader(ImageShaderProgram, VertexShader);
        gl->glAttachShader(ImageShaderProgram, FragShader);
        gl->glLinkProgram(ImageShaderProgram);
        
        char Log[512];
        int LogLength;
        gl->glGetProgramInfoLog(ImageShaderProgram, 512, &LogLength, Log);
        
        GLint LinkStatus;
        gl->glGetProgramiv(ImageShaderProgram, GL_LINK_STATUS, &LinkStatus);
        Assert(LinkStatus);
        
        gl->glDeleteShader(VertexShader);
        gl->glDeleteShader(FragShader);
        
        GLint Error = glGetError();
        Assert(!Error);
        if (Error)
        {
            ImageShaderProgram = MAX_UINT32; //NOTE(james): this signals that loading failed
        }
        
#if 0
        //
        // Introspection
        //
        GLint numUniforms = 0;
        gl->glGetProgramInterfaceiv(ImageShaderProgram, GL_UNIFORM, GL_ACTIVE_RESOURCES, &numUniforms);
        const GLenum properties[4] = {GL_BLOCK_INDEX, GL_TYPE, GL_NAME_LENGTH, GL_LOCATION};
        
        for(int unif = 0; unif < numUniforms; ++unif)
        {
            GLint values[4];
            gl->glGetProgramResourceiv(ImageShaderProgram, GL_UNIFORM, unif, 4, properties, 4, NULL, values);
            
            //Skip any uniforms that are in a block.
            if(values[0] != -1)
                continue;
            
            char NameData[128];
            gl->glGetProgramResourceName(ImageShaderProgram, GL_UNIFORM, unif, 128, NULL, &NameData[0]);
            int x= 0;
        }
#endif
        
        return ImageShaderProgram;
    }
    else
    {
        return MAX_UINT32;
    }
}

inline GLint
GetUniformLocation(gl_wrapped* gl, GLint Shader, char* Name)
{
    //YamProf(GetLoc);
    GLint Result = gl->glGetUniformLocationUnsafe(Shader, Name);
    Assert(Result > -1);
    return Result;
}

inline GLint
GetAttribLocation(gl_wrapped* gl, GLint Shader, char* Name)
{
    GLint Result = gl->glGetAttribLocationUnsafe(Shader, Name);
    Assert(Result > -1);
    return Result;
}

//
// IMGUI
//

extern "C" OPENGL_IMGUI_LOADFONTS(OpenGLImGuiLoadFonts)
{
    State->gl.glBindVertexArray(State->ImGuiContext.VaoHandle);
    
    // Upload texture to graphics system
    GLint last_texture;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
    glGenTextures(1, &State->ImGuiContext.FontTexture);
    glBindTexture(GL_TEXTURE_2D, State->ImGuiContext.FontTexture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FontInfo->Width, FontInfo->Height,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, FontInfo->Pixels);
    
    // Store our identifier
    
    ImGuiIO& IO = FontInfo->GetIOPtr();
    IO.Fonts->TexID = (void*)(intptr_t)State->ImGuiContext.FontTexture;
    
    // Restore state
    glBindTexture(GL_TEXTURE_2D, last_texture);
    
    
}

//NOTE(james): this is pretty much a copy-paste from ImGui source
void SetupImGui(gl_wrapped* gl, gl_imgui_context* ImGuiContext)
{
    
    
    //GLint LastTexture, LastArrayBuffer, LastVertexArray;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &ImGuiContext->LastTexture);
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &ImGuiContext->LastArrayBuffer);
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &ImGuiContext->LastVertexArray);
    
    GLchar* VertexShader = "#version 330\n"
        "uniform mat4 ProjMtx;\n"
        "layout(location = 0)in vec2 Position;\n"
        "layout(location = 4)in vec2 UV;\n"
        "layout(location = 2)in vec4 Color;\n"
        "out vec2 Frag_UV;\n"
        "out vec4 Frag_Color;\n"
        "void main()\n"
        "{\n"
        "	Frag_UV = UV;\n"
        "	Frag_Color = Color;\n"
        "	gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
        "}\n";
    
    GLchar* FragmentShader = "#version 330\n"
        "uniform sampler2D Texture;\n"
        "in vec2 Frag_UV;\n"
        "in vec4 Frag_Color;\n"
        "out vec4 Out_Color;\n"
        "void main()\n"
        "{\n"
        "	Out_Color = Frag_Color * texture( Texture, Frag_UV.st);\n"
        "}\n";
    
    ImGuiContext->Shader = LoadShaderProgram(gl, VertexShader, FragmentShader);
    /*g_AttribLocationPosition = glGetAttribLocation(Result.Shader, "Position");
    g_AttribLocationUV = glGetAttribLocation(Result.Shader, "UV");
    g_AttribLocationColor = glGetAttribLocation(Result.Shader, "Color");*/
    
    gl->glGenBuffers(1, &ImGuiContext->VboHandle);
    gl->glGenBuffers(1, &ImGuiContext->ElementsHandle);
    
    gl->glGenVertexArrays(1, &ImGuiContext->VaoHandle);
    gl->glBindVertexArray(ImGuiContext->VaoHandle);
    gl->glBindBuffer(GL_ARRAY_BUFFER, ImGuiContext->VboHandle);
    
#define OFFSETOF(TYPE, ELEMENT) ((size_t) & (((TYPE*)0)->ELEMENT))
    gl->glVertexAttribPointer(CHANNEL_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, pos));
    gl->glEnableVertexAttribArray(CHANNEL_POSITION);
    gl->glVertexAttribPointer(CHANNEL_UV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, uv));
    gl->glEnableVertexAttribArray(CHANNEL_UV);
    gl->glVertexAttribPointer(CHANNEL_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, col));
    gl->glEnableVertexAttribArray(CHANNEL_COLOR);
#undef OFFSETOF
    
    ImGuiContext->TextureID = GetUniformLocation(gl, ImGuiContext->Shader, "Texture");
    ImGuiContext->ProjMtxID = GetUniformLocation(gl, ImGuiContext->Shader, "ProjMtx");
    
    
    //ImGuiCreateFontsTexture(&Result);
    
    // Restore modified GL state
    glBindTexture(GL_TEXTURE_2D, ImGuiContext->LastTexture);
    gl->glBindBuffer(GL_ARRAY_BUFFER, ImGuiContext->LastArrayBuffer);
    gl->glBindVertexArray(ImGuiContext->LastVertexArray);
    
    
}

//NOTE(james): this is pretty much a copy-paste from ImGui source
void DrawImGui(gl_wrapped* gl, ImDrawData* DrawData, gl_imgui_context* ImGuiContext, v2 DisplaySize, v2 DisplayFramebufferScale)
{
    YamProf(DrawImGui);
    
    
    // Avoid rendering when minimized, scale coordinates for retina displays (screen coordinates != framebuffer coordinates)
    int fb_width = (int)(DisplaySize.x * DisplayFramebufferScale.x);
    int fb_height = (int)(DisplaySize.y * DisplayFramebufferScale.y);
    if (fb_width == 0 || fb_height == 0)
        return;
    
    const ImVec2 Scale = ImVec2(DisplayFramebufferScale.x, DisplayFramebufferScale.y);
    DrawData->ScaleClipRects(Scale);
#if 1
    // Backup GL state
    GLint last_program;
    glGetIntegerv(GL_CURRENT_PROGRAM, &last_program);
    GLint last_texture;
    glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
    GLint last_array_buffer;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
    GLint last_element_array_buffer;
    glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &last_element_array_buffer);
    GLint last_vertex_array;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
    GLint last_blend_src;
    glGetIntegerv(GL_BLEND_SRC, &last_blend_src);
    GLint last_blend_dst;
    glGetIntegerv(GL_BLEND_DST, &last_blend_dst);
    GLint last_blend_equation_rgb;
    glGetIntegerv(GL_BLEND_EQUATION_RGB, &last_blend_equation_rgb);
    GLint last_blend_equation_alpha;
    glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &last_blend_equation_alpha);
    GLint last_viewport[4];
    glGetIntegerv(GL_VIEWPORT, last_viewport);
    GLboolean last_enable_blend = glIsEnabled(GL_BLEND);
    GLboolean last_enable_cull_face = glIsEnabled(GL_CULL_FACE);
    GLboolean last_enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
    GLboolean last_enable_scissor_test = glIsEnabled(GL_SCISSOR_TEST);
    
    
#endif
    // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
    glEnable(GL_BLEND);
    gl->glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);
    gl->glActiveTexture(GL_TEXTURE0);
    
    // Setup viewport, orthographic projection matrix
    glViewport(0, 0, (GLsizei)fb_width, (GLsizei)fb_height);
    const float ortho_projection[4][4] = {
        { 2.0f / DisplaySize.x, 0.0f, 0.0f, 0.0f },
        { 0.0f, 2.0f / -DisplaySize.y, 0.0f, 0.0f },
        { 0.0f, 0.0f, -1.0f, 0.0f },
        { -1.0f, 1.0f, 0.0f, 1.0f },
    };
    
    gl->glUseProgram(ImGuiContext->Shader);
    
    gl->glUniform1i(ImGuiContext->TextureID, 0);
    
    gl->glUniformMatrix4fv(ImGuiContext->ProjMtxID, 1, GL_FALSE, &ortho_projection[0][0]);
    
    gl->glBindVertexArray(ImGuiContext->VaoHandle);
    
    
    
    for (int n = 0; n < DrawData->CmdListsCount; n++)
    {
        const ImDrawList* cmd_list = DrawData->CmdLists[n];
        const ImDrawIdx* idx_buffer_offset = 0;
        
        gl->glBindBuffer(GL_ARRAY_BUFFER, ImGuiContext->VboHandle);
        gl->glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.size() * sizeof(ImDrawVert), (GLvoid*)&cmd_list->VtxBuffer.front(), GL_STREAM_DRAW);
        
        gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ImGuiContext->ElementsHandle);
        gl->glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.size() * sizeof(ImDrawIdx), (GLvoid*)&cmd_list->IdxBuffer.front(), GL_STREAM_DRAW);
        
        
#define OFFSETOF(TYPE, ELEMENT) ((size_t) & (((TYPE*)0)->ELEMENT))
        gl->glVertexAttribPointer(CHANNEL_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, pos));
        gl->glEnableVertexAttribArray(CHANNEL_POSITION);
        
        gl->glVertexAttribPointer(CHANNEL_UV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, uv));
        gl->glEnableVertexAttribArray(CHANNEL_UV);
        
        gl->glVertexAttribPointer(CHANNEL_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)OFFSETOF(ImDrawVert, col));
        gl->glEnableVertexAttribArray(CHANNEL_COLOR);
#undef OFFSETOF
        
        for (const ImDrawCmd* pcmd = cmd_list->CmdBuffer.begin(); pcmd != cmd_list->CmdBuffer.end(); pcmd++)
        {
            
            if (pcmd->UserCallback)
            {
                pcmd->UserCallback(cmd_list, pcmd);
            }
            else
            {
                glBindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
                
                
                
                glScissor((int)pcmd->ClipRect.x, (int)(fb_height - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
                glDrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
            }
            idx_buffer_offset += pcmd->ElemCount;
        }
    }
    
#if 1
    // Restore modified GL state
    gl->glUseProgram(last_program);
    glBindTexture(GL_TEXTURE_2D, last_texture);
    gl->glBindVertexArray(last_vertex_array);
    gl->glBindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
    gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, last_element_array_buffer);
    gl->glBlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
    glBlendFunc(last_blend_src, last_blend_dst);
    if (last_enable_blend)
        glEnable(GL_BLEND);
    else
        glDisable(GL_BLEND);
    if (last_enable_cull_face)
        glEnable(GL_CULL_FACE);
    else
        glDisable(GL_CULL_FACE);
    if (last_enable_depth_test)
        glEnable(GL_DEPTH_TEST);
    else
        glDisable(GL_DEPTH_TEST);
    if (last_enable_scissor_test)
        glEnable(GL_SCISSOR_TEST);
    else
        glDisable(GL_SCISSOR_TEST);
    glViewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
    
    //gl->glDisableVertexAttribArray(CHANNEL_POSITION);
    //gl->glDisableVertexAttribArray(CHANNEL_UV);
    //gl->glDisableVertexAttribArray(CHANNEL_COLOR);
#endif
    
    
}

void UseMesh(gl_state* State, mesh* Mesh)
{
    YamProf(UseMesh);
    gl_wrapped* gl = &State->gl;
    
    if (IsValidBuffer(&Mesh->Colors))
    {
        gl->glBindVertexArray(State->GlobalWithColorVao);
    }
    else if (IsValidBuffer(&Mesh->Colors))
    {
        gl->glBindVertexArray(State->GlobalWithColorVao);
    }
    else
    {
        gl->glBindVertexArray(State->GlobalWithoutColorVao);
    }
    
    //
    //
    //
    if (IsValidBuffer(&Mesh->Vertices))
    {
        gl->glBindVertexBuffer(CHANNEL_POSITION, Mesh->Vertices.Handle, 0, sizeof(v3));
    }
    
    if (IsValidBuffer(&Mesh->Normals))
    {
        gl->glBindVertexBuffer(CHANNEL_NORMAL, Mesh->Normals.Handle, 0, sizeof(v3));
    }
    
    if (IsValidBuffer(&Mesh->Elements))
    {
        //gl->glBindVertexBuffer(CHANNEL_ELEMENT, Mesh->Elements.Handle, 0, sizeof(uint32));
        gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Mesh->Elements.Handle);
    }
    
    if (IsValidBuffer(&Mesh->Colors))
    {
        gl->glBindVertexBuffer(CHANNEL_COLOR, Mesh->Colors.Handle, 0, sizeof(v4));
    }
    
    if (IsValidBuffer(&Mesh->UVs))
    {
        gl->glBindVertexBuffer(CHANNEL_UV, Mesh->UVs.Handle, 0, sizeof(v2));
    }
    
    
    
}

void UseShader(gl_wrapped* gl, shader* Shader, void* Data)
{
    
    Assert(Shader->ProgramHandle != MAX_UINT32);
    Assert(Shader->UniformsStructHandle != MAX_UINT32);
    Assert(Shader->UniformsStructSize != MAX_UINT32);
    
    //TODO(james): it'd be cool if we could somehow assert that the 
    //mesh provided has the attributes the shader wants
    YamProf(UseShader);
    gl->glUseProgram(Shader->ProgramHandle);
    
    if (Shader->UniformsStructHandle != -1)
    {
        gl->glBindBuffer(GL_UNIFORM_BUFFER, Shader->UniformsStructHandle);
        gl->glBufferSubData(GL_UNIFORM_BUFFER, 0, Shader->UniformsStructSize, Data);
        gl->glBindBufferRange(GL_UNIFORM_BUFFER, 1, Shader->UniformsStructHandle, 0, Shader->UniformsStructSize);
    }
    
    
    
}

//TODO(james): if you do this before UseShader we get an error, can we assert for that?
void GL_SetTexture(gl_state* State, texture* Texture, uint32 Slot, bool32 IsShadowmap)
{
    
    gl_wrapped* gl = &State->gl;
    Assert(Texture->Handle != MAX_UINT32);
    gl->glActiveTexture(GL_TEXTURE0+Slot);
    
    if (Texture->IsMultisampled)
    {
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, Texture->Handle);
    }
    else
    {
        glBindTexture(GL_TEXTURE_2D, Texture->Handle);
    }
    
    
    if (IsShadowmap)
    {
        gl->glBindSampler(Slot, State->RefToTextureSampler);
        State->LastSlotBoundToSampler = Slot;
    }
    
    
    gl->glUniform1i(YAM_TEXTURE0_LOCATION+Slot, Slot); // Active Texture ID, not the Texture Handle!
    
    
    
}

void MeshDrawCall(gl_state* State, mesh* Mesh, render_mode RenderMode)
{
    gl_wrapped* gl = &State->gl;
    YamProf(MeshDrawCall);
    
    switch (RenderMode)
    {
        case RenderMode_Wireframe:
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        break;
        case RenderMode_Fill:
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        break;
        case RenderMode_Point:
        glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
        break;
    }
    
    if (Mesh->IsIndexedMesh)
    {
        if (Mesh->MeshType == MeshType_Triangles)
        {
            glDrawElements(GL_TRIANGLES, Mesh->Elements.Count, GL_UNSIGNED_INT, 0);
        }
        else if (Mesh->MeshType == MeshType_TriangleStrip)
        {
            glDrawElements(GL_TRIANGLE_STRIP, Mesh->Elements.Count, GL_UNSIGNED_INT, 0);
        }
        else if (Mesh->MeshType == MeshType_TriangleFan)
        {
            glDrawElements(GL_TRIANGLE_FAN, Mesh->Elements.Count, GL_UNSIGNED_INT, 0);
        }
        else if (Mesh->MeshType == MeshType_TriangleList_Adj)
        {
            //            glEnable(GL_PRIMITIVE_RESTART);
            //            glPrimitiveRestartIndex(MAX_UINT32);
            glDrawElements(GL_TRIANGLES_ADJACENCY, Mesh->Elements.Count, GL_UNSIGNED_INT, 0);
        }
        else if (Mesh->MeshType == MeshType_TriangleStrip_Adj)
        {
            glDrawElements(GL_TRIANGLE_STRIP_ADJACENCY, Mesh->Elements.Count, GL_UNSIGNED_INT, 0);
        }
        else
        {
            Assert(0);
        }
        gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    else
    {
        if (Mesh->MeshType == MeshType_Triangles)
        {
            glDrawArrays(GL_TRIANGLES, 0, Mesh->Vertices.Count);
        }
        else if (Mesh->MeshType == MeshType_TriangleStrip)
        {
            glDrawArrays(GL_TRIANGLE_STRIP, 0, Mesh->Vertices.Count);
        }
        else if (Mesh->MeshType == MeshType_TriangleFan)
        {
            glDrawArrays(GL_TRIANGLE_FAN, 0, Mesh->Vertices.Count);
        }
        else if (Mesh->MeshType == MeshType_Lines)
        {
            glDrawArrays(GL_LINES, 0, Mesh->Vertices.Count);
        }
        else if (Mesh->MeshType == MeshType_LineLoop)
        {
            glDrawArrays(GL_LINE_LOOP, 0, Mesh->Vertices.Count);
        }
        else
        {
            Assert(0);
        }
    }
    
    //NOTE(james): this could also be a glGetSampler(...) kinda call but this makes more sense to me
    if (State->LastSlotBoundToSampler != MAX_UINT32)
    {
        gl->glBindSampler(State->LastSlotBoundToSampler, 0);
        State->LastSlotBoundToSampler = MAX_UINT32;
    }
    
    gl->glUseProgram(0);
}

void UseParticles(gl_state* State, mesh* BaseMesh, graphics_buffer* ParticleCenters)
{
    gl_wrapped* gl = &State->gl;
    
    gl->glBindVertexArray(State->ParticlesVao);
    //
    // mesh
    //
    //gl->glBindVertexBuffer(CHANNEL_POSITION, BaseMesh->Vertices.Handle, 0, sizeof(v3));
    real32 Stride = sizeof(v3) + sizeof(v4);
    gl->glBindVertexBuffer(CHANNEL_PARTICLE_CENTER, ParticleCenters->Handle, 0, Stride);
    gl->glBindVertexBuffer(CHANNEL_PARTICLE_COLOR, ParticleCenters->Handle, sizeof(v3), Stride);
    
    //TODO(james): should this be per frame or done at VAO bind time?
    gl->glVertexBindingDivisor(CHANNEL_PARTICLE_CENTER, 1);
}

void DrawParticles(gl_state* State, mesh* BaseMesh, graphics_buffer* ParticleCenters)
{
    gl_wrapped* gl = &State->gl;
    
    gl->glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, ParticleCenters->Count);
    gl->glUseProgram(0);
}

//
// Initialization
//

//TODO(james): Great place for some macro / metaprogramming magic
gl_wrapped LoadFunctions()
{
    gl_wrapped gl;
    gl.glCreateShader = (_glCreateShader*)wglGetProcAddress("glCreateShader");
    gl.glShaderSource = (_glShaderSource*)wglGetProcAddress("glShaderSource");
    gl.glCompileShader = (_glCompileShader*)wglGetProcAddress("glCompileShader");
    gl.glGetShaderiv = (_glGetShaderiv*)wglGetProcAddress("glGetShaderiv");
    gl.glCreateProgram = (_glCreateProgram*)wglGetProcAddress("glCreateProgram");
    gl.glAttachShader = (_glAttachShader*)wglGetProcAddress("glAttachShader");
    gl.glLinkProgram = (_glLinkProgram*)wglGetProcAddress("glLinkProgram");
    gl.glDeleteShader = (_glDeleteShader*)wglGetProcAddress("glDeleteShader");
    gl.glGetShaderInfoLog = (_glGetShaderInfoLog*)wglGetProcAddress("glGetShaderInfoLog");
    gl.glGetProgramInterfaceiv = (_glGetProgramInterfaceiv*)wglGetProcAddress("glGetProgramInterfaceiv");
    gl.glGetProgramResourceiv = (_glGetProgramResourceiv*)wglGetProcAddress("glGetProgramResourceiv");
    gl.glGetProgramResourceName = (_glGetProgramResourceName*)wglGetProcAddress("glGetProgramResourceName");
    
    gl.glGenVertexArrays = (_glGenVertexArrays*)wglGetProcAddress("glGenVertexArrays");
    gl.glBindVertexArray = (_glBindVertexArray*)wglGetProcAddress("glBindVertexArray");
    gl.glGenBuffers = (_glGenBuffers*)wglGetProcAddress("glGenBuffers");
    gl.glBindBuffer = (_glBindBuffer*)wglGetProcAddress("glBindBuffer");
    gl.glBufferData = (_glBufferData*)wglGetProcAddress("glBufferData");
    gl.glGetUniformLocationUnsafe = (_glGetUniformLocation*)wglGetProcAddress("glGetUniformLocation");
    gl.glGetAttribLocationUnsafe = (_glGetAttribLocation*)wglGetProcAddress("glGetAttribLocation");
    gl.glVertexAttribPointer = (_glVertexAttribPointer*)wglGetProcAddress("glVertexAttribPointer");
    gl.glEnableVertexAttribArray = (_glEnableVertexAttribArray*)wglGetProcAddress("glEnableVertexAttribArray");
    gl.glDisableVertexAttribArray = (_glDisableVertexAttribArray*)wglGetProcAddress("glDisableVertexAttribArray");
    gl.glUseProgram = (_glUseProgram*)wglGetProcAddress("glUseProgram");
    gl.glUniformMatrix4fv = (_glUniformMatrix4fv*)wglGetProcAddress("glUniformMatrix4fv");
    gl.glUniform2f = (_glUniform2f*)wglGetProcAddress("glUniform2f");
    gl.glUniform3f = (_glUniform3f*)wglGetProcAddress("glUniform3f");
    gl.glUniform4f = (_glUniform4f*)wglGetProcAddress("glUniform4f");
    gl.glDeleteVertexArrays = (_glDeleteVertexArrays*)wglGetProcAddress("glDeleteVertexArrays");
    gl.glDeleteBuffers = (_glDeleteBuffers*)wglGetProcAddress("glDeleteBuffers");
    
    gl.glBlendEquation = (_glBlendEquation*)wglGetProcAddress("glBlendEquation");
    gl.glActiveTexture = (_glActiveTexture*)wglGetProcAddress("glActiveTexture");
    gl.glBlendEquationSeparate = (_glBlendEquationSeparate*)wglGetProcAddress("glBlendEquationSeparate");
    gl.glUniform1i = (_glUniform1i*)wglGetProcAddress("glUniform1i");
    
    gl.glPrimitiveRestartIndex = (_glPrimitiveRestartIndex*)wglGetProcAddress("glPrimitiveRestartIndex");
    
    gl.glCompressedTexImage2D = (_glCompressedTexImage2D*)wglGetProcAddress("glCompressedTexImage2D");
    
    gl.glGenFramebuffers = (_glGenFramebuffers*)wglGetProcAddress("glGenFramebuffers");
    gl.glBindFramebuffer = (_glBindFramebuffer*)wglGetProcAddress("glBindFramebuffer");
    gl.glFramebufferTexture2D = (_glFramebufferTexture2D*)wglGetProcAddress("glFramebufferTexture2D");
    gl.glCheckFramebufferStatus = (_glCheckFramebufferStatus*)wglGetProcAddress("glCheckFramebufferStatus");
    gl.glGetProgramiv = (_glGetProgramiv*)wglGetProcAddress("glGetProgramiv");
    gl.glTexImage2DMultisample = (_glTexImage2DMultisample*)wglGetProcAddress("glTexImage2DMultisample");
    
    gl.glGetProgramInfoLog = (_glGetProgramInfoLog*)wglGetProcAddress("glGetProgramInfoLog");
    
    gl.glGetUniformBlockIndex = (_glGetUniformBlockIndex*)wglGetProcAddress("glGetUniformBlockIndex");
    gl.glUniformBlockBinding = (_glUniformBlockBinding*)wglGetProcAddress("glUniformBlockBinding");
    gl.glBindBufferRange = (_glBindBufferRange*)wglGetProcAddress("glBindBufferRange");
    gl.glBufferSubData = (_glBufferSubData*)wglGetProcAddress("glBufferSubData");
    
    gl.glGenSamplers = (_glGenSamplers*)wglGetProcAddress("glGenSamplers");
    gl.glBindSampler = (_glBindSampler*)wglGetProcAddress("glBindSampler");
    gl.glSamplerParameteri = (_glSamplerParameteri*)wglGetProcAddress("glSamplerParameteri");
    
    gl.glMapBuffer = (_glMapBuffer*)wglGetProcAddress("glMapBuffer");
    gl.glUnmapBuffer = (_glUnmapBuffer*)wglGetProcAddress("glUnmapBuffer");
    gl.glMapBufferRange = (_glMapBufferRange*)wglGetProcAddress("glMapBufferRange");
    gl.glBindVertexBuffer = (_glBindVertexBuffer*)wglGetProcAddress("glBindVertexBuffer");
    gl.glVertexAttribBinding = (_glVertexAttribBinding*)wglGetProcAddress("glVertexAttribBinding");
    gl.glVertexAttribFormat = (_glVertexAttribFormat*)wglGetProcAddress("glVertexAttribFormat");
    
    gl.glDebugMessageCallbackARB = (_glDebugMessageCallbackARB*)wglGetProcAddress("glDebugMessageCallbackARB");
    
    gl.glDrawArraysInstanced = (_glDrawArraysInstanced*)wglGetProcAddress("glDrawArraysInstanced");
    
    gl.glVertexAttribDivisor = (_glVertexAttribDivisor*)wglGetProcAddress("glVertexAttribDivisor");
    gl.glVertexBindingDivisor = (_glVertexBindingDivisor*)wglGetProcAddress("glVertexBindingDivisor");
    
    gl.glObjectLabel = (_glObjectLabel*)wglGetProcAddress("glObjectLabel");
    gl.glPushDebugGroup = (_glPushDebugGroup*)wglGetProcAddress("glPushDebugGroup");
    gl.glPopDebugGroup = (_glPopDebugGroup*)wglGetProcAddress("glPopDebugGroup");
    
    return gl;
}

internal opengl_info
OpenGLGetInfo(bool32 HasModernContext)
{
    opengl_info Result = {};
    
    Result.HasModernContext = HasModernContext;
    Result.Vendor = (char*)glGetString(GL_VENDOR);
    Result.Renderer = (char*)glGetString(GL_RENDERER);
    Result.Version = (char*)glGetString(GL_VERSION);
    if (Result.HasModernContext)
    {
        Result.ShadingLanguageVersion = (char*)glGetString(GL_SHADING_LANGUAGE_VERSION);
    }
    else
    {
        Result.ShadingLanguageVersion = "(none)";
        InvalidCodePath;
    }
    
    _glGetStringi* glGetStringi = (_glGetStringi*)wglGetProcAddress("glGetStringi");
    
    GLint n, i;
    glGetIntegerv(GL_NUM_EXTENSIONS, &n);
    
    for (i = 0; i < n; i++) 
    {
        char* Extension = (char*)glGetStringi(GL_EXTENSIONS, i);
        if (C_StringsAreEqualUnion(Extension, "GL_EXT_texture_sRGB"))
        {
            Result.GL_EXT_texture_sRGB = true;
        }
        else if (C_StringsAreEqualUnion(Extension, "GL_EXT_framebuffer_sRGB"))
        {
            Result.GL_EXT_framebuffer_sRGB = true;
        }
        else if (C_StringsAreEqualUnion(Extension, "GL_ARB_framebuffer_sRGB"))
        {
            Result.GL_EXT_framebuffer_sRGB = true;
        }
        else if (C_StringsAreEqualUnion(Extension, "GL_ARB_multisample"))
        {
            Result.GL_ARB_multisample = true;
        }
        else if (C_StringsAreEqualUnion(Extension, "GL_ARB_texture_cube_map"))
        {
            Result.GL_ARB_texture_cube_map = true;
        }
        else if (C_StringsAreEqualUnion(Extension, "GL_ARB_texture_compression"))
        {
            Result.GL_ARB_texture_compression = true;
        }
        else if (C_StringsAreEqualUnion(Extension, "GL_EXT_texture_compression_s3tc"))
        {
            Result.GL_EXT_texture_compression_s3tc = true;
        }
    }
    
    return (Result);
}

extern "C" OPENGL_INIT(OpenGLInit)
{
    YamProfiling = Profiling;
    YamProf(GL_Init);
    
    memory_arena TempArena;
    InitializeArena(&TempArena, TempMemorySize, TempMemory);
    temporary_memory TempMem = BeginTemporaryMemory(&TempArena);
    
    gl_state State = {};
    
    opengl_info Info = OpenGLGetInfo(ModernContext);
    
    OpenGLDefaultInternalTextureFormat = GL_RGBA8;
    if (Info.GL_EXT_texture_sRGB)
    {
        OpenGLDefaultInternalTextureFormat = GL_SRGB8_ALPHA8;
    }
    
    // TODO(casey): Need to go back and use extended version of choose pixel format
    // to ensure that our framebuffer is marked as SRGB?
    if (Info.GL_EXT_framebuffer_sRGB)
    {
        glEnable(GL_FRAMEBUFFER_SRGB);
    }
    
    State.gl = LoadFunctions();
    
    gl_wrapped* gl = &State.gl;
    
#if YAMGINE_DEBUG
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    gl->glDebugMessageCallbackARB(OpenGLDebugCallback, 0);
#endif
    
    //ubo
    gl->glGenBuffers(1, &State.GlobalMatrices);
    gl->glBindBuffer(GL_UNIFORM_BUFFER, State.GlobalMatrices);
    gl->glBufferData(GL_UNIFORM_BUFFER, sizeof(global_uniforms), NULL, GL_STATIC_DRAW);
    gl->glBindBufferRange(GL_UNIFORM_BUFFER, 0, State.GlobalMatrices, 0, sizeof(global_uniforms));
    
    
    SetupImGui(gl, &State.ImGuiContext);
    
    uint32 Sampler = MAX_UINT32;
    gl->glGenSamplers(1, &Sampler);
    gl->glSamplerParameteri(Sampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    gl->glSamplerParameteri(Sampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    gl->glSamplerParameteri(Sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR); 
    gl->glSamplerParameteri(Sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 
    gl->glSamplerParameteri(Sampler, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    gl->glSamplerParameteri(Sampler, GL_TEXTURE_COMPARE_FUNC,  GL_GEQUAL);
    State.RefToTextureSampler = Sampler;
    State.LastSlotBoundToSampler = MAX_UINT32;
    
    /*
    // NOTE(james): handedness settings
    {
        glFrontFace(GL_CW);
        glDepthFunc(GL_GEQUAL);
        glClearDepth(0);
    }
    */
    
    //
    //
    //
    gl->glGenVertexArrays(1, &State.GlobalWithColorVao);
    gl->glBindVertexArray(State.GlobalWithColorVao);
    //gl->glVertexAttribPointer(CHANNEL_POSITION, 3, GL_FLOAT, GL_FALSE,0, 0);
    gl->glEnableVertexAttribArray(CHANNEL_POSITION);
    gl->glVertexAttribFormat(CHANNEL_POSITION, CHANNEL_POSITION_WIDTH, GL_FLOAT, GL_FALSE, 0);
    gl->glVertexAttribBinding(CHANNEL_POSITION, CHANNEL_POSITION);
    
    gl->glEnableVertexAttribArray(CHANNEL_NORMAL);
    gl->glVertexAttribFormat(CHANNEL_NORMAL, CHANNEL_NORMAL_WIDTH, GL_FLOAT, GL_FALSE, 0);
    gl->glVertexAttribBinding(CHANNEL_NORMAL, CHANNEL_NORMAL);
    
    gl->glEnableVertexAttribArray(CHANNEL_UV);
    gl->glVertexAttribFormat(CHANNEL_UV, CHANNEL_UV_WIDTH, GL_FLOAT, GL_FALSE, 0);
    gl->glVertexAttribBinding(CHANNEL_UV, CHANNEL_UV);
    
    gl->glEnableVertexAttribArray(CHANNEL_COLOR);
    gl->glVertexAttribFormat(CHANNEL_COLOR, CHANNEL_COLOR_WIDTH, GL_FLOAT, GL_FALSE, 0);
    gl->glVertexAttribBinding(CHANNEL_COLOR, CHANNEL_COLOR);
    
    gl->glBindVertexArray(0);
    
    //
    //
    //
    gl->glGenVertexArrays(1, &State.GlobalWithoutColorVao);
    gl->glBindVertexArray(State.GlobalWithoutColorVao);
    //gl->glVertexAttribPointer(CHANNEL_POSITION, 3, GL_FLOAT, GL_FALSE,0, 0);
    gl->glEnableVertexAttribArray(CHANNEL_POSITION);
    gl->glVertexAttribFormat(CHANNEL_POSITION, CHANNEL_POSITION_WIDTH, GL_FLOAT, GL_FALSE, 0);
    gl->glVertexAttribBinding(CHANNEL_POSITION, CHANNEL_POSITION);
    
    gl->glEnableVertexAttribArray(CHANNEL_NORMAL);
    gl->glVertexAttribFormat(CHANNEL_NORMAL, CHANNEL_NORMAL_WIDTH, GL_FLOAT, GL_FALSE, 0);
    gl->glVertexAttribBinding(CHANNEL_NORMAL, CHANNEL_NORMAL);
    
    gl->glEnableVertexAttribArray(CHANNEL_UV);
    gl->glVertexAttribFormat(CHANNEL_UV, CHANNEL_UV_WIDTH, GL_FLOAT, GL_FALSE, 0);
    gl->glVertexAttribBinding(CHANNEL_UV, CHANNEL_UV);
    
    gl->glBindVertexArray(0);
    
    //
    //
    //
    gl->glGenVertexArrays(1, &State.ParticlesVao);
    gl->glBindVertexArray(State.ParticlesVao);
    
    /*
        gl->glVertexAttribPointer(CHANNEL_POSITION, 3, GL_FLOAT, GL_FALSE,0, 0);
        gl->glEnableVertexAttribArray(CHANNEL_POSITION);
        gl->glVertexAttribFormat(CHANNEL_POSITION, CHANNEL_POSITION_WIDTH, GL_FLOAT, GL_FALSE, 0);
        gl->glVertexAttribBinding(CHANNEL_POSITION, CHANNEL_POSITION);
        */
    
    gl->glEnableVertexAttribArray(CHANNEL_PARTICLE_CENTER);
    gl->glVertexAttribFormat(CHANNEL_PARTICLE_CENTER, CHANNEL_PARTICLE_CENTER_WIDTH, GL_FLOAT, GL_FALSE, 0);
    
    gl->glEnableVertexAttribArray(CHANNEL_PARTICLE_COLOR);
    gl->glVertexAttribFormat(CHANNEL_PARTICLE_COLOR, CHANNEL_PARTICLE_COLOR_WIDTH, GL_FLOAT, GL_FALSE, sizeof(v3));
    
    // NOTE(james): This shows how the attribute index and the binding index are different!
    gl->glVertexAttribBinding(CHANNEL_PARTICLE_CENTER, CHANNEL_PARTICLE_CENTER);
    gl->glVertexAttribBinding(CHANNEL_PARTICLE_COLOR, CHANNEL_PARTICLE_CENTER);
    
    gl->glBindVertexArray(0);
    
    //glLineWidth(2); //TODO(james): variable line widths
    glEnable(GL_LINE_SMOOTH);
    
    
    
    EndTemporaryMemory(TempMem);
    
    return State;
}

extern "C" OPENGL_EXIT(OpenGLExit)
{
}

//
// Update
//

/*
NOTE(james): Line drawing could happen through drawing a quad thats severly warped in one direction,
and ignores the rotation portion of the View matrix, so that its always facing the camera correctly
*/
internal void
RenderLayer(render_list* List, gl_state* State)
{
    //Prof(GL_RenderLayer);
    YamProf(GL_RenderLayer);
    
    matrix4 TView = IdentityMatrix();
    matrix4 TProjection = IdentityMatrix();
    
    
    
    v2 ViewportDim = GetDim(List->Viewport);
    if (ViewportDim.x <= 0 || ViewportDim.y <= 0)
    {
        Assert(0);
        return;
    }
    
    gl_wrapped* gl = &State->gl;
    uint8* PushBufferLocation = List->PushBufferBase;
    
    
    
    glViewport(0,
               0,
               ViewportDim.x,
               ViewportDim.y);
    
    int32 FboDepth = 0;
    
    //glEnable(GL_DEPTH_TEST);
    
    //glClear(GL_DEPTH_BUFFER_BIT);
    
    
    
    PushBufferLocation = List->PushBufferBase;
    
    for (uint32 EntryIndex = 0;
         EntryIndex < List->PushBufferElementCount;
         ++EntryIndex)
    {
        render_group_entry_header* Header = reinterpret_cast<render_group_entry_header*>(PushBufferLocation);
        
        void* Data = (uint8*)Header + sizeof(*Header);
        PushBufferLocation += sizeof(*Header);
        
        switch (Header->Type)
        {
            case RenderGroupEntryType_render_entry_color_clear:
            {
                PushBufferLocation += sizeof(render_entry_color_clear);
                
                render_entry_color_clear* Entry = reinterpret_cast<render_entry_color_clear*>(Data);
                
                glClearColor(Entry->Color.r, Entry->Color.g, Entry->Color.b, Entry->Color.a);
                glClear(GL_COLOR_BUFFER_BIT);
            }
            break;
            
            case RenderGroupEntryType_render_entry_depth_clear:
            {
                PushBufferLocation += sizeof(render_entry_depth_clear);
                
                render_entry_depth_clear* Entry = reinterpret_cast<render_entry_depth_clear*>(Data);
                
                glClear(GL_DEPTH_BUFFER_BIT);
            }
            break;
            
            case RenderGroupEntryType_render_entry_disable_depth:
            {
                PushBufferLocation += sizeof(render_entry_disable_depth);
                
                glDisable(GL_DEPTH_TEST);
            }
            break;
            
            case RenderGroupEntryType_render_entry_enable_depth:
            {
                PushBufferLocation += sizeof(render_entry_enable_depth);
                
                glEnable(GL_DEPTH_TEST);
            }
            break;
            
            case RenderGroupEntryType_render_entry_set_cull_face_mode:
            {
                glEnable(GL_CULL_FACE);
                
                PushBufferLocation += sizeof(render_entry_set_cull_face_mode);
                render_entry_set_cull_face_mode* Entry = reinterpret_cast<render_entry_set_cull_face_mode*>(Data);
                switch (Entry->CullFaceMode)
                {
                    case CullFaceMode_Front:
                    glCullFace(GL_FRONT);
                    //glPolygonOffset(1, 10);
                    break;
                    case CullFaceMode_Back:
                    glCullFace(GL_BACK);
                    //glPolygonOffset(0, 0);
                    break;
                    case CullFaceMode_FrontAndBack:
                    glCullFace(GL_FRONT_AND_BACK);
                    break;
                    case CullFaceMode_None:
                    glDisable(GL_CULL_FACE);
                    break;
                }
            }
            break;
            
            case RenderGroupEntryType_render_entry_fbo_begin:
            {
                render_entry_fbo_begin* FboBegin = reinterpret_cast<render_entry_fbo_begin*>(Data);
                
                Assert(FboBegin->Fbo->FboHandle > 0);
                
                gl->glBindFramebuffer(GL_FRAMEBUFFER, FboBegin->Fbo->FboHandle);
                
                if (FboBegin->Fbo->HasColor)
                {
                    glViewport(0,
                               0,
                               FboBegin->Fbo->Texture.Width,
                               FboBegin->Fbo->Texture.Height);
                }
                else if (FboBegin->Fbo->HasDepth)
                {
                    glViewport(0,
                               0,
                               FboBegin->Fbo->DepthTexture.Width,
                               FboBegin->Fbo->DepthTexture.Height);
                }
                
                
                PushBufferLocation += sizeof(render_entry_fbo_begin);
                FboDepth++;
                
                gl->glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, 1, -1, FboBegin->Fbo->DebugName);
            }
            break;
            
            case RenderGroupEntryType_render_entry_fbo_end:
            {
                gl->glBindFramebuffer(GL_FRAMEBUFFER, 0);
                
                PushBufferLocation += sizeof(render_entry_fbo_end);
                FboDepth--;
                
                gl->glPopDebugGroup();
            }
            break;
            
            case RenderGroupEntryType_render_entry_imgui:
            {
                
                PushBufferLocation += sizeof(render_entry_imgui);
                render_entry_imgui* Entry = reinterpret_cast<render_entry_imgui*>(Data);
                DrawImGui(gl, Entry->DrawData, &State->ImGuiContext,
                          Entry->DisplaySize, Entry->DisplayFramebufferScale);
            }
            break;
            
            case RenderGroupEntryType_render_entry_set_blend:
            {
                render_entry_set_blend* Entry = reinterpret_cast<render_entry_set_blend*>(Data);
                if (Entry->BlendType == RenderBlendType_none)
                {
                    glDisable(GL_BLEND);
                }
                else if (Entry->BlendType == RenderBlendType_one_minus_src)
                {
                    glEnable(GL_BLEND);
                    //TODO(james): how does this differ from glBlendFuncSeparate?
                    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
                }
                PushBufferLocation += sizeof(render_entry_set_blend);
            }
            break;
            
            
            case RenderGroupEntryType_render_entry_set_line_width:
            {
                render_entry_set_line_width* Entry = reinterpret_cast<render_entry_set_line_width*>(Data);
                
                PushBufferLocation += sizeof(render_entry_set_line_width);
                
                //glLineWidth(Entry->LineWidth);
            }
            break;
            
            case RenderGroupEntryType_render_entry_set_view_projection:
            {
                render_entry_set_view_projection* Entry = (render_entry_set_view_projection*)Data;
                TView = Transpose(Entry->GlobalUniforms.View);
                TProjection = Transpose(Entry->GlobalUniforms.Projection);
                
                State->gl.glBindBuffer(GL_UNIFORM_BUFFER, State->GlobalMatrices);
                State->gl.glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(matrix4), &TView);
                State->gl.glBufferSubData(GL_UNIFORM_BUFFER, sizeof(matrix4), sizeof(matrix4), &TProjection);
                State->gl.glBindBufferRange(GL_UNIFORM_BUFFER, 0, State->GlobalMatrices, 0, sizeof(global_uniforms));
                
                
                
                PushBufferLocation += sizeof(render_entry_set_view_projection);
            }
            break;
            
            case RenderGroupEntryType_render_entry_use_mesh:
            {
                render_entry_use_mesh* Entry = (render_entry_use_mesh*)Data;
                PushBufferLocation += sizeof(render_entry_use_mesh);
                
                UseMesh(State, Entry->Mesh);
            }
            break;
            
            case RenderGroupEntryType_render_entry_use_shader:
            {
                render_entry_use_shader* Entry = (render_entry_use_shader*)Data;
                PushBufferLocation += sizeof(render_entry_use_shader);
                PushBufferLocation += Entry->Shader->UniformsStructSize;
                
                UseShader(gl, Entry->Shader, Entry->Uniforms);
            }
            break;
            
            case RenderGroupEntryType_render_entry_set_texture:
            {
                render_entry_set_texture* Entry = (render_entry_set_texture*)Data;
                PushBufferLocation += sizeof(render_entry_set_texture);
                
                GL_SetTexture(State, Entry->Texture, Entry->Slot, Entry->IsShadowmap);
            }
            break;
            
            case RenderGroupEntryType_render_entry_draw_mesh:
            {
                render_entry_draw_mesh* Entry = (render_entry_draw_mesh*)Data;
                PushBufferLocation += sizeof(render_entry_draw_mesh);
                
                MeshDrawCall(State, Entry->Mesh, Entry->RenderMode);
            }
            break;
            
            case RenderGroupEntryType_render_entry_use_particles:
            {
                render_entry_use_particles* Entry = (render_entry_use_particles*)Data;
                PushBufferLocation += sizeof(render_entry_use_particles);
                
                UseParticles(State, Entry->BaseMesh, Entry->ParticleCenters);
            }
            break;
            
            case RenderGroupEntryType_render_entry_draw_particles:
            {
                render_entry_draw_particles* Entry = (render_entry_draw_particles*)Data;
                PushBufferLocation += sizeof(render_entry_draw_particles);
                
                DrawParticles(State, Entry->BaseMesh, Entry->ParticleCenters);
            }
            break;
            
            InvalidDefaultCase;
        }
        
        
        int x = 0;//so we can actually see the content of Header...
    }
    
    Assert(FboDepth == 0);
    
    
}

extern "C" OPENGL_RENDER_COMMANDS(OpenGLRenderCommands)
{
    YamProfiling = Profiling;
    YamProf(GL_RenderCommands);
    
    YamProfBegin(GL_Test);
    
    YamProfEnd();
    {
        YamProf(GL_GetIntProblem);
        int last_texture;
        glGetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
        
    }
    
    glEnable(GL_DEPTH_TEST);
    
    glClear(GL_DEPTH_BUFFER_BIT);
    
    
    
    RenderLayer(List, State);
    
    //State->Fences [State->BufferNumber] = gl->glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE , 0) ;
    //
}

// NOTE(james): Allocation functions are separate from the render list functions
// because it is a two way communication, the resource handles must be provided back
// to the game so they can persist
extern "C" OPENGL_ALLOCATE_TEXTURE(OpenGLAllocateTexture)
{
    // allocated a unique name that won't be reused until deleted
    GLuint TextureHandle;
    glGenTextures(1, &TextureHandle);
    
    // tell gl what kind of texture this handle will be
    glBindTexture(GL_TEXTURE_2D, TextureHandle);
    
    // looping settings
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    
    
    // filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // behavior for scaling smaller than original
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // behavior for scaling larger than original
    
    //glGenerateMipmap(GL_TEXTURE_2D);
    
    if (Texture->CompressionType == TextureCompressionType_Compressed)
    {
        gl->glCompressedTexImage2D(GL_TEXTURE_2D, //target texture type
                                   0, // level-of-detail, 0 to nth mipmaps
                                   COMPRESSED_RGB_S3TC_DXT1_EXT, // base format
                                   Texture->Width, Texture->Height,
                                   0, // must be 0
                                   Texture->SizeOnDisk, // size of image in bytes
                                   Texture->Data);
    }
    else 
    {
        if (Texture->ChannelType == TextureChannelType_RGB)
        {
            glTexImage2D(GL_TEXTURE_2D, //target texture type
                         0, // level-of-detail, 0 to nth mipmaps
                         GL_RGB, // base format
                         Texture->Width, Texture->Height,
                         0, // must be 0
                         GL_RGB, // format on disk
                         GL_UNSIGNED_BYTE, //type of pixel data
                         Texture->Data);
        }
        else if (Texture->ChannelType == TextureChannelType_RGBA)
        {
            glTexImage2D(GL_TEXTURE_2D, //target texture type
                         0, // level-of-detail, 0 to nth mipmaps
                         GL_RGBA, // base format
                         Texture->Width, Texture->Height,
                         0, // must be 0
                         GL_RGBA, // format on disk
                         GL_UNSIGNED_BYTE, //type of pixel data
                         Texture->Data);
        }
    }
    
    
    Texture->Handle = TextureHandle;
}

extern "C" OPENGL_CREATE_FBO(OpenGLCreateFbo)
{
    fbo Fbo = {};
    
    bool32 IsMultisampled = false;
    if (FboOptions & FboOptions_Multisample)
    {
        IsMultisampled = true;
    }
    
    if (FboOptions & FboOptions_Depth)
    {
        Fbo.HasDepth = true;
    }
    
    if (FboOptions & FboOptions_Color)
    {
        Fbo.HasColor = true;
    }
    
    GLenum ChannelType = 0;
    if (FboOptions & FboOptions_Alpha)
    {
        Assert(Fbo.HasColor);
        Fbo.Texture.ChannelType = TextureChannelType_RGBA;
        ChannelType = GL_RGB;
    }
    else
    {
        Fbo.Texture.ChannelType = TextureChannelType_RGB;
        ChannelType = GL_RGBA;
    }
    
    Assert(Fbo.HasColor || Fbo.HasDepth);
    
    
    uint32 TextureType = GL_TEXTURE_2D;
    
    if (IsMultisampled)
    {
        TextureType = GL_TEXTURE_2D_MULTISAMPLE;
    }
    
    //
    // Color Tex
    //
    if (Fbo.HasColor)
    {
        Fbo.Texture.Width = Width;
        Fbo.Texture.Height = Height;
        Fbo.Texture.CompressionType = TextureCompressionType_Uncompressed;
        
        if (IsMultisampled)
        {
            TextureType = GL_TEXTURE_2D_MULTISAMPLE;
            Fbo.Texture.IsMultisampled = true;
        }
        
        glGenTextures(1, &Fbo.Texture.Handle);
        glBindTexture(TextureType, Fbo.Texture.Handle);
        
        if (Fbo.Texture.IsMultisampled)
        {
            
            Fbo.Texture.Samples = 8;
            
            gl->glTexImage2DMultisample(TextureType, //target texture type
                                        Fbo.Texture.Samples, // number of samples
                                        ChannelType, // base format
                                        Width, Height,
                                        true); //NOTE(james): fixedsamplelocations - not really sure what this is
            
        }
        else
        {
            glTexParameteri(TextureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(TextureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            
            glTexParameteri(TextureType, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(TextureType, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            
            GLenum FormatOnDisk = GL_BGR;
            if (Fbo.Texture.ChannelType == TextureChannelType_RGBA)
            {
                FormatOnDisk = GL_BGRA;
            }
            
            glTexImage2D(TextureType, //target texture type
                         0, // level-of-detail, 0 to nth mipmaps
                         ChannelType, // base format
                         Width, Height,
                         0, // must be 0
                         FormatOnDisk, // format on disk
                         GL_UNSIGNED_BYTE, //type of pixel data
                         NULL);
        }
        
        glBindTexture(TextureType, 0);
    }
    
    //
    // Depth Tex
    //
    if (Fbo.HasDepth)
    {
        if (IsMultisampled)
        {
            Fbo.DepthTexture.IsMultisampled = true;
        }
        
        if (Fbo.HasDepth && !Fbo.HasColor)
        {
            Fbo.Texture.Width = MAX_UINT32;
            Fbo.Texture.Height = MAX_UINT32;
        }
        
        Fbo.DepthTexture.Width = Width;
        Fbo.DepthTexture.Height = Height;
        Fbo.DepthTexture.CompressionType = TextureCompressionType_Uncompressed;
        Fbo.DepthTexture.ChannelType = TextureChannelType_Depth;
        
        glGenTextures(1, &Fbo.DepthTexture.Handle);
        
        glBindTexture(TextureType, Fbo.DepthTexture.Handle);
        
        if (Fbo.DepthTexture.IsMultisampled)
        {
            Fbo.DepthTexture.Samples = 8;
            //TODO(james): support other formats
            gl->glTexImage2DMultisample(TextureType, //target texture type
                                        Fbo.DepthTexture.Samples, // level-of-detail, 0 to nth mipmaps
                                        GL_DEPTH_COMPONENT, // base format
                                        Width, Height,
                                        true);
        }
        else
        {
            glTexParameteri(TextureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(TextureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            // behavior for scaling smaller than original
            glTexParameteri(TextureType, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
            glTexParameteri(TextureType, GL_TEXTURE_MAG_FILTER, GL_NEAREST); 
            
            //glTexParameteri(TextureType, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
            //glTexParameteri(TextureType, GL_TEXTURE_COMPARE_FUNC, GL_GEQUAL);
            
            //TODO(james): support other formats
            glTexImage2D(TextureType, //target texture type
                         0, // level-of-detail, 0 to nth mipmaps
                         GL_DEPTH_COMPONENT, // base format
                         Width, Height,
                         0, // must be 0
                         GL_DEPTH_COMPONENT, // format on disk
                         GL_FLOAT, //type of pixel data
                         NULL);
        }
        
        glBindTexture(TextureType, 0);
    }
    
    //
    // Setup FBO
    //
    gl->glGenFramebuffers(1, &Fbo.FboHandle);
    gl->glBindFramebuffer(GL_FRAMEBUFFER, Fbo.FboHandle);
    if (Fbo.HasColor)
    {
        gl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, TextureType, Fbo.Texture.Handle, 0);
    }
    
    if (Fbo.HasDepth)
    {
        gl->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, TextureType, Fbo.DepthTexture.Handle, 0);
    }
    
    GLenum FrameBufferStatus = gl->glCheckFramebufferStatus(GL_FRAMEBUFFER);
    
    // FrameBufferStatus of 36182 == GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE
    Assert(FrameBufferStatus == GL_FRAMEBUFFER_COMPLETE);
    
    gl->glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    //
    // setup debug names
    //
    if (Fbo.HasColor)
    {
        C_CopyString(Fbo.Texture.DebugName, 32, DebugName);
        C_AppendString_(Fbo.Texture.DebugName, 32, " C", 2);
        gl->glObjectLabel(GL_TEXTURE, Fbo.Texture.Handle, -1, Fbo.Texture.DebugName);
    }
    C_CopyString(Fbo.DebugName, 32, DebugName);
    if (Fbo.HasDepth)
    {
        C_CopyString(Fbo.DepthTexture.DebugName, 32, DebugName);   
        C_AppendString_(Fbo.DepthTexture.DebugName, 32, " D", 2);
        gl->glObjectLabel(GL_TEXTURE, Fbo.DepthTexture.Handle, -1, Fbo.DepthTexture.DebugName);
    }
    
	

    return Fbo;
}

extern "C" OPENGL_RESIZE_FBO(OpenGLResizeFbo)
{
    
    uint32 ColorTextureType = GL_TEXTURE_2D;
    if (Fbo->HasColor)
    {
        if (Fbo->Texture.IsMultisampled)
        {
            ColorTextureType = GL_TEXTURE_2D_MULTISAMPLE;
        }
        
        GLenum ChannelType = GL_RGB;
        GLenum TypeOnDisk = GL_BGR;
        if (Fbo->Texture.ChannelType == TextureChannelType_RGBA)
        {
            ChannelType = GL_RGBA;
            TypeOnDisk = GL_BGRA;
        }
        
        glBindTexture(ColorTextureType, Fbo->Texture.Handle);
        
        if (Fbo->Texture.IsMultisampled)
        {
            gl->glTexImage2DMultisample(ColorTextureType, //target texture type
                                        Fbo->Texture.Samples, // number of samples
                                        ChannelType, // base format
                                        Width, Height,
                                        true); //NOTE(james): fixedsamplelocations - not really sure what this is
        }
        else
        {
            glTexImage2D(ColorTextureType, //target texture type
                         0, // level-of-detail, 0 to nth mipmaps
                         ChannelType, // base format
                         Width, Height,
                         0, // must be 0
                         TypeOnDisk, // format on disk
                         GL_UNSIGNED_BYTE, //type of pixel data
                         NULL);
        }
        
        Fbo->Texture.Width = Width;
        Fbo->Texture.Height = Height;
    }
    
    if (Fbo->HasDepth)
    {
        if (Fbo->DepthTexture.IsMultisampled)
        {
            ColorTextureType = GL_TEXTURE_2D_MULTISAMPLE;
        }
        
        glBindTexture(ColorTextureType, Fbo->DepthTexture.Handle);
        if (Fbo->Texture.IsMultisampled)
        {
            gl->glTexImage2DMultisample(ColorTextureType, //target texture type
                                        Fbo->DepthTexture.Samples, // number of samples 
                                        GL_DEPTH_COMPONENT, // base format
                                        Width, Height,
                                        true); //NOTE(james): fixedsamplelocations - not really sure what this is
        }
        else
        {
            glTexImage2D(ColorTextureType, //target texture type
                         0, // level-of-detail, 0 to nth mipmaps
                         GL_DEPTH_COMPONENT, // base format
                         Width, Height,
                         0, // must be 0
                         GL_DEPTH_COMPONENT, // format on disk
                         GL_FLOAT, //type of pixel data
                         NULL);
        }
        
        Fbo->DepthTexture.Width = Width;
        Fbo->DepthTexture.Height = Height;
    }
    
    glBindTexture(ColorTextureType, 0);
}

extern "C" OPENGL_CREATE_BUFFER(OpenGLCreateBuffer)
{
    *Buffer = {};
    gl->glGenBuffers(1, &Buffer->Handle);
    
    Buffer->Channel = Channel;
    
    if (Buffer->Channel == CHANNEL_ELEMENT)
    {
        gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Buffer->Handle);
    }
    else
    {
        gl->glBindBuffer(GL_ARRAY_BUFFER, Buffer->Handle);
        
    }
    Buffer->CountPerEntry = ChannelWidth;
    Buffer->Residency = Residency;
}

extern "C" OPENGL_FILL_BUFFER(OpenGLFillBuffer)
{
    Assert(Buffer->Handle != MAX_UINT32);
    GLenum GLResidency = GL_STATIC_DRAW;
    switch (Buffer->Residency)
    {
        case MeshResidency_Static:
        {
            GLResidency = GL_STATIC_DRAW;
        }break;
        
        case MeshResidency_Dynamic:
        {
            GLResidency = GL_DYNAMIC_DRAW;
        }break;
        
        case MeshResidency_Stream:
        {
            GLResidency = GL_STREAM_DRAW;
        }break;
    }
    
    if (Buffer->Channel == CHANNEL_ELEMENT)
    {
        gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Buffer->Handle);
        gl->glBufferData(GL_ELEMENT_ARRAY_BUFFER, Buffer->Count * Buffer->CountPerEntry * sizeof(GLuint), Buffer->Data, GLResidency);
        gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    else
    {
        gl->glBindBuffer(GL_ARRAY_BUFFER, Buffer->Handle);
        gl->glBufferData(GL_ARRAY_BUFFER, Buffer->Count * Buffer->CountPerEntry * sizeof(float), Buffer->Data, GLResidency);
        gl->glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    
    Assert(IsValidBuffer(Buffer));
}

#define STORAGE_STRAT 0

#if 0
global_variable uint32 FrameIndex = 0;
//TODO(james): this is creating a sync point, check out this thread! https://www.opengl.org/discussion_boards/showthread.php/179677-Updating-dynamic-vertex-buffer-%28frequently%29

// NOTE(james): I rabbit holed on this "problem" for a while. Lets not worry about this unless we are actually missing frames because of it. I think we are seeing perf hang here because the app is all caught up with itself. If this actually problem, case 3 has not been explored yet. Also checkout the lovely work here 
// http://www.seas.upenn.edu/~pcozzi/OpenGLInsights/OpenGLInsights-AsynchronousBufferTransfers.pdf
extern "C" OPENGL_UPDATE_MESH(OpenGLUpdateMesh)
{
    FrameIndex++;
    FrameIndex %= 3;
    
    gl->glBindVertexArray(Mesh->VaoHandle);
    
    GLenum Residency = 0;
    switch (Mesh->Residency)
    {
        case MeshResidency_Static:
        {
            Residency = GL_STATIC_DRAW;
        }break;
        case MeshResidency_Dynamic:
        {
            Residency = GL_DYNAMIC_DRAW;
        }break;
        case MeshResidency_Stream:
        {
            Residency = GL_STREAM_DRAW;
        }break;
    }
    
    //vertex
    gl->glBindBuffer(GL_ARRAY_BUFFER, Mesh->VertexHandle);
    
    uint32 SizeToCopy = Mesh->VertexCount * sizeof(v3);
    #if STORAGE_STRAT == 0
    gl->glBufferData(GL_ARRAY_BUFFER, SizeToCopy, NULL, Residency);
    gl->glBufferData(GL_ARRAY_BUFFER, SizeToCopy, Mesh->Vertices, Residency);
#elif STORAGE_STRAT == 1
    //gl->glBufferData(GL_ARRAY_BUFFER, SizeToCopy, NULL, Residency);
    void* ptr = gl->glMapBufferRange(GL_ARRAY_BUFFER , 0, SizeToCopy,
        GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    memcpy(ptr, Mesh->Vertices, SizeToCopy);
    Assert(gl->glUnmapBuffer(GL_ARRAY_BUFFER));
    #elif STORAGE_STRAT == 2
    gl->glBufferSubData(GL_ARRAY_BUFFER, 500 * sizeof(v3) * FrameIndex,
                        SizeToCopy, Mesh->Vertices);
    #elif STORAGE_SRAT == 3
    // Wait until buffer is free to use , in most cases this should not wait
    // because we are using three buffers in chain , glClientWaitSync
    // function can be used for check if the TIMEOUT is zero
    GLenum result = gl->glClientWaitSync( State->Fences [ buffer_number], 0, TIMEOUT );
    if ( result == GL_TIMEOUT_EXPIRED || result == GL_WAIT_FAILED)
    {
        // Something is wrong
        Assert(0);
    }
    glDeleteSync( State->Fences [ buffer_number]) ;
    glBindBuffer( GL_ARRAY_BUFFER , buffers [ buffer_number]) ;
    void * ptr = glMapBufferRange( GL_ARRAY_BUFFER , offset , size , GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
    // Fill ptr with useful data
    glUnmapBuffer( GL_ARRAY_BUFFER);
    #endif
    
    
    
    if (Mesh->IsIndexedMesh)
    {
        //elements
        gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, Mesh->ElementHandle);
        
        SizeToCopy = Mesh->ElementCount * sizeof(uint32);
#if STORAGE_STRAT == 0
        gl->glBufferData(GL_ELEMENT_ARRAY_BUFFER, SizeToCopy, NULL, Residency);
        gl->glBufferData(GL_ELEMENT_ARRAY_BUFFER, SizeToCopy, Mesh->Elements, Residency);
#elif STORAGE_STRAT == 1
        //gl->glBufferData(GL_ELEMENT_ARRAY_BUFFER, SizeToCopy, NULL, Residency);
        ptr = gl->glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER , 0, SizeToCopy,
                                     GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        memcpy(ptr, Mesh->Elements, SizeToCopy);
        Assert(gl->glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER));
#else
        gl->glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 1000 * sizeof(uint32) * FrameIndex,
                            SizeToCopy, Mesh->Elements);
#endif
    }
    
    if (Mesh->HasNormals)
    {
        //normals
        gl->glBindBuffer(GL_ARRAY_BUFFER, Mesh->NormalHandle);
        gl->glBufferData(GL_ARRAY_BUFFER, Mesh->VertexCount * 3 * sizeof(real32), Mesh->Normals, Residency);
    }
    
    
    if (Mesh->HasColors)
    {
        //colors
        gl->glBindBuffer(GL_ARRAY_BUFFER, Mesh->ColorsHandle);
        SizeToCopy = Mesh->VertexCount * sizeof(v4);
#if STORAGE_STRAT == 0
        gl->glBufferData(GL_ARRAY_BUFFER, SizeToCopy, NULL, Residency);
        gl->glBufferData(GL_ARRAY_BUFFER, SizeToCopy, Mesh->Colors, Residency);
#elif STORAGE_STRAT == 1
        //gl->glBufferData(GL_ARRAY_BUFFER, SizeToCopy, NULL, Residency);
        ptr = gl->glMapBufferRange(GL_ARRAY_BUFFER , 0, SizeToCopy,
                                     GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
        memcpy(ptr, Mesh->Colors, SizeToCopy);
        Assert(gl->glUnmapBuffer(GL_ARRAY_BUFFER));
#else
        gl->glBufferSubData(GL_ARRAY_BUFFER, SizeToCopy * FrameIndex,
                            SizeToCopy, Mesh->Colors);
        #endif
    }

    if (Mesh->HasUVs)
    {
        //UVs
        gl->glBindBuffer(GL_ARRAY_BUFFER, Mesh->UVsHandle);
        gl->glBufferData(GL_ARRAY_BUFFER, Mesh->VertexCount * 2 * sizeof(real32), Mesh->UVs, Residency);
    }
    
    gl->glBindVertexArray(0);
    
    
}
#endif

/*
#define PLATFORM_CREATE_SHADER(name) shader name(char* MetashaderFilepath, char* Header,char* ShaderPreproc, char* DestinationFolder,  uint32 UniformStructSize, temporary_memory* Temp, platform_api* API)
*/
OPENGL_CREATE_SHADER(OpenGLCreateShader) 
{
    bool32 Result = false;
    
    if (C_StringLength(Shader->FileName.Text) == 0)
    {
        Shader->FileName.Text = &Shader->__FileName[0];
        C_SetString(&Shader->FileName, MetashaderFilePath);
    }
    
    if (C_StringLength(Shader->PreprocessorHeader.Text) == 0)
    {
        Shader->PreprocessorHeader.Text = &Shader->__PreprocessorHeader[0];
        C_SetString(&Shader->PreprocessorHeader, ShaderPreproc);
    }
    
    if (C_StringLength(Shader->CompiledPath.Text) == 0)
    {
        Shader->CompiledPath.Text = &Shader->__CompiledPath[0];
        C_SetString(&Shader->CompiledPath, DestinationFolder);
    }
    
    
    STRING(New_MetashaderFilePath, 255);
    AppendStringEx(&New_MetashaderFilePath, "../source/code/shaders/%s.metashader", MetashaderFilePath);
    
    STRING(ShaderPreprocGLSL, 255);
    AppendStringEx(&ShaderPreprocGLSL, "#define GLSL\n%s", ShaderPreproc);
#if YAMGINE_DEBUG
    GenerateShader(New_MetashaderFilePath.Text,ShaderPreprocGLSL.Text, Header, 
                   DestinationFolder, Temp, PlatformAPI);
#endif
    
    Shader->SourceFileHandle= CreateFileA(New_MetashaderFilePath.Text, 0, FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE, 0, OPEN_EXISTING, 0, 0);
    Assert(Shader->SourceFileHandle != INVALID_HANDLE_VALUE);
    
    STRING(VertPath, 255);
    AppendStringEx(&VertPath, "%s.vert", DestinationFolder);
    char* Vert = LoadTextFile(VertPath.Text, Temp, PlatformAPI);
    
    STRING(FragPath, 255);
    AppendStringEx(&FragPath, "%s.frag", DestinationFolder);
    char* Frag = LoadTextFile(FragPath.Text, Temp, PlatformAPI);
    
    Shader->ProgramHandle = LoadShaderProgram(gl, Vert, Frag);
    
    GLuint UniformBlockIndex = gl->glGetUniformBlockIndex(Shader->ProgramHandle, "global_uniforms");
    gl->glUniformBlockBinding(Shader->ProgramHandle, UniformBlockIndex, 0);
    
    gl->glGenBuffers(1, &Shader->UniformsStructHandle);
    gl->glBindBuffer(GL_UNIFORM_BUFFER, Shader->UniformsStructHandle);
    gl->glBufferData(GL_UNIFORM_BUFFER, 
                     UniformStructSize, NULL, GL_STATIC_DRAW);
    gl->glBindBufferRange(GL_UNIFORM_BUFFER, 1, Shader->UniformsStructHandle, 0, UniformStructSize);
    
    Shader->UniformsStructSize = UniformStructSize;
    
    
    
    return Result;
}