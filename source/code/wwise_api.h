#include <AK/SoundEngine/Common/AkSoundEngine.h>                // Sound engine
#include <AK/MusicEngine/Common/AkMusicEngine.h>                // Music Engine
#include <../../build/media/audio/GeneratedSoundBanks/Wwise_IDs.h>

typedef AKRESULT wwise_set_position (AkGameObjectID in_GameObjectID,
                                     const AkSoundPosition & in_Position	 
                                     );

typedef AkPlayingID wwise_post_event	(	AkUniqueID 	in_eventID,
                                                AkGameObjectID 	in_gameObjectID,
                                                AkUInt32 	in_uFlags,
                                                AkCallbackFunc 	in_pfnCallback,
                                                void * 	in_pCookie,
                                                AkUInt32 	in_cExternals,
                                                AkExternalSourceInfo * 	in_pExternalSources,
                                                AkPlayingID 	in_PlayingID	 
);

typedef AKRESULT wwise_register_game_obj	(	AkGameObjectID 	in_gameObjectID,
                                                   const char * 	in_pszObjName,
                                                   AkUInt32 	in_uListenerMask
);


typedef AKRESULT wwise_unregister_game_obj	(	AkGameObjectID 	in_gameObjectID	 );


typedef AKRESULT wwise_set_state	(	AkStateGroupID 	in_stateGroup,
                                            AkStateID 	in_state	 
                                            );

struct wwise_api
{
    wwise_set_position* SetPosition;
    wwise_post_event* PostEvent;
    wwise_set_state* SetState;
    wwise_register_game_obj* RegisterGameObj;
    wwise_unregister_game_obj* UnregisterGameObj;
};

//NOTE(james): typedef's can't have default params, so we have to do this weird roundabout with the function ptr
AkPlayingID WwisePostEvent(wwise_api* API, 
                           AkUniqueID 	in_eventID,
                           AkGameObjectID 	in_gameObjectID,
                           AkUInt32 	in_uFlags= 0,
                           AkCallbackFunc 	in_pfnCallback=NULL,
                           void * 	in_pCookie=NULL,
                           AkUInt32 	in_cExternals=0,
                           AkExternalSourceInfo * 	in_pExternalSources=NULL,
                           AkPlayingID 	in_PlayingID=AK_INVALID_PLAYING_ID	 
                           )			
{
    return API->PostEvent(in_eventID, in_gameObjectID, in_uFlags, in_pfnCallback, in_pCookie, in_cExternals, in_pExternalSources, in_PlayingID);
}
