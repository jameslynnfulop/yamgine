#if !defined(YAMGINE_META_H)

enum meta_type
{
    MetaType_uint32,
    MetaType_uint64,
    MetaType_bool32,
    MetaType_real32,
    MetaType_int32,
    MetaType_char,

    MetaType_aabb,
    MetaType_collider_type,
    MetaType_collision_layer,
    MetaType_matrix4,
    MetaType_transform,
    MetaType_mesh,
    MetaType_contact_info,
    MetaType_matrix3,
    MetaType_model,
    MetaType_transparency_mode,

    MetaType_v2,
    MetaType_v3,
    MetaType_v4,
    MetaType_rectangle2,
    MetaType_rectangle3,
    MetaType_entity,

    MetaType_material,

    MetaType_string,

    MetaType_glyph_table_entry,

    MetaType_player_settings,
    MetaType_editor_settings,

    MetaType_no_base
};

enum member_definition_flag
{
    MetaMemberFlag_IsPointer = 0x1,
};
struct member_definition
{
    uint32 Flags;
    meta_type Type;
    char* TypeName;

    meta_type BaseType;
    char* BaseTypeName;

    char* Name;
    uint32 Offset;
};

#define YAMGINE_META_H
#endif