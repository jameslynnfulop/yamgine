

struct scene
{
    char Name[128];
};


introspect(category: "editor") struct editor_settings
{
    serialize("param") char LastOpenScene[128];
};


enum game_playback
{
    GamePlayback_Playing,
    GamePlayback_Paused,
    GamePlayback_Stopped
};

struct undo_stack_entry
{
    game_state* GameStateChange;
    uint32 GameStateSize;
    uint8* GameStateArenaChange;
    uint32 GameStateArenaSize;
};


struct editor_state
{
    editor_settings EditorSettings;
    
    entity* EditorCamera;
    
    entity* SelectedEntity;
    entity* SelectedHandle;
    
    entity* EntityToCopy;
    
    mesh* SelectedMesh;
    bool IsDroppingAsset;
    
    v3 InitMovementDelta;
    v3 StartingPosTranslation;
    
    bool32 FlipDrawEntityOrder;
    
    entity* Handle;
    entity* XAxisHandle;
    entity* YAxisHandle;
    entity* ZAxisHandle;
    
    bool IsGuiOpen;
    
    linked_list* EntityMaster;
    collision_group* CollisionGroup;
    render_assets* RenderAssets;
    
    v3 MouseWorldPosition;
    v3 MouseWorldDirection;
    
    v3 SpherePosInitial;
    v4 InitialRotation;
    v3 ConstraintAxis;
    
    string EditName;
    char __EditNameMemory[128];
    
    entity* LastSelectedEntity;
    
    bool32 SnappingWindowOpen;
    real32 TranslationSnappingInterval; //TODO(james): serialize this
    real32 RotationSnappingInterval; //TODO(james): serialize this
    
    bool32 WorldSpaceDragging;
    bool32 RotateDragger;
    
    entity* RotationHandleSphereCollider;
    bool32 UseRotationTool;
    
    bool32 WorldSpaceRotating;
    
    fbo SelectionFrame;
    fbo Depthmap;
    
    game_playback GamePlaybackState;
    
    float GameWindowForcedAspectRatio;
    
    texture* SelectedTexture;
    real32 TextureZoom;
    
    undo_stack_entry UndoStackEntries[255];
    int32 UndoStackPosition;
    memory_arena UndoStackMemory;
    bool32 TranslationHandleDragged;
    
#define MAX_SCENES 8
    scene Scenes[MAX_SCENES];
    uint32 SceneCount;
    
    scene* CurrentScene;
    
    bool32 OpenSceneDialogIsOpen;
    bool32 SaveAsNewSceneDialogIsOpen;
    
    char NewSceneName[128];
    
    imgui_font_info ImGuiFontInfo;
    
    bool32 UIHoveredLastFrame;
};
