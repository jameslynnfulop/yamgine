
struct tokenizer
{
    char* At;
    bool EnableMinusToken;
};

enum token_type
{
    Token_Unknown,

    Token_OpenParen,
    Token_CloseParen,
    Token_Colon,
    Token_Semicolon,
    Token_Asterisk,
    Token_OpenBracket,
    Token_CloseBracket,
    Token_OpenBrace,
    Token_CloseBrace,
    Token_Period,
    Token_Equals,
    Token_Pound,
    Token_Plus,
    Token_Minus,
    Token_Multiply,
    Token_Divide,
    Token_Comma,
    Token_LessThan,
    Token_GreaterThan,

    Token_String,
    Token_Identifier,
    Token_Number,

    Token_ValueType, //real32, bool, transform, v3...

    Token_EndOfStream,
};

struct token
{
    token_type Type;
    size_t TextLength;
    char* Text;
};

inline bool
IsAlpha(char C)
{
    bool Result = (((C >= 'a') && (C <= 'z')) || ((C >= 'A') && (C <= 'Z')) || C == '_');

    return (Result);
}

inline bool
IsNumber(char C)
{
    bool Result = ((C >= '0') && (C <= '9'));

    return (Result);
}

static void
EatAllWhitespace(tokenizer* Tokenizer)
{
    for (;;)
    {
        if (IsWhitespace(Tokenizer->At[0]))
        {
            ++Tokenizer->At;
        }
        else if ((Tokenizer->At[0] == '/') && (Tokenizer->At[1] == '/'))
        {
            Tokenizer->At += 2;
            while (Tokenizer->At[0] && !IsEndOfLine(Tokenizer->At[0]))
            {
                ++Tokenizer->At;
            }
        }
        else if ((Tokenizer->At[0] == '/') && (Tokenizer->At[1] == '*'))
        {
            Tokenizer->At += 2;
            while (Tokenizer->At[0] && !((Tokenizer->At[0] == '*') && (Tokenizer->At[1] == '/')))
            {
                ++Tokenizer->At;
            }

            if (Tokenizer->At[0] == '*')
            {
                Tokenizer->At += 2;
            }
        }
        else
        {
            break;
        }
    }
}

static token
GetToken(tokenizer* Tokenizer)
{
    EatAllWhitespace(Tokenizer);

    token Token = {};
    Token.TextLength = 1;
    Token.Text = Tokenizer->At;
    char C = Tokenizer->At[0];
    ++Tokenizer->At;
    switch (C)
    {
        case '\0':
        {
            Token.Type = Token_EndOfStream;
        }
        break;

        case '(':
        {
            Token.Type = Token_OpenParen;
        }
        break;
        case ')':
        {
            Token.Type = Token_CloseParen;
        }
        break;
        case ':':
        {
            Token.Type = Token_Colon;
        }
        break;
        case ';':
        {
            Token.Type = Token_Semicolon;
        }
        break;
        case '*':
        {
            Token.Type = Token_Asterisk;
        }
        break;
        case '[':
        {
            Token.Type = Token_OpenBracket;
        }
        break;
        case ']':
        {
            Token.Type = Token_CloseBracket;
        }
        break;
        case '{':
        {
            Token.Type = Token_OpenBrace;
        }
        break;
        case '}':
        {
            Token.Type = Token_CloseBrace;
        }
        break;
        case '.':
        {
            Token.Type = Token_Period;
        }
        break;
        case '=':
        {
            Token.Type = Token_Equals;
        }
        break;
        case '#':
        {
            Token.Type = Token_Pound;
        }
        break;
        case '+':
        {
            Token.Type = Token_Plus;
        }
        break;

        case '/':
        {
            Token.Type = Token_Divide;
        }
        break;
        case ',':
        {
            Token.Type = Token_Comma;
        }
        break;
        case '<':
        {
            Token.Type = Token_LessThan;
        }
        break;
        case '>':
        {
            Token.Type = Token_GreaterThan;
        }
        break;

        case '"':
        {
            Token.Type = Token_String;

            Token.Text = Tokenizer->At;

            while (Tokenizer->At[0] && Tokenizer->At[0] != '"')
            {
                if ((Tokenizer->At[0] == '\\') && Tokenizer->At[1])
                {
                    ++Tokenizer->At;
                }
                ++Tokenizer->At;
            }

            Token.TextLength = Tokenizer->At - Token.Text;
            if (Tokenizer->At[0] == '"')
            {
                ++Tokenizer->At;
            }
        }
        break;

        case '-':
        {
            if (Tokenizer->EnableMinusToken)
            {
                Token.Type = Token_Minus;
                break;
            }
            else
            {
                //NOTE(james): purposely fall through to default case
            }
        }

        default:
        {
            if (IsAlpha(C))
            {
                Token.Type = Token_Identifier;

                while (IsAlpha(Tokenizer->At[0]) || IsNumber(Tokenizer->At[0]) || (Tokenizer->At[0] == '_'))
                {
                    ++Tokenizer->At;
                }

                Token.TextLength = Tokenizer->At - Token.Text;
            }
            else if (IsNumber(C) || C == '-')
            {
                Token.Type = Token_Number;
                while (IsAlpha(Tokenizer->At[0]) || IsNumber(Tokenizer->At[0]) || Tokenizer->At[0] == '.')
                {
                    ++Tokenizer->At;
                }

                Token.TextLength = Tokenizer->At - Token.Text;
            }
            else if (C == '"')
            {
                //++Tokenizer->At;

                Token.Type = Token_String;
                while (Tokenizer->At[0] != '"')
                {
                    // if a string contains a quote mark it will have a backslash encoded in it
                    if (*Tokenizer->At == '\\' && (*(Tokenizer->At + 1) == '"' || *(Tokenizer->At + 1) == '\\'))
                    {
                        ++Tokenizer->At;
                    }

                    ++Tokenizer->At;
                }

                ++Token.Text; //skip first quote
                Token.TextLength = (Tokenizer->At - Token.Text);
                ++Tokenizer->At; //pass second quote
            }
            else
            {
                Token.Type = Token_Unknown;
            }
        }
        break;
    }

    return (Token);
}

static bool
RequireToken(tokenizer* Tokenizer, token_type DesiredType)
{
    token Token = GetToken(Tokenizer);
    bool Result = (Token.Type == DesiredType);
    return (Result);
}

static void
AssertToken(tokenizer* Tokenizer, token_type DesiredType)
{
    token Token = GetToken(Tokenizer);
    bool Result = (Token.Type == DesiredType);
    Assert(Result);
}

inline bool
TokenEquals(token Token, char* Match)
{
    char* At = Match;
    for (uint32 Index = 0;
         Index < Token.TextLength;
         ++Index, ++At)
    {
        if ((*At == 0) || (Token.Text[Index] != *At))
        {
            return (false);
        }
    }

    bool Result = (*At == 0);
    return (Result);
}

static token
GetRestOfLine(tokenizer* Tokenizer)
{
    token Result = {};
    Result.Type = Token_Unknown;
    Result.Text = Tokenizer->At;

    while (*Tokenizer->At != '\n')
    {
        Tokenizer->At++;
    }
    Result.TextLength = Tokenizer->At - Result.Text;
    return Result;
}
